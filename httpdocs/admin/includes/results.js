var asInitVals = new Array();
var myApp = myApp || {};
var g_plot; 
var g_overview;
var some_data_set = [];
var globalDS = [];

// Bar chart options
var options = {
	bars: {
		show: true,
		align: "center"
	},
	points: {
		show: true
	},
	grid: {
		hoverable: true,
		clickable: true,
		color: '#2F96B4'
	},
	xaxis: {
		mode: 'categories'
	},
	yaxis: {
		min: 0,
		max: 100,
		tickDecimals: 0,
		tickSize: 10,
		zoomRange: [0,0],
		panRange: [0,100]
	}
};
    
// Half width Pie
var pie_options = {
	series: {
		pie: {
            radius: 80,
			innerRadius: 0.35,
			show: true,
			offset: {
				top: 2,
				left: -50
			}
		}
	},
	legend: {
		noColumns: 2
	},
	grid: {
		hoverable: true,
		clickable: true
	}
};
    
// Full width pie
var full_pie_options = {
	series: {
		pie: {
            radius: 80,
			innerRadius: 0.35,
			show: true,
			offset: {
				top: 2,
				left: -245
			}
		}
	},
	legend: {
		noColumns: 2
	},
	grid: {
		hoverable: true,
		clickable: true
	}
};

// Overview Bar chart
var overview_options = {
	legend: {
		show: false
	},
	bars: {
		show: true,
		align: "center"
	},
	grid: {
		color: '#2F96B4'
	},
	xaxis: {
		mode: "categories",
		show: false
	},
	yaxis: {
		min: 0,
		max: 100,
		tickDecimals: 0,
		tickSize: 10,
		show: false
	},
	selection: {
		mode: "xy"
	}
};

$(document).ready(function(){
	
	// Breadcrumbs
	var page_crumbs = {
    	0 : 	{ 'url' : "/admin/",		'text'	: "Dashboard" },
    	1 : 	{ 'url'	: "/admin/surveys.php",	'text'	: "Surveys" },
    	2 : 	{ 'url'	: "/admin/survey/" + survey_id + "/",	'text'	: survey_name },
    	3 : 	{ 'url'	: "/admin/results/" + survey_id + "/",'text'	: "Results" }
    };
    
    gen_crumbs(page_crumbs);
      
    bind_events();
    
    $('#intro').show();
    $('#survey_group').hide();
    $('#question_group').hide();
    $('#dump_group').hide();
    
    $('#survey_buttons').hide();
    $('#question_buttons').hide();
    
    $('#search_filter').search_filter({
		'values':[
			{
				'id': 		'search_filter_from_input',
				'type': 	'input',
				'class':	'date',
				'value': 	'from',
				'html':		'From Date'
			},
			{
				'id': 		'search_filter_to_input',
				'type': 	'input',
				'class':	'date',
				'value': 	'to',
				'html':		'To Date'
			}
		],
		'button_class': 'button ui-button-info',
		'override': true,
		'callback': build_results_list,
		'type_name': 'search_filter_type'
	});
    
    $('#search_filter_question').search_filter({
		'values':[
		     {
				'id': 		'search_filter_question_od',
				'type': 	'staff_selector',
				'value': 	'od',
				'html':		'Operational Director',
				'params':	{
					type:		'od'
				}
			 },
			 {
				'id': 		'search_filter_question_rm',
				'type': 	'staff_selector',
				'value': 	'rm',
				'html':		'Regional Manager',
				'params':	{
					type:		'rm'
				}
			 },
			 {
				'id': 		'search_filter_question_pm',
				'type': 	'staff_selector',
				'value': 	'pm',
				'html':		'Property Manager',
				'params':	{
					type:		'pm'
				}
			 },
		     {
				'id': 		'search_filter_question_rmc',
				'type': 	'rmc_selector',
				'value': 	'rmc',
				'html':		'Property'
			},
			{
				'id': 		'search_filter_question_from_input',
				'type': 	'input',
				'class':	'date',
				'value': 	'from',
				'html':		'From Date'
			},
			{
				'id': 		'search_filter_question_to_input',
				'type': 	'input',
				'class':	'date',
				'value': 	'to',
				'html':		'To Date'
			}
		],
		'button_class': 'button ui-button-info',
		'override': true,
		'callback': build_questions_list,
		'type_name': 'search_filter_question_type'
	});
    
    $('#search_filter_dump').report_builder({
    	page: '/admin/results.php',
		form_name: 'form_dump_search',
		extra_params:{
			'survey_id' :	survey_id
		},
		config:[
			{
				name: 'Data Dump',
				phpvar: 'a',
				phpvalue: 'dump',
				extra_fields: [
					{
						id: 'dump_from_input',
						name: 'From Date',
						mandatory: 'Please enter a From Date',
						type: {
							type: 'input',
							'default': '01/01/2013',
							params: 'date'
						}
					},
					{
						id: 'dump_to_input',
						name: 'To Date',
						mandatory: 'Please enter a To Date',
						type: {
							type: 'input',
							'default': '01/04/2014',
							params: 'date'
						}
					}
				]
			}
		]
	});
    
});

function bind_events(){
	
	$("#survey_link").on('click', function () {
		$('#intro_group').hide();
	    $('#survey_group').show();
	    $('#data-div').hide();
	    $('#group-div').hide();
	});
	
	$("#question_link").on('click', function () {
		$('#intro_group').hide();
	    $('#question_group').show();
	    $('#data-question-div').hide();
	    $('#group-question-div').hide();
	    setTimeout(function(){
			$('#search_filter_question').search_filter('show_field', 1);
		}, 200);
	});
	
	$("#dump_link").on('click', function () {
		$('#intro_group').hide();
	    $('#dump_group').show();
	});

	$("#graph-view-button").on('click', function () {
		$('#graph_level').val('od');
		$('#graph_value').val('');
		$('#graph_previous_value').val('');
		 graph_button('bar');
	});

	$("#pie-view-button").on('click', function () {
		$('#graph_level').val('od');
		$('#graph_value').val('');
		$('#graph_previous_value').val('');
		 graph_button('pie');
	});

	$("#data-view-button").on('click', function () {
		$('#graph_level').val('od');
		$('#graph_value').val('');
		$('#graph_previous_value').val('');
		 data_button();
	});

	$("#graph-question-view-button").on('click', function () {
		 graph_question_button('bar');
	});

	$("#pie-question-view-button").on('click', function () {
		 graph_question_button('pie');
	});

	$("#data-question-view-button").on('click', function () {
		 build_questions_list();
	});
    
    $('#data-up-button').on('click', function(){
    
    	$('#graph_value').val($('#graph_previous_value').val());
    
   		if($('#graph_level').val() == 'rm'){
			$('#graph_level').val('od');
		}else if($('#graph_level').val() == 'pm'){
        	$('#graph_level').val('rm');
        }else if($('#graph_level').val() == 'rmc'){
        	$('#graph_level').val('pm');
        }else if($('#graph_level').val() == 'tenant'){
        	$('#graph_level').val('rmc');
        }else if($('#graph_level').val() == ''){
        	$('#graph_level').val('tenant');
        }
	        
        if($('#graph_level').val() != ''){
       		data_button();
       	}	
    });
    
}

function graph_button(type){

	var processor = $('body').processing('start', 'Building Graphs');
	
	var data = [];

	$('#graph-div').show();
	$('#data-div').hide();
	$('#graph-div').html('');
	
	var aoData = [];
	
	pushFormData('#form_search', aoData);
	aoData.push({name : 'no_fields', value : $('#search_filter').search_filter('get_no_fields')});
	
	aoData.push({name : 'a', value : 'rmcs_graph'});
	aoData.push({name : 'graph_level', value : $('#graph_level').val()});
	aoData.push({name : 'graph_value', value : $('#graph_value').val()});
	aoData.push({name : 'survey_id', value : survey_id});
	
	var dataurl = "/admin/results.php";
	
	$.post(dataurl, 
	aoData,
	function(series){		
		
		var i = 0;
		
		// Set graph options
		if(type == 'bar'){
			height = '350px';
			width = '100%';
			chart_options = options;
		}else if(type == 'pie'){
			height = '200px';
			width = '100%';
			chart_options = full_pie_options;
		}	
			
		if(type == 'bar'){
			data.push(series);
		}else if(type == 'pie'){
			
			// Loop through each object at this tier, adjusting the data for pie format
			for(j=0;j<series['data'].length;j++){
				
				var answer_len = series['data'][j][0].length;
				
				d_array = {
					'label': series['data'][j][0],
					'data': series['data'][j][1]
				}
				
				data.push(d_array);
			}
		}
		
		$('#graph_previous_value').val(series.previous_value);
		
		var graph = create_graph(type, series, i, width, height);
		graph.appendTo($('#graph-div'));

		// Click down event
		$('#graph-' + i)
			.off("plotclick")
			.on("plotclick", function (event, pos, item) {
			
				if (item) {
				
					var gl = $('#graph_level').val();
					
					if($('#graph_level').val() == 'od'){
						$('#graph_level').val('rm');
					}else if($('#graph_level').val() == 'rm'){
			        	$('#graph_level').val('pm');
			        }else if($('#graph_level').val() == 'pm'){
			        	$('#graph_level').val('rmc');
			        }else if($('#graph_level').val() == 'rmc'){
			        	$('#graph_level').val('tenant');
			        }else if($('#graph_level').val() == 'tenant'){
			        	$('#graph_level').val('');
			        }
			        
			        if($('#graph_level').val() != ''){
			        	if(type == 'bar'){
			        		$('#graph_value').val(item.series.data[item.dataIndex][0]);
			        	}else{
			        		$('#graph_value').val(item.series.label)
			        	}
						$('#graph_previous_value').val(item.series.previous_value);
			       		graph_button(type);
			       	}else{
			       		$('#graph_level').val(gl);
			       	}        
			    }
			});

		g_plot = $.plot('#graph-' + i, data, chart_options);
		
		// Create overview
		var overview_buttons = $( document.createElement('div') )
			.addClass('clearfix button_group');
		
		var hint = $( document.createElement('span') )
			.attr('id', "overview_hint")
			.addClass('label label-success pull-left')
			.html("Highlight a section on the mini graph below to zoom in on a specific area")
			.appendTo(overview_buttons);
		
		var up_button = $( document.createElement('button') )
			.attr('id', "graph-up-button")
			.addClass('button  ui-button-info pull-right')
			.html("Up Level")
			.on('click', function(){
			    
		    	$('#graph_value').val($('#graph_previous_value').val());
		    
		   		if($('#graph_level').val() == 'rm'){
					$('#graph_level').val('od');
				}else if($('#graph_level').val() == 'pm'){
		        	$('#graph_level').val('rm');
		        }else if($('#graph_level').val() == 'rmc'){
		        	$('#graph_level').val('pm');
		        }else if($('#graph_level').val() == 'tenant'){
		        	$('#graph_level').val('rmc');
		        }else if($('#graph_level').val() == ''){
		        	$('#graph_level').val('tenant');
		        }
			        
		        if($('#graph_level').val() != ''){
		       		graph_button(type);
		       	}	
		    })
			.appendTo(overview_buttons);
		
		overview_buttons.appendTo($('#graph-div'))
		
		// If there are more than 5 points on a bar graph, show overview graph
		if(series.data.length >= 5 && type == 'bar'){
			
			var overview = $( document.createElement('div') )
				.attr('id', 'overview-' + i)
				.addClass('flot_overview')
			    .on("plotselected", function (event, ranges) {
			        g_plot.setSelection(ranges);
			    })
				.appendTo($('#graph-div'));
				
			g_overview = $.plot("#overview-" + i, data, overview_options);
		}else{
			$('#overview_hint').hide();
		}
		
		if($('#graph_level').val() == 'od'){
			$('#graph-up-button').hide();
		}else{
			$('#graph-up-button').show();
		}
		
		$("#rmcs-graph").show();

		$('body').processing('end', processor);
		
	},
	'json');	
}

function graph_question_button(type){

	var processor = $('body').processing('start', 'Building Graphs');

	$('#graph-question-div').show();
	$('#data-question-div').hide();
	$('#graph-question-div').html('');
	
	var aoData = [];
	aoData.push({name : 'a', value : 'question_graph'});
	aoData.push({name : 'graph_level', value : $('#graph_level').val()});
	aoData.push({name : 'graph_value', value : $('#graph_value').val()});
	aoData.push({name : 'survey_id', value : survey_id});
	
	var dataurl = "/admin/results.php";
	
	$.post(dataurl, 
	aoData,
	function(series){
		
		var k = 0;
		
		// Iterate through questions
		for(i=0;i<series.length;i++){
			
			k ++;

			// Set graph options
			if(type == 'bar'){
				height = '350px';
				width = '100%';
				chart_options = options;
				float = 'none';
			}else if(type == 'pie'){
				height = '200px';
				width = '48%';
				chart_options = pie_options;
				float = 'left';
			}
			
			var data = [];
			
			if(type == 'bar'){
				data.push(series[i]);
			}else if(type == 'pie'){
				
				var y = k%2;
				
				if(y == 0){
					float = 'right';
				}
				
				var no_answers = series[i]['data'].length;

				// Loop through each answer for this question, adjusting the data for pie format
				for(j=0;j<no_answers;j++){
					
					var answer_len = series[i]['data'][j][0].length;
					
					// If there are more than 4 answers as one is at least 10 chars, fullwidth pie, else half width
					if(answer_len > 10 && no_answers > 4){
						width = '100%';
						chart_options = full_pie_options
						float = 'none';
						k = 0;
					}
					
					d_array = {
						'label': series[i]['data'][j][0],
						'data': series[i]['data'][j][1]
					}
					
					data.push(d_array);
				}
			}
			
			var graph = create_graph(type, series[i], i, width, height, float, 'question-');
			graph.appendTo($('#graph-question-div'));

			$.plot("#question-graph-" + i , data, chart_options);
		}

		$('body').processing('end', processor);
		
	},
	'json');
}

function data_button(){
	
	$('#graph-div').hide();
	$('#data-div').show();

	build_results_list();
	
}

function data_question_button(){
	
	$('#graph-question-div').hide();
	$('#data-question-div').show();

	build_question_results_list();
	
}

function build_results_list() {

	$('#tenants-div').hide();
	$('#rmcs-div').show();
	$('#level').val('rmcs');

	$('#graph-div').hide();
	$('#data-div').show();

	var processor = $('body').processing('start', 'Building Results', 33);
	
	var sort_array = new Array();

	sort_array = [[ 0 , 'asc' ]];

	$('#results_table').show();
	
	if($('#graph_level').val() == 'rmc'){
		var columns =  [
			{ 
				sWidth: '125', 
				"bVisible": true
			},
			{ 
				sWidth: '250',
				"sTitle": "Property Name"
			},
			{ sWidth: '105' },
			{ sWidth: '80' },
			{ sWidth: '65' },
			{ sWidth: '120' },
			{ sWidth: '90' }
       	]
	}else{
		var columns =  [
			{ 
				sWidth: '125', 
				"bVisible": false
			},
			{ 
				sWidth: '375',
				"sTitle": "Name"
			},
			{ sWidth: '105' },
			{ sWidth: '80' },
			{ sWidth: '65' },
			{ sWidth: '120' },
			{ sWidth: '90' }
       	]
	}
	
	if($('#graph_level').val() == 'od'){
		$('#data-up-button').hide();
	}else{
		$('#data-up-button').show();
	}
	
	myApp.table = $('#results_table')
		.dataTable({
			"bJQueryUI": true,
			"bDestroy": true,
	        "bAutoWidth": true,
			"bDeferRender": true,
	        "bFilter": true,
	        "aoColumns": columns,
	        "oLanguage": {
		       	"sInfoFiltered": "",
		       	"sSearch":"Search Columns Below: ",
	            "sLengthMenu": "Show _MENU_"
	        },
			"sPaginationType": "full_numbers",
	        "bServerSide": true,
	        "sAjaxSource": $(location).attr('href'),
	        "sServerMethod": "POST",
	        "fnServerData": function ( sSource, aoData, fnCallback ) {
	        	$('#a').val('search')

	        	$('body').processing('update', processor, 66);

	        	pushFormData('#form_search', aoData);
	        	aoData.push({name : 'no_fields', value : $('#search_filter').search_filter('get_no_fields')});
	        	aoData.push({name : 'a', value : 'rmcs'});
	        	aoData.push({name : 'graph_level', value : $('#graph_level').val()});
	        	aoData.push({name : 'graph_value', value : $('#graph_value').val()});
	        	aoData.push({name : 'survey_id', value : survey_id});

	        	$.post(sSource, 
	        	aoData, 
				function(json){
					
					$('#graph_previous_value').val(json['previous_value']);
					
	        		fnCallback(json);
				}, 
				"json");
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					
				$(nRow).on('click', function(e){
					var id = $(this).attr('id');
					
					var gl = $('#graph_level').val();
				
					if($('#graph_level').val() == 'tenant'){
			        	$('#graph_level').val('');
			        }else if($('#graph_level').val() == 'rmc'){
			        	$('#graph_level').val('tenant');
			        }else if($('#graph_level').val() == 'pm'){
			        	$('#graph_level').val('rmc');
			        }else if($('#graph_level').val() == 'rm'){
			        	$('#graph_level').val('pm');
			        }else if($('#graph_level').val() == 'od'){
						$('#graph_level').val('rm');
					}
					
			        if($('#graph_level').val() == ''){
			       		$('#graph_level').val(gl);
			       	}
			        
			        if($('#graph_level').val() != 'tenant'){
		        		$('#graph_value').val(aData[1]);
			       		build_results_list();
			       	}else{
						$('#graph_value').val(aData[0]);
			       		build_tenants_list(id);
			       	}
					
				});
	        },
	        "fnDrawCallback": function( oSettings ) {
	        
				aData = oSettings['aoData'][0]['_aData'];
		        
		        if($('#graph_level').val() != ''){
					
					$('#graph_previous_value').val(aData['previous_value']);
		       	}
	        	
		        $('#survey_buttons').show();

				$('body').processing('end', processor);
	        	
	        },
	        "aaSorting": sort_array
		});
	
	if(myApp.table === undefined){
	
		$("#results_table tfoot input").each( function (i) {
	        asInitVals[i] = this.value;
	    } );
	     
	    $("#results_table tfoot input").on('focus', function () {
	        if ( this.className == "search_init" )
	        {
	            this.className = "";
	            this.value = "";
	        }
	    } );
	     
	    $("#results_table tfoot input").on('blur', function (i) {
	        if ( this.value == "" ){
	            this.className = "search_init";
	            this.value = asInitVals[$("#results_table tfoot input").index(this)];
	        }
	    });

	    $("#results_table tfoot input").on('keypress', function(e){
	    	var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13){ 
        		myApp.table.fnFilter( this.value, $("#results_table tfoot input").index(this) );
			}
	    });

	    $('#results_table_wrapper .dataTables_filter input')
		    .off('keypress keyup')
		    .on('keypress', function(e){
		    	var code = (e.keyCode ? e.keyCode : e.which);
			    if (code == 13) {
			    	myApp.table.fnFilter($(this).val());
			    }
		    });
	}
}

function build_tenants_list(rmc) {

	$('#tenants-div').show();
	$('#rmcs-div').hide();
	$('#level').val('tenants');

	$('#graph-div').hide();
	$('#data-div').show();

	var processor = $('body').processing('start', 'Building Tenant List', 33);
	
	var sort_array = new Array();

	sort_array = [[ 0 , 'asc' ]];

	$('#tenants_table').show();
	
	myApp.ttable = $('#tenants_table')
		.dataTable({
			"bJQueryUI": true,
			"bDestroy": true,
	        "bAutoWidth": true,
			"bDeferRender": true,
	        "bFilter": true,
	        "oLanguage": {
		       	"sInfoFiltered": "",
		       	"sSearch":"Search Columns Below: ",
	            "sLengthMenu": "Show _MENU_"
	        },
			"sPaginationType": "full_numbers",
	        "bServerSide": true,
	        "sAjaxSource": $(location).attr('href'),
	        "sServerMethod": "POST",
	        "fnServerData": function ( sSource, aoData, fnCallback ) {
	        	$('#a').val('search')
				
	        	$('body').processing('update', processor, 66);
	        	
	        	pushFormData('#form_search', aoData);
	        	aoData.push({name : 'no_fields', value : $('#search_filter').search_filter('get_no_fields')});
	        	
	        	aoData.push({name : 'a', value : 'rmcs'});
	        	aoData.push({name : 'rmc_num', value : rmc});
	        	aoData.push({name : 'graph_level', value : $('#graph_level').val()});
	        	aoData.push({name : 'graph_value', value : $('#graph_value').val()});
	        	aoData.push({name : 'survey_id', value : survey_id});

	        	$.post(sSource, 
	        	aoData, 
				function(json){
					
					$('#graph_previous_value').val(json['previous_value']);
					
	        		 fnCallback(json);
				}, 
				"json");
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				var id = $(nRow).attr('id');
				var no_tds = 0;
				
				$(nRow).find('td').each(function(){
					if(no_tds != 4){
						$(this).on('click', {'id':id}, function(e){
							var id = e.data['id'];
							
							do_question(id);
						});
					}

					no_tds++;
				});
	        },
	        "fnDrawCallback": function( oSettings ) {

	    		$('body').processing('end', processor);
	        	$('.button').not('.ui-button').button();
	        },
	        "aaSorting": sort_array
		});
		

	if(myApp.ttable === undefined){

		$("#tenants_table tfoot input").each( function (i) {
	        asInitVals[i] = this.value;
	    } );
	     
	    $("#tenants_table tfoot input").on('focus', function () {
	        if ( this.className == "search_init" )
	        {
	            this.className = "";
	            this.value = "";
	        }
	    } );
	     
	    $("#tenants_table tfoot input").on('blur', function (i) {
	        if ( this.value == "" ){
	            this.className = "search_init";
	            this.value = asInitVals[$("#tenants_table tfoot input").index(this)];
	        }
	    });

	    $("#tenants_table tfoot input").on('keypress', function(e){
	    	var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13){ 
        		myApp.ttable.fnFilter( this.value, $("#tenants_table tfoot input").index(this) );
			}
	    });

	    $('#tenants_table_wrapper .dataTables_filter input')
		    .off('keypress keyup')
		    .on('keypress', function(e){
		    	var code = (e.keyCode ? e.keyCode : e.which);
			    if (code == 13) {
			    	myApp.ttable.fnFilter($(this).val());
			    }
		    });
	}
}

function build_questions_list() {

	$('#graph-question-div').hide();
	$('#data-question-div').show();

	var processor = $('body').processing('start', 'Building Question Results', 33);
	
	var sort_array = new Array();

	sort_array = [[ 0 , 'asc' ]];

	$('#results_question_table').show();
	
	myApp.qtable = $('#results_question_table')
		.dataTable({
			"bJQueryUI": true,
			"bDestroy": true,
	        "bAutoWidth": true,
			"bDeferRender": true,
	        "bFilter": true,
	        "oLanguage": {
		       	"sInfoFiltered": "",
		       	"sSearch":"Search Columns Below: ",
	            "sLengthMenu": "Show _MENU_"
	        },
			"sPaginationType": "full_numbers",
	        "bServerSide": true,
	        "sAjaxSource": $(location).attr('href'),
	        "sServerMethod": "POST",
	        "fnServerData": function ( sSource, aoData, fnCallback ) {
	        	$('#a').val('question')

	        	$('body').processing('update', processor, 66);

	        	pushFormData('#form_question_search', aoData);
	        	aoData.push({name : 'no_fields', value : $('#search_filter_question').search_filter('get_no_fields')});
	        	
	        	aoData.push({name : 'a', value : 'question'});
	        	aoData.push({name : 'graph_level', value : $('#graph_level').val()});
	        	aoData.push({name : 'graph_value', value : $('#graph_value').val()});
	        	aoData.push({name : 'survey_id', value : survey_id});

	        	$.post(sSource, 
	        	aoData, 
				function(json){
					
	        		fnCallback(json);
				}, 
				"json");
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	        },
	        "fnDrawCallback": function( oSettings ) {
	        	
	        	$('#question_buttons').show();

	    		$('body').processing('end', processor);
	        	
	        },
	        "aaSorting": sort_array
		});
	
	if(myApp.qtable === undefined){
	
		$("#results_question_table tfoot input").each( function (i) {
	        asInitVals[i] = this.value;
	    } );
	     
	    $("#results_question_table tfoot input").on('focus', function () {
	        if ( this.className == "search_init" )
	        {
	            this.className = "";
	            this.value = "";
	        }
	    } );
	     
	    $("#results_question_table tfoot input").on('blur', function (i) {
	        if ( this.value == "" ){
	            this.className = "search_init";
	            this.value = asInitVals[$("#results_question_table tfoot input").index(this)];
	        }
	    });

	    $("#results_question_table tfoot input").on('keypress', function(e){
	    	var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13){ 
        		myApp.qtable.fnFilter( this.value, $("#results_question_table tfoot input").index(this) );
			}
	    });

	    $('#results_question_table_wrapper .dataTables_filter input')
		    .off('keypress keyup')
		    .on('keypress', function(e){
		    	var code = (e.keyCode ? e.keyCode : e.which);
			    if (code == 13) {
			    	myApp.qtable.fnFilter($(this).val());
			    }
		    });
	}	
}
