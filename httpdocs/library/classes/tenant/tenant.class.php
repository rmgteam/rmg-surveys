<?php
require_once($UTILS_SERVER_PATH."library/classes/search_builder/search_builder.class.php");
require_once($UTILS_SERVER_PATH."library/classes/survey/survey_resident.class.php");

class tenant {
	
	var $tenant_ref;
	var $tenant_num;
	var $tenant_name;
	var $tenant_email;
	var $rmc_num;
	var $rmc_ref;
	var $rmc_name;
	var $tenant_address_1;
	var $tenant_address_2;
	var $tenant_address_3;
	var $tenant_address_4;
	var $tenant_county;
	var $tenant_city;
	var $tenant_postcode;
	var $tenant_country;
	var $survey_optout;

	function tenant($num=''){
		if($num != ''){
			$mysql = new mysql();
			$sql = "SELECT r.*,
			l.resident_ref,
			c.rmc_name,
			lr.rmc_ref,
			e.survey_optout
			FROM cpm_residents r
			INNER JOIN cpm_lookup_residents l ON r.resident_num = l.resident_lookup
			INNER JOIN cpm_residents_extra e ON r.resident_num = e.resident_num
			INNER JOIN cpm_rmcs c ON c.rmc_num = r.rmc_num
			INNER JOIN cpm_lookup_rmcs lr ON c.rmc_num = lr.rmc_lookup
			WHERE r.resident_num = '".$num."'";
			$result = $mysql->query($sql, 'Get Resident');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				$row = $mysql->fetch_array($result);
				
				$this->rmc_num = $row['rmc_num'];
				$this->rmc_ref = $row['rmc_ref'];
				$this->rmc_name = $row['rmc_name'];
				
				$this->tenant_ref = $row['resident_ref'];
				$this->tenant_num = $row['resident_num'];
				$this->tenant_name = $row['resident_name'];
				
				$this->tenant_email = $row['resident_email'];
				$this->tenant_address_1 = $row['resident_address_1'];
				$this->tenant_address_2 = $row['resident_address_2'];
				$this->tenant_address_3 = $row['resident_address_3'];
				$this->tenant_address_4 = $row['resident_address_4'];
				$this->tenant_county = $row['resident_address_county'];
				$this->tenant_city = $row['resident_address_city'];
				$this->tenant_postcode = $row['resident_address_postcode'];
				$this->tenant_country = $row['resident_address_country'];
				$this->survey_optout = $row['survey_optout'];
			}
		}
	}
	
	function ref_to_tenant($ref=''){
		if($ref != ''){
			$mysql = new mysql();
			$sql = "SELECT resident_lookup
			FROM cpm_lookup_residents
			WHERE resident_ref = '".$ref."'";
			$result = $mysql->query($sql, 'Get Ref');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				$row = $mysql->fetch_array($result);
				$this->tenant($row['resident_lookup']);
			}
		}
	}

	function get_address($type="string"){
		
		$address = '';
		
		$address_arr = array();
		
		$address_arr[] = $this->tenant_address_1;
		$address_arr[] = $this->tenant_address_2;
		$address_arr[] = $this->tenant_address_3;
		$address_arr[] = $this->tenant_address_4;
		$address_arr[] = $this->tenant_county;
		$address_arr[] = $this->tenant_city;
		$address_arr[] = $this->tenant_postcode;
		
		foreach($address_arr as $address_item){
			if(!is_null($address_item)){
				if($address_item != ''){
					$address .= $address_item . "<br/>";
				} 	
			}
		}
		
		$address = substr($address, 0, - 5);
		
		if($type == "string"){
			return $address;
		}else{
			return $address_arr;
		}
	}

	function get_list ($request, $survey_id){
	
		$mysql = new mysql();
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();
		$no_fields = $request['no_fields'];
		$rmc_num = '';

		$values_array = $search_builder->request_to_array($request, 'search_filter_');
		
		if ($no_fields > 0 ){
			foreach($values_array as $key=>$value) {
		
				$search_term = $value;
		
				switch($key){
					case "rmc":
						$field = "r.rmc_num";
						$type = "equals";
						$rmc_num = $security->clean_query( $search_term );
						break;
					case "director":
						$field = "r.is_resident_director";
						$type = "equals";
						$rmc_num = $security->clean_query( $search_term );
						break;
					default:
						$field = $key;
						break;
				}
				if($type == "equals"){
					$search_builder->add_filter($field, "=", $security->clean_query( $search_term ), 'AND');
				}else{
					$search_builder->add_filter($field, "LIKE", '%' . $security->clean_query( $search_term ) . '%', 'AND');
				}
			}
		}
	
		$input = $request;
		
		$residents = '';
		
		// Get id's for select all
		$sql = "SELECT * 
		FROM cpm_residents
		WHERE rmc_num = '".$rmc_num."'";
		
		$result = $mysql->query($sql, 'Get all residents - no letter sent');
		$num_results = $mysql->num_rows($result);
		if($num_results > 0){
			while($row = $mysql->fetch_array($result)){
				$residents .=  $row['resident_num'] . ',';
			}
			$residents = substr($residents, 0, -1);
		}
		
		if(isset($request['results_table_all_node_radios-1_answer'])){
			if($request['results_table_all_node_radios-1_answer'] == '1'){
				$request['residents'] = $residents;
			}
		}
		
		if(isset($request['letters'])){
			$search_builder->add_tick_column('r.resident_num', explode(',',$request['residents']));
			$search_builder->add_filter('e.survey_optout', '=', 'N', 'AND');
		}
		
		$search_builder->add_index_column('r.resident_num');
		$search_builder->add_column('resident_ref');
		$search_builder->add_column('resident_name');
		$search_builder->add_function_column('r.resident_num', 'survey_resident', 'can_send_info', array($survey_id));
		$search_builder->add_column('resident_email');
		$iColumnCount = count($search_builder->columns);
	
		//Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
	
		//Ordering
		$iSortingCols = intval( $request['iSortingCols'] );
		for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
			if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
				$search_builder->add_order($search_builder->columns[intval($request['iSortCol_'.$i])], ($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
	
		//Filtering
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
	
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
		
		$search_builder->add_filter('r.resident_is_active', "=", '1', 'AND');
	
		$sql = "
		FROM cpm_residents r
		INNER JOIN cpm_lookup_residents lr ON lr.resident_lookup = r.resident_num
		INNER JOIN cpm_residents_extra e ON e.resident_num = r.resident_num";
	
		$search_builder->add_sql($sql);
		
		$letter_types = json_decode($request['letter_types']);
	
		$output = $search_builder->output($request);
		$output['residents'] = $request['residents'];
		$output['letter_types'] = $request['letter_types'];
		$output['all_resident_ids'] = $residents;
		$output['rmc_num'] = $rmc_num;
	
		return $output;
	
	}
}

?>