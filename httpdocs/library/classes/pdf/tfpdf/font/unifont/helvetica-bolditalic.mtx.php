<?php
$name='Helvetica_cyr-BoldOblique';
$type='TTF';
$desc=array (
  'Ascent' => 915,
  'Descent' => -217,
  'CapHeight' => 915,
  'Flags' => 262148,
  'FontBBox' => '[-102 -217 1109 915]',
  'ItalicAngle' => 0,
  'StemV' => 165,
  'MissingWidth' => 783,
);
$up=-133;
$ut=20;
$ttffile='C:\Inetpub\wwwroot\intranet-marc\trunk\trunk\library\classes\pdf\tfpdf/font/unifont/Helvetica-BoldItalic.ttf';
$originalsize=42262;
$fontkey='helveticaBI';
?>