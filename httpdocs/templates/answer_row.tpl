<table id="answer_row" class="template_include">
	<thead>
		<tr>
			<th width="40%">Answer</th>
			<th width="40%">Value</th>
			<th width="20%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataTables_empty">Loading</td>
			<td class="dataTables_empty">Data From</td>
			<td class="dataTables_empty">Server</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th><input type="text" value="Search Answer" class="search_init" /></th>
			<th><input type="text" value="Search Value" class="search_init" /></th>
			<th></th>
		</tr>
	</tfoot>
</table>