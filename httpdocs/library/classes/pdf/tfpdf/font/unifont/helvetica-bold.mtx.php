<?php
$name='Helvetica-Black_cyr-Bold';
$type='TTF';
$desc=array (
  'Ascent' => 818,
  'Descent' => -182,
  'CapHeight' => 818,
  'Flags' => 262148,
  'FontBBox' => '[-325 -182 1165 818]',
  'ItalicAngle' => 0,
  'StemV' => 165,
  'MissingWidth' => 818,
);
$up=-52;
$ut=70;
$ttffile='C:\Inetpub\wwwroot\intranet-marc\trunk\trunk\library\classes\pdf\tfpdf/font/unifont/Helvetica-Bold.ttf';
$originalsize=37470;
$fontkey='helveticaB';
?>