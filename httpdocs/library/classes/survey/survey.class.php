<?php
require_once($UTILS_SERVER_PATH."library/classes/core/core.class.php");
require_once($UTILS_SERVER_PATH."library/classes/tenant/tenant.class.php");
require_once($UTILS_SERVER_PATH."library/classes/question/question.class.php");
require_once($UTILS_SERVER_PATH."library/classes/answer/answer.class.php");
require_once($UTILS_SERVER_PATH."library/classes/search_builder/search_builder.class.php");

class survey extends core {
	
	var $survey_serial;
	var $survey_name;
	var $survey_desc;
	var $survey_id;
	var $survey_once;
	var $survey_director;
	var $survey_frequency;
	var $survey_frequency_string;
	var $survey_amount;
	
	function __construct($ref="", $ref_type="serial"){
		$this->survey($ref, $ref_type);
	}
	
	/**
	 * Get Survey
	 * 
	 * @param	string	$num			Survey ID / Suvrey Serial
	 * @param	string	$ref_type		Type (id or serial)
	 */
	function survey($num='', $ref_type="serial"){
		
		if($num != ''){
			
			$mysql = new mysql();
			
			if( $ref_type == "serial" ){
				$ref_clause = " survey_serial = '".$num."'";
			}
			else{
				$ref_clause = " survey_id = '".$num."'";
			}
			
			$sql = "
			SELECT *
			FROM survey
			WHERE 
			".$ref_clause;
			$result = $mysql->query($sql, 'Get Survey');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				
				$row = $mysql->fetch_array($result);
				$this->survey_serial = $row['survey_serial'];
				$this->survey_name = $row['survey_name'];
				$this->survey_desc = $row['survey_desc'];
				$this->survey_id = $row['survey_id'];
				$this->survey_once = $row['survey_once'];
				$this->survey_director = $row['survey_director'];
				$this->survey_frequency = $row['survey_frequency'];
				$this->survey_frequency_string = $this->get_frequency_name($row['survey_frequency']); 
				$this->survey_amount = $row['survey_amount'];
			}
		}
	}
	
	/**
	 * Get Survey from serial
	 * 
	 * @param	string	$ref			Suvrey Serial
	 */
	function serial_to_survey($ref=''){
		if($ref != ''){
			$this->survey($ref, 'serial');
		}
	}

	/**
	 * Get List of surveys
	 * 
	 * @param	array	$request		$_REQUEST object
	 * @return	array	$output			Datatables formatted array
	 */
	function get_list ($request){
	
		$mysql = new mysql();
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();
	
		$input = $request;
		
		$search_builder->add_index_column('survey_id');
		$search_builder->add_column('survey_name');
		$search_builder->add_column('survey_serial');
		$search_builder->add_button_column('survey_id', 'Delete', "pre_delete_survey", array(), 'button ui-button-info');
		$iColumnCount = count($search_builder->columns);
	
		$search_builder->add_filter('survey_discon', "=", "N", 'AND');
	
		//Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
	
		//Ordering
		$iSortingCols = intval( $request['iSortingCols'] );
		for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
			if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
				$search_builder->add_order($search_builder->columns[intval($request['iSortCol_'.$i])], ($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
	
		//Filtering
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
	
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
	
		$sql = "
		FROM survey";
	
		$search_builder->add_sql($sql);
	
		$output = $search_builder->output($request);
	
		return $output;
	
	}
	
	/**
	 * Delete Answer
	 * 
	 * @return	boolean					Has answer been deleted
	 */
	function delete(){
		
		$mysql = new mysql();
		
		$sql = "
		UPDATE survey SET 
		survey_discon = 'Y' 
		WHERE 
		survey_id = '".$this->survey_id."'		
		";
		$result = $mysql->query($sql, 'Delete survey');
		if( $result !== false ){
			return true;
		}
		return false;
	}

	/**
	 * Check if resident has been sent survey 
	 * 
	 * @param	string	$resident_ref	Resident Ref
	 * @param	string	$survey_serial	Survey Serial
	 * @return	boolean					Boolean to show if found
	 */
	function resident_survey($resident_ref='', $survey_serial=''){
		if($resident_ref != '' && $survey_serial != ''){
			$this->serial_to_survey($survey_serial);
			$tenant = new tenant();
			$tenant->ref_to_tenant($resident_ref);
			
			$mysql = new mysql();
			$sql = "SELECT *
			FROM survey_resident
			WHERE survey_id = '".$this->survey_id."'
			AND resident_num = '".$tenant->tenant_num."'";
			$result = $mysql->query($sql, 'Get resident-survey');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				return true;
			}
		}

		return false;
	}

	/**
	 * Check if resident has been sent survey 
	 * 
	 * @return	Resource	$result		MySQL Resource
	 */
	function get_types(){
		$mysql = new mysql();
		$sql = "SELECT *
		FROM survey_type";
		$result = $mysql->query($sql, 'Get Types');
		return $result;
	}
	
	/**
	 * Check Data has been entered
	 * 
	 * @param	array	$request		$_REQUEST object
	 * @return	array	$results_array	Array of message, field, boolean if errored
	 */
	function check($request){
	
		$error_fields = array();
		$security = new security();
		$data = new data;
		$field = new field();
		$error_counter = 0;
		
		$is_valid = true;
			
		// check if the survey name is filled in, if not show error
			
		$error_message[$error_counter] = "Please enter a Survey Name";
		$error_fields[$error_counter] = "survey_name_label";
		
		if ($request["survey_name_input"] == ""){
			$is_valid = false;
			$error_has[$error_counter] = "true";
		}else{
			$error_has[$error_counter] = "false";
		}
			
		$error_counter++;
			
		// check if can answer multiple times is filled in, if not show error
			
		$error_message[$error_counter] = "Please select whether the survey can be answered more than once";
		$error_fields[$error_counter] = "survey_once_label";
		
		if ($request["survey_once_select"] == ""){
			$is_valid = false;
			$error_has[$error_counter] = "true";
		}else{
			$error_has[$error_counter] = "false";
		}
			
		$error_counter++;
			
		// check if can answer multiple times is filled in, if not show error
			
		$error_message[$error_counter] = "Please select whether the survey can only be answered by a director";
		$error_fields[$error_counter] = "survey_director_label";
		
		if ($request["survey_director_select"] == ""){
			$is_valid = false;
			$error_has[$error_counter] = "true";
		}else{
			$error_has[$error_counter] = "false";
		}
			
		$error_counter++;
		
		if ($request["survey_once_select"] == "N"){
			
			// check if number of survey asnwers is filled in, if not show error
				
			$error_message[$error_counter] = "Please select the answer frequency type";
			$error_fields[$error_counter] = "survey_frequency_label";
			
			if ($request["survey_frequency_select"] == ""){
				$is_valid = false;
				$error_has[$error_counter] = "true";
			}else{
				$error_has[$error_counter] = "false";
			}
				
			$error_counter++;
			
			if ($request["survey_frequency_select"] != "0"){
			
				// check if number of survey asnwers is filled in, if not show error
				
				$error_message[$error_counter] = "Please enter the answer frequency";
				$error_fields[$error_counter] = "survey_amount_label";
					
				if ($request["survey_amount_input"] == ""){
					$is_valid = false;
					$error_has[$error_counter] = "true";
				}else{
					$error_has[$error_counter] = "false";
				}
				
				$error_counter++;

				// check if number of survey asnwers is filled in, if not show error
					
				$error_message[$error_counter] = "The frequency must be a number";
				$error_fields[$error_counter] = "survey_amount_label";
				
				if ($request["survey_amount_input"] != "" && !is_numeric($request["survey_amount_input"])){
					$is_valid = false;
					$error_has[$error_counter] = "true";
				}else{
					$error_has[$error_counter] = "false";
				}
					
				$error_counter++;
			}
			
		}
			
		//check if survey summary is filled in, if not show error
			
		$error_message[$error_counter] = "Please enter a survey summary";
		$error_fields[$error_counter] = "survey_info_label";
		
		if ($request["survey_info_input"] == ""){
			$is_valid = false;
			$error_has[$error_counter] = "true";
		}else{
			$error_has[$error_counter] = "false";
		}
			
		$error_counter++;
	
		if($is_valid == true){
			$results_array["save_result"] = "success";
			$results_array["message"] = implode("|", $error_message);
			$results_array["fields"] = implode("|", $error_fields);
			$results_array["errored"] = implode("|", $error_has);
		}else{
			$results_array["save_result"] = "error";
			$results_array["message"] = implode("|", $error_message);
			$results_array["fields"] = implode("|", $error_fields);
			$results_array["errored"] = implode("|", $error_has);
		}
		
		return $results_array;
	}
	
	/**
	 * Save Survey
	 * 
	 * @param	array	$request		$_REQUEST object
	 * @return	array	$results_array	Array of whether it completed and ID of record
	 */
	function save($request){
		
		try{
	
			$mysql = new mysql();
			
			$security = new security();
			$data = new data;
			
			//if id is filled in then we are editing, if not it is a new record
			if($request['survey_id'] == ''){
				$sql = "INSERT INTO survey SET
				survey_created = '" . $data->today_to_ymd() . "', 
				survey_serial = '" . $security->gen_unique_serial('survey', 'survey_serial') . "', ";
			}else{
				$sql = "UPDATE survey SET";
			}
			
			$sql_middle = "
			survey_name = '". $security->clean_query($request['survey_name_input']) . "',
			survey_desc = '". $security->clean_query($request['survey_info_input']) . "',
			survey_once = '". $security->clean_query($request['survey_once_select']) . "',
			survey_director = '". $security->clean_query($request['survey_director_select']) . "',
			survey_frequency = '". $security->clean_query($request['survey_frequency_select']) . "',
			survey_amount = '". $security->clean_query($request['survey_amount_input']) . "'";
			
			$has_error=false;
			
			$last_id = "";
			
			if($request['survey_id'] == ''){
				// insert record
				$sql .= $sql_middle;
				$msg = 'Insert New Survey';
			}else{
				// insert record
				$sql .= $sql_middle.'
				WHERE survey_id = "'.$request['survey_id'].'"';
				$msg = 'Update Survey';
				$last_id = $request['survey_id'];
			}
			
			$has_error = $mysql->insert($sql, $msg);
			
			//if there was no issues inserting, return the id of the new tracker
			if (!is_bool($has_error) && $request['survey_id'] == ''){
				$last_id = $has_error;
			}
			
			if (!is_bool($has_error)){
				$results_array["save_result"] = "success";
				$results_array["id"] = $last_id;
			}else{
				$results_array["save_result"] = "This survey could not be saved.";
			}
				
			return $results_array;	
			
		}catch(Exception $e){
			$results_array["save_result"] = "This survey could not be saved.";
			$results_array["sql"] = $e->getMessage();
			return $results_array;
		}
	}

	/**
	 * Return surveys
	 * 
	 * @return	Resource	$result		MySQL Resource
	 */
	function get_surveys(){
		$mysql = new mysql();
		$sql = "
		SELECT *,
		(
			SELECT count(*)
			FROM survey_question
			WHERE survey_id = s.survey_id
		) as no_questions
		FROM survey s 
		WHERE 
		survey_discon = 'N' 
		";
		$result = $mysql->query($sql, 'Get Surveys');
		return $result;
	}

	/**
	 * Get num of surveys created since Date
	 * 
	 * @param	string		$ymd		Date in form of Ymd
	 * @return	int			$num_rows	Number of surveys
	 */
	function num_new_surveys($ymd=''){
		$data = new data();
		
		if($ymd==''){
			$ymd = $data->today_to_ymd();	
		}
		
		$mysql = new mysql();
		$sql = "SELECT COUNT(*)
		FROM survey s
		WHERE 
		survey_discon = 'N' AND 
		survey_created >= '".$ymd."'";
		$result = $mysql->query($sql, 'Get Surveys');
		while($row = $mysql->fetch_array($result)){
			$num_rows = $row[0];
		}
		return $num_rows;
	}

	/**
	 * Get num of surveys created since Date
	 * 
	 * @param	string		$id			Frequency ID
	 * @return	string					Frequency Name
	 */
	function get_frequency_name($id){
		
		if($id == '1'){
			return 'Years';
		}elseif($id == '2'){
			return 'Months';
		}elseif($id == '3'){
			return 'Weeks';
		}
	
	}
}

?>