var asInitVals = new Array();
var myApp = myApp || {};

$(document).ready(function(){
	
	$('#gDocs').pdfEmbed({ width: 850, height: 350 });
	
	build_question_list();
    
	$('#edit_button')
		.click(function(){do_survey_edit();});
	
	$('#question-tabs').tabs({
		activate: function(event, ui) {
		
			var id = ui.newTab.attr('aria-controls');

			if (id == 'answers'){
				build_answer_list( $('#question_id').val() );
			}
		}

	});		
	
	$('#survey-send-details').hide();
	$('#question-info').hide();
	$('#answer-info').hide();
	$('#letter_table').hide();

	$('#question-back-button').click(function(){
		form_read('question');
		build_results();
		$('#survey-send-details').hide();
		$('#question-info').hide();
		$('#questions-order-div').hide();
		$('#questions-div').show("FadeIn");
	});
	
	$('#question_edit_button').click(function(){
		form_edit('question'); 
		$('#question_save_button').show();
		$('#question_cancel_button').show();
		$('#question_edit_button').hide();
		
		var page_crumbs = {
	    	0 : 	{ 'url' 	: "/admin/",		'text'	: "Dashboard" },
	    	1 : 	{ 'url'	: "/admin/surveys.php",	'text'	: "Surveys" },
	    	2 : 	{ 'url'	: "/admin/survey/" + survey_id + "/",	'text'	: survey_name },
	    	3 : 	{ 'url'	: "/admin/survey/" + survey_id + "/",	'text'	: "Edit Question" }
	    };
	    gen_crumbs(page_crumbs); 
	});

	$('#question_cancel_button').click(function(){
		do_question($('#question_id').val());
	});

	$('#question_save_button').click(function(){
		do_question_save();
	});

	$('#question_add_button').click(function(){
		clear_question();
		form_edit('question');
		$('#survey-send-details').hide();
		$('#questions-div').hide();
		$('#questions-order-div').hide();
		$('#question-info').show("FadeIn");
		$('#question_edit_button').hide();
		$('#question_cancel_button').hide();
		$('#question-tabs .ui-tabs-nav').hide();
		
		var page_crumbs = {
	    	0 : 	{ 'url' 	: "/admin/",		'text'	: "Dashboard" },
	    	1 : 	{ 'url'	: "/admin/surveys.php",	'text'	: "Surveys" },
	    	2 : 	{ 'url'	: "/admin/survey/" + survey_id + "/",	'text'	: survey_name },
	    	3 : 	{ 'url'	: "/admin/survey/" + survey_id + "/",	'text'	: "New Question" }
	    };
	    gen_crumbs(page_crumbs); 
	});
	
	$('#send_survey_button').click(function(){
		$('#survey-send-details').show("FadeIn");
		$('#question-info').hide();
		$('#questions-order-div').hide();
		$('#questions-div').hide();
		
		$.post($(location).attr('href'), 
		{"survey_id" :  survey_id , 'a': 'survey'},
		function(data){
			if(data['survey_id'] != ''){
				if(data['survey_director'] == 'Y'){
					$('#search_filter').search_filter('show_field', 1);
					$('#search_filter').search_filter('add_field', 1);
					$('#search_filter').search_filter('set_field', 2, 'director', 'Y');
					$('#search_filter_clear_button').hide();
					$('#search_filter').search_filter('disable_field', 2, 'director');
				}else{
					$('#search_filter').search_filter('show_field', 1);
				}
			}
		},
		'json');
	});
	
	$('#question-order-button').click(function(){
		$('#survey-send-details').hide();
		$('#questions-div').hide();
		gen_q_order_list(function(){
			$('#questions-order-div').show().fadeIn("slow"); 
		});
	});
	
	$('#save_q_order_button').click(function(){
		save_q_order();
	});

	$('#answer_back_button').click(function(){
		form_read('answer');
		build_answer_list($('#question_id').val());
		$('#answer-info').hide();
		$('#answer-div').show("FadeIn");
	});
	$('#answer_edit_button').click(function(){
		form_edit('answer');
		$('#answer_edit_button').hide();
		$('#answer_save_button').show();
		$('#answer_cancel_button').show();
		$('#answer_back_button').hide();
	});

	$('#answer_cancel_button').click(function(){
		if( $('#answer_id').val() != "" ){
			form_read('answer');
			$('#answer_back_button').show();
		}
		else{
			form_read('answer');
			build_answer_list( $('#question_id').val() );
		}
	});

	$('#answer_save_button').click(function(){
		do_answer_save();
	});

	$('#answer-add-button').click(function(){
		clear_answer();
		form_edit('answer');
		$('#answer-div').hide();
		$('#answer-info').show("FadeIn");
		$('#answer_edit_button').hide();
		$('#answer_save_button').show();
		$('#answer_cancel_button').show();
		$('#answer_back_button').hide();
	});
	
	$('#enter_results_button').click(function(){
		$("#enter_dialog").dialog('open');
	});
	
	$('#question_type_select').on('change', function(){
		
		if($('#question_type_select').val() == '1'){
			$('#question_no-selections_input').val('1');
			$('#question_no-selections_input').attr('disabled', 'disabled');
		}else{
			$('#question_no-selections_input').removeAttr('disabled');
		}
	});
	
	var page_crumbs = {
    	0 : 	{ 'url' : "/admin/",		'text'	: "Dashboard" },
    	1 : 	{ 'url'	: "/admin/surveys.php",	'text'	: "Surveys" },
    	2 : 	{ 'url'	: "/admin/survey/" + survey_id + "/",	'text'	: survey_name }
    };
    
    gen_crumbs(page_crumbs); 
    
    $('#search_filter').search_filter({
		'values':[
			{
				'id': 		'search_filter_rmc',
				'type': 	'rmc_selector',
				'value': 	'rmc',
				'html':		'Property'
			},
			{
				'id': 		'search_filter_director_input',
				'type': 	'select',
				'params':	[
					{
						'value':	'N',
						'name':		'No'
					},
					{
						'value':	'Y',
						'name':		'Yes'
					}
				],
				'value': 	'director',
				'html':		'Resident Director'
			}
		],
		'button_class': 'button ui-button-info',
		'override': true,
		'callback': gen_letters_list,
		'type_name': 'search_filter_type',
		'extra_buttons': [
			{
				'id':		'do_send_button',
				'name':		'Send Emails/Letters',
				'event':	function(){
					do_send();
				}
			}                  
		]
	});
    
    $('#question-form').display_toggle({
    	'elements':[
			{
				'id': '#question_no-selections_row, #question_weight_row',
				'conditions': [
					{
						'field': '#question_type_select',
						'operator': '!=',
						'value' : '2',
						'visible_override' : true
					}
				]
			},
			{
				'id': '#question_mandatory_text_row',
				'conditions': [
					{
						'field': '#question_mandatory_select',
						'operator': '==',
						'value' : 'Y',
						'visible_override' : true
					}
				]
			}
		]
    });
	
	$("#enter_dialog").dialog({  
		modal: true,
		autoOpen: false,
		resizable: false,
		width: '800px',
	   	closeOnEscape: false,
	   	open: function(event, ui) {
	   		var dialog = $(event.target).parents(".ui-dialog.ui-widget");
	   		dialog.focus();
	   		dialog.find(".ui-dialog-buttonpane").find('button').removeClass('ui-state-focus ui-state-hover ui-state-active').addClass('ui-button-info').addClass('pull-right');
	   	},
	   	buttons: [
	   		{
	   			text: 'Go',
	   			id: 'enter_button_go',
	   			click: function() {
		   			do_survey_enter();
				}
			},
			{
				text: 'Cancel',
	   			click: function() {
		   			$(this).dialog('close');
	   			}
			}
		]
	});
	
	setTimeout(function(){$('#do_send_button').hide();},500);
	
});

function do_survey_enter(){
	if($('#survey_ref_input').val() != ''){
		window.open('/survey/' + survey_serial + '/' + $('#survey_ref_input').val())
		$("#enter_dialog").dialog('close');
	}
}

function save_q_order(){

	var processor = $('body').processing('start', 'Saving Question Order');
	
	var q_order_array = [];
	var qid;
	
	$("#order_table tbody tr").each(function() {
		qid = $(this).attr("id");
		qid = qid.replace("q_order_","");
		q_order_array[q_order_array.length] = qid;
	});
	
	var q_order = q_order_array.join(",");
	
	$.post($(location).attr('href'), 
	{"survey_id" : survey_id , 'a': 'save_question_order', 'order': q_order},
	function(data){
		
		if( data['save_result'] == "success" ){
			gen_q_order_list();
		}

		$('body').processing('end', processor);
		
	},
	'json');
}		

function gen_q_order_list(callback){

	var processor = $('body').processing('start', 'Building Order List', 33)
	
	var sort_array = new Array();

	sort_array = [[ 0 , 'asc' ]];

	$('#order_table').show();
	
	myApp.otable = $('#order_table')
		.dataTable({
			"bJQueryUI": true,
	        "bFilter": false,
	        "bAutoWidth": true,
	        "bDestroy": true,
			"bDeferRender": true,
	       	"oLanguage": {
		       	"sInfoFiltered": "",
		       	"sSearch":"Search Columns Below: ",
	            "sLengthMenu": "Show _MENU_"
	        },
			"bPaginate": false,
	        "sAjaxSource": $(location).attr('href'),
	        "sServerMethod": "POST",
	        "fnServerData": function ( sSource, aoData, fnCallback ) {
	        	$('#a').val('search')

	        	$('body').processing('update', processor, 66);
	        	
	        	aoData.push({name : 'a', value : 'questions'});
	        	aoData.push({name : 'order', value : 'true'});
	        	aoData.push({name : 'survey_id', value : survey_id });

	        	$.post(sSource, 
	        	aoData, 
				function(json){
	        		 fnCallback(json);
				}, 
				"json");
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				
	        },
	        "fnDrawCallback": function( oSettings ) {

	        	$('body').processing('end', processor);
	        	
	        	$("#order_table tbody").sortable({
    				helper: fixHelper
				}).disableSelection();
	        	
	        },
	        "aaSorting": sort_array
		});
		
		
		$('#order_table_wrapper div.fg-toolbar:first').hide();
	

	if($.isFunction(callback) === true){
		callback();
	}
}

function gen_letters_list() {
	var processor = $('body').processing('start', 'Building Letter List', 33);

	$('#letter_table_residents').val('');
	$('#letter_table_all_residents').val('');
	$('#resident_ref').val('');

	if($('#letter_table_all').data('radio') == "true"){
		$('#letter_table_all').radio('set', '');
	}

	var sort_array = new Array();

	sort_array = [[ 1 , 'asc' ]];

	$('#letter_table').show();

	myApp.ltable = $('#letter_table')
		.dataTable({
			"bJQueryUI": true,
			"bFilter": true,
			"bAutoWidth": true,
			"bDestroy": true,
			"bDeferRender": true,
			"oLanguage": {
				"sInfoFiltered": "",
				"sSearch":"Search Columns Below: ",
				"sLengthMenu": "Show _MENU_"
			},
			"aoColumns": [
				{ bSortable: false },
				{ bSortable: true },
				{ bSortable: true },
				{ bSortable: true }
			],
			"sPaginationType": "full_numbers",
			"bServerSide": true,
			"sAjaxSource": $(location).attr('href'),
			"sServerMethod": "POST",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$('#a').val('letter_search')

				$('body').processing('update', processor, 66);

				pushFormData('#form_search', aoData);
				aoData.push({name : 'no_fields', value : $('#search_filter').search_filter('get_no_fields')});
				aoData.push({name : 'a', value : 'letter_search'});
				aoData.push({name : 'survey_id', value : survey_id});

				var resident_refs = $('#letter_table_residents').val();

				aoData.push({name : 'residents', value : resident_refs});

				var letter_types = [];
				var types = $('#letter_types').val().split(',');

				for(var i=0; i<types.length; i++){
					var item = types[i].split(':');

					letter_types.push({name: item[0], value: item[1]});
				}

				$('.editable-hidden-input').each(function(){
					letter_types.push({name: $(this).attr('id'), value: $(this).val()});
				});

				aoData.push({name : 'letter_types', value : JSON.stringify(letter_types)});

				$.post(sSource,
				aoData,
				function(json){
					$('#rmc_input').val(json['rmc_num']);
					$('#letter_table_residents').val(json['residents']);
					$('#letter_table_all_residents').val(json['all_resident_ids']);

					var letter_obj = JSON.parse(json['letter_types']);
					var letter_string = '';

					for(var i=0; i<letter_obj.length; i++){
						var item = letter_obj[i];

						if(item['value'] !== undefined){
							letter_string += item['name'] + ':' + item['value'] + ',';
						}
					}

					if(letter_string != ''){
						letter_string = letter_string.slice(0,-1);
					}

					$('#letter_types').val(letter_string);

					fnCallback(json);
				},
				"json");
			},
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				var id = $(nRow).attr('id');
				var no_tds = 0;

				$(nRow).find('td').each(function(){
					if(no_tds == 0){
						$(this).bind('click', {'id':id, 'aData':aData}, function(e){
							var id = e.data['id'];
							var aData = e.data['aData'];

							var can_email = aData[4];
							var set = false;

							$(this).find('input:hidden').each(function(){
								if($(this).attr('id').indexOf('radio') >= 0){
									if($(this).val() != ''){
										set = true
									}
								}
							});

							if(can_email == '' && set == true){
								// xeditable here
							}
						});
					}

					no_tds++;
				});
			},
			"fnDrawCallback": function( oSettings ) {

				$('body').processing('end', processor);

				$('#do_send_button').show();

				if($('#letter_table_all').data('radio') != "true"){
					$('#letter_table_all').radio({'checked':false});
					$('#letter_table_all').bind('radio_click', function(e,f){

						var set = f['id'];

						select_all('letter_table');

						if($('#letter_table_all_node_radios-1_answer').val() == '1'){
							$('#letter_table_residents').val($('#letter_table_all_residents').val());
						}else{
							$('#letter_table_residents').val('');
						}

					});
				}

				$('#letter_table tbody div.tickbox').each(function(){
					var id = $(this).closest('tr').attr('id');
					var checked = false;

					if($(this).hasClass('ticked_1') || $('#letter_table_all_node_radios-1_answer').val() == '1'){
						checked = true;
					}

					$(this)
						.removeClass('unticked_1 ticked_1')
						.attr('id', 'letter_table_radio_' + id)
						.on('radio_click', {'id':id}, function(e, f){
							var set = f['id'];
							var id = e.data['id'];
							var residents = $('#letter_table_residents').val();

							if(set == '1'){
								residents += id + ',';
							}else{
								residents = residents.replace(id + ',', '');
								$('#letter_table_all').radio('set', '');
							}

							$('#letter_table_residents').val(residents);

							$(this).parent().trigger('click');
						});

					if($('#letter_table_radio_' + id  + "_answer").length == 0){
						$(this).radio({'checked':checked});
					}
				});

				$('#letter_table tbody a.editable').each(function(){

					var id = $(this).closest('tr').attr('id');

					var types = $('#letter_types').val().split(',');
					var default_val = 1;

					for(var i=0; i<types.length; i++){

						var item = types[i].split(':');

						if(item[0] == 'letter_input_' + id){
							default_val = item[1];
						}

					}

					var input = $( document.createElement('input') )
						.attr({
							'id': 'letter_input_' + id,
							'name': 'letter_input_' + id,
							'type': 'hidden'
						})
						.addClass('editable-hidden-input')
						.val(default_val)
						.appendTo($(this).parent());

					$(this)
						.attr('id', 'letter_table_edit_' + id)
						.editable({
							'type': "select",
							'showbuttons': false,
							'source': [
								{value: 1, text: 'Link'},
								{value: 2, text: 'Full Survey'}
							],
							'success': function(response, newValue) {
								$('#letter_input_' + id).val(newValue);
							},
							'value': default_val
						});

					$(this).editable('render');
				});

			},
			"aaSorting": sort_array
		});


	if(myApp.ltable === undefined){
		$("#letter_table tfoot input").each( function (i) {
			asInitVals[i] = this.value;
		} );

		$("#letter_table tfoot input").bind('focus', function () {
			if ( this.className == "search_init" )
			{
				this.className = "";
				this.value = "";
			}
		} );

		$("#letter_table tfoot input").bind('blur', function (i) {
			if ( this.value == "" ){
				this.className = "search_init";
				this.value = asInitVals[$("#letter_table tfoot input").index(this)];
			}
		});

		$("#letter_table tfoot input").bind('keypress', function(e){
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13){
				myApp.ltable.fnFilter( this.value, $("#letter_table tfoot input").index(this) );
			}
		});

		$('#letter_table_wrapper .dataTables_filter input')
			.unbind('keypress keyup')
			.bind('keypress', function(e){
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code == 13) {
					myApp.ltable.fnFilter($(this).val());
				}
			});
	}
}

function do_send(){
	
	$("#confirm_dialog").dialog({
		modal: true,
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
		buttons:{
			"Yes": function(){
			
				$('#a').val('letters');
				
				var processor = $('body').processing('start', 'Sending Letters');

				
				$.post($(location).attr('href'),
				{'a':'letters', 'letter_table_residents' : $('#letter_table_residents').val(), 'survey_id': survey_id , 'rmc_num' : $('#search_filter_rmc1_input').val()},  
				function(data){
					$('body').processing('end', processor);
				
					if(data['success'] == 'Y'){
						create_letters();
					}
					$("#confirm_dialog").dialog("close");
				},
				"json");
			},
			"Cancel": function() {
				$(this).dialog("close");
			}
		}
	});
}

function create_letters(){
	
	var letter_types = '';
	
	$('.editable-hidden-input').each(function(){
		letter_types += $(this).attr('id') + ':' + $(this).val() + ',';
	});
	//$('#ghj').html("/admin/letter_print.php?residents=" + $('#letter_table_residents').val() + "&survey_id=" + survey_id + '&letter_types=' + letter_types);
	$('#gDocs').pdfEmbed('load', "/admin/letter_print.php?residents=" + $('#letter_table_residents').val() + "&rmc_num=" + $('#search_filter_rmc1_input').val() + "&survey_id=" + survey_id + '&letter_types=' + letter_types, true );
	
	$("#letter_dialog").dialog({
		modal: true,
		width : 900,
		height : 515,
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
		buttons:{
			"Close": function() {
				gen_letters_list();
				$(this).dialog("close");
			}
		}
	});
	
	$("#letter_dialog").dialog('open');
	
	
}

// Return a helper with preserved width of cells
var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};

function build_question_list() {

	var processor = $('body').processing('start', 'Building Question List', 33);
	
	var sort_array = new Array();

	sort_array = [[ 0 , 'asc' ]];

	$('#results_table').show();
	
	if(can_delete == 'Y'){
		var aoColumns = [
 			{ bVisible: true },
 			{ bVisible: true },
 			{ bVisible: true },
 			{ bVisible: true },
 			{ bVisible: true }
 		];
	}else{
		var aoColumns = [
			{ bVisible: true },
			{ bVisible: true },
			{ bVisible: true },
			{ bVisible: true },
			{ bVisible: false }
		];
	}
	
	if(can_create == 'Y'){
		$('#question_add_button').show();
		$('#edit_button').show();
	}else{
		$('#question_add_button').hide();
		$('#edit_button').hide();
	}
	
	if(can_send == 'Y'){
		$('#send_survey_button').show();
	}else{
		$('#send_survey_button').hide();
	}
	
	myApp.table = $('#results_table')
		.dataTable({
			"bJQueryUI": true,
			"bProcessing": false,
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bDestroy": true,
			"bDeferRender": true,
	       	"oLanguage": {
		       	"sInfoFiltered": "",
		       	"sSearch":"Search Columns Below: ",
	            "sLengthMenu": "Show _MENU_"
	        }, 
			"aoColumns": aoColumns,
			"sPaginationType": "full_numbers",
	        "bServerSide": true,
	        "sAjaxSource": $(location).attr('href'),
	        "sServerMethod": "POST",
	        "fnServerData": function ( sSource, aoData, fnCallback ) {
	        	$('#a').val('search')

	        	$('body').processing('update', processor, 66);
	        	
	        	aoData.push({name : 'a', value : 'questions'});
	        	aoData.push({name : 'survey_id', value :  survey_id });

	        	$.post(sSource, 
	        	aoData, 
				function(json){
	        		 fnCallback(json);
				}, 
				"json");
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				var id = $(nRow).attr('id');
				var no_tds = 0;
				
				$(nRow).find('td').each(function(){
					if(no_tds != 4){
						$(this).bind('click', {'id':id}, function(e){
							var id = e.data['id'];
							
							do_question(id);
						});
					}

					no_tds++;
				});
	        },
	        "fnDrawCallback": function( oSettings ) {
	        	$('body').processing('end', processor);
	        	$('.button').not('.ui-button').button();
	        },
	        "aaSorting": sort_array
		});

	if(myApp.table === undefined){

		$("#results_table tfoot input").each( function (i) {
	        asInitVals[i] = this.value;
	    } );
	     
	    $("#results_table tfoot input").bind('focus', function () {
	        if ( this.className == "search_init" )
	        {
	            this.className = "";
	            this.value = "";
	        }
	    } );
	     
	    $("#results_table tfoot input").bind('blur', function (i) {
	        if ( this.value == "" ){
	            this.className = "search_init";
	            this.value = asInitVals[$("#results_table tfoot input").index(this)];
	        }
	    });

	    $("#results_table tfoot input").bind('keypress', function(e){
	    	var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13){ 
        		myApp.table.fnFilter( this.value, $("#results_table tfoot input").index(this) );
			}
	    });

	    $('#results_table_wrapper .dataTables_filter input')
		    .unbind('keypress keyup')
		    .bind('keypress', function(e){
		    	var code = (e.keyCode ? e.keyCode : e.which);
			    if (code == 13) {
			    	myApp.table.fnFilter($(this).val());
			    }
		    });
		    
	}
}

function pre_delete_question(s){
	
	$("#msg_dialog").dialog("option", "title", "Confirm...");
	$('#msg_dialog_message').html("Are you sure you want to delete this question?");
	$("#msg_dialog").dialog({
		buttons : [
			{
				text: 'Yes, continue',
				click: function (){
					$(this).dialog('close');	
					delete_question(s);
				}
			},
			{
				text: 'No',
				click: function (){
					$(this).dialog('close');	
				}
			}
		]
	});
	$("#msg_dialog").dialog('open');
}

function delete_question(s){

	var processor = $('body').processing('start', 'Deleting Question');
	
	$.post($(location).attr('href'), 
	$('#question-form').serialize() + "&a=delete_question&s=" + s,
	function(data){
	
		if(data['save_result'] == 'success'){
			build_question_list();
		}

		$('body').processing('end', processor);
		
	},
	'json');
}

function do_question(id){
	
	clear_question();
	form_read('question');

	get_question(id, function(){
		$('#questions-div').hide();
		$('#questions-order-div').hide();
		$('#question-info').show("FadeIn");
	});
}

var get_question = function(id, callback){

	if(id != ""){
		
		if(can_create == 'Y'){
			$('#question_edit_button').show();
		}else{
			$('#question_edit_button').hide();
		}

		var processor = $('body').processing('start', 'Building Question');
		
		$.post($(location).attr('href'), 
		{'survey_id' :  survey_id , 'a': 'question', 'question_id' : id},
		function(data){
			if(data['question_id'] != ''){
				$('#question_id').val(data['id']); 

				$('#question_name_input').val(data['question']);
				$('#question_info_input').val(data['info']);
				get_type(function(){
					$('#question_type_select').selectpicker('val', data['type']);
				});
				$('#question_weight_select').selectpicker('val', data['weight']);
				$('#question_mandatory_select').selectpicker('val', data['question_mandatory']);
				$('#question_mandatory_text_input').val(data['mandatory']);
				$('#question_no-selections_input').selectpicker('val', data['selections']);

				$('#question_name_span').html(data['question']);
				$('#question_info_span').html(data['info']);
				$('#question_type_span').html(data['type_string']);
				$('#question_weight_span').html(data['weight']);
				if(data['question_mandatory'] == 'Y'){
					$('#question_mandatory_span').html('Yes');
				}else if(data['question_mandatory'] == 'N'){
					$('#question_mandatory_span').html('No');
				}
				$('#question_mandatory_text_span').html(data['mandatory']);
				$('#question_no-selections_span').html(data['selections']);

				if(data['type'] == "2"){
					$('#question-tabs').tabs('disable',1);
				}
				
				var page_crumbs = {
			    	0 : 	{ 'url' : "/admin/",		'text'	: "Dashboard" },
			    	1 : 	{ 'url'	: "/admin/surveys.php",	'text'	: "Surveys" },
			    	2 : 	{ 'url'	: "/admin/survey/" + survey_id + "/",	'text'	: survey_name },
			    	3 : 	{ 'url'	: "/admin/survey/" + survey_id + "/",	'text'	: "Question" }
			    };
			    gen_crumbs(page_crumbs); 
				
				$('#question-tabs .ui-tabs-nav').show();
				$('#question_save_button').hide();
				$('#question_cancel_button').hide();
				clear_errors();
				$('#question-form').display_toggle('execute');
				
				if($.isFunction(callback) === true){
					callback();
				}
			}

			$('body').processing('end', processor);
		},
		'json');
	}
};

function clear_question(){
	$('#question_id').val('');
	$('#question_name_input').val('');
	$('#question_info_input').val('');
	get_type();
	$('#question_weight_select').selectpicker('clear');
	$('#question_mandatory_select').selectpicker('clear');
	$('#question_mandatory_text_input').selectpicker('clear');
	$('#question_no-selections_input').selectpicker('clear');

	$('#question_name_span').html('');
	$('#question_info_span').html('');
	$('#question_type_span').html('');
	$('#question_weight_span').html('');
	$('#question_mandatory_span').html('');
	$('#question_mandatory_text_span').html('');
	$('#question_no-selections_span').html('');
	$('#question-form').display_toggle('execute');
}

function build_answer_list(id){

	var processor = $('body').processing('start', 'Building Answer List', 33);

	$('#answer-info').hide();
	$('#answer-div').show();
	$('#answer_edit_button').hide();
	$('#answer_save_button').hide();
	$('#answer_cancel_button').hide();
	$('#answer_back_button').hide();
	
	var sort_array = new Array();

	sort_array = [[ 0 , 'asc' ]];
	
	if(can_delete == 'Y'){
		var aoColumns = [
 			{ bVisible: true },
 			{ bVisible: true },
 			{ bVisible: true }
 		];
	}else{
		var aoColumns = [
			{ bVisible: true },
			{ bVisible: true },
			{ bVisible: false }
		];
	}
	
	if(can_create == 'Y'){
		$('#answer-add-button').show();
	}else{
		$('#answer-add-button').hide();
	}

	$('#answer_table').show();
	
	myApp.atable = $('#answer_table')
		.dataTable({
			"bJQueryUI": true,
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bDestroy": true,
			"bDeferRender": true,
	       	"oLanguage": {
		       	"sInfoFiltered": "",
		       	"sSearch":"Search Columns Below: ",
	            "sLengthMenu": "Show _MENU_"
	         }, 
	         "aoColumns": aoColumns,
			"sPaginationType": "full_numbers",
	        "bServerSide": true,
	        "sAjaxSource": $(location).attr('href'),
	        "sServerMethod": "POST",
	        "fnServerData": function ( sSource, aoData, fnCallback ) {
	        	$('#a').val('answers')

	        	$('body').processing('update', processor, 66);
	        	
	        	aoData.push({name : 'a', value : 'answers'});
	        	aoData.push({name : 'question_id', value : id});
	        	aoData.push({name : 'survey_id', value : survey_id });

	        	$.post(sSource, 
	        	aoData, 
				function(json){
	        		 fnCallback(json);
				}, 
				"json");
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				var id = $(nRow).attr('id');
				var no_tds = 0;
				
				$(nRow).find('td').each(function(){
					if(no_tds != 2){
						$(this).bind('click', {'id':id}, function(e){
							var id = e.data['id'];
							
							do_answer(id);
						});
					}

					no_tds++;
				});
	        },
	        "fnDrawCallback": function( oSettings ) {

	        	$('body').processing('end', processor);
	        	$('.button').not('.ui-button').button();
	        },
	        "aaSorting": sort_array
		});
	
	if(myApp.atable === undefined){

		$("#answer_table tfoot input").each( function (i) {
	        asInitVals[i] = this.value;
	    } );
	     
	    $("#answer_table tfoot input").bind('focus', function () {
	        if ( this.className == "search_init" )
	        {
	            this.className = "";
	            this.value = "";
	        }
	    } );
	     
	    $("#answer_table tfoot input").bind('blur', function (i) {
	        if ( this.value == "" ){
	            this.className = "search_init";
	            this.value = asInitVals[$("#answer_table tfoot input").index(this)];
	        }
	    });

	    $("#answer_table tfoot input").bind('keypress', function(e){
	    	var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13){ 
        		myApp.atable.fnFilter( this.value, $("#answer_table tfoot input").index(this) );
			}
	    });

	    $('#answer_table_wrapper .dataTables_filter input')
		    .unbind('keypress keyup')
		    .bind('keypress', function(e){
		    	var code = (e.keyCode ? e.keyCode : e.which);
			    if (code == 13) {
			    	myApp.atable.fnFilter($(this).val());
			    }
		    });
		    
	}else{
		myApp.atable.fnSort(sort_array);
	}
}

function do_survey_edit(){

	$('#survey_name_input').selectpicker('clear');
	$('#survey_info_input').selectpicker('clear');
	$('#survey_once_select').selectpicker('clear');
	$('#survey_director_select').selectpicker('clear');
	$('#survey_name_span').html('');
	$('#survey_info_span').html('');
	$('#survey_once_span').html('');
	$('#survey_director_span').html('');
	
	$('#survey_button_save').hide();
	$('#survey_button_cancel').show();
	$('#survey_button_edit').show();

	var processor = $('body').processing('start', 'Retrieving Survey Details');
	
	$.post($(location).attr('href'), 
	{"survey_id" :  survey_id , 'a': 'survey'},
	function(data){
		if(data['survey_id'] != ''){

			form_read('survey');

			$('#survey_name_input').val(data['survey_name']);
			$('#survey_info_input').val(data['survey_info']);
			$('#survey_once_select').selectpicker('val', data['survey_once']);
			$('#survey_director_select').selectpicker('val', data['survey_director']);
			$('#survey_amount_input').selectpicker('val', data['survey_amount']);
			$('#survey_frequency_select').selectpicker('val', data['survey_frequency']);
			
			$('#survey_name_span').html(data['survey_name']);
			$('#survey_info_span').html(data['survey_info']);
			
			if(data['survey_once'] == 'Y'){
				$('#survey_once_span').html('Yes');
			}else if(data['survey_once'] == 'N'){
				$('#survey_once_span').html('No');
			}
			
			if(data['survey_director'] == 'Y'){
				$('#survey_director_span').html('Yes');
			}else if(data['survey_director'] == 'N'){
				$('#survey_director_span').html('No');
			}
			
			$('#survey_frequency_span').html(data['survey_frequency_string']);
			$('#survey_amount_span').html(data['survey_amount']);

			$('body').processing('end', processor);
			$('#survey-form').display_toggle('execute');
			
			survey_callback = function(){
				build_question_list();
			};
			$("#survey-dialog").dialog('open');
		}
	},
	'json');
}

function do_answer(id){
	clear_answer();
	form_read('answer');

	get_answer(id, function(){

		$('#answer-div').hide();
		$('#answer-info').show("FadeIn");
	});
}

var get_answer = function(id, callback){

	clear_errors();	

	
	if(can_create == 'Y'){
		$('#answer_edit_button').show();
	}else{
		$('#answer_edit_button').hide();
	}

	var processor = $('body').processing('start', 'Retrieving Answer');
	
	$.post($(location).attr('href'), 
	{'survey_id' :  survey_id, 'a': 'answer',' answer_id' : id},
	function(data){
		if(data['answer_id'] != ''){
			$('#answer_id').val(data['id']);

			$('#answer_name_input').val(data['answer']);
			$('#answer_value_input').val(data['value']);
			
			$('#answer_name_span').html(data['answer']);
			$('#answer_value_span').html(data['value']);
			
			$('#answer_save_button').hide();
			$('#answer_cancel_button').hide();
			$('#answer_back_button').show();
	
			if($.isFunction(callback) === true){
				callback();
			}
		}

		$('body').processing('end', processor);
	},
	'json');
};

function clear_answer(){
	$('#answer_id').val('');
	$('#answer_name_input').val('');
	$('#answer_value_input').val('');
	
	$('#answer_name_span').html('');
	$('#answer_value_span').html('');
}

function do_question_save(){

	clear_errors();	

	var processor = $('body').processing('start', 'Saving Question');
	
	$.post($(location).attr('href'), 
	$('#question-form').serialize() + "&a=save_question&survey_id=" + survey_id ,
	function(data){

		$('body').processing('end', processor);
		
		if(data['save_result'] == 'success'){
			//do_question(data['id']);
			build_question_list();
			
			$('#questions-div').show();
			$('#questions-order-div').hide();
			$('#question-tabs .ui-tabs-nav').show();
			$('#question-info').hide();
			
			$('#survey-send-details').hide();
			$('#question-info').hide();
			$('#answer-info').hide();
			$('#letter_table').hide();
		}
		else{
			show_error(data['fields'], data['message'], data['errored']);
		}
	},
	'json');
}

function do_answer_save(){
	
	clear_errors();	

	var processor = $('body').processing('start', 'Saving Answer');
	
	$.post($(location).attr('href'), 
	$('#answer-form').serialize() + "&a=save_answer&question_id="+$('#question_id').val() +"&survey_id=" + survey_id,
	function(data){

		$('body').processing('end', processor);
	
		if(data['save_result'] == 'success'){
			//do_answer(data['id']);
			$('#answer-div').show();
			$('#answer-info').hide();
			build_answer_list( $("#question_id").val() );
		}
		else{
			show_error(data['fields'], data['message'], data['errored']);
		}
	},
	'json');
}

function pre_delete_answer(s){
	
	$("#msg_dialog").dialog("option", "title", "Confirm...");
	$('#msg_dialog_message').html("Are you sure you want to delete this answer?");
	$("#msg_dialog").dialog({
		buttons : [
			{
				text: 'Yes, continue',
				click: function (){
					$(this).dialog('close');	
					delete_answer(s);
				}
			},
			{
				text: 'No',
				click: function (){
					$(this).dialog('close');	
				}
			}
		]
	});
	$("#msg_dialog").dialog('open');
}

function delete_answer(s){

	var processor = $('body').processing('start', 'Deleting Answer');
	
	$.post($(location).attr('href'), 
	$('#answer-form').serialize() + "&a=delete_answer&s=" + s,
	function(data){
	
		if(data['save_result'] == 'success'){
			build_answer_list( $("#question_id").val() );
		}

		$('body').processing('end', processor);
		
	},
	'json');
}

var get_type = function(callback){

	var $el = $("#question_type_select");
	$el.empty();
	$el.append($("<option></option>").text('Loading'));
	
	$.post($(location).attr('href'),
	{ 'a' : 'type' },
	function(data){
		var $el = $("#question_type_select");
		
		$el.empty();
		
		for(d=0;d<data.length;d++){			
			$el.append($("<option></option>").attr("value", data[d]['value']).text(data[d]['name']));
		}
		
		$el.selectpicker('refresh');
		
		if($.isFunction(callback) === true){
			callback();
		}
	}, 
	"json");
};