<?
//==================
// Mail script
//==================
ini_set('display_errors', '1');
ini_set("max_execution_time", "1440000");
require_once("utils.php");
Global $UTILS_SERVER_PATH;

require_once($UTILS_SERVER_PATH."library/classes/mailer/smtp_validate.class.php");
require_once($UTILS_SERVER_PATH."library/classes/mailer/mailer.class.php");

$thistime = time();

$email_string = '<table cellspacing="0" cellpadding="0" border="0"><tr><td>Brand</td><td>Resident Ref</td><td>Email</td><td>Outcome</td></tr>';

$mysql = new mysql();

$sql = "SELECT r.resident_email,
lr.resident_ref,
s.subsidiary_ecom_code
FROM cpm_residents r
INNER JOIN cpm_lookup_residents lr ON lr.resident_lookup = r.resident_num
INNER JOIN cpm_subsidiary s ON s.subsidiary_id = r.subsidiary_id
WHERE resident_is_active = '1'
AND resident_email <> ''
		LIMIT 1000";
$result = $mysql->query($sql);
$num_rows = $mysql->num_rows($result);
if($num_rows > 0){
	$i = 1;
	while($row = $mysql->fetch_array($result)){
		
		$mail_error = "N";
		$can_send = false;
		$to_email = $row['resident_email'];
		$code = $row['subsidiary_ecom_code'];
		$resident_ref = $row['resident_ref'];
		
		$to_email_array = explode('@', $to_email);
		$hostname = $to_email_array[1];

		// Get MX record
		$dns = dns_get_record($hostname, DNS_MX);
		if(count($dns) > 0){
			
			$dns_found = false;
			
			// Check if MX records target contains the domain name
			foreach($dns as $record){
				if(strpos($record['target'], $record['host']) !== false){
					$dns_found = true;
				}
			}
			
			/*// Validate mailbox exists
			$valid = new SMTP_validateEmail();
			$validated = $valid->validate(array($to_email), $to_email);
			
			// If mailbox is valid
			if($validated[$to_email] == true){
				//echo $to_email . " is valid<br />";
				$can_send = true;
			}else{*/
				if($dns_found == false){
					//echo $to_email . " is deemed valid as it uses a 3rd party<br />";
					$can_send = true;
				}else{
					//echo $to_email . " is invalid<br />";
					$email_string .= '<tr><td>'.$code.'</td><td>'.$resident_ref.'</td><td>'.$to_email.'</td><td>DNS is invalid</td></tr>';
					$can_send = false;
				}
			//}
		}else{
			//echo $to_email . " is not valid as it has no MX records<br />";
			$email_string .= '<tr><td>'.$code.'</td><td>'.$resident_ref.'</td><td>'.$to_email.'</td><td>is not valid as it has no MX records</td></tr>';
		}
		
		print $i . "\r\n";
		$i++;
		
		unset($dns);
		unset($record);
		unset($valid);
		unset($validated);
		unset($dns_found);
		unset($to_email_array);
		unset($can_send);
		unset($hostname);
		unset($mail_error);
		unset($to_email);
		unset($code);
		unset($resident_ref);
		
	}
}
$email_string .= '</table>';

$headers = 'From: ' .$UTILS_EMAIL . "\r\n";
$headers .= 'Reply-To: ' . $UTILS_EMAIL;

$email = new mailer();
$email->set_mail_type("Email Fails");
$email->set_mail_to("marc.evans@rmgltd.co.uk");
$email->set_mail_from($UTILS_EMAIL);
$email->set_mail_subject("Emails that will fail");
$email->set_mail_html($email_string, true);
$email->send();

?>