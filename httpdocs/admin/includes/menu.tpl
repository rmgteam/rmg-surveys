<script>
$(document).ready(function(){

	<?php 
	if(strpos($UTILS_PAGE,'survey') !== false){ ?>
	$('#survey_btn').addClass('active');
	<?php }elseif(strpos($UTILS_PAGE,'user') !== false){ ?>
	$('#users_btn').addClass('active');
	<?php }else{ ?>
	$('#<?=$UTILS_PAGE;?>_btn').addClass('active');
	<?php } ?>
	
});
</script>
<div id="sideBar" class="span3">
	<ul class="nav nav-list bs-docs-sidenav">
		<li id="index_btn">
			<a href="/admin/">
				<i class="icon-chevron-right"></i> 
				Home
			</a>
		</li>
		
		<li id="my_details_btn">
			<a href="/admin/my_details.php">
				<i class="icon-chevron-right"></i> 
				My Details
			</a>
		</li>
		
		<? if( $admin_user->can_access_surveys() === true ){?>
		<li id="survey_btn">
			<a href="/admin/surveys.php">
				<i class="icon-chevron-right"></i> 
				Surveys 
				<?php if($new_surveys > 0){ ?>
				<span style="margin-left:5px;" class="badge badge-small badge-info" title="New Surveys"><?=$new_surveys;?></span>
				<?php } ?>
			</a>
		</li>
		<? }?>
		
		<? if( $admin_user->survey_user_administrate_users == "Y" ){?>
		<li id="users_btn">
			<a href="/admin/users/">
				<i class="icon-chevron-right"></i> 
				Users
			</a>
		</li>
		<? }?>
		
	</ul>
</div>