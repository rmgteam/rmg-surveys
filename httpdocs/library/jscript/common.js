var interval;

$(document).ready(function(){
	build_dialogs();
	    
	$().acknowledgeinput();
	$(':input[placeholder]').placeholder({
		placeholderCSS: { 
		      'color':'#a5a2a5', 
		      'position': 'absolute', 
		      'left':'5px',
		      'top':'-13px',
		      'margin': '0',
		      'text-align': 'left'
		    }
	});

	$('#header-login-button').click(function(){
		$('#login-dialog').dialog('open');
	});

	$('#header-logout-button').click(function(){
		do_logout();
	});

	//$('#header-logout-button').hide();
	
	onDomChange(function(){ 
		load_classes();
	});
	
	$('<div id="tooltip" class="flot_tip"></div>').appendTo("body");
	
	$('body').processing();
	$('body').build_tooltip();
	
	$('body').find('[data-toggle=tooltip]').each(function(){
		$('body').build_tooltip('add_tooltip', $(this));
	});
});

(function (window) {
    var last = +new Date();
    var delay = 100; // default delay
    
    // Manage event queue
    var stack = [];
    var support = {};
    var el = document.documentElement;
    var remain = 3;

    function callback() {
        var now = +new Date();
        if (now - last > delay) {
            for (var i = 0; i < stack.length; i++) {
                stack[i]();
            }
            last = now;
        }
    }

    // Public interface
    var onDomChange = function (fn, newdelay) {
        if (newdelay) delay = newdelay;
        stack.push(fn);
    };
    // callback for the tests
    function decide() {
        if (support.DOMNodeInserted) {
            window.addEventListener("DOMContentLoaded", function () {
                if (support.DOMSubtreeModified) { // for FF 3+, Chrome
                    el.addEventListener('DOMSubtreeModified', callback, false);
                } else { // for FF 2, Safari, Opera 9.6+
                    el.addEventListener('DOMNodeInserted', callback, false);
                    el.addEventListener('DOMNodeRemoved', callback, false);
                }
            }, false);
        } else if (document.onpropertychange) { // for IE 5.5+
            document.onpropertychange = callback;
        }
    }

    // checks a particular event
    function event_test(event) {
        el.addEventListener(event, function fn() {
            support[event] = true;
            el.removeEventListener(event, fn, false);
            if (--remain === 0) decide();
        }, false);
    }

    // attach test events
    if (window.addEventListener) {
    	event_test('DOMSubtreeModified');
    	event_test('DOMNodeInserted');
    	event_test('DOMNodeRemoved');
    } else {
        decide();
    }

    // expose
    window.onDomChange = onDomChange;
})(window);

function load_classes(){
	
	$('.button').not('.ui-button').button();
	
	$('select').not('[data-selectpicker=true]').selectpicker();

	$("input.date").not('.hasDatepicker').datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true });
	
}

var pushFormData = function(sFormSelector, aoData){
    var filter = $(sFormSelector).serializeArray();
    for(var index in filter) {
        aoData.push({name : filter[index]['name'], value : filter[index]['value']});
    }
};

var stringifyData = function(aoData){
	
	var the_string = '';
	var i = 0;
	
	$.each(aoData,function(index, value){
		if(i == 0){
			the_string += '?';
		}else{
			the_string += '&';
		}
		
		the_string += value['name'] + '=' + value['value'];
		i++;
    });
	
	return the_string;
};

var get_options = function(a, select, callback, query, url){
	
	var loading = $( document.createElement('div') )
		.attr('id', select + '_loading')
		.addClass('loading_msg_1');
	
	$("#" + select).parent().append(loading);
	$("#" + select + '_input').hide();
	$("#" + select + '_loading').show();

	
	if(query === undefined){
		query = '';
	}
	
	if(url === undefined){
		url = $(location).attr('href');
	}
	
	$.post(url,
	'a=' + a + query,
	function(data){
		
		var $el;
		
		if(typeof select === 'string'){
			$el = $("#" + select);
		}else{
			$el = select;
		}
		
		$el.empty();
		
		for(d=0;d<data.length;d++){			
			$el.append($("<option></option>").attr("value", data[d]['value']).text(data[d]['name']));
		}
		
		$("#" + select + '_loading').remove();
		$("#" + select + '_input').show();
		$('#' + select).combobox('update');

		if($.isFunction(callback) === true){
			callback();
		}
	}, 
	"json");
};

function show_error(fields, message, errored){
	
	clear_errors();
	var error_message = "";
	
	var error_fields = fields.split("|");
	var error_messages = message.split("|");
	var error_has = errored.split("|");
	
	try{
		for(r=0;r<error_fields.length;r++){
			if(error_has[r] != ""){
				if (error_has[r] == "true"){
					
					if(error_fields.length > 0){
						$('#'+error_fields[r]).css("color", 'red');
					}
					
					error_message += "<li>" + error_messages[r] + "</li>";
				}
			}
		}
	}catch(err){
		alert(err.description);
	}
	
	if(error_message != ''){
		error_message = "<ul>" + error_message + "</ul>";
	}
	
	$('#msg_dialog_message').html(error_message);
	
	if (error_message != ""){
		$("#msg_dialog").dialog('option', 'title', 'Error!');
		$("#msg_dialog").dialog('open');	
	}
}

function clear_errors(){
	$('.control-label').css("color", '#333333');
}

function build_dialogs(){

	
	$("#login-dialog").dialog({  
		modal: true,
		autoOpen: false,
		resizable: false,
		width: '800px',
	   	closeOnEscape: false,
	   	open: function(event, ui) {
	   		var dialog = $(event.target).parents(".ui-dialog.ui-widget");

	   		var i = 0;
	   		dialog.find(".ui-dialog-buttonpane").find("button").each(function(){
	   			i++;
	   			if(i==1){
		   			$(this).addClass("ui-button-primary");
	   			}
	   		});
	   		dialog.find(".ui-dialog-buttonpane").find('button').removeClass('ui-state-focus ui-state-hover ui-state-active');
		},
	   	buttons: {
	   		Login: function() {
		   		do_login();
				$(this).dialog('close');
			}
		}
	});
	
	$("#msg_dialog").dialog({  
		modal: true,
		autoOpen: false,
		resizable: false,
		width: '600px',
	   	closeOnEscape: false,
	   	open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
	   	close: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
	   	buttons: { 
	   		Ok: function() {
				$(this).dialog('close');
			}
		}
	});
	
	/* Browser Dialog */
	
	var a = $( document.createElement('div') );
	a.attr({
		'id': "browser_dialog",
		'title': "Browser Aware"
	});
	a.addClass("dialog_1");
	
	var p = $( document.createElement('p'))
		.html('We have detected that you are using old browser. To get the best out of RMG Surveys we recommend that you upgrade.')
		.addClass('browser_text');
	
	a.append(p);
	
	var image1 = $( document.createElement('a'))
		.attr({
			'href'			:	'http://www.google.com/chromeframe?prefersystemlevel=true',
			'target'		:	'_blank',
			'data-toggle'	:	'tooltip',
			'data-tooltip'	:	'This is a plugin for Internet Explorer that keeps the look and feel whilst adding enhanced security, a faster JavaScript Engine and a better rendering engine.',
			'data-title'	:	'Chrome Frame'
		})
		.addClass('browser-icon chrome_frame');
	
	a.append(image1);
	
	var image1 = $( document.createElement('a'))
		.attr({
			'href'			:	'https://www.google.com/intl/en/chrome/browser/',
			'target'		:	'_blank',
			'data-toggle'	:	'tooltip',
			'data-tooltip'	:	'A fast new browser from Google',
			'data-title'	:	'Chrome'
		})
		.addClass('browser-icon chrome');
	
	a.append(image1);
	
	var image1 = $( document.createElement('a'))
		.attr({
			'href'			:	'http://www.firefox.com/',
			'target'		:	'_blank',
			'data-toggle'	:	'tooltip',
			'data-tooltip'	:	"Your online security is Firefox's top priority. Firefox is free, and made to help you get the most out of the web.",
			'data-title'	:	'Firefox'
		})
		.addClass('browser-icon firefox');
	
	a.append(image1);
	
	var image1 = $( document.createElement('a'))
		.attr({
			'href'			:	'http://www.opera.com/',
			'target'		:	'_blank',
			'data-toggle'	:	'tooltip',
			'data-tooltip'	:	'The fastest browser on Earth�secure, powerful and easy to use, with excellent privacy protection. And it is free.',
			'data-title'	:	'Opera'
		})
		.addClass('browser-icon opera');
	
	a.append(image1);
	
	var image1 = $( document.createElement('a'))
		.attr({
			'href'			:	'http://www.apple.com/safari/',
			'target'		:	'_blank',
			'data-toggle'	:	'tooltip',
			'data-tooltip'	:	'Safari for Mac from Apple, the world�s most innovative browser.',
			'data-title'	:	'Safari'
		})
		.addClass('browser-icon safari');
	
	a.append(image1);
	
	$('.wrapper').append(a);
	
	$("#browser_dialog").dialog({  
		modal: true,
		autoOpen: false,
		resizable: false,
	   	closeOnEscape: false,
	   	width: 800,
	   	height: 500,
	   	open: function(event, ui) { $(".ui-dialog-titlebar-close").show(); },
	   	buttons: {
			Close: function() {
				$(this).dialog('close');
			}
		}
	});
}

function kill_dialogs(){
	$('#error_dialog').remove();
	$('#processing_dialog').remove();
	$('#msg_dialog').remove();
	$('#browser_dialog').remove();
}

function rebuild_dialogs(){
	kill_dialogs();
	build_dialogs();
}

function select_all(id){
	var has = 0;
	
	$('#' + id + ' tbody div.tickbox').each(function(){
		var id = $(this).attr('id') + '_node_radios-1_answer';
		if($('#' + id).val() == '1'){
			has++;
			return false;
		}
	});
	
	$('#' + id + ' tbody div.tickbox').each(function(){
		if(has > 0){
			$(this).radio('set', '');
		}else{
			$(this).radio('set', '1');
		}
	});

	if(has > 0){
		$('#' + id + '_all').radio('set', '');
	}else{
		$('#' + id + '_all').radio('set', '1');
	}
}


function do_logout(){
	
	location.href = "/admin/login.php?a=logout";
}

function do_login(){
	$.post($(location).attr('href'), 
	$('#login-form').serialize(),
	function(data){
		if(data['result'] == 'success'){
			
		}else{

		}
	}, 
	"json");
}

function form_read(name){ 
	$('#' + name + '-form').find('input,select,textarea').each(function(){
		$('#' + $(this).attr('id').replace('_input', '_edit').replace('_select', '_edit')).hide();
		$('#' + $(this).attr('id').replace('_input', '_span').replace('_select', '_span')).show();
		$(this).val( $('#' + $(this).attr('id').replace('_input', '_span').replace('_select', '_span')).html() );
	});
	$('#' + name + '_buttons_top').hide();
	$('#' + name + '_buttons_bottom').hide();
	$('#' + name + '_buttons_view').show();
}

function form_edit(name){
	$('#' + name + '-form').find('input,select,textarea').each(function(){
		$('#' + $(this).attr('id').replace('_input', '_edit').replace('_select', '_edit')).show();
		$('#' + $(this).attr('id').replace('_input', '_span').replace('_select', '_span')).hide();
	});
	$('#' + name + '_buttons_top').show();
	$('#' + name + '_buttons_bottom').show();
	$('#' + name + '_buttons_view').hide();
}

function gen_crumbs(obj){
	
	var crumbs = "";
	$("#breadcrumbs_container").html("");
	
	if( typeof obj != "undefined" ){
	
		crumbs = '<ul id="breadcrumbs" class="breadcrumb pull-left" style="padding-bottom:0;">';
		var c = 0;
		
		for (var crumb in obj) {
			crumbs += '<li><a href="' + obj[crumb]['url'] + '">' + obj[crumb]['text'] + '</a></li>';
			
			var length = 0;
			for(var prop in obj){
			    if(obj.hasOwnProperty(prop)){
			        length++;
			    }
			}
			
			if(c < (length - 1) ){
				crumbs += ' <span class="divider">&gt;</span> ';
			}
			c++;
		}
		crumbs += '</ul>';
		$("#breadcrumbs_container").html(crumbs);
		$("#breadcrumbs_container").show();
	}
	else{
		$("#breadcrumbs_container").hide();
	}
}

//Get Bar chart data betwee two points
function getData(x1, x2, flot) {
	
	var d_series = flot.getData()[0];
	var d = [];
	
	for (var i = x1; i <= x2; ++i) {
		d.push(d_series.data[i]);
	}
 return [
     { label: d_series.label, color: d_series.color, data: d }
 ];
}

//Create graph
function create_graph(type, series, i, width, height, float, prefix, format){
	
	if(prefix === undefined){
		prefix = '';
	}
	
	if(format === undefined){
		format = '';
	}
	
	if(float === undefined){
		float = 'none';
	}
	
	// Surround for pie chart
	var outer_box = $( document.createElement('div') )
		.addClass('flot_box')
		.css({
			'width': width,
			'float': float
		});
	
	var the_label = series.label;
	
	if(series.length > 1){
		the_label = series[0].label;
	}
	
	var heading = $( document.createElement('div') )
		.addClass('full')
		.html(the_label)
	
	var heading_box = $( document.createElement('div') )
		.addClass('record_heading');
	
	heading.appendTo(heading_box);
		
	heading_box.appendTo(outer_box);
	
	var surround_box = $( document.createElement('div') )
		.attr({
			'id' :		prefix + 'surround-' + i
		})
		.css({
			'width': '100%'
		})
		.addClass('form-container');
	
	var question_box = $( document.createElement('div') )
		.attr({
			'id' :		prefix + 'graph-' + i
		})
		.css({
			'height': height,
			'width': '100%'
		});
	
	if(type == 'bar'){
		question_box
			// Hover for more information
			.on("plothover", function (event, pos, item) {
				if (item) {
					if(format == ''){
						var x = item.series.data[item.dataIndex][0];
					}else if(format == 'time'){
						var d = new Date(item.series.data[item.dataIndex][0]);
						var x = d.getUTCDate() + "/" + (d.getUTCMonth() + 1) + "/" + d.getUTCFullYear();
					}

					var y = item.datapoint[1];
					
					$("#tooltip").html(x + " has " + y + "%")
						.css({top: pos.pageY+15, left: pos.pageX+15})
						.fadeIn(200);
				} else {
					$("#tooltip").hide();
				}
			})
			// Overview trigger
			.on("plotselected", function (event, ranges) {
		        // do the zooming
		        g_plot = $.plot('#' + $(this).attr('id'), getData(parseInt(ranges.xaxis.from), Math.round(ranges.xaxis.to), g_overview), options );
		        
		        // don't fire event on the overview to prevent eternal loop
		        g_overview.setSelection(ranges, true);
		    })
			.appendTo(surround_box);
		
	}else if(type == 'pie'){
		question_box
			// Hover for more information
			.on("plothover", function (event, pos, item) {
				if (item) {
					var x = item.series.label,
						y = item.series.percent.toFixed(2);

					$("#tooltip").html(x + " has " + y + "%")
						.css({top: pos.pageY+15, left: pos.pageX+15})
						.fadeIn(200);
				} else {
					$("#tooltip").hide();
				}
			})
			.appendTo(surround_box);
	}
	
	surround_box.appendTo(outer_box);
	
	return outer_box;
	
}