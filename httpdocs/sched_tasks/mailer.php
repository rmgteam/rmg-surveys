<?
//==================
// Mail script
//==================
ini_set('display_errors', '1');
ini_set("max_execution_time", "180");
require_once("utils.php");
Global $UTILS_SERVER_PATH;

include("Mail.php");
include("Mail/mime.php");
require_once($UTILS_SERVER_PATH."library/classes/mime/mime.class.php");
require_once($UTILS_SERVER_PATH."library/classes/mailer/smtp_validate.class.php");

$thistime = time();
$smime = new mime();
$mysql = new mysql();
$sql = "SELECT * FROM survey_mailer WHERE mail_sent = 'N' LIMIT 500";
$result = $mysql->query($sql);
$num_rows = $mysql->num_rows($result);
if($num_rows > 0){
	while($row = $mysql->fetch_array($result)){
		
		$mail_error = "N";
		$can_send = false;
		$to_email = $row['mail_to'];
		
		$to_email_array = explode('@', $to_email);
		$hostname = $to_email_array[1];

		// Get MX record
		$dns = dns_get_record($hostname, DNS_MX);
		if(count($dns) > 0){
			
			$dns_found = false;
			
			// Check if MX records target contains the domain name
			foreach($dns as $record){
				if(strpos($record['target'], $record['host']) !== false){
					$dns_found = true;
				}
			}
			
			/*// Validate mailbox exists
			$valid = new SMTP_validateEmail();
			$validated = $valid->validate(array($to_email), $to_email);
			
			// If mailbox is valid
			if($validated[$to_email] == true){
				echo $to_email . " is valid\n";
				$can_send = true;
			}else{*/
				if($dns_found == false){
					echo $to_email . " uses 3rd party\n";
					$can_send = true;
				}else{
					echo $to_email . " is invalid<br />";
					$can_send = false;
				}
			//}
		}else{
			echo $to_email . " has no MX records\n";
		}
		
		if($can_send == true){
		
			$hdrs = array(
				'From' => $row['mail_from'],
				'Subject' => $row['mail_subject']
			);
			
			$headers = explode(PHP_EOL, $row['mail_headers']);
			
			$to = str_replace(' ', '', $row['mail_to']);
			
			foreach ($headers as &$value){
				$header_item = explode(":", $value);
				
				if($header_item[0] != ''){
				
					$hdrs[trim($header_item[0])] = trim($header_item[1]);
					
					if(strtoupper(trim($header_item[0])) == "BCC" OR strtoupper(trim($header_item[0])) == "CC"){
						$to .= ", " . trim($header_item[1]);
					}
				}
			}
			
			$hdrs['To'] = $row['mail_to'];
			$recipients = $to;
			$crlf = "\n";
			
			$mime = new Mail_mime(array('eol' => $crlf));
			
			$images = explode(";", $row['mail_images']);
			
			foreach ($images as &$value){
				
				if($value != ''){
					$image = $value;
					
					if(strpos($value, '|') !== false){
						$vars = explode("|", $value);
						$image = $vars[0];
						$cid = $vars[1];
					}else{
						$cid = Null;				}
					
					print("Embed - " . $image . ' as ' . $cid . "\n");
				
					if($image != ""){
						$added_image = $mime->addHTMLImage($image, $smime->get_mime($value), '', true, $cid);
						print($added_image . "\n");	
					}
				}
				
			}
			
			$mime->setTXTBody($row['mail_message']);
			$mime->setHTMLBody($row['mail_html']);
			
			//max attachment filename length is 69 including extension
			
			if($row['mail_attachment'] != ''){
				if(strpos($row['mail_attachment'], ';') !== false){
					$attachments = explode(";",$row['mail_attachment']);
					foreach($attachments as $value){
						if($value != ''){
							$mime->addAttachment($value, $smime->get_mime($value));
						}
					}
				}else{
					$mime->addAttachment($row['mail_attachment'], $smime->get_mime($row['mail_attachment']));
				}
			}
			
			$body = $mime->get();
			$hdrs = $mime->headers($hdrs);
			
			/* SMTP server name, port, user/passwd */
			$smtpinfo["host"] = "94.250.233.132";	// hodd1clex1
			$smtpinfo["port"] = "25";
			$smtpinfo["auth"] = false;
			/* Create the mail object using the Mail::factory method */
			$mail_object =& Mail::factory("smtp", $smtpinfo);
			/* Ok send mail */
			
			if(!$mail_object->send($recipients, $hdrs, $body)){
				$mail_error = "Y";
			}
			
			if($mail_error == "N"){			
				$sql_up = "
				UPDATE survey_mailer SET
				mail_sent_ts = '".$thistime."',
				mail_sent = 'Y'
				WHERE mail_id = ".$row['mail_id'];
				$mysql->query($sql_up);
				
				if($row['mail_del_attach'] == 'Y'){
					if($row['mail_attachment'] != ''){
						if(strpos($row['mail_attachment'], ';') !== false){
							$attachments = explode(";",$row['mail_attachment']);
							foreach($attachments as $value){
								if($value != ''){
									unlink($value);
								}
							}
						}else{
							unlink($row['mail_attachment']);
						}
					}
					
				}
			}
			else{
				mail("webteam@rmgltd.co.uk","RMG Surveys MAILER ERROR","Mail ID ".$row['mail_id']." failed to send.");
			}
		}else{
			// Cannot send email
			$sql_up = "
			UPDATE survey_mailer SET
			mail_address_failure = 'Y'
			WHERE mail_id = ".$row['mail_id'];
			$mysql->query($sql_up);
		}
	}
}
?>