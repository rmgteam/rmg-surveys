(function( $, undefined ) {

	var emptyFunction = function(){};
	
	$.widget( "ui.search_filter", {
		
		options: {
			objID: '',
			a: 'search',
			page: $(location).attr('href'),
			beforeEnd: emptyFunction,
			callback: emptyFunction,
			included: [],
			button_class: 'jButton',
			values: {},
			extra_buttons: {},
			no_fields: 0,
			override: false,
			type_name: 'search_filter_type',
			bind_object: 'button',
			value_delimiter:',',
			search_empty: false,
			condition_met: 'A new option is available in subsequet criterion.',
			filter_width: 0,
			operators: {
			    "==": function(a,b){return a==b;},
			    "=": function(a,b){return a==b;},
			    "<=": function(a,b){return a<=b;},
			    ">=": function(a,b){return a>=b;},
			    "<": function(a,b){return a<b;},
			    ">": function(a,b){return a>b;},
			    "!=": function(a,b){return a!=b;},
			    "::": function(a,b){
			    	if(a.indexOf(b) != -1){
			    		return true;
			    	}else{
			    		return false;
			    	}
			    }
			}
		},
	
		_create: function() {
			
			var self = this;
			var options = this.options;
	
			options.objID = this.element;
			options.objID.addClass("search_filter");
			
			var row = $( document.createElement('div') )
				.addClass('search_filter_row')
				.attr('id', options.objID.attr('id') + '_fields');
			
			options.objID.append(row);
			
			var row = $( document.createElement('div') )
				.addClass('search_filter_row')
				.attr('id', options.objID.attr('id') + '_buttons');
			
			var button = $( document.createElement('a') )
				.attr('id', options.objID.attr('id') + '_add_button')
				.addClass(options.button_class)
				.html('Add Criterion')
				.button()
				.css('marginRight', '8px')
				.bind('click', {'self': self}, function(e){
					var self = e.data['self'];
					self.add_field();
				});
			
			row.append(button);
			
			var button = $( document.createElement('a') )
				.attr('id', options.objID.attr('id') + '_clear_button')
				.addClass(options.button_class)
				.html('Clear Criteria')
				.button()
				.css('marginRight', '8px')
				.bind('click', {'self': self}, function(e){
					var self = e.data['self'];
					self.clear_field();
				});
			
			row.append(button);
			
			if($.isArray(options.extra_buttons)){
				if(options.extra_buttons.length > 0){
					/*Create a row for each extra field with a label td and element*/
					for(j=0;j<options.extra_buttons.length;j++){
						var extra = options.extra_buttons[j];
						
						var button = $( document.createElement('a') )
							.addClass(options.button_class)
							.attr('id', extra['id'])
							.css('margin-right', '8px')
							.html(extra['name'])
							.click({extra: extra}, function(event){var data = event.data; data.extra['event']();});
						row.append(button);
					}
				}
			}	
			
			var button = $( document.createElement('a') )
				.attr('id', options.objID.attr('id') + '_search_button')
				.addClass(options.button_class)
				.html('Go')
				.button()
				.bind('click', {'self': self}, function(e){
					var self = e.data['self'];
					self.submit();
				});
			
			row.append(button);
			
			options.objID.append(row);
			
			self._initSource(function(){				
				self.add_field();
			});
		},
		
		_initSource: function(callback) {
			var self = this,
				array,
				url;
			
			if ( typeof self.options.values === "string" ) {
				url = this.options.values;
				
				$.post(url, 
				{},
				function(data){
					var arr = $.makeArray(data);
					self.options.values = arr;
					
					if($.isFunction(callback) === true){
						callback();
					}
				}, 
				"json");
			} else {
				this.elements = this.options.values;
				
				if($.isFunction(callback) === true){
					callback();
				}
			}
		},
		
		data_post: function(callback, url){
			var self = this;
			var options = this.options;
			
			$.post(url,
			function(data){
				
				if($.isFunction(callback) === true){
					callback(data);
				}
				
				self._trigger('data_post');
			}, 
			"json");
			
		},
		
		setOption: function(key, value){

			$.Widget.prototype._setOption.apply( this, arguments );
		},
		
		set_field: function(field_no, value, input_value){
			var self = this;
			var options = this.options;
			
			$('#' + options.type_name + field_no).selectpicker('val', value);
			
			var l = 0;
			
			var this_element = {};
			
			for(l=0;l<options.values.length;l++){
				var field = options.values[l];
				if(field['value'] == value){
					this_element = field;
				}
			}
			
			if(this_element['type'] !== undefined){
				type =  this_element['type'];
				
				if(type == 'input' || type == 'multiselect'){
					$('#' +  this_element['id'] + field_no).val(input_value);
				}else if(type == 'select'){
					$('#' +  this_element['id'] + field_no).selectpicker('val', input_value);
				}else{
					$('#' +  this_element['id'] + field_no)[type]('set', input_value);
				}
			}
			
			self.show_field(field_no);
			
			this._trigger('set_field');
		},
		
		disable_field: function(field_no, value){
			var self = this;
			var options = this.options;
			
			var l = 0;
			
			var this_element = {};
			
			for(l=0;l<options.values.length;l++){
				var field = options.values[l];
				if(field['value'] == value){
					this_element = field;
				}
			}
			
			if(this_element['type'] !== undefined){
				type =  this_element['type'];
				
				if(type == 'input' || type == 'multiselect'){
					$('#' +  this_element['id'] + field_no).attr('disabled', 'disabled');
				}else if(type == 'select'){
					$('#' +  this_element['id'] + field_no).selectpicker('disable');
				}else{
					$('#' +  this_element['id'] + field_no)[type]('disable');
				}
			}
			
			this._trigger('disable_field');
		},
		
		enable_field: function(field_no, value){
			var self = this;
			var options = this.options;
			
			var l = 0;
			
			var this_element = {};
			
			for(l=0;l<options.values.length;l++){
				var field = options.values[l];
				if(field['value'] == value){
					this_element = field;
				}
			}
			
			if(this_element['type'] !== undefined){
				type =  this_element['type'];
				
				if(type == 'input' || type == 'multiselect'){
					$('#' +  this_element['id'] + field_no).removeAttr('disabled');
				}else if(type == 'select'){
					$('#' +  this_element['id'] + field_no).selectpicker('enable');
				}else{
					$('#' +  this_element['id'] + field_no)[type]('enable');
				}
			}
			
			this._trigger('disable_field');
		},
		
		set_focus: function(field_no, value){
			var self = this;
			var options = this.options;
			
			$('#' + options.type_name + field_no).selectpicker('val', value);
			
			var this_element = options.values[field_no-1];	
			
			if(this_element['type'] !== undefined){
				type =  this_element['type'];
				
				if(type == 'input' || type == 'multiselect' || type == 'select'){
					$('#' +  this_element['id'] + field_no).focus();
				}
			}
			
			this._trigger('set_field');
		},

		add_field: function(selection){
			var self = this;
			var options = this.options;
			
			if(selection === undefined){
				selection = 0;
			}

			options.no_fields ++;
			
			var row = $( document.createElement('div') )
				.addClass('search_filter_row')
				.attr('id', options.objID.attr('id') + '_row_' + options.no_fields);
			
			var column = $( document.createElement('div') )
				.addClass('search_filter_col')
				.attr('id', options.objID.attr('id') + '_label_' + options.no_fields);
			
			var label = $( document.createElement('label') )
				.html('Type:');
			
			column.append(label);
			
			row.append(column);
			
			var column = $( document.createElement('div') )
				.addClass('search_filter_col')
				.attr('id', options.objID.attr('id') + '_select_container_' + options.no_fields);
			
			var select = $( document.createElement('select') )
				.attr({
					'name': options.type_name + options.no_fields,
					'id': options.type_name + options.no_fields
				})
				.bind('change', {'self': self, 'no_fields': options.no_fields}, function(e){
					var self = e.data['self'];
					var no_fields = e.data['no_fields'];
					
					self.show_field(no_fields);
				});
			
			var label = $( document.createElement('span') )
				.addClass('search_filter_one_label')
				.attr({
					'id': options.type_name + options.no_fields + '_label'
				});
			
			var l = 0;
			
			for(l=0;l<options.values.length;l++){
				if(options.values[l] !== undefined){
					var this_element = options.values[l];
					
					var condition_met = false;
					condition_met = self.check_condition(l);
					
					if(condition_met == true){
						var option = $( document.createElement('option') )
							.attr('value', this_element['value'])
							.html(this_element['html']);
						
						if(selection == l){
							option.attr('selected','selected'); 
						}
						
						select.append(option);
						
						label.html(this_element['html']);
					}
				}
			}
			
			column.append(select);
			column.append(label);
			
			row.append(column);
			
			var column = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr('id', options.objID.attr('id') + '_field_' + options.no_fields);
			
			if(options.values.length == 1){
				$('#' + options.objID.attr('id') + '_add_button').hide();
				$('#' + options.objID.attr('id') + '_clear_button').hide();	
			}
			
			for(l=0;l<options.values.length;l++){
				if(options.values[l] !== undefined){
					var this_element = options.values[l];					
					if(this_element['type'] !== undefined){
						var type = this_element['type'];
						
						if(type == 'input'){
							var page_element = $( document.createElement('input') )
								.attr({
									'id' : this_element['id'] + options.no_fields,
									'name' : this_element['id'] + options.no_fields,
									'type' : 'text'
								})
								.addClass(this_element['class']);
							
							if(this_element['class'] !== undefined){
								page_element.addClass(this_element['class']);
							}
							
							page_element.addClass('tinput');
							
							page_element.bind('keydown', {'self': self}, function(e){
								var self = e.data['self'];
								var code = (e.keyCode ? e.keyCode : e.which);
								if ( code == 9 || code == 27 || code == 18 || code == 91 || 
						             // Allow: Ctrl+A
						            (code == 65 && e.ctrlKey === true) || 
						             // Allow: home, end, left, right
						            (code >= 35 && code <= 40)) {
						                 // let it happen, don't do anything
						                 return;
						        }else{
						        	
						        	if(code == 13){
							        	setTimeout(function(){
							        		self.submit();
										}, 0);
							        	return;
						        	}
								}
							});
							
							column.append(page_element);
							
						}else if(type== 'multiselect'){
							
							self.add_multiselect(this_element, column, l);
							
						}else if(type== 'select'){
							
							self.add_select(this_element, column);
							
						}else{
							
							var page_element = $( document.createElement('div') )
								.attr({
									'id' : this_element['id'] + options.no_fields
								})
								.css({
									/*'width' : '100%'*/
									'display'	:	'inline-block'
								});
							
							column.append(page_element);
						}
					}
				}
			}
			
			row.append(column);
			$('#' + options.objID.attr('id') + '_fields').append(row);
			$('#' + options.type_name + options.no_fields).selectpicker();
			
			var width = $('#' + options.type_name + options.no_fields + '-menu').actual('width') + 20;
			
			$('#' + options.type_name + options.no_fields + '-menu').css('width', width + 'px');
			
			$('#' + options.type_name + options.no_fields + '-button').css('width', width + 'px');

			$('.tinput').keydown(function(e){ 
				if(e.keyCode == 13){
					return false;
				}
			});
			
			if(options.no_fields == options.values.length){
				$('#' + options.objID.attr('id') + '_add_button').hide();
			}
			
			this._trigger('add_field');
			
			if(options.values.length == 1){
				select.parent().find('.bootstrap-select').hide();			
			}else{
				label.hide();
			}
			
			self.build_selectors(function(){
				self.show_field(options.no_fields);
			});
		},

		clear_field: function(){
			var self = this;
			var options = this.options;

			options.no_fields = 0;
			var l = 0;
			
			for(l=0;l<options.values.length;l++){
				if(options.values[l] !== undefined){
					var this_element = options.values[l];					
					if(this_element['type'] !== undefined){
						var type = this_element['type'];
						
						if(type == 'input' || type == 'multiselect' || type == 'select'){
							
						}else{
							var i = 0;
							for(i=0;i<=options.no_fields;i++){
								try{
									$('#' + this_element['id'] + i)[type]('destroy');
								}catch(err){
								  //Handle errors here
								}
							}
						}
					}
				}
			}
			
			$('#' + options.objID.attr('id') + '_fields').html("");
			$('#' + options.objID.attr('id') + '_add_button').show();

			this._trigger('clear_field');
			this.add_field();
		},
		
		show_field: function(no_field){
			var self = this;
			var options = this.options;
			
			for(var l=0;l<options.values.length;l++){
				if(options.values[l] !== undefined){
					var this_element = options.values[l];
					
					type =  this_element['type'];
					
					if(type == 'select'){
						$('#' + this_element['id'] + no_field).not('[data-selectpicker=true]').selectpicker();
						$('#' + this_element['id'] + no_field).parent().find('.bootstrap-select').hide();
					}
					
					$('#' + this_element['id'] + no_field).hide();
				}
			}
			
			for(var l=0;l<options.values.length;l++){
				if(options.values[l] !== undefined){
					var this_element = options.values[l];
					
					type =  this_element['type'];
					
					if($('#' + options.type_name + no_field).val() == this_element['value']){
						if(type == 'select'){
							$('#' + this_element['id'] + no_field + "_div").show();
						}else{
							$('#' + this_element['id'] + no_field).show();
						}

						if($('#' + options.objID.attr('id') + '_fields').is(':visible')){
							options.filter_width = $('#' + options.objID.attr('id') + '_fields').actual('innerWidth');
						}
						
						var the_width = options.filter_width;
						
						$('#' + options.objID.attr('id') + '_row_' + no_field).find('div.search_filter_col').each(function(){
							var temp_width = $(this).actual( 'outerWidth', { includeMargin : true });
							the_width -= temp_width;
						});
						
						$('#' + options.objID.attr('id') + '_row_' + no_field).find('.search_filter_fields').width(the_width - 25);
						
						var condition_met = self.check_condition(no_field);
					}else{
						if(this_element['type'] !== undefined){
							var type = this_element['type'];
							
							if(type == 'advanced_select'){
								try{
									$('#' + this_element['id'] + no_field)[type]('clear');
								}catch(err){
								  //Handle errors here
								}
							}
						}
					}
				}
			}
			
			this._trigger('show_field');
		},
		
		build_options: function(array, object, callback){
			
			var self = this;
			var options = this.options;
			var i = 0;
			
			if($.isArray(array)){
				for(i=0;i<array.length;i++){
					var item = array[i];
					var e = $( document.createElement('option') );
					e.attr('value', item['value']);
					e.html(item['name']);
					object.append(e);
				}
			}
			
			if($.isFunction(callback) === true){
				callback();
			}
			
			this._trigger('build_options');
		},
		
		/*Add a multiselect to supplied object*/
		add_multiselect: function(item, object, item_no){
			
			var self = this;
			var options = this.options;
			
			if(object === undefined || object == ''){
				object = $('#' + options.objID.attr('id'));
			}
			
			self.include('/library/jscript/jquery.multiselect.js');
			
			var a = $( document.createElement('select') )
				.attr({
					'id': item['id'] + "_temp_" + options.no_fields,
					'name': item['id'] + "_temp_" + options.no_fields
				})
				.addClass('multiselect');
		
			if(typeof(item['event']) !== undefined){
				if($.isFunction(item['event']) === true){
					a.change(function(e){
						item['event']();
					});
				}
			}
			
			if(typeof(item['params']) !== undefined){
				if(typeof(item['params']) == 'string'){
					self.data_post(function(data){
						self.build_options(data, a, function(){
							$('#' + item['id'] + "_temp_" + options.no_fields).multiselect({
						    	'noneSelectedText'	:	'Select at least one entity',
						    	'selectedList'		:	2,
						    	'minWidth'			:	325,
						    	'id'				:	item['id'] + options.no_fields,
						    	'appendLocation'	:	options.objID.parents('form:first')
							});
						});
						
						if(item_no > 0){
							$('#' + item['id'] + "_temp_" + options.no_fields).bind('multiselectrefresh', {'id':item['id']}, function(e){
								var id = e.data['id'];
								$('#' + id + options.no_fields).hide();
							});
						}
						
						$('#' + item['id'] + "_temp_" + options.no_fields).multiselect('refresh');
						$('#' + item['id'] + "_temp_" + options.no_fields).multiselect('uncheckAll');
						
						$('#' + item['id'] + "_temp_" + options.no_fields).unbind('multiselectrefresh');
					},item['params']);
				}else{
					self.build_options(item['params'], a);
				}
			}
				
			object.append(a);
			
			this._trigger('add_multiselect');
		},
		
		/*Add a select to supplied object*/
		add_select: function(item, object){
			
			var self = this;
			var options = this.options;
			
			if(object === undefined){
				object = $('#' + options.objID.attr('id'));
			}
			
			var a = $( document.createElement('select') )
				.attr({
					'id': item['id'] + options.no_fields,
					'name': item['id'] + options.no_fields
				})
				.addClass(item['class']);
		
			if(typeof(item['event']) !== undefined){
				if($.isFunction(item['event']) === true){
					a.change(function(e){
						item['event']();
					});
				}
			}
			
			if(typeof(item['params']) !== undefined){
				if(typeof(item['params']) == 'string'){
					self.data_post(function(data){
						self.build_options(data, a);
					},item['params']);
				}else{
					self.build_options(item['params'], a);
				}
			}
				
			object.append(a);
			this._trigger('add_select');
		},
		
		build_selectors: function(callback){
			var self = this;
			var options = this.options;
			
			var l = 0;
			
			for(l=0;l<options.values.length;l++){
				if(options.values[l] !== undefined){
					var this_element = options.values[l];					
					if(this_element['type'] !== undefined){
						var type = this_element['type'];
						
						if(type == 'input' || type == 'multiselect' || type == 'select'){
							
						}else{
							
							var element_id = this_element['id'] + options.no_fields;
							var param = this_element['params'];
							
							if(options.values.length == 1){
								$('#' + element_id).bind(type + 'set', {'self':self}, function(e){
									var self = e.data['self'];
									self.submit();
								});
							}
							
							if(param !== undefined){								
								$('#' + element_id)[type](param);
							}else if($.isFunction($('#' + element_id)[type]) === true){
								$('#' + element_id)[type]();
							}
						}
					}
					if(this_element['conditions'] !== undefined){
						$('#' +  this_element['conditions'][0]['field'] + options.no_fields).on(type + "set", {'self' : self, 'no_fields' : options.no_fields, 'id': l}, function(e){
							var self = e.data['self'];
							var no_fields = e.data['no_fields'];
							var id = e.data['id'];
							var options = self.options;
							
							var this_element = options.values[id];
							
							var condition_met = self.check_condition(id);
							
							if(condition_met == true){
								var div = $( document.createElement('div') )
									.attr('id', options.objID.attr('id') + "_field_alert" + no_fields)
									.addClass('search_filter_alert')
									.html(options.condition_met);
								
								$('#' + options.objID.attr('id') + "_field_" + no_fields).append(div);
							}else{
								$('#' + options.objID.attr('id') + "_field_alert" + no_fields).remove();
							}
							
							$('#' + options.objID.attr('id') + '_row_' + no_fields).find('.search_filter_fields').width('auto');
							
							var i = 0;
							for(i=no_fields+1;i<=options.no_fields;i++){
								if(condition_met == true){
									if($('#' + options.type_name + i + ' option[value="'+this_element['value']+'"]').length == 0){
										var option = $( document.createElement('option') )
											.attr('value', this_element['value'])
											.html(this_element['html']);
										
										$('#' + options.type_name + i).append(option);
									}
								}else{
									if($('#' + options.type_name + i + ' option[value="'+this_element['value']+'"]').length > 0){
										
										if($('#' + options.type_name + i).val() == this_element['value']){
											$('#' + options.type_name + i).selectpicker('val', options.values[0]['value']);
										}
										
										$('#' + options.type_name + i + ' option[value="'+this_element['value']+'"]').remove();
									}
								}
								
								$('#' + options.type_name + i).selectpicker('destroy');
								$('#' + options.type_name + i).selectpicker();
								var width = $('#' + options.type_name + i + '-menu').actual('width') + 20;
								
								$('#' + options.type_name + i + '-menu').css('width', width + 'px');
								$('#' + options.type_name + i + '-button').css('width', width + 'px');
								
								self.show_field(i);
							}
							
						});
						
						$('#' +  this_element['conditions'][0]['field'] + options.no_fields).on(type + "clear", {'self' : self, 'no_fields' : options.no_fields}, function(e){
							var self = e.data['self'];
							var no_fields = e.data['no_fields'];
							var options = self.options;
							
							$('#' + options.objID.attr('id') + "_field_alert" + no_fields).remove();
							
						});
					}
				}
			}
			
			if($.isFunction(callback) === true){
				callback();
			}
			this._trigger('build_selectors');
		},
		
		submit: function(){
			var self = this;
			var options = this.options;
			
			$('#a').val(options.a);
			
			var can_continue = true;
			
			if(options.search_empty == false){
				options.objID.find('input:visible').each(function(){
					if($(this).val() == ''){
						can_continue = false;
					}
				});
			}
			
			if(can_continue == true){
				
				if($.isFunction(options.callback) === true && options.override == true){
					options.callback()
					this._trigger('_submit');
					return false;
				}
				
				$("#processing_dialog").dialog('open');
				
				$.post(options.page,
				options.objID.parents('form:first').serialize() + "&no_fields=" + options.no_fields,
				function(data){
					
					$("#processing_dialog").dialog('close');
	
					if($.isFunction(options.callback) === true){
						if(options.callback.length < 1){
							options.callback();
						}else{
							options.callback(data);
						}
					}
				}, 
				"json");
			}else{
				$("#msg_dialog").dialog("option", "title", "Error!");
				$('#msg_dialog_message').html("Please fill in all criteria!");
				$("#msg_dialog").dialog('open');
			}
			this._trigger('_submit');
		},
		
		include: function(jsFile){
			var self = this;
			var options = this.options;
			
			if($.inArray(jsFile, options.included) < 0){
				$('body').append('<script type="text/javascript" src="'+ jsFile + '"></script>');
				options.included.push(jsFile);
			}
		},
		
		get_no_fields: function(){
			var self = this;
			var options = this.options;
			
			return options.no_fields;
		},
		
		check_condition: function(id){
			var self = this;
			var options = this.options;
			
			var this_element = options.values[id];
			var condition_array = new Array();
			
			var j = 0;
			
			if(this_element != undefined){
			
				if(this_element['conditions'] !== undefined){
				
					for(j=0;j<this_element['conditions'].length;j++){
						var this_condition = this_element['conditions'][j];
					
						var parent = this_condition['field'];
						var value = this_condition['value'];
						var operator = this_condition['operator'];
						var type = '';
						var select_value = '';
		
						var value_array = [];
		
						if(typeof value === 'string'){
							if(value.indexOf(options.value_delimiter) !== false){
								value_array = value.split(options.value_delimiter);
							}else{
								value_array.push(value);
							}
						}else if(typeof value !== 'undefined'){
							value_array.push(value);
						}
						
						if(typeof operator === 'undefined'){
							operator = "==";
						}
						
						if(typeof parent !== 'undefined'){
							
							var i = 0;
							for(i=0;i<options.values.length;i++){
								if(options.values[i] !== undefined){
									var the_element = options.values[i];
									if(the_element['id'] == parent){
										type = the_element['type'];
										select_value = the_element['value'];
									}
								}
							}
							var test_element = '';
							
							var i = 0;
							for(i=1;i<=options.no_fields;i++){
								if($('#' + options.type_name + i).val() == select_value){
									if(type == 'input' || type == 'multiselect' || type == 'select'){
										
										test_element = '#' + parent + i;
										
									}else{
										
										test_element = '#' + parent + i + '_input';
										
									}
								}
							}
							
							if(test_element != ''){
								var type_of_field = $(test_element).get(0).tagName;
			
								var temp_value = '';
								
								if(type_of_field == "INPUT" || type_of_field == "TEXTAREA" || type_of_field == "SELECT"){
									if($(test_element).attr('type') == 'radio' || $(test_element).attr('type') == 'checkbox'){
										temp_value = $(test_element + ':checked').val();
									}else{
										temp_value = $(test_element).val();
									}
								}else{
									temp_value = $(test_element).html();
								}
			
								var found = false;
								var if_what = true;
								
								if(operator == '!='){
									found = true;
									if_what = false;
								}
								
								var b = 0;
								
								if($(test_element).parent().is(':visible')){
									for(b=0;b<value_array.length;b++){
										var the_value = value_array[b];
										
										if(operator in options.operators && options.operators[operator](temp_value, the_value) == if_what) {
											//alert(test_element + ': ' + temp_value + ' ' + operator + ' ' + the_value);
											if(operator == '!='){
												found = false;
											}else{
												found = true;
											}
										}
									}
								}else{
									found = false;
								}
								
								condition_array[j] = found;
							}else{
								return false;
							}
						}
					}
					
					if(this_element['logic'] !== undefined){
						var logic = this_element['logic'];
					}else{
						var logic = '0';
					}
					
					for(j=0;j<this_element['conditions'].length;j++){
						logic = logic.replace(j.toString(), condition_array[j].toString() + ' == true');
					}
					
					var logic_out = eval(logic);
						
					if(logic_out == true){
						return true;
					}else{
						return false;
					}
				}else{
					return true;
				}
			}else{
				return true;
			}
		}
	});
}( jQuery ) );