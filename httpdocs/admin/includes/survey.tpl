<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.rmc_selector.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.search_filter.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.slidedeck.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.pdfEmbed.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.radio.js"></script>

<script type="text/javascript">
	var survey_id = <?=$_REQUEST["survey_id"]?>;
	var survey_name = '<?=$survey->survey_name;?>';
	var survey_serial = '<?=$survey->survey_serial;?>';
	
	var can_create = '<?=$session["create_surveys"];?>';
	var can_delete = '<?=$session["delete_surveys"];?>';
	var can_send = '<?=$session["send_surveys"];?>';
</script>

<script type="text/javascript" src="<?=$UTILS_WEBROOT?>admin/includes/survey.js"></script>
<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/selectors.css" />
<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.search_filter.css" />
<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.radio.css" />
<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/slidedeck.skin.css" />

<div id="letter_dialog" title="Letters" style="display:none;">
	<div style="padding:10px;overflow:hidden;">
		<div id="gDocs"></div>
	</div>
</div>

<div id="confirm_dialog" title="Are you sure?" style="display:none;">
	<div style="padding:10px;overflow:hidden;">
		<p><strong>Are you sure you want to send out letters?</strong></p>
	</div>
</div>

<div id="enter_dialog" title="Enter the Unique Reference" style="display:none;">
	<form id="enter-form" method="post" class="form-horizontal" style="width:470px;margin:0 0 0 100px;">
		<fieldset>
			<div class="control-group">
				<label class="control-label" id="survey_name_label">Unique Reference</label>
				<div class="controls">
					<div class="input-prepend input-append" data-role="acknowledge-input">
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter the unique reference at the top of the survey."><i class="icon-question-sign"></i></div>
			        	<input name="survey_ref_input" id="survey_ref_input" type="text" required="required" placeholder="Unique Ref." data-type="text" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>

<div id="questions-div">
	<div class="clearfix button_group">
		<div class="btn-toolbar btn-toolbar2 pull-left">
			<div class="btn-group">
				<a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
					Action
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu">
					<li>
						<div class="btn-group">
							<a class="btn btn-info" id="edit_button">Edit Survey</a>
							<a class="btn btn-info" id="question_add_button">Add a Question</a>
							<a class="btn btn-info" id="question-order-button">Order Questions</a>
						</div>
					</li>
				</ul>
			</div>
			<div class="btn-group">
				<a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
					Survey Options
					<span class="caret"></span>
					</a>
				<ul class="dropdown-menu">
					<li>
						<div class="btn-group">
							<a class="btn btn-info" id="send_survey_button">Send Survey</a>
							<a class="btn btn-info" target="_blank" href="/survey/<?=$survey->survey_serial;?>/<?=$_SESSION['admin_user_serial'];?>">View Live Survey</a>
							<a class="btn btn-info" id="enter_results_button">Enter Results</a>
							<a class="btn btn-info" id="survey_results_button" href="/admin/results/<?=$_REQUEST["survey_id"]?>/">View Results</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<table class="display" id="results_table" border="0" cellspacing="0" cellpadding="0" width="100%">
		<?=$question_data ?>
	</table>
</div>

<div id="questions-order-div">
	<div class="clearfix button_group">
		<a id="save_q_order_button" class="button ui-button-info pull-right">Save Question Order</a>
	</div>
	<table class="display" id="order_table" border="0" cellspacing="0" cellpadding="0" width="100%">
		<?=$order_data ?>
	</table>
</div>

<div id="survey-send-details">
	<form id="form_search" name="form_search"> 
		<div class="record_heading">
			<div class="left">Search Facility</div>
		</div>
		<div id="search_filter">
		</div>
	</form>
	<table class="display" id="letter_table" border="0" cellspacing="0" cellpadding="0" width="100%">
		<?=$letter_data ?>
	</table>
	<div id="ghj"></div>
</div>

<div id="question-info" class="form-container">
			
	<div id="question-tabs">
		<ul>
			<li>
				<a href="#question">Question</a>
			</li>
			<li>
				<a href="#answers">Answers</a>
			</li>
		</ul>
		<div id="question">
			
			<div class="clearfix button_group_inner">
				<button class="button ui-button-info pull-right" id="question_cancel_button">Cancel</button>
				<button class="button ui-button-info pull-right" id="question_save_button">Save</button>
				<button class="button ui-button-info pull-right" id="question_edit_button">Edit Question</button>
			</div>
			
				<form id="question-form" method="post" class="form-horizontal">
					<fieldset>
						<div class="control-group">
							<label class="control-label" id="question_name_label">Question Text</label>
							<div class="controls">
								<div id="question_name_edit" class="input-prepend input-append" data-role="acknowledge-input">
						    		<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter the question to be asked"><i class="icon-question-sign"></i></div>
						        	<input class="span4" name="question_name_input" id="question_name_input" type="text" required="required" placeholder="Question" data-type="text" />
						        	<div class="add-on" data-role="acknowledgement"><i></i></div>
								</div>
								<span id="question_name_span" class="span5 uneditable-textarea">&nbsp;</span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" id="question_info_label">Info Line</label>
							<div class="controls">
								<div id="question_info_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
									<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter some text to help the user. For example: Please select at least one."><i class="icon-align-left"></i></div>
									<input class="span4" name="question_info_input" id="question_info_input" type="text" placeholder="Info Line" data-type="text" />
						        	<div class="add-on" data-role="acknowledgement"><i></i></div>
								</div>
								<span id="question_info_span" class="span5 uneditable-textarea">&nbsp;</span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" id="question_type_label">Question Type</label>
							<div class="controls">
								<div id="question_type_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
									<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Select a type of field"><i class="icon-search"></i></div>
									<select class="span4" name="question_type_select" id="question_type_select" required="required">
									</select>
									<div class="add-on" data-role="acknowledgement"><i></i></div>
								</div>
								<span id="question_type_span" class="span5 uneditable-input">&nbsp;</span>
							</div>
						</div>
						<div class="control-group" id="question_weight_row">
							<label class="control-label" id="question_weight_label">Weighting</label>
							<div class="controls">
								<div id="question_weight_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
									<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Select a weight, where 1 is normal, 5 is important and 10 is highly important"><i class="icon-tasks"></i></div>
									<select class="span4" name="question_weight_select" id="question_weight_select" required="required">
										<option value=""> - Please Select - </option>
										<option value="0">0</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
									</select>
									<div class="add-on" data-role="acknowledgement"><i></i></div>
								</div>
								<span id="question_weight_span" class="span5 uneditable-input">&nbsp;</span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" id="question_mandatory_label">Mandatory?</label>
							<div class="controls">
								<div id="question_mandatory_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
									<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Please select whether this question must be answered"><i class="icon-ok-sign"></i></div>
									<select class="span4" name="question_mandatory_select" id="question_mandatory_select" required="required">
										<option value=""> - Please Select - </option>
										<option value="Y">Yes</option>
										<option value="N">No</option>
									</select>
									<div class="add-on" data-role="acknowledgement"><i></i></div>
								</div>
								<span id="question_mandatory_span" class="span5 uneditable-input">&nbsp;</span>
							</div>
						</div>
						<div class="control-group" id="question_mandatory_text_row">
							<label class="control-label" id="question_mandatory_text_label">Mandatory Text</label>
							<div class="controls">
								<div id="question_mandatory_text_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
									<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter some text to be displayed if the user fails to answer the question"><i class="icon-file-alt"></i></div>
									<input class="span4" name="question_mandatory_text_input" id="question_mandatory_text_input" type="text" placeholder="Mandatory Text" data-type="text" />
						        	<div class="add-on" data-role="acknowledgement"><i></i></div>
								</div>
								<span id="question_mandatory_text_span" class="span5 uneditable-textarea">&nbsp;</span>
							</div>
						</div>
						<div class="control-group" id="question_no-selections_row">
							<label class="control-label" id="question_no-selections_label">Max No. of Selections</label>
							<div class="controls">
								<div id="question_no-selections_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
									<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter the number of selections that can be made"><i class="icon-list-alt"></i></div>
									<input class="span4" name="question_no-selections_input" id="question_no-selections_input" type="text" placeholder="No. of Selections" data-type="text" />
						        	<div class="add-on" data-role="acknowledgement"><i></i></div>						        	
								</div>
								<span id="question_no-selections_span" class="span5 uneditable-input">&nbsp;</span>
							</div>
						</div>
			    	</fieldset>
			    	<input type="hidden" id="question_id" name="question_id" value="" />
				</form>
			
		</div>
		<div id="answers">
			<div id="answer-div">
				<div class="clearfix button_group_inner">
					<button class="button ui-button-info pull-right" id="answer-add-button">Add Answer</button>
				</div>
				<table class="display" id="answer_table" border="0" cellspacing="0" cellpadding="0">
					<?=$answer_data ?>
				</table>
			</div>
			<div id="answer-info">
				
				<div class="clearfix button_group_inner">
					<button class="button ui-button-info pull-right" id="answer_cancel_button">Cancel</button>
					<button class="button ui-button-info pull-right" id="answer_save_button">Save</button>
					<button class="button ui-button-info pull-right" id="answer_edit_button">Edit Answer</button>
					<button class="button ui-button-info pull-left" id="answer_back_button"><i class="icon-chevron-left"></i> Back to Answers</button>
				</div>
				
				<form id="answer-form" method="post" class="form-horizontal">
					<fieldset>
						<div class="control-group">
							<label class="control-label" id="answer_name_label">Text</label>
							<div class="controls">
								<div id="answer_name_edit" class="input-prepend input-append" data-role="acknowledge-input">
						    		<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter an answer to the question being asked"><i class="icon-comment"></i></div>
						        	<input class="span4" name="answer_name_input" id="answer_name_input" type="text" required="required" placeholder="Answer Text" data-type="text" />
						        	<div class="add-on" data-role="acknowledgement"><i></i></div>
								</div>
								<span id="answer_name_span" class="span5 uneditable-input">&nbsp;</span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" id="answer_value_label">Weighting</label>
							<div class="controls">
								<div id="answer_value_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
									<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="For example 2 for Good, 1 for Average, 0 for Bad."><i class="icon-certificate"></i></div>
									<input class="span4" name="answer_value_input" id="answer_value_input" type="text" placeholder="Weighting" data-type="number" />
						        	<div class="add-on" data-role="acknowledgement"><i></i></div>
								</div>
								<span id="answer_value_span" class="span5 uneditable-input">&nbsp;</span>
							</div>
						</div>
			    	</fieldset>
			    	<input type="hidden" id="answer_id" name="answer_id" value="" />
				</form>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="rmc_input" name="rmc_input" value="" />
<input type="hidden" id="letter_table_residents" name="letter_table_residents" value="" />
<input type="hidden" id="letter_types" name="letter_types" value="" />
<input type="hidden" id="letter_table_all_residents" name="letter_table_all_residents" value="" />
<input type="hidden" id="resident_ref" name="resident_ref" value="" />