<?php

Global $UTILS_SERVER_PATH;


$name='Arial-BoldItalicMT';
$type='TTF';
$desc=array (
  'Ascent' => 905,
  'Descent' => -212,
  'CapHeight' => 715,
  'Flags' => 262212,
  'FontBBox' => '[-560 -376 1390 1018]',
  'ItalicAngle' => -12,
  'StemV' => 165,
  'MissingWidth' => 750,
);
$up=-106;
$ut=105;
$ttffile = $UTILS_SERVER_PATH.'library/classes/pdf/tfpdf/font/unifont/Arial-BoldItalic.ttf';
$originalsize=561616;
$fontkey='arialBI';
?>