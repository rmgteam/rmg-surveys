/*
 * $('#search_results_div').report_builder({
			config:[
				{
					name: 'Status Report',
					phpvar: 'a',
					phpvalue: 'search',
					extra_fields: [
						{
							id: 'search_status',
							name: 'Status',
							type: {
								type: 'select',
								params: 'electrical_report.php?a=status'
							},
							event: function(){
								if ($('#search_status').val() == "nddni" || $('#search_status').val() == "not_due"){
									$('#no_days_row').show();
								}else{
									$('#no_days_row').hide();
								}
							}
						},
						{
							id: 'search_status',
							name: 'Status',
							type: {
								type: 'select',
								params:[
								        {
									        'name': 'report1',
									        'value': '1'
								        },
								        {
									        'name': 'report2',
									        'value': '2'
								        }
								]
							}
						},
						{
							id: 'staff',
							type: 'odrmpm_block'
						},
						{
							id: 'no_days',
							name: 'No. of Days',
							type: {
								type: 'input',
								params: 'numeric'
							}
						}
					]
				},
				{
					name: 'Remedial Report',
					phpvar: 'a',
					phpvalue: 'remedial'
				},
				{
					name: 'Board Report',
					phpvar: 'a',
					phpvalue: 'board',
					non_sortable: {
						0: { 
							sorter: false
						}
					},
					extra_buttons: [
					                {
						                id: 'button1',
						                name: 'Chase Me',
						                event: function(){
							                alert('whooops');
						                }
					                }
								]
				},
				{
					name: 'Audit Report',
					phpvar: 'a',
					phpvalue: 'audit',
					extra_fields: [
								{
									id: 'et_rmc',
									name: 'Property',
									type: {
										type: 'rmc_selector'
									},
									params: {
										beforeEnd: function(){alert('here');}
									},
									event: function(){
										if($('#et_rmc_input').val() == ''){
											$('#et_report').html('');
										}else{
											$("#processing_dialog").dialog('open');
											$('#a').val("get_tests");
											
											$.post("electrical_report.php", 
											$("#form11").serialize(), 
											function(data){
												$('#et_report').html(data['result']);
												$("#processing_dialog").dialog('close');
											}, 
											"json");
										}
									}
								},
								{
									id: 'et_report',
									name: 'Electrical Test',
									type: 'span'
								}
							]
				}
			]
		});
 */

(function( $, undefined ) {

	$.widget( "ui.report_builder", {
	
		options: {
			objID: '',
			num_cols: 0,
			num_rows: 0,
			building: '',
			scroll_width: 16,
			page: $(location).attr('href'),
			form_name: '',
			excel_value: 'excel',
			extra_padding: 25,
			groups: false,
			search_select: 'search_select',
			header_table_class: 'results_header_table',
			highlight_class: 'highlight',
			col_widths: [],
			total_width: 0,
			headers: [],
			values: [],
			main_title: 'Search Facility',
			extra_params: {},
			config: [],
			currentSort: [],
			included: [],
			items_page: 300,
			no_pages: 0,
			current_page: 1,
			shift_down: false,
			table_click: false,
			reload_fix: 0,
			reorder_event: function(){},
			debug: false,
			myApp: {},
			scrollX: '0px',
			th_css: {}
		},
		
		/*Initialise*/
		_create: function(){
			var self = this;
			var options = this.options;
			
			options.objID = this.element;
			
			$(document).keydown(function(evt) {
				if (evt.which == 16) {
					options.shift_down = true;
				}
			}).keyup(function(evt) {
				if (evt.which == 16) {
					options.shift_down = false;
					if($.isFunction(options.reorder_event) === true){
						options.reorder_event();
					}
				}
			});
			
			options.sScrollX = options.objID.actual('width') + 'px';

			var ss = document.styleSheets;

		    for (var i=0; i<ss.length; i++) {
		        var rules = ss[i].cssRules || ss[i].rules;

		        for (var j=0; j<rules.length; j++) {
			        var selector_text = rules[j].selectorText;
		            if (selector_text === "table.display thead th, table.display thead th.ui-state-default") {
		            	options.th_css['font-weight'] = rules[j].style['fontWeight'];
		            	options.th_css['font-size'] = rules[j].style['fontSize'];
		            	options.th_css['padding'] = rules[j].style['padding'];
		            }
		        }
		    }
			self.build_search();
		},
		
		/*Load new data set*/
		load: function(values){
			var self = this;
			var options = this.options;
			
			$("#" + options.objID.attr('id')).show();
			
			if(typeof(values) !== undefined && values != ''){
				self.empty();
				options.values = values;
				
				self._initSource(function(){
					options.current_page = 1;
					self.build_table();
				});
			}
		},
		
		/*Build data arrays from php function string*/
		_initSource: function(callback) {
			var self = this;
			var options = self.options;
			
			self.data_post(function(data){
				/*First Array is the header array*/
				var arr = $.makeArray(data['header']);
				options.headers = arr;
				
				/*Second Array is the data array*/
				arr = $.makeArray(data['data']);
				options.values = arr;
								
				if($.isFunction(callback) === true){
					callback();
				}
			},options.page);
		},
		
		/*Post supplied url, return data to callback function and optional parameter to include serialised form*/
		data_post: function(callback, url, serialize) {
			var self = this,
				array;
			var options = self.options;
			
			if ( typeof url === "string" ) {
				
				$("#processing_dialog").dialog('open');
				
				var serial_array = [];
				if(serialize === undefined){
					if(options.form_name != ''){
						pushFormData('#' + options.form_name, serial_array);
					}
				}else{
				     if(serialize == true){
				    	 if(options.form_name != ''){
							pushFormData('#' + options.form_name, serial_array);
						}
				     }
				}
				
				$.each(options.extra_params,function(index, value){
					serial_array.push({'name' : index, 'value' : value});
				});
				
				$.post(url, 
				serial_array,
				function(data){
					$("#processing_dialog").dialog('close');
					
					if($.isFunction(callback) === true){
						callback(data);
					}
				}, 
				"json");
			}
		},
		
		/*Build Search Facility*/
		build_search: function(){
			
			var self = this;
			var options = this.options;
			var config_array = options.config;
			var i = 0;
			var j = 0;
			
			/*Create wrapper div*/
			var wrapper = $( document.createElement('div') )
				.attr('id', options.objID.attr("id") + '_search');
			
			/*Create header bar*/
			var a = $( document.createElement('div') )
				.addClass('record_heading');
			
			var b = $( document.createElement('div') )
				.addClass('left')
				.html(options.main_title);
			
			a.append(b);
			wrapper.append(a);
			
			/*Create Main filter table*/
			var a = $( document.createElement('div') )
				.addClass('search_filter')
				.attr({
					id: options.objID.attr("id") + '_filter'
				})
				.css({
					'-webkit-border-radius' : '0px',
					'-moz-border-radius': '0px',
					'border-radius': '0px'
				});
			
			var b = $( document.createElement('div') )
				.addClass('search_filter_row');
			var c = $( document.createElement('div') )
				.addClass('search_filter_col')
				.css({
					'width': '30%',
					'font-weight': 'bold'
				})
				.html('Please select a report');
			
			b.append(c);
			
			var c = $( document.createElement('div') )
				.addClass('search_filter_fields');
			var d = $( document.createElement('select') )
				.attr({
					'id': options.search_select,
					'name': options.search_select
				})
				.change(function(e){
					self.show_report();
				});
			
			/*Create options for select drop down list from config*/
			if(config_array.length > 0){
				var e = $( document.createElement('option') )
					.attr('value', '')
					.html(' - Please Select - ');
				d.append(e);
				for(i=0;i<config_array.length;i++){
					var item = config_array[i];
					
					var e = $( document.createElement('option') )
						.attr('value', item['phpvalue'])
						.html(item['name']);
					d.append(e);
					
					//Check for phpvar named input on page
					if($('#' + item['phpvar']).length > 0){
					}else{
						//Create missing input
						var input = $( document.createElement('input') )
							.attr({
								'id': item['phpvar'],
								'name': item['phpvar'],
								'type': 'hidden'
							})
							.val("");
						
						//Find which form the element for this object is within then save form name and add input to form
						if($('body').find('form').length > 0){
							$('body').find('form').each(function(){
								if($(this).find('#' + options.objID.attr('id')).length > 0){
									options.form_name = $(this).attr('id');
									$(this).append(input);
								}
							});
						}else{
							//If there is no form, create one and wrap it round the element for this object 
							var form = $( document.createElement('form') )
								.attr({
									'id' : options.objID.attr('id') + '_form',
									'name' : options.objID.attr('id') + '_form'
								});
							
							options.form_name = options.objID.attr('id') + '_form';
							
							$('#' + options.objID.attr('id')).wrap(form);
						}
					}
					
				}
			}
			
			c.append(d);
			b.append(c);
			a.append(b);

			wrapper.append(a);
			
			/*Create a table for each report*/
			if(config_array.length > 0){
				for(i=0;i<config_array.length;i++){
					var item = config_array[i];
					
					var a = $( document.createElement('div') )
						.addClass('search_filter')
						.attr({
							border: '0',
							cellspacing: '0',
							cellpadding: '0',
							id: item['phpvalue'] + '_tbl'
						});
					
					var colspan = 0;
					if(item['extra_fields'] !== undefined){
						if(item['extra_fields'].length > 0){
							colspan = 2;
							/*Create a row for each extra field with a label td and element*/
							for(j=0;j<item['extra_fields'].length;j++){
								var extra = item['extra_fields'][j];
								
								if(extra['name'] === undefined){
									//If name does not exist, then extra field is a block, add block of fields
									self.field_type(extra, a);
								}else{
									
									var html = $( document.createElement('label') )
										.html(extra['name']);
									
									//If name exists, add new row, label and field
									var b = $( document.createElement('div') )
										.addClass('search_filter_row')
										.attr('id', extra['id'] + '_row');
									var c = $( document.createElement('div') )
										.addClass('search_filter_col')
										.css('width', '30%')
										.append(html);
									
									if(extra['info'] !== undefined){
										var d = $( document.createElement('label') )
											.attr({
												'id': extra['id'] + '_row_info',
												'lang': extra['info']
											})
											.addClass('hoverCard');
										var e = $( document.createElement('img') )
											.attr({
												"src": "/images/information.png",
												"width": "18",
												"height": "18"
											});
										
										c.find('label').css({
											'margin-right': '20px',
											'float': 'left',
											'position': 'relative',
											'height': '18px',
											'line-height': '18px'
										});
										
										d.append(e);
										c.append(d);
									}

									b.append(c);
									
									var c = $( document.createElement('div') )
										.addClass('search_filter_fields');
									self.field_type(extra, c);
									b.append(c);
									a.append(b);
								}
							}
						}
					}

					//Create button row					
					var b = $( document.createElement('div') )
						.addClass('search_filter_row');
					var c = $( document.createElement('div') )
						.addClass('search_filter_col');
					
					//Create buttons
					var d = $( document.createElement('a') )
						.addClass('jButton')
						.attr('id','search_button_' + item['name'])
						.css('margin-right', '20px')
						.html('Print To Screen')
						.click(function(e){self.do_screen();});
					c.append(d);
					var d = $( document.createElement('a') )
						.addClass('jButton')
						.attr('id','export_button_' + item['name'])
						.css('margin-right', '20px')
						.html('Export To Excel')
						.click(function(e){self.do_excel();});
					c.append(d);
					
					if(item['extra_buttons'] !== undefined){
						if(item['extra_buttons'].length > 0){
							/*Create a row for each extra field with a label td and element*/
							for(j=0;j<item['extra_buttons'].length;j++){
								var extra = item['extra_buttons'][j];
								
								var d = $( document.createElement('a') )
									.addClass('jButton')
									.attr('id', extra['id'])
									.css('margin-right', '20px')
									.html(extra['name'])
									.click({extra: extra}, function(event){var data = event.data; data.extra['event']();});
								c.append(d);
							}
						}
					}					
					
					b.append(c);
					a.append(b);
					a.hide();
					wrapper.append(a);
				}
			}

			
			var iframe = $( document.createElement('iframe') )
				.attr('id', 'build_search_iframe')
				.hide();
			
			wrapper.append(iframe);
			
			$('#' + options.objID.attr("id")).before(wrapper);
			
			if(config_array.length == 1){
				$('#' + options.objID.attr("id") + '_filter').hide(); 
				$('#' + config_array[0]['phpvalue'] + "_tbl").show();
				$('#' + options.search_select).val(config_array[0]['phpvalue']);
			}
			
			self.build_selectors();
		},
		
		/*Chcek field type*/
		field_type: function(item, object){
			
			var self = this;
			var options = this.options;
			var type = '';
			
			//Set type based on whether the type is an array or string
			if(typeof(item['type']) == "string"){
				var type = item['type'].toLowerCase();
			}else{
				var type = item['type']['type'].toLowerCase();				
			}
			
			//Run command for type
			if(type == 'span'){
				self.add_span(item,object);
			}else if(type == 'select'){
				self.add_select(item, object);
			}else if(type == 'multiselect'){
				self.add_multiselect(item, object);
			}else if(type == 'input'){
				self.add_input(item, object);
			}else if(type == 'odrmpm_block'){
				self.add_odrmpm(item, object);
			}else{
				//this is for jQuery Plugins
				self.add_selector(item,object);
			}	
		},
		
		/*Add a multiselect to supplied object*/
		add_multiselect: function(item, object){
			
			var self = this;
			var options = this.options;
			
			self.include('/library/jscript/jquery.multiselect.js');
			
			var a = $( document.createElement('select') )
				.attr({
					'id': item['id'],
					'name': item['id']
				})
				.addClass('multiselect');
		
			if(typeof(item['event']) !== undefined){
				if($.isFunction(item['event']) === true){
					a.change(function(e){
						item['event']();
					});
				}
			}
			
			if(typeof(item['type']['params']) !== undefined){
				if(typeof(item['type']['params']) == 'string'){
					self.data_post(function(data){
						self.build_options(data, a);
						
						$('#' + item['id']).multiselect('refresh');
						$('#' + item['id']).multiselect('uncheckAll');
					},item['type']['params'],false);
				}else{
					self.build_options(item['type']['params'], a);
				}
			}
				
			object.append(a);
		},
		
		/*Add a selector to supplied object*/
		add_odrmpm: function(item, object){
			
			var self = this;
			var options = this.options;
			
			var b = $( document.createElement('div') )
				.addClass('search_filter_row')
				.attr('id', item['id'] + '_od_row');
			
			var label = $( document.createElement('label') )
				.html('Operational Director');
			
			var c = $( document.createElement('div') )
				.addClass('search_filter_col')
				.css('width', '30%')
				.append(label);

			b.append(c);
			
			var c = $( document.createElement('div') )
				.addClass('search_filter_fields');
			
			var a = $( document.createElement('span') )
				.attr({
					'id': item['id'] + '_od'
				});
			
			c.append(a);
			
			b.append(c);
				
			object.append(b);
			
			var b = $( document.createElement('div') )
				.addClass('search_filter_row')
				.attr('id', item['id'] + '_rm_row');
			
			var label = $( document.createElement('label') )
				.html('Regional Manager');
			
			var c = $( document.createElement('div') )
				.addClass('search_filter_col')
				.css('width', '30%')
				.append(label);
			
			b.append(c);
			
			var c = $( document.createElement('div') )
				.addClass('search_filter_fields');
			
			var a = $( document.createElement('span') )
				.attr({
					'id': item['id'] + '_rm'
				});
			
			c.append(a);
			
			b.append(c);
				
			object.append(b);
			
			var b = $( document.createElement('div') )
				.addClass('search_filter_row')
				.attr('id', item['id'] + '_pm_row');
			var label = $( document.createElement('label') )
				.html('Property Manager');
		
			var c = $( document.createElement('div') )
				.addClass('search_filter_col')
				.css('width', '30%')
				.append(label);
			
			b.append(c);
			
			var c = $( document.createElement('div') )
				.addClass('search_filter_fields');
			
			var a = $( document.createElement('span') )
				.attr({
					'id': item['id'] + '_pm'
				});
			
			c.append(a);
			
			b.append(c);
				
			object.append(b);
		},
		
		/*Add a selector to supplied object*/
		add_selector: function(item, object){
			
			var self = this;
			var options = this.options;
			
			var a = $( document.createElement('span') )
				.attr({
					'id': item['id']
				});
				
			object.append(a);
		},
		
		/*Add a drop down to supplied object*/
		add_select: function(item, object){
			
			var self = this;
			var options = this.options;
			
			var a = $( document.createElement('select') )
				.attr({
					'id': item['id'],
					'name': item['id']
				});
			
			if(typeof(item['event']) !== undefined){
				if($.isFunction(item['event']) === true){
					a.change(function(e){
						item['event']();
					});
				}
			}
			
			if(typeof(item['type']['params']) !== undefined){
				if(typeof(item['type']['params']) == 'string'){
					self.data_post(function(data){
						self.build_options(data, a);
					},item['type']['params'],false);
				}else{
					self.build_options(item['type']['params'], a);
				}
			}
				
			object.append(a);
		},
		
		/*Build options list*/
		build_options: function(array, object){
			
			var self = this;
			var options = this.options;
			var i = 0;
			
			if($.isArray(array)){
				for(i=0;i<array.length;i++){
					var item = array[i];
					var e = $( document.createElement('option') );
					e.attr('value', item['value']);
					e.html(item['name']);
					object.append(e);
				}
			}
			
		},
		
		/*Add an input to supplied object*/
		add_input: function(item, object){
			
			var self = this;
			var options = this.options;
			
			var a = $( document.createElement('input') )
				.attr({
					'type': 'text',
					'id': item['id'],
					'name': item['id']
				});
			
			if(typeof(item['type']['default']) !== undefined){
				a.val(item['type']['default']);
			}
			
			if(typeof(item['type']['params']) !== undefined){
				a.addClass(item['type']['params']);
			}
				
			object.append(a);
		},
		
		/*Add an input to supplied object*/
		add_span: function(item, object){
			
			var self = this;
			var options = this.options;
			
			var a = $( document.createElement('span') )
				.attr({
					'id': item['id']
				});
			
			if(typeof(item['type']['params']) !== undefined){
				a.html(item['type']['params']);
			}
				
			object.append(a);
		},
		
		/*Build Datatable table*/
		build_table: function(callback){
			
			var self = this;
			var options = this.options;
			
			if(options.debug){
				var time = new Date();
				self.benchmark('Building Table', time);
			}
			
			var b = $( document.createElement('table') );
			b.attr({
				'id': "search_results_div_" + options.objID.attr('id'),
				'cellpadding': "0",
				'cellspacing': "0",
				'border': "0"
			});
			b.addClass("display");
			
			if(options.debug){
				self.benchmark('Start Data Table', time);
			}
			
			var aoColumns = [];
			
			var inner_width = 0;
			
			if(options.headers.length > 0){				
				// For each header
				for(i=0;i<options.headers.length;i++){
					
					// Create span and fill with column text 
					var t = $( document.createElement('span') )
						.attr('id', options.objID.attr('id') + '_text_test')
						.css(options.th_css)
						.css({
							'display': 'inline-block'
						})
						.html(options.headers[i]);
					
					$('body').prepend(t);
					
					// Get text width
					var text_width = $('#' + options.objID.attr('id') + '_text_test').actual( 'outerWidth', { includeMargin : true }) + 40;
					
					inner_width += parseInt(text_width);
					
					// Remove temporary text
					$('#' + options.objID.attr('id') + '_text_test').remove();
					
					// Create column
					aoColumns.push({'sWidth': text_width + 'px', 'sTitle' : options.headers[i], 'bSortable': true});
				}
			}
			
			console.log(aoColumns);
			
			options.groups = false;
			
			var aaData = [];
			
			var sScrollXInner = inner_width + 'px';
			
			/*If there is data*/
			if(options.values.length > 0){
	
				// For each row
				for (var i = 0; i < options.values.length; i++) {					
					
					var temp_array = [];
					
					var data = '';
					
					if(options.values[i]['data'] !== undefined){
						data = options.values[i]['data'];
					}else{
						data = options.values[i];
					}
					
					var j = 0;
					
					// For each column
					for(j=0;j<data.length;j++){
						
						// Add column to temp array
						temp_array.push(data[j]);
					}
					
					temp_array["DT_RowId"] = 'row_' + i;
					aaData.push(temp_array);
	    		}
			}
			
			$("#" + options.objID.attr('id')).append(b);
			
			if(options.debug){
				self.benchmark('Finish Real Table', time);
			}
			
			// Calculate row height
			var row_height = $('#' + "search_results_div_" + options.objID.attr('id')).find('tr:first').height();
			
			$('#' + "search_results_div_" + options.objID.attr('id') + ' tfoot').hide();
			$('#' + "search_results_div_" + options.objID.attr('id') + '_wrapper .dataTables_scroll .dataTables_scrollFoot .dataTables_scrollFootInner table tfoot').hide()
			
			// If there is more than one column
			if(options.headers.length > 0){
				options.myApp.table = $('#' + "search_results_div_" + options.objID.attr('id')).dataTable({
			    	'bJQueryUI' : true,
			    	'bDestroy' : true,
					"bAutoWidth": false,
					"sPaginationType": "full_numbers",
		        	'aoColumns' : aoColumns,
		        	'aaData': aaData,
		        	"fnDrawCallback": function( oSettings ) {
		        		
		        		$('#' + "search_results_div_" + options.objID.attr('id')).wrap('<div class="dataTables_scroll"></div>;')
		        		
		        		$('#' + "search_results_div_" + options.objID.attr('id') + '_wrapper .dataTables_scroll').css({
		        			'overflow': 'hidden',
		        			'overflow-x': 'scroll',
		        			'width': options.sScrollX
		        		});
		        		
		        		$('#' + "search_results_div_" + options.objID.attr('id')).attr('width',sScrollXInner.replace('px', ''));
		        	},
		        	"fnInitComplete": function( oSettings ) {
		        	},
		        	"aaSorting": [[ 1 , 'asc' ]]
		      	});
			}else{
				$('#' + "search_results_div_" + options.objID.attr('id')).html('');
			}
			
			if($.isFunction(callback) === true){
				callback();
			}
		},
		
		reorder: function(){
			
			var self = this;
			var options = this.options;
			
			options.reload_fix ++;
			
			if(options.no_pages > 1 && options.reload_fix < 2){
				if(options.shift_down == false){
					
					options.reorder_event = function(){};
	    		
		    		options.values = options.page;
		    		
		    		self._initSource(function(){
						self.build_results();
					});
				}else{
					options.reorder_event = function(){
						self.reorder();
					};
				}
	    	}
		},
		
		change_page: function(page_no){
			
			var self = this;
			var options = this.options;
			
			options.current_page = page_no;
			
			$("#processing_dialog").dialog('open');
			
			//setTimeout must be used to show processing dialog whilst it is rebuilding the tables
			setTimeout(function() {
				self.build_results(function(){
					$("#processing_dialog").dialog('close');
				});
			},0); 
			
		},
		
		/*Create loading Dialog if one does not exist*/
		create_dialog: function(){
			/* Processing Dialog */
			
			if($("#processing_dialog").length > 0){
			}else{
				var a = $( document.createElement('div') );
				a.attr({
					'id': "processing_dialog",
					'title': "Processing"
				});
				a.addClass("dialog_1");
				
				var b = $( document.createElement('p') );
				b.attr({
					"style" : "text-align:center;display:none;margin-bottom:10px;",
					"id" : "processing_dialog_message"
				});
				
				var c = $( document.createElement('p') );
				c.attr("style", "text-align:center;");
				c.html('<img src="/images/ajax-loader.gif" />');
				
				a.append(b);
				a.append(c);
				$('#' + options.form_name).prepend(a);
				
				$("#processing_dialog").dialog({  
					modal: true,
					resizable: false,
					autoOpen: false,
				   	closeOnEscape: false,
				   	open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
				});
			}
			
			/* Error Dialog */
			if($("#error_dialog").length > 0){
			}else{
				var a = $( document.createElement('div') );
				a.attr({
					'id': "error_dialog",
					'title': "Errors!"
				});
				a.addClass("dialog_1");
				
				var b = $( document.createElement('p') );
				b.attr({
					'id': 'error_message',
					'style': 'text-align:left;'
				});
				
				a.append(b);
				$('#' + options.form_name).prepend(a);
				
				$("#error_dialog").dialog({  
					modal: true,
					autoOpen: false,
					resizable: false,
				   	closeOnEscape: false,
				   	open: function(event, ui) { $(".ui-dialog-titlebar-close").show(); },
				   	buttons: {
						Close: function() {
							$(this).dialog('close');
						}
					}
				});
			}
		},
		
		/*Check for mandatory fields*/
		do_check: function(callback){
			var self = this;
			var options = this.options;
			
			var config_array = options.config;
			var url = options.page;
			var i = 0;
			var messages = [];
			
			var open = true;
			
			if(config_array.length > 0){
				for(i=0;i<config_array.length;i++){
					var item = config_array[i];
					if(item['phpvalue'] == $('#' + options.search_select).val()){
						if(item['extra_fields'] !== undefined){
							if(item['extra_fields'].length > 0){
								/*Create a row for each extra field with a label td and element*/
								for(j=0;j<item['extra_fields'].length;j++){
									var extra = item['extra_fields'][j];
									var id = extra['id'];
									var type = '';
									
									if(typeof(extra['type']) == "string"){
										type = extra['type'].toLowerCase();
									}else{
										type = extra['type']['type'].toLowerCase();
									}
									
									if(type == 'span'){
									}else if(type == 'select'){
									}else if(type == 'input'){
									}else if(type == 'odrmpm_block'){
									}else{
										id += '_input';
									}	
									
									if(extra['mandatory'] !== undefined && $('#' + id).val() == '' && $('#' + extra['id'] + '_row').css('display') != 'none'){
										var arr = [];
										arr.push(true);
										arr.push(extra['mandatory']);
										messages.push(arr);
										open = false;
									}
								}
							}
						}
					}
				}
			}
			options.building = $('#' + options.search_select).val();
			self.show_error(messages, callback);
		},
		
		/*Screen button click*/
		do_screen: function(){
			var self = this;
			var options = this.options;
			
			var config_array = options.config;
			var url = options.page;
			var i = 0;
			
			self.do_check(function(data){
				if(data === true){
					if(config_array.length > 0){
						for(i=0;i<config_array.length;i++){
							var item = config_array[i];
							if(item['phpvalue'] == $('#' + options.search_select).val()){
								$('#' + item['phpvar']).val(item['phpvalue']);
							}
						}
					}
					options.building = $('#' + options.search_select).val();
					self.load(url);
				}
			});
		},
		
		/*Excel button click*/
		do_excel: function(){
			var self = this;
			var options = this.options;
			
			var config_array = options.config;
			var url = options.page;
			var i = 0;
			
			
			self.do_check(function(data){
				if(data === true){
					if(config_array.length > 0){
						for(i=0;i<config_array.length;i++){
							var item = config_array[i];
							if(item['phpvalue'] == $('#' + options.search_select).val()){
								$('#' + item['phpvar']).val(options.excel_value);
							}
						}
					}
					
					var serial_array = [];
					
					if(options.form_name != ''){
						pushFormData('#' + options.form_name, serial_array);
					}
					
					$.each(options.extra_params,function(index, value){
						serial_array.push({'name' : index, 'value' : value});
					});
					
					$('#build_search_iframe').attr('src', options.page + stringifyData(serial_array));
					
				}
			});
			
		},

		show_error: function(messages, callback){
			var self = this;
			var options = this.options;
			
			var error_message = "";
			
			if(messages.length > 0){
				error_message += "<ul>";
			}
			
			try{
				for(r=0;r<messages.length;r++){
					if($.isArray(messages[r])){
						if(messages[r][0] == true){
							error_message += "<li>" + messages[r][1] + "</li>";
						}
					}
				}
			}catch(err){
				alert(err.description);
			}
			
			if(messages.length > 0){
				error_message += "</ul>";
			}
			
			$('#error_message').html(error_message);
			
			if (error_message != ""){
				$("#error_dialog").dialog('open');	
			}else if($.isFunction(callback) === true){
				callback(true);
			}
		},
		
		show_report: function (){
			var self = this;
			var options = this.options;
			var which = $('#' + options.search_select).val();
			var i = 0;
			var j = 0;
			
			$("#" + options.objID.attr('id')).hide();
			
			options.currentSort = [];
			
			var config_array = options.config;
			
			if(config_array.length > 0){
				for(i=0;i<config_array.length;i++){
					var item = config_array[i];
					$('#' + item['phpvalue'] + '_tbl').hide();
					if(which == item['phpvalue']){
						if(item['extra_fields'] !== undefined){
							if(item['extra_fields'].length > 0){
								/*Create a row for each extra field with a label td and element*/
								for(j=0;j<item['extra_fields'].length;j++){
									var extra = item['extra_fields'][j];
									if(typeof(extra['event']) !== undefined){
										if($.isFunction(extra['event']) === true){
											extra['event']();
										}
									}
								}
							}
						}
					}
				}
			}
			
			if($('#' + options.objID.attr('id') + '_pager').length > 0){
				$('#' + options.objID.attr('id') + '_pager').remove();	
			}
						
			if(which != ""){
				$('#' + which + '_tbl').show();
			}
		},
		
		/*Initialise any selectors*/
		build_selectors: function(){
			var self = this;
			var options = this.options;
			var config_array = options.config;
			
			var i = 0;
			var j = 0;
			var info_found = false;
			
			if(config_array.length > 0){
				//For each report
				for(i=0;i<config_array.length;i++){
					var item = config_array[i];
					if(item['extra_fields'] !== undefined){
						if(item['extra_fields'].length > 0){
							for(j=0;j<item['extra_fields'].length;j++){
								
								var extra = item['extra_fields'][j];
								
								if(typeof(extra['type']) == "string"){
									var type = extra['type'].toLowerCase();
								}else{
									var type = extra['type']['type'].toLowerCase();
								}
								
								if(extra['info'] !== undefined){
									info_found = true;
								}
								
								if(type == 'span'){
								}else if(type == 'input'){
								}else if(type == 'multiselect'){
									
									$('#' + extra['id']).multiselect({
								    	noneSelectedText: 'Select at least one entity',
								    	selectedList: 2,
								    	appendLocation: $('#' + options.form_name),
								    	minWidth: 325
									});
									
								}else if(type == 'odrmpm_block'){
									$('#' + extra['id'] + '_od').staff_selector({
										od_limit: true
									});
									$('#' + extra['id'] + '_rm').staff_selector({
										od_id: extra['id'] + '_od_input'
									});
									$('#' + extra['id'] + '_pm').staff_selector({
										rm_id: extra['id'] + '_rm_input'
									});
								}else{
									//If there are parameters add them to selector
									if(extra['params'] !== undefined){
										$('#' + extra['id'])[type](extra['params']);
									}else if($.isFunction($('#' + extra['id'])[type]) === true){
										$('#' + extra['id'])[type]();
									}
									
									//Bind data event
									if(typeof(extra['event']) !== undefined){
										if($.isFunction(extra['event']) === true){
											$('#' + extra['id']).bind(type + 'set', {extra: extra}, function(event, ui){
												var data = event.data;
												data.extra['event']();
											});
										}
									}
								}
							}
						}
					}
				}
			}
			
			if(info_found == true){
				self.include('/library/jscript/jquery.hovercard.js');

				$('.hoverCard').each(function(index) {
					var a = $( document.createElement('div') )
						.html($(this).attr('lang'))
						.addClass('hc-details')
						.attr('id', 'testhover');
					
					$('body').append(a);
					
					font_weight = $('#testhover').css('font_weight');
					font_size = $('#testhover').css('font_size');

					$('#testhover').remove();
					
					var a = $( document.createElement('span') )
						.html($(this).attr('lang'))
						.css({
							'font_weight': font_weight,
							'font_size': font_size
						})
						.attr('id', 'testhover');
				
					$('body').append(a);
					
					the_width = $('#testhover').width();
					$('#testhover').remove();
					
				    $(this).hovercard({
				    	detailsHTML: $(this).attr('lang'),
				    	width: the_width
				    });
				});
			}
		},
		
		/*Delete created html and reset defaults*/
		empty: function(){
			var self = this;
			var options = this.options;
			
			options.objID.html('');
			options.num_cols = 0;
			options.num_rows = 0;
			options.col_widths = [];
			options.headers = {};
			options.values = {};
			options.groups = false;
		},
		
		include: function(jsFile){
			var self = this;
			var options = this.options;
			
			if($.inArray(jsFile, options.included) < 0){
				$('body').append('<script type="text/javascript" src="'+ jsFile + '"></script>');
				options.included.push(jsFile);
			}
		},
		
		/*Destroy object*/
		destroy: function() {
			
			var self = this;
			var options = this.options;
			
			self.empty();
			$.Widget.prototype.destroy.call( this );
		},
		
		benchmark: function(s, d) {
			
			var self = this;
			var options = this.options;
			
            self.log(s + "," + (new Date().getTime() - d.getTime()) + "ms");
        },

        log: function(s) {
            if (typeof console != "undefined" && typeof console.debug != "undefined") {
                console.log(s);
            } else {
                alert(s);
            }
        }
		
	});
}( jQuery ) );