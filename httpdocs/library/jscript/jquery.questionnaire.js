/*
 * values can be supplied as either a string to a json array or an array in the following formats
 * 
 * values:{
 * 		0:{
 * 			id: 0,
 * 			question: 'Please Select',
 * 			info: 'Select a #######',
 * 			type: '1',
 * 			answers : {
 * 				0 : {
 * 					value : 'Y',
 * 					text : 'Yes'
 * 				},
 * 				1 : {
 * 					value : 'N',
 * 					text : 'No'
 * 				}
 * 			}
 * 		},
 * 		1:{
 * 			id: 1,
 * 			question: 'Something',
 * 			info: 'Some thing',
 * 			type: '1',
 * 			answers : {
 * 				0 : {
 * 					value : 'Y',
 * 					text : 'Yes'
 * 				},
 * 				1 : {
 * 					value : 'N',
 * 					text : 'No'
 * 				}
 * 			}
 * 		},
 * }
 * 
 * OR
 * 
 * values: 'somefile.php?var=something'
 * 
 * the php page should be in the following format
 * 
 * $array[$i]['id'] = 0;
 * $array[$i]['question'] = 'Did you speak to your property manager, $pm_name?';
 * $array[$i]['info'] = 'Some information';
 * $array[$i]['type'] = '1';
 * $array[$i]['answers'][0]['value'] = 'Y';
 * $array[$i]['answers'][0]['text'] = 'Yes';
 * $array[$i]['answers'][1]['value'] = 'N';
 * $array[$i]['answers'][1]['text'] = 'No';
 * $i++;
 * echo json_encode($array);
 * */

(function( $, undefined ) {

	$.widget( "ui.questionnaire", {
	
		options: {
			objID: '',
			request_var: 'a',
			save_result: 'save',
			check_result: 'check',
			data_var: 'result',
			data_result: 'success',
			manco_var: 'man_co',
			page: $(location).attr('href'),
			form_name: 'questionnaire_1',
			survey_id: '',
			serial_ref: '',
			tenant_ref: '',
			mandatory: new Array(),
			values: {}
		},
		
		_create: function(){
			var self = this;
			var options = this.options;
			
			options.objID = this.element;
			
			var h = $( document.createElement('form') )
			h.attr({
				'id' : options.form_name,
				'name' : options.form_name
			});
			
			options.objID.wrapInner(h);
			
			self._initSource(function(){
				self.set();
				self.build_dialogs();
			});
		},
		
		
		// Sets the source of the questionnaire
		_initSource: function(callback) {
			var self = this,
				array,
				url;
			var options = self.options;
			
			if ( typeof options.values === "string" ) {
				url = options.values;
				
				$.post(url + "&survey_id=" + options.survey_id + "&serial_ref=" + options.serial_ref, 
				{},
				function(data){
					
					if(data['save_result'] == 'error'){
						self.build_done_dialog("You have already filled in this survey");
					}else{
						var arr = $.makeArray(data);
						options.values = arr;
						
						if($.isFunction(callback) === true){
							callback();
						}
					}
				}, 
				"json");
			} else {
				this.values = options.values;
				
				if($.isFunction(callback) === true){
					callback();
				}
			}
		},
		
		set: function(){
			var self = this;
			var options = this.options;
			
			value = options.values;
			
			if((typeof(value) !== "undefined" && value != '') || typeof(value) === "undefined"){
				
				var count = 0;
	
				for (i in options.values) {
				    count++;
				}
				
				var linkId = options.objID.attr('id');
				
				// Builds the survey content...
				for(i=0;i<count;i++){
					
					if(!!options.values[i]){
						
						// Create questions out-shell
						var c = $( document.createElement('div') );
						c.attr('id', linkId + "_surround_" + options.values[i]['id']);
						c.addClass('question_surround');
						
						var d = $( document.createElement('div') );
						d.attr('id', linkId + "_" + options.values[i]['id']);
						d.addClass("question ui-corner-all");
						
						var s = $( document.createElement('div') );
						d.attr('id', linkId + "_" + options.values[i]['id'] + "_hr_div");
						s.addClass('question_hr');
						
						var r = $( document.createElement('hr') );
						r.attr('id', linkId + "_" + options.values[i]['id'] + "_hr");
						r.addClass("symbol");
						
						var e = $( document.createElement('div') );
						e.attr('id', linkId + "_" + options.values[i]['id'] + "_text");
						e.addClass("question_text");
						e.html(options.values[i]['question']);
						
						var n = $( document.createElement('div') );
						n.attr('id', linkId + "_" + options.values[i]['id'] + "info");
						n.addClass("question_info");
						n.html(options.values[i]['info']);
						
						var f = $( document.createElement('div') );
						f.attr('id', linkId + "_" + options.values[i]['id'] + "_answer");
						f.addClass("question_answer");
						
						d.append(e);
						d.append(n);
						
						// Radio (single select question type)
						if(parseInt(options.values[i]['type']) == 1){
							
							var answers = 0;
							
							for (j in options.values[i]['answers']){
								answers ++;
							}
							
							for(j=0;j<answers;j++){
								var g = $( document.createElement('input') )
									.attr({
										'type' : 'radio',
										'id' : linkId + "_answer_" + options.values[i]['id'] + "_" + options.values[i]['answers'][j]['id'],
										'name' : linkId + "_answer_" + options.values[i]['id'],
										'value' : options.values[i]['answers'][j]['value']
									})
									.removeAttr('checked');
								
								var t = $( document.createElement('span') );
								t
									.attr('id', linkId + "_test_" + options.values[i]['id'])
									.html(options.values[i]['answers'][j]['text']);
								options.objID.append(t)
								
								var text_width = $("#" + linkId + "_test_" + options.values[i]['id']).width();
								$("#" + linkId + "_test_" + options.values[i]['id']).remove();
								
								var h = $( document.createElement('label') )
									.attr('for', linkId + "_answer_" + options.values[i]['id'] + "_" + options.values[i]['answers'][j]['id'])
									.html(options.values[i]['answers'][j]['text'])
									.css('width', (text_width + 45) + "px")
									.addClass('ui-btn-small-no-corners');
								
								f.append(g);
								f.append(h);
							}
						
						}
						// Text entry question
						else if(parseInt(options.values[i]['type']) == 2){
							var g = $( document.createElement('textarea') )
								.attr({
									'id' : linkId + "_answer_" + options.values[i]['id'],
									'name' : linkId + "_answer_" + options.values[i]['id']
								})
								.addClass('question_answer_box');
							
							f.append(g);
						}
						// Multi-select question type
						else if(parseInt(options.values[i]['type']) == 3){
							
							var answers = 0;
							
							for (j in options.values[i]['answers']){
								answers ++;
							}
							
							var g = $( document.createElement('input') )
								.attr({
									'id' : linkId + "_answer_" + options.values[i]['id'],
									'name' : linkId + "_answer_" + options.values[i]['id'],
									'type': 'hidden'
								});
							
							f.append(g);
							
							for(j=0;j<answers;j++){
								var g = $( document.createElement('input') )
									.attr({
										'type' : 'checkbox',
										'id' : linkId + "_answer_" + options.values[i]['id'] + "_" + options.values[i]['answers'][j]['id'],
										'name' : linkId + "_answer_" + options.values[i]['id'] + "_" + options.values[i]['answers'][j]['id'],
										'value' : options.values[i]['answers'][j]['value']
									})
									.removeAttr('checked');
								
								var t = $( document.createElement('span') );
								t
									.attr('id', linkId + "_test_" + options.values[i]['id'])
									.html(options.values[i]['answers'][j]['text']);
								options.objID.append(t)
								
								var text_width = $("#" + linkId + "_test_" + options.values[i]['id']).width();
								$("#" + linkId + "_test_" + options.values[i]['id']).remove();
								
								var h = $( document.createElement('label') )
									.attr('for', linkId + "_answer_" + options.values[i]['id'] + "_" + options.values[i]['answers'][j]['id'])
									.html(options.values[i]['answers'][j]['text'])
									.css('width', (text_width + 45) + "px")
									.addClass('ui-btn-small-no-corners');
								
								f.append(g);
								f.append(h);
							}
						
						}
						
						$("#" + linkId + "_answer_" + options.values[i]['id'] + "_0").parent().buttonset();
						
						d.append(f);
						c.append(d);
						s.append(r);
						c.append(s);
						$('#' + options.form_name).append(c);						

						if(parseInt(options.values[i]['type']) == 1 || parseInt(options.values[i]['type']) == 3){
							
							
							$("#" + linkId + "_" + options.values[i]['id'] + "_answer input[type!='hidden']")
						    	.button({
							    	icons: {
								    	primary:'ui-icon-radio-on'
									}
						    	})
								.next('label')
								.removeClass('ui-corner-left')
								.addClass('ui-corner-all')
						    	.css("margin-right", "5px")
								.click({param1:i}, function (event) {
									var i = event.data.param1;
									var myself = this;
									setTimeout(function(){
										var no_selections = 0;
										
										if(parseInt(options.values[i]['type']) == 3){
											$('#' + linkId + "_answer_" + options.values[i]['id']).val('');
											$("#" + linkId + "_" + options.values[i]['id'] + "_answer input[type!='hidden']").each(function () {
								                if ($(this).is(':checked')) {
								                	if(parseInt(options.values[i]['type']) == 3){
								                		no_selections ++;
								                	}
								                }
											});
										}
										if(no_selections > parseInt(options.values[i]['selections']) && parseInt(options.values[i]['selections']) > 0){
											$('#' + $(myself).attr('for')).removeAttr("checked");
											$(myself).removeClass('ui-state-active');
										}
										$("#" + linkId + "_" + options.values[i]['id'] + "_answer input[type!='hidden']").each(function () {
							                if ($(this).is(':checked')) {
							                	if(parseInt(options.values[i]['type']) == 3){
							                		$('#' + linkId + "_answer_" + options.values[i]['id']).val($('#' + linkId + "_answer_" + options.values[i]['id']).val() + ',' + $(this).val());
							                	}
							                    $(this).next("label").addClass("white").find(".ui-icon").removeClass("ui-icon-radio-on").addClass("ui-icon-check");
							                } else {
							                	if(parseInt(options.values[i]['type']) == 3){
							                		$('#' + linkId + "_answer_" + options.values[i]['id']).val($('#' + linkId + "_answer_" + options.values[i]['id']).val().replace(',' + $(this).val(), ''));
							                	}
							                    $(this).next("label").removeClass("white").find(".ui-icon").removeClass("ui-icon-check").addClass("ui-icon-radio-on");
							                }
							            });
									}, 100);
						    	});
							
							$("#" + linkId + "_" + options.values[i]['id'] + "_answer").buttonset();
							
						}
						
						if(parseInt(options.values[i]['dependant_q']) > 0){
							
							if(parseInt(options.values[i]['type']) == 1 || parseInt(options.values[i]['type']) == 3){
								$("#" + linkId + "_surround_" + options.values[i]['id']).hide();
								
								$("#" + linkId + "_" + options.values[i]['dependant_q'] + "_answer input").click({param1: i}, function(event){
									var i = event.data.param1;
									setTimeout(function(){
										if($('#' + linkId + "_answer_" + options.values[i]['dependant_q'] + "_" + options.values[i]['dependant_a']).is(":checked")){
											$("#" + linkId + "_surround_" + options.values[i]['id']).show();
										}else{
											$("#" + linkId + "_surround_" + options.values[i]['id']).hide();
										}
									}, 100);
								});
							}else if(parseInt(options.values[i]['type']) == 2){
								$("#" + linkId + "_surround_" + options.values[i]['id']).hide();
								
								$( '#' + linkId + "_answer_" + options.values[i]['dependant_q']).change({param1: i}, function(event){
									var i = event.data.param1;
									setTimeout(function(){
										if($('#' + linkId + "_answer_" + options.values[i]['dependant_q']).val() != ''){
											$("#" + linkId + "_surround_" + options.values[i]['id']).show();
										}else{
											$("#" + linkId + "_surround_" + options.values[i]['id']).hide();
										}
									}, 100);
								});
							}
						}else{
							$("#" + linkId + "_surround_" + options.values[i]['id']).show();
						}
						
						if(options.values[i]['mandatory'] != ''){
							var arr = new Array();
							arr.push(options.values[i]['id']);
							arr.push(options.values[i]['mandatory']);
							options.mandatory.push(arr);
						}
						
					}
				}
				
				if(count > 0){
					
					var g = $( document.createElement('div') )
						.attr('id', linkId + "_holder")
						.css({
							'display': 'block',
					        'margin-left': 'auto',
					        'margin-right': 'auto',
					        'width' : '200px'
						});
						
					$('#'+options.form_name).append(g);
					
					var g = $( document.createElement('input') );
					g.attr({
						'id' : options.request_var,
						'name' : options.request_var,
						'type' : 'hidden'
					});
						
					$('#'+options.form_name).prepend(g);
					
					var g = $( document.createElement('a') )
						.attr('id', linkId + "_submit")
						.html('<i class="icon-save"></i> Submit');
						
					$('#'+linkId + "_holder").append(g);
					
					
					var g = $( document.createElement('a') )
						.attr('id', linkId + "_cancel")
						.html('<i class="icon-remove-sign"></i> Cancel');
						
					$('#'+linkId + "_holder").append(g);
					
					$("#" + linkId + "_cancel")
						.addClass('ui-button-danger')
						.button()
						.css({
							'float': 'left',
							'display': 'block',
					        'margin-right': '2px',
					        'width' : '65px'
				    	})
				    	.click({param1 : self}, function (event) {
				    		var self = event.data.param1;
				    		var options = self.options;
				    		
				    		$(location).attr('href', '/thanks.php');
				    	});
						
					
					$("#" + linkId + "_submit")
						.addClass('ui-button-success')
						.button()
						.css({
							'float': 'left',
							'display': 'block',
					        'margin-left': '2px',
					        'width' : '65px'
				    	})
						.click({param1 : self}, function (event) {
							
							var self = event.data.param1;
							var options = self.options;
							var can_continue = true;
							var messages = new Array();
							
							$('#' + options.objID.attr('id') + "_submit").hide();
							
							if(options.mandatory.length > 0){
								for(i=0;i<options.mandatory.length;i++) {
									var checked = false;
									$("[name=" + linkId + "_answer_" + options.mandatory[i][0] + "][type='radio']").each(function () {
										if ($(this).is(':checked')) {
						                	checked = true;
						                }
						            });
									
									$("[name=" + linkId + "_answer_" + options.mandatory[i][0] + "][type!='radio']").each(function(){
										if($(this).val() != ''){
											checked = true;
										}
									});
									
									if(checked == false){
										can_continue = false;
										var arr = options.mandatory[i];
										arr.push(linkId + "_answer_" + options.mandatory[i][0]);
										//console.log(linkId + "_answer_" + options.mandatory[i][0]);
										messages.push(options.mandatory[i]);
									}
								}
							}
							
							if(can_continue == true){
								
								$('#'+options.request_var).val(options.save_result);
								$("#processing_dialog").dialog('open');
								$('#processing_dialog_message').show();
								$('#processing_dialog_message').html('Saving your Survey');
								
								$.post(options.page, 
								$("#" + options.form_name).serialize() + "&survey_id=" + options.survey_id + "&tenant_ref=" + options.tenant_ref + "&prefix=" + options.objID.attr('id'), 
								function(data){
									if(data[options.data_var] == options.data_result){
										$(location).attr('href', '/thanks.php');
									}
									$('#processing_dialog_message').html('');
									$('#processing_dialog_message').hide();
									$("#processing_dialog").dialog('close');
								}, 
								"json");
							}else{
								$('#' + options.objID.attr('id') + "_submit").show();
								self.show_error(messages);
							}
				    	});
				}
			}
			
			this._trigger('set');
		},
		
		build_done_dialog: function(message){
			var self = this;
			var options = this.options;

			/* Done Dialog */
			
			var a = $( document.createElement('div') );
			a.attr({
				'id': "done_dialog",
				'title': "Survey"
			});
			a.addClass("dialog_1");
			
			var b = $( document.createElement('p') );
			b.attr({
				'id': 'done_message',
				'style': 'text-align:left;'
			});
			
			a.append(b);
			$('#' + options.form_name).prepend(a);
			
			$("#done_dialog").dialog({  
				modal: true,
				autoOpen: false,
				resizable: false,
			   	closeOnEscape: false,
			   	width: '800px',
			   	open: function(event, ui) {
			   		var dialog = $(event.target).parents(".ui-dialog.ui-widget");

			   		var i = 0;
			   		dialog.find(".ui-dialog-buttonpane").find("button").each(function(){
			   			i++;
			   			if(i==1){
				   			$(this).addClass("ui-button-danger");
			   			}
			   		});
			   		
			   		dialog.find(".ui-dialog-buttonpane").find('button').removeClass('ui-state-focus ui-state-hover ui-state-active');
				},
			   	buttons: {
					Close: function() {
						$(this).dialog('close');
						$(location).attr('href','/');
					}
				}
			});
			
			if(message !== undefined){
				$("#done_message").html(message);
				$("#done_dialog").dialog('open');
			}
		},
		
		build_dialogs: function(){
			var self = this;
			var options = this.options;
			
			self.build_done_dialog();
			
			/* Processing Dialog */
			
			var a = $( document.createElement('div') );
			a.attr({
				'id': "processing_dialog",
				'title': "Processing"
			});
			a.addClass("dialog_1");
			
			var b = $( document.createElement('p') );
			b.attr({
				"style" : "text-align:center;display:none;margin-bottom:10px;",
				"id" : "processing_dialog_message"
			});
			
			var c = $( document.createElement('p') );
			c.attr("style", "text-align:center;");
			c.html('<img src="/images/ajax-loader.gif" />');
			
			a.append(b);
			a.append(c);
			$('#' + options.form_name).prepend(a);
			
			$("#processing_dialog").dialog({  
				modal: true,
				width: '600px',
				resizable: false,
				autoOpen: false,
			   	closeOnEscape: false,
			   	open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
			});
			
			/* Tenant Dialog */
			/*
			var a = $( document.createElement('div') );
			a.attr({
				'id': "tenant_dialog",
				'title': "Tenant Reference"
			});
			a.addClass("dialog_1");
			
			var b = $( document.createElement('input') );
			b
				.attr({
					'type' : 'text',
					'id' : 'tenant_ref_input',
					'name' : 'tenant_ref_input'
				})
				.css({
					'font' : 'inherit',
					'color' : 'inherit',
					'text-align' : 'left',
					'outline' : 'none',
					'cursor' : 'text'
				 })
				 .bind('keypress', function(e){
					var code = (e.keyCode ? e.keyCode : e.which);
					if(code == 13){ 
						self.check();
					}
				 });
			
			var c = $( document.createElement('p') );
			c.attr("style", "text-align:center; font-weight:bold;margin-bottom:15px;");
			c.html('Please enter your Tenant Reference');
			
			a.append(c);
			a.append(b);
			$('#' + options.form_name).prepend(a);
			
			$("#tenant_dialog").dialog({  
				modal: true,
				resizable: false,
				width: '800px',
				autoOpen: false,
			   	closeOnEscape: false,
			   	open: function(event, ui) {
			   		var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			   		$(".ui-dialog-titlebar-close").hide();

			   		var i = 0;
			   		dialog.find(".ui-dialog-buttonpane").find("button").each(function(){
			   			i++;
			   			if(i==1){
				   			$(this).addClass("ui-button-primary");
			   			}
			   		});
			   		dialog.find(".ui-dialog-buttonpane").find('button').removeClass('ui-state-focus ui-state-hover ui-state-active');
				},
			   	buttons: {
					Ok: function() {
						self.check();
					}
				}
			});
			*/
			/* Confirm Dialog */
			/*
			var a = $( document.createElement('div') );
			a.attr({
				'id': "confirm_dialog",
				'title': "Confirmation"
			});
			a.addClass("dialog_1");
			
			var b = $( document.createElement('p') );
			b
				.attr('style', 'font-weight:bold;')
				.html('Please Confirm your Management Company is ');
			
			var c = $( document.createElement('span') );
			c.attr("id", "tenant_man_co_span");
			
			b.append(c);
			a.append(b);
			$('#' + options.form_name).prepend(a);
			
			$("#confirm_dialog").dialog({  
				modal: true,
				resizable: false,
				width: '800px',
				autoOpen: false,
			   	closeOnEscape: false,
			   	open: function(event, ui) {
			   		var dialog = $(event.target).parents(".ui-dialog.ui-widget");
			   		$(".ui-dialog-titlebar-close").hide();

			   		var i = 0;
			   		dialog.find(".ui-dialog-buttonpane").find("button").each(function(){
			   			i++;
			   			if(i==1){
				   			$(this).addClass("ui-button-primary");
			   			}
			   			if(i==2){
				   			$(this).addClass("ui-button-danger");
			   			}
			   		});
			   		dialog.find(".ui-dialog-buttonpane").find('button').removeClass('ui-state-focus ui-state-hover ui-state-active');
				},
			   	buttons: {
					Yes: function() {
						$(this).dialog('close');
						$("#tenant_dialog").dialog('close');
						options.tenant_ref = $('#tenant_ref_input').val();
					},
					No: function() {
						$(this).dialog('close');
						$('#tenant_ref_input').val('');
						$('#tenant_dialog').dialog( 'open' );
					}
				}
			});
			*/
			/* Error Dialog */
			var a = $( document.createElement('div') );
			a.attr({
				'id': "error_dialog",
				'title': "Errors!"
			});
			a.addClass("dialog_1");
			
			var b = $( document.createElement('p') );
			b.attr({
				'id': 'error_message',
				'style': 'text-align:left;'
			});
			
			a.append(b);
			$('#' + options.form_name).prepend(a);
			
			$("#error_dialog").dialog({  
				modal: true,
				autoOpen: false,
				resizable: false,
				width: '800px',
			   	closeOnEscape: false,
				open: function(event, ui) {
					var dialog = $(event.target).parents(".ui-dialog.ui-widget");
					
					var i = 0;
			   		dialog.find(".ui-dialog-buttonpane").find("button").each(function(){
			   			i++;
			   			if(i==1){
				   			$(this).addClass("ui-button-danger");
			   			}
			   		});
			   		
			   		dialog.find(".ui-dialog-buttonpane").find('button').removeClass('ui-state-focus ui-state-hover ui-state-active');
				},
			   	buttons: {
					Close: function() {
						$(this).dialog('close');
					}
				}
			});
			
			if ( options.serial_ref == '' ){
				$("#done_message").html("You are missing part of your survey ref");
				$("#done_dialog").dialog('open');
			}else{
				if(options.serial_ref.length == 8){
					//$('#tenant_dialog').dialog( 'open' );
				}else{
					$('#' + options.objID.attr('id') + '_holder').hide();
				}
			}
		},
		
		check_auto_ref: function(){
			
			var self = this;
			var options = this.options;
			
			$('#'+options.request_var).val(options.check_result);
			$("#processing_dialog").dialog('open');
			$('#processing_dialog_message').show();
			$('#processing_dialog_message').html('Checking your Reference');
			
			$.post(options.page, 
			$("#" + options.form_name).serialize() + "&tenant_ref_input=" + options.tenant_ref + "&serial_ref=" + options.serial_ref, 
			function(data){
				if(data[options.data_var] == options.data_result){
					$("#tenant_dialog").dialog('close');
				}else if(data['reason'] == "done"){
					$("#done_message").html("You have already filled in this survey");
					$("#done_dialog").dialog('open');
				}else{
					options.tenant_ref = '';
					$("#tenant_dialog").dialog('open');
				}
				$('#processing_dialog_message').html('');
				$('#processing_dialog_message').hide();
				$("#processing_dialog").dialog('close');
			}, 
			"json");
			
		},
		
		check: function(){
			
			var self = this;
			var options = this.options;
			
			$('#'+options.request_var).val(options.check_result);
			$("#processing_dialog").dialog('open');
			$('#processing_dialog_message').show();
			$('#processing_dialog_message').html('Checking your Reference');
			
			$.post(options.page, 
			$("#" + options.form_name).serialize() + "&tenant_ref_input=" + $('#tenant_ref_input').val() + "&survey_id=" + options.survey_id + "&serial_ref=" + options.serial_ref, 
			function(data){
				if(data[options.data_var] == options.data_result){
					$('#tenant_man_co_span').html(data[options.manco_var]);
					$("#confirm_dialog").dialog('open');
				}else if(data['reason'] == 'ref'){
					var messages = new Array();
					
					messages[0] = new Array();
					
					messages[0][0] = '';
					messages[0][1] = 'Your tenant reference does not match the survey reference';
					
					self.show_error(messages);
				}
				$('#processing_dialog_message').html('');
				$('#processing_dialog_message').hide();
				$("#processing_dialog").dialog('close');
			}, 
			"json");
			
		},

		show_error: function(messages){
			var self = this;
			var options = this.options;
			
			var error_message = "";
			
			if(messages.length > 0){
				error_message += "<ul>";
			}
			
			try{
				for(r=0;r<messages.length;r++){
					if($.isArray(messages[r])){
						//$('#'+error_fields[r].replace("input", "label").replace("select", "label")).css("color", 'red');
						error_message += "<li>" + messages[r][1] + "</li>";
					}
				}
			}catch(err){
				alert(err.description);
			}
			
			if(messages.length > 0){
				error_message += "</ul>";
			}
			
			$('#error_message').html(error_message);
			
			if (error_message != ""){
				$("#error_dialog").dialog('open');
			}
		},
		
		disable: function(){
			var self = this;
			var options = this.options;
			
			options.disabled = true
			options.objID.slider('disable');
			
			this._trigger('disable');
		},
		
		enable: function(){
			var self = this;
			var options = this.options;
			
			options.disabled = false
			options.objID.slider('enable');
			
			this._trigger('enable');
		}
	});
}( jQuery ) );