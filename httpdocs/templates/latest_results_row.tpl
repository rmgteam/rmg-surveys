<table id="latest_results_row" class="template_include">
	<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="35%">Survey</th>
			<th width="7%">Score</th>
			<th width="9%">Average</th>
			<th width="14%">Completed</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataTables_empty">Loading</td>
			<td class="dataTables_empty">Data</td>
			<td class="dataTables_empty">From</td>
			<td class="dataTables_empty">Server</td>
			<td class="dataTables_empty"></td>
		</tr>
	</tbody>
</table>