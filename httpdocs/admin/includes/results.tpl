<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.rmc_selector.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.staff_selector.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.search_filter.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.slidedeck.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.radio.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.report_builder.js"></script>

<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/flot/jquery.flot.categories.min.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/flot/jquery.flot.navigate.min.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/flot/jquery.flot.selection.min.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/flot/jquery.flot.pie.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/selectors.css" />
<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.search_filter.css" />
<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.radio.css" />
<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/slidedeck.skin.css" />

<script type="text/javascript">
	var survey_id = <?=$_REQUEST["survey_id"]?>;
	var survey_name = '<?=$survey->survey_name;?>';
</script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>admin/includes/results.js"></script>

<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/flot.css" />

<input type="hidden" id="level" name="level" value="rmcs" />
<input type="hidden" id="question_id" name="level" value="0" />
<input type="hidden" id="graph_level" name="graph_level" value="od" />
<input type="hidden" id="graph_value" name="graph_value" value="" />
<input type="hidden" id="graph_previous_value" name="graph_previous_value" value="" />

<div id="intro_group">
	<ul class="dash-menu">
		<li>
			<a href="javascript:;" id="survey_link">
				<i class="icon-file-alt"></i> 
				Per Survey
				<span class="badge badge-info" title="Perfect Score"><?=$perfect_score;?></span>
			</a>
		</li>
		<li>
			<a href="javascript:;" id="question_link">
				<i class="icon-question-sign"></i> 
				Per Question
			</a>
		</li>
		<li>
			<a href="javascript:;" id="dump_link">
				<i class="icon-download-alt"></i> 
				Data Dump
			</a>
		</li>
	</ul>
</div>

<div id="survey_group">
	<form id="form_search" name="form_search"> 
		<div class="record_heading">
			<div class="left">Search Facility</div>
		</div>
		<div id="search_filter">
		</div>
	</form>
	<div id="survey_buttons" class="clearfix button_group">
		<span class="label label-success pull-left">The Perfect Score is <?=$perfect_score;?></span>
		
		<div class="btn-toolbar btn-toolbar2 pull-right">
			<div class="btn-group">
				<a class="btn btn-info" id="data-view-button">
					Data
				</a>
			</div>
			<div class="btn-group">
				<a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
					Graph
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<div class="btn-group">
							<a class="btn btn-info" id="graph-view-button">Column Graph</a>
							<a class="btn btn-info" id="pie-view-button">Doughnut Chart</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div id="data-div">
		<div id="rmcs-div">
			<table class="display" id="results_table" border="0" cellspacing="0" cellpadding="0" width="100%">
				<?=$results_data ?>
			</table>
		</div>
		<div id="tenants-div">
			<table class="display" id="tenants_table" border="0" cellspacing="0" cellpadding="0" width="100%">
				<?=$tenants_data ?>
			</table>
		</div>
		<div class="clearfix button_group">
			<button class="button ui-button-info pull-right" id="data-up-button">Up Level</button>
		</div>
	</div>
	<div id="graph-div">
	</div>
</div>

<div id="question_group">
	<form id="form_question_search" name="form_question_search"> 
		<div class="record_heading">
			<div class="left">Search Facility</div>
		</div>
		<div id="search_filter_question">
		</div>
	</form>
	<div id="question_buttons" class="clearfix button_group">
		<div class="btn-toolbar btn-toolbar2 pull-right">
			<div class="btn-group">
				<a class="btn btn-info" id="data-question-view-button">
					Data
				</a>
			</div>
			<div class="btn-group">
				<a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
					Graph
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<div class="btn-group">
							<a class="btn btn-info" id="graph-question-view-button">Column Graph</a>
							<a class="btn btn-info" id="pie-question-view-button">Doughnut Chart</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div id="data-question-div">
		<table class="display" id="results_question_table" border="0" cellspacing="0" cellpadding="0" width="100%">
			<?=$question_data ?>
		</table>
	</div>
	<div id="graph-question-div">
	</div>
</div>

<div id="dump_group">
	<form id="form_dump_search" name="form_dump_search"> 
		<div id="search_filter_dump">
		</div>
	</form>
</div>
