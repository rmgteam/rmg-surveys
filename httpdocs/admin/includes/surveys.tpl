<script type="text/javascript">	
	var can_create = '<?=$session["create_surveys"];?>';
	var can_delete = '<?=$session["delete_surveys"];?>';
	var can_send = '<?=$session["send_surveys"];?>';
</script>

<script type="text/javascript" src="<?=$UTILS_WEBROOT?>admin/includes/surveys.js"></script>


<div class="clearfix button_group">
	<p style="width:600px;float:left;">Below is a list of surveys that have been created. To view details of a survey, click the appropriate row in the table below.</p>
	<button id="new_survey_button" class="ui-button-small ui-button-info pull-right">Create a new Survey</button>
</div>

<table class="display" id="results_table" border="0" cellspacing="0" cellpadding="0" width="100%">
	<?=$survey_data ?>
</table>
