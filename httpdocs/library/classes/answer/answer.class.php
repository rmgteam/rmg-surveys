<?php
require_once($UTILS_SERVER_PATH."library/classes/core/core.class.php");

class answer extends core {

	var $answer_id;
	var $question_id;
	var $answer_value;
	var $answer_text;
	var $answer_discon;
	
	function __construct($ref=""){	
		$this->answer($ref);
	}
	
	/**
	 * Get Answer information
	 * 
	 * @param	string	$ref			Answer ID
	 */
	function answer($ref=""){
		
		if($ref != ''){
				
			$mysql = new mysql();
				
			$sql = "
			SELECT * 
			FROM survey_answer 
			WHERE
			answer_id = '".$ref."'";
			$result = $mysql->query($sql, 'Get Answer');
			$row = $mysql->fetch_array($result);
		}
		
		$this->answer_id = $row['answer_id'];
		$this->question_id = $row['question_id'];
		$this->answer_value = $row['answer_value'];
		$this->answer_text = $row['answer_text'];
		$this->answer_discon = $row['answer_discon'];		
	}

	/**
	 * Get List of answers
	 * 
	 * @param	id		$question_id	Question ID
	 * @param	array	$request		$_REQUEST object
	 * @return	array	$output			Datatables formatted array
	 */
	function get_list ($question_id, $request){
	
		$mysql = new mysql();
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();
	
		$input = $request;
	
		$search_builder->add_index_column('answer_id');
		$search_builder->add_column('answer_text');
		$search_builder->add_column('answer_value');
		$search_builder->add_button_column('answer_id', 'Delete', "pre_delete_answer", array(), 'button ui-button-info');
		$iColumnCount = count($search_builder->columns);
	
		$search_builder->add_filter('answer_discon', "=", "N", 'AND');
		$search_builder->add_filter('question_id', "=", $question_id, 'AND');
	
		//Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
	
		//Ordering
		$iSortingCols = intval( $request['iSortingCols'] );
		for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
			if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
				$search_builder->add_order($search_builder->columns[intval($request['iSortCol_'.$i])], ($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
	
		//Filtering
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
	
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
	
		if(isset($request['letters'])){
			$search_builder->add_filter('cpm_contractors_letter_sent', "=", $request['letters'], 'AND');
		}
	
		$sql = "
		FROM survey_answer";
	
		$search_builder->add_sql($sql);
	
		$output = $search_builder->output($request);
	
		return $output;
	
	}

	/**
	 * Get Mysql list of answers
	 * 
	 * @param	id		$question_id	Question ID
	 * @return	array	$result			Mysql result
	 */
	function get_answers($id){
		$mysql = new mysql();
		$sql = "SELECT *
		FROM survey_answer
		WHERE
		question_id = '".$id."' AND
		answer_discon = 'N'
		";
		$result = $mysql->query($sql, 'Get Answers');
		return $result;
	}
	
	/**
	 * Get array of answer information
	 * 
	 * @param	int		$answer_id		Answer ID
	 * @return	array	$array			Array of information
	 */
	function get_answer($answer_id = ''){
		if($answer_id != ''){
			$i = 0;
			$array = array();
			
			$mysql = new mysql();
			
			$sql = "SELECT *
			FROM survey_answer 
			WHERE answer_id = '".$answer_id."'";
			$result = $mysql->query($sql, 'Get Question Infor');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				while($row = $mysql->fetch_array($result)){
					$array['id'] = $row['answer_id'];
					$array['answer'] = $row['answer_text'];
					$array['value'] = $row['answer_value'];
					$i++;
				}
			}
			
			return $array;
		}
	}
	
	/**
	 * Get answer text from value/survey
	 * 
	 * @param	int		$answer_value		Answer Value
	 * @param	int		$question_id		Question ID
	 * @return	array	$array				Array of information
	 */
	function get_answer_text($answer_value = '', $question_id = ''){
		
		$name = '';
		
		if($answer_value != ''){
			$i = 0;
			$array = array();
			
			$mysql = new mysql();
			
			$sql = "SELECT *
			FROM survey_answer ";
			
			if(strpos($answer_value, ',') !== false){
				$ids = explode(',', $answer_value);
				$sql .= "
				WHERE (";
				
				foreach($ids as $id){
					if($id != ""){
						$sql .= "
						answer_value = '".$id."' OR";
					}
				}
				
				$sql = substr($sql, 0, -3) . "
				)";
			}else{
				$sql .= "
				WHERE answer_value = '".$answer_value."'";
			}
			
			$sql .= "
			AND question_id = '".$question_id."'";
			
			$result = $mysql->query($sql, 'Get Question Infor');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				while($row = $mysql->fetch_array($result)){
					$name .= $row['answer_text'] . ',';
				}
				return substr($name, 0, -1);
			}else{
				return $answer_value;
			}
			
		}else{
			return $answer_value;
		}
	}
	
	/**
	 * Check Data has been entered
	 * 
	 * @param	array	$request		$_REQUEST object
	 * @return	array	$results_array	Array of message, field, boolean if errored
	 */
	function check($request){
	
		$error_fields = array();
		$security = new security();
		$data = new data;
		$field = new field();
		$error_counter = 0;
		
		$is_valid = true;
			
		// check if the answer name is filled in, if not show error
			
		$error_message[$error_counter] = "Please enter an answer";
		$error_fields[$error_counter] = "answer_name_label";
		
		if ($request["answer_name_input"] == ""){
			$is_valid = false;
			$error_has[$error_counter] = "true";
		}else{
			$error_has[$error_counter] = "false";
		}
			
		$error_counter++;
			
		// check if answer value is filled in, if not show error
			
		$error_message[$error_counter] = "Please enter the value of the question";
		$error_fields[$error_counter] = "answer_value_label";
		
		if ($request["answer_value_input"] == ""){
			$is_valid = false;
			$error_has[$error_counter] = "true";
		}else{
			$error_has[$error_counter] = "false";
		}
			
		$error_counter++;
	
		if($is_valid == true){
			$results_array["save_result"] = "success";
			$results_array["message"] = implode("|", $error_message);
			$results_array["fields"] = implode("|", $error_fields);
			$results_array["errored"] = implode("|", $error_has);
		}else{
			$results_array["save_result"] = "error";
			$results_array["message"] = implode("|", $error_message);
			$results_array["fields"] = implode("|", $error_fields);
			$results_array["errored"] = implode("|", $error_has);
		}
		
		return $results_array;
	}
	
	/**
	 * Save answer
	 * 
	 * @param	array	$request		$_REQUEST object
	 * @return	array	$results_array	Array of whether it completed and ID of record
	 */
	function save($request){
		
		try{
	
			$mysql = new mysql();
			$security = new security();
			$data = new data;
			
			//if id is filled in then we are editing, if not it is a new record
			if($request['answer_id'] == ''){
				$sql = "INSERT INTO survey_answer SET
				question_id = '" . $request['question_id'] . "', ";
			}else{
				$sql = "UPDATE survey_answer SET";
			}
			
			$sql_middle = "
			answer_text = '". $security->clean_query($request['answer_name_input']) . "',
			answer_value = '". $security->clean_query($request['answer_value_input']) . "'";
			
			$has_error=false;
			
			$last_id = "";
			
			if($request['answer_id'] == ''){
				// insert record
				$sql .= $sql_middle;
				$msg = 'Insert New Answer';
			}else{
				// insert record
				$sql .= $sql_middle.'
				WHERE answer_id = "'.$request['answer_id'].'"';
				$msg = 'Update Answer';
				$last_id = $request['answer_id'];
			}
			
			$has_error = $mysql->insert($sql, $msg);
			
			//if there was no issues inserting, return the id of the new tracker
			if (!is_bool($has_error) && $request['answer_id'] == ''){
				$last_id = $has_error;
			}
			
			if (!is_bool($has_error)){
				$results_array["save_result"] = "success";
				$results_array["id"] = $last_id;
			}else{
				$results_array["save_result"] = "Technical fault - this record failed to save.";
			}
				
			return $results_array;	
			
		}catch(Exception $e){
			$results_array["save_result"] = "Technical fault - this record failed to save.";
			$results_array["sql"] = $e->getMessage();
			return $results_array;
		}
	}
	
	/**
	 * Delete Answer
	 * 
	 * @return	boolean					Has answer been deleted
	 */
	function delete(){
	
		$mysql = new mysql();
	
		$sql = "
		UPDATE survey_answer SET 
		answer_discon = 'Y' 
		WHERE 
		answer_id = '".$this->answer_id."'";
		
		$result = $mysql->query($sql, 'Delete answer');
		if( $result !== false ){
			return true;
		}
		return false;
	}
	
}



?>