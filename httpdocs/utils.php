<?
if($start_session !== false){
	session_start();
}
header('Cache-Control: no-cache');
header('Pragma: no-cache');

//========================================================
// Determines where to look for standard include files
//========================================================

$url = "http://" . $_SERVER['SERVER_NAME'];
$parsed = parse_url($url);
$domain = explode('.', $parsed['host']);

$subdomain = '';

if ($domain[0] == 'www'){
	$subdomain = $domain[1];
}else{
	$subdomain = $domain[0];
}
if($subdomain == "rmgsurveys"){
	$subdomain = '';
}

$db_location = "";
$fs_location = "";

Global $UTILS_WEBROOT;
Global $UTILS_LOG_PATH;
Global $UTILS_WEBROOT;
Global $UTILS_URL_BASE;
Global $UTILS_PAGE;
Global $UTILS_EMAIL;
Global $conn;

$UTILS_TEL_MAIN = "0345 002 4444";
$UTILS_TEL_MAIN_FAX = '0345 002 4455';
$UTILS_EMAIL = "customerservice@rmg.gb.com";
$UTILS_CCC_PHONE = "0345 002 4444";
$UTILS_WEBROOT = "/";
$UTILS_SUPPORT_EMAIL = "service.desk@rmgltd.co.uk";
$UTILS_PAGE = str_replace('.php','',basename($_SERVER['PHP_SELF']));
$UTILS_PAGE = strtolower( str_replace("/admin/", "", $UTILS_PAGE) );
$UTILS_PAGE = strtolower( str_replace("/", "", $UTILS_PAGE) );

$UTILS_SERVER_PATH = "D:/sites/rmgsurveys.co.uk/httpdocs/";
$UTILS_CLASS_PATH = "D:/sites/rmgsurveys.co.uk/httpdocs/library/classes/";
$UTILS_URL_BASE = "http://www.rmgsurveys.co.uk/";
$UTILS_LOG_PATH = "D:/sites/rmgsurveys.co.uk/private/logs/";

$db_location = "live";
$fs_location = "rmgsurveys.co.uk";

if( isset($argv) ){

	if($argv[1] != ""){

		$subdomain = $argv[1];
	}
}
	
if($subdomain != ''){
	$UTILS_SERVER_PATH = "D:/sites/" . $subdomain . ".rmgsurveys.co.uk/httpdocs/";
	$UTILS_CLASS_PATH = "D:/sites/" . $subdomain . ".rmgsurveys.co.uk/httpdocs/library/classes/";
	$UTILS_URL_BASE = "http://" . $subdomain . ".rmgsurveys.co.uk/";
	$UTILS_LOG_PATH = "D:/sites/" . $subdomain . ".rmgsurveys.co.uk/private/logs/";
	
	$db_location = "dev";
	$fs_location = $subdomain . ".rmgsurveys.co.uk";
}

require_once($UTILS_SERVER_PATH."library/classes/mysql/mysql.class.php");
require_once($UTILS_SERVER_PATH."library/classes/template/survey_template.class.php");

$conn = '';
	
$mysql = new mysql();

// Connect to Intranet database
if($db_location == "dev"){
	
	$conn = $mysql->connect("localhost","admin","cwc5my5ql");
	$mysql->select_db("cpm_living_dev");
	
}elseif($db_location == "live"){

	$conn = $mysql->connect("localhost","admin","cwc5my5ql");
	$mysql->select_db("cpm_living");
}

?>