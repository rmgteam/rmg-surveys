<?php
require_once("utils.php");

require_once($UTILS_SERVER_PATH."library/classes/survey/survey.class.php");
require_once($UTILS_SERVER_PATH."library/classes/question/question.class.php");
require_once($UTILS_SERVER_PATH."library/classes/answer/answer.class.php");
require_once($UTILS_SERVER_PATH."library/classes/template/admin_template.class.php");
require_once($UTILS_SERVER_PATH."library/classes/user/user.class.php");
require_once($UTILS_SERVER_PATH."library/classes/shorturl/shorturl.class.php");

if(!isset($_SESSION['admin_user_serial']) || $_SESSION['admin_user_serial'] == ''){
	$admin_user = new user($_REQUEST['user_serial'], "serial");
}else{
	$admin_user = new user($_SESSION['admin_user_serial'], "serial");
}

// Check if logged in
if($admin_user->survey_user_serial == ''){
	header("Location: /admin/login.php");
	exit;
}
$survey = new survey($_REQUEST['survey_id'], "id");

	
if($_REQUEST['a'] == 'questions'){

	$mysql = new mysql();
	$i = 0;
	$result_array = array();
	
	$survey_question = new question();
	
	$result_array = $survey_question->get_list($_REQUEST['survey_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
	
}elseif($_REQUEST['a'] == 'answers'){

	$mysql = new mysql();
	$i = 0;
	$result_array = array();
	
	$survey_answer = new answer();
	
	$result_array = $survey_answer->get_list($_REQUEST['question_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
}elseif($_REQUEST['a'] == 'type'){

	$mysql = new mysql();
	$i = 0;
	$result_array = array();
	
	$result = $survey->get_types();
	$num_rows = $mysql->num_rows($result);
	if($num_rows > 0){
		$result_array[$i]['name'] = ' - Please Select - ';
		$result_array[$i]['value'] = '';
		$i++;
		while($row = $mysql->fetch_array($result)){
			$result_array[$i]['name'] = $row['type_string'];
			$result_array[$i]['value'] = $row['type_id'];
			$i++;
		}
	}
	
	echo json_encode($result_array);
	exit;
}elseif($_REQUEST['a'] == 'question'){

	$survey_question = new question();
	
	$result_array = $survey_question->get_question_info($_REQUEST['question_id']);
	
	echo json_encode($result_array);
	exit;
}elseif($_REQUEST['a'] == 'answer'){

	$survey_answer = new answer();
	
	$result_array = $survey_answer->get_answer($_REQUEST['answer_id']);
	
	echo json_encode($result_array);
	exit;
}elseif($_REQUEST['a'] == 'survey'){
	$result_array['survey_name'] = $survey->survey_name;
	$result_array['survey_id'] = $survey->survey_id;
	$result_array['survey_once'] = $survey->survey_once;
	$result_array['survey_director'] = $survey->survey_director;
	$result_array['survey_info'] = $survey->survey_desc;
	$result_array['survey_frequency'] = $survey->survey_frequency;
	$result_array['survey_frequency_string'] = $survey->survey_frequency_string;
	$result_array['survey_amount'] = $survey->survey_amount;
	
	echo json_encode($result_array);
	exit;
}elseif($_REQUEST['a'] == 'save'){	
	
	$save_result = array();
	$save_result['save_result'] = "success";
	$save_result['id'] = "";
	
	$save_result = $survey->check($_REQUEST);
	if($save_result['save_result'] == "success"){
		$save_result = $survey->save($_REQUEST);
	}

	echo json_encode($save_result);
	exit;
}elseif($_REQUEST['a'] == 'save_question'){	

	$survey_question = new question();
	
	$save_result = array();
	$save_result['save_result'] = "success";
	$save_result['id'] = "";
	
	$save_result = $survey_question->check($_REQUEST);
	if($save_result['save_result'] == "success"){
		$save_result = $survey_question->save($_REQUEST);
	}

	echo json_encode($save_result);
	exit;
}elseif($_REQUEST['a'] == 'save_question_order'){	

	$survey_question = new question();
	
	$result_array = array();
	$result_array['save_result'] = "fail";
	$save_result = $survey_question->save_question_order($_REQUEST);
	if($save_result === true){
		$result_array['save_result'] = "success";
	}
	echo json_encode($result_array);
	exit;
}elseif($_REQUEST['a'] == 'save_answer'){	

	$survey_answer = new answer();
	
	$save_result = array();
	$save_result['save_result'] = "success";
	$save_result['id'] = "";
	$save_result = $survey_answer->check($_REQUEST);
	if($save_result['save_result'] == "success"){
		$save_result = $survey_answer->save($_REQUEST);
	}
	
	echo json_encode($save_result);
	exit;	
}elseif($_REQUEST['a'] == 'delete_question'){
	
	$question = new question($_REQUEST['s']);
	$result_array = array();
	$result_array['save_result'] = "fail";

	$save_result = $question->delete();
	if( $save_result === true ){
		$result_array['save_result'] = "success";
	}

	echo json_encode($result_array);
	exit;
}elseif($_REQUEST['a'] == 'delete_answer'){
	
	$answer = new answer($_REQUEST['s']);
	$result_array = array();
	$result_array['save_result'] = "fail";

	$save_result = $answer->delete();
	if( $save_result === true ){
		$result_array['save_result'] = "success";
	}

	echo json_encode($result_array);
	exit;
}elseif ($_REQUEST['a'] == 'letter_search'){
	
	$tenant = new tenant('');
	
	$request = $_REQUEST;
	$request['letters'] = 'N';
	
	$result_array = $tenant->get_list($request, $_REQUEST['survey_id']);
	
	echo json_encode($result_array);
	exit;
	
}elseif($_REQUEST['a'] == 'letters'){
	
	Global $UTILS_EMAIL;
	Global $UTILS_SERVER_PATH;
	
	$template = new Template();
	$result_array = array();
		
	$result_array['sent'] = 'N';
	
	$sql = "SELECT r.* 
	FROM cpm_residents r
	INNER JOIN cpm_residents_extra e ON e.resident_num = r.resident_num
	WHERE r.resident_is_active = '1'
	AND r.resident_email <> ''
	AND e.survey_optout = 'N'";
	
	$residents = $_REQUEST['letter_table_residents'];
	
	$tenant_array = explode(",", $residents);
	
	if(count($tenant_array) > 0){
	
		$sql_clause = '';
	
		foreach($tenant_array as $resident){
			if($resident != ''){
				$sql_clause .= "
				resident_num = '".$resident."' OR";
			}
		}
	
		$sql_clause = substr($sql_clause, 0, -3);

		if($sql_clause != ''){
			$sql .= "
			AND (" . $sql_clause . "
			)";
		}
	}
	
	$survey = new survey($_REQUEST['survey_id'], "id");
	if($survey->survey_director == 'Y'){
		$sql .= "
		AND is_resident_director = 'Y'";
	}
	if($sql_clause == ''){
		$sql .= "
		AND rmc_num = '".$_REQUEST['rmc_num']."'";
	}
	
	$result = $mysql->query($sql, 'Get Post Contractors');
	$num_rows = $mysql->num_rows($result);
	if($num_rows > 0){	
		while($row = $mysql->fetch_array($result)){
	
			$tenant = new tenant($row['resident_num']);
			$survey_resident = new survey_resident();		
			$survey = new survey($_REQUEST['survey_id'], 'id');
			
			$request = array();
			$request['survey_id'] = $_REQUEST['survey_id'];
			$request['tenant_num'] = $tenant->tenant_num;
			
			if($survey_resident->can_send($survey->survey_id, $tenant->tenant_num)){
				$results_array = $survey_resident->create($request);
				
				$survey_resident = new survey_resident($results_array["id"]);
				
				$survey_resident_serial = $survey_resident->survey_resident_serial;
				$survey_ref = $survey->survey_serial;
				$letter_type = '1';
			
				$survey_url = "http://www.rmgsurveys.co.uk/survey/".$survey_ref."/".$survey_resident_serial;
				
				$short_url = new shorturl();
				
				$short_url_string = "http://www.rmgsurveys.co.uk/".$short_url->addHashByString($survey_url);
				
				$text = $template->fetch($UTILS_SERVER_PATH."templates/email_text.php", get_defined_vars());
		
				$headers = 'From: ' .$UTILS_EMAIL . "\r\n";
				$headers .= 'Reply-To: ' . $UTILS_EMAIL;
				
				$email = new mailer();
				$email->set_mail_type("Welcome Email");
				$email->set_mail_to($tenant->tenant_email);
				//$email->set_mail_to("marc.evans@rmgltd.co.uk");
				//$email->set_mail_to("clive.gannon@rmgltd.co.uk");
				$email->set_mail_from($UTILS_EMAIL);
				$email->set_mail_subject("Welcome to RMG Surveys");
				$email->set_mail_html($text);
				$email->set_mail_headers($headers);
				$email->send();
				
				$result_array['sent'] = 'Y';
			}
		}
		$result_array['sql'] = $sql;
	}
	
	$result_array['success'] = 'Y';
	
	echo json_encode($result_array);
	exit;
	
}elseif($_REQUEST['a'] == 'all_email'){
	
	$cmd 							= '"D:\\PHP\\v5.4\\php.exe" "'.$UTILS_SERVER_PATH.'sched_tasks\\all_emails.php" survey_id='.$_REQUEST['survey_id'];
	
	// Exectute script
	if (substr(php_uname(), 0, 7) == "Windows"){
		$WshShell 					= new COM("WScript.Shell");
			
		$oExec 						= $WshShell->Exec('cmd /C "'.$cmd.'"');
			
		//$oExec						= exec('psexec.exe -accepteula -i -d "'.$cmd.'" 2>&1');
			
		return $oExec;
	}
	else {
		exec($cmd . " > /dev/null &");
	}
	
	echo json_encode($result_array);
	exit;
	
}else{
	
	$title = 'Survey - ' . $survey->survey_name;
	$icon = 'file-alt';
	$tpl = new admin_template(get_defined_vars());
	$tpl->set( 'question_data', $tpl->set_datatable($UTILS_SERVER_PATH."templates/question_row.tpl") );
	$tpl->set( 'answer_data', $tpl->set_datatable($UTILS_SERVER_PATH."templates/answer_row.tpl") );
	$tpl->set( 'order_data', $tpl->set_datatable($UTILS_SERVER_PATH."templates/order_row.tpl") );
	$tpl->set( 'letter_data', $tpl->set_datatable($UTILS_SERVER_PATH."templates/letter_row.tpl") );
	$tpl->set( 'session', $_SESSION );
	
	echo $tpl->fetch();
}
?>