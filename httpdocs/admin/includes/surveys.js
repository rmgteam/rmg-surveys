var asInitVals = new Array();
var myApp = myApp || {}; 

$(document).ready(function(){
	
	build_survey_list();
	
	bind_events();
		
    var page_crumbs = {
    	0 : 	{ 'url' 	: "/admin/",		'text'	: "Dashboard" },
    	1 : 	{ 'url'	: "/admin/surveys.php",	'text'	: "Surveys" }
    };
    gen_crumbs(page_crumbs); 

});

function bind_events(){
    
	$('#new_survey_button')
		.button()
		.click(function(){
			do_new_survey();
		});
}

function build_survey_list() {

	var processor = $('body').processing('start', 'Building Survey List', 33);
	
	var sort_array = new Array();

	sort_array = [[ 0 , 'asc' ]];
	
	if(can_create == 'Y'){
		$('#new_survey_button').show();
	}else{
		$('#new_survey_button').hide();
	}
	
	if(can_delete == 'Y'){
		var aoColumns = [
 			{ bVisible: true },
 			{ bVisible: true },
 			{ bVisible: true }
 		];
	}else{
		var aoColumns = [
			{ bVisible: true },
			{ bVisible: true },
			{ bVisible: false }
		];
	}

	$('#results_table').show();
	
	myApp.table = $('#results_table')
		.dataTable({
			"bJQueryUI": true,
			"bProcessing": false,
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bDestroy": true,
			"bDeferRender": true,
	        "oLanguage": {
		       	"sInfoFiltered": "",
		       	"sSearch":"Search Columns Below: ",
	            "sLengthMenu": "Show _MENU_"
	        },
	        "aoColumns" : aoColumns,
			"sPaginationType": "full_numbers",
	        "bServerSide": true,
	        "sAjaxSource": $(location).attr('href'),
	        "sServerMethod": "POST",
	        "fnServerData": function ( sSource, aoData, fnCallback ) {
	        	$('#a').val('search')

	        	$('body').processing('update', processor, 66);
	        	
	        	aoData.push({name : 'a', value : 'surveys'});

	        	$.post(sSource, 
	        	aoData, 
				function(json){
	        		 fnCallback(json);
				}, 
				"json");
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				var id = $(nRow).attr('id');
				var no_tds = 0;
				
				$(nRow).find('td').each(function(){
					if(no_tds != 2){
						$(this).bind('click', {'id':id}, function(e){
							var id = e.data['id'];
							
							$(location).attr('href', '/admin/survey/' + id + '/');
						});
					}

					no_tds++;
				});
	        },
	        "fnDrawCallback": function( oSettings ) {

	        	$('body').processing('end', processor);
	        	$('.button').not('.ui-button').button();
	        },
	        "aaSorting": sort_array
		});


	if(myApp.table === undefined){
		$("#results_table tfoot input").each( function (i) {
	        asInitVals[i] = this.value;
	    } );
	     
	    $("#results_table tfoot input").bind('focus', function () {
	        if ( this.className == "search_init" )
	        {
	            this.className = "";
	            this.value = "";
	        }
	    } );
	     
	    $("#results_table tfoot input").bind('blur', function (i) {
	        if ( this.value == "" ){
	            this.className = "search_init";
	            this.value = asInitVals[$("#results_table tfoot input").index(this)];
	        }
	    });

	    $("#results_table tfoot input").bind('keypress', function(e){
	    	var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13){ 
        		myApp.table.fnFilter( this.value, $("#results_table tfoot input").index(this) );
			}
	    });

	    $('#results_table_wrapper .dataTables_filter input')
		    .unbind('keypress keyup')
		    .bind('keypress', function(e){
		    	var code = (e.keyCode ? e.keyCode : e.which);
			    if (code == 13) {
			    	myApp.table.fnFilter($(this).val());
			    }
		    });
		    
	}
}

function pre_delete_survey(s){
	
	$("#msg_dialog").dialog("option", "title", "Confirm...");
	$('#msg_dialog_message').html("Are you sure you want to delete this survey?");
	$("#msg_dialog").dialog({
		buttons : [
			{
				text: 'Yes, continue',
				click: function (){
					$(this).dialog('close');	
					delete_survey(s);
				}
			},
			{
				text: 'No',
				click: function (){
					$(this).dialog('close');	
				}
			}
		]
	});
	$("#msg_dialog").dialog('open');
}

function delete_survey(s){


	var processor = $('body').processing('start', 'Deleting Survey');
	$.post($(location).attr('href'), 
	$('#question-form').serialize() + "&a=delete_survey&s=" + s,
	function(data){
	
		if(data['save_result'] == 'success'){
			build_survey_list();
		}

		$('body').processing('end', processor);
		
	},
	'json');
}


function do_new_survey(){

	reset_survey();
	form_edit('survey');

	$('#survey_name_input').val('');
	$('#survey_info_input').val('');
	$('#survey_once_select').selectpicker('clear');
	$('#survey_name_span').html('');
	$('#survey_info_span').html('');
	$('#survey_once_span').html('');
	survey_callback = function(){
		build_survey_list();
	};
	
	$('#survey_button_save').show();
	$('#survey_button_cancel').show();
	$('#survey_button_edit').hide();
	
	$("#survey-dialog").dialog('open');
}