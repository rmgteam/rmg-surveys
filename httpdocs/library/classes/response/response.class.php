<?php
require_once($UTILS_SERVER_PATH."library/classes/core/core.class.php");
require_once($UTILS_SERVER_PATH."library/classes/rmc.class.php");

class response extends core {

	var $response_id;
	var $survey_resident_id;
	var $question_id;
	var $response;
	
	function __construct($ref=""){	
		$this->response($ref);
	}
	
	/**
	 * Get response
	 *
	 * @param	string	$response_id	Response ID
	 */
	function response($response_id){
	
		$mysql = new mysql;
	
		$sql = "
		SELECT *
		FROM survey_response
		WHERE
		response_id = '".$response_id."'";
		$result = $mysql->query($sql, 'Get Answer');
		$num_rows = $mysql->num_rows($result);
		if( $num_rows > 0 ){
			while($row = $mysql->fetch_array($result)){
				$this->response_id = $row['response_id'];
				$this->survey_resident_id = $row['survey_resident_id'];
				$this->question_id = $row['question_id'];
				$this->response = $row['response'];
			}
		}
	}
	
	/**
	 * Get response by question and survey_resident
	 *
	 * @param	string	$survey_resident_id		Survey Resident ID
	 * @param	string	$question_id			Question ID
	 */
	function response_by_question_survey_resident($survey_resident_id, $question_id){
	
		$mysql = new mysql;
	
		$sql = "
		SELECT *
		FROM survey_response
		WHERE survey_resident_id = '".$survey_resident_id."'
		AND question_id = '".$question_id."'";
		$result = $mysql->query($sql, 'Get Answer');
		$num_rows = $mysql->num_rows($result);
		if( $num_rows > 0 ){
			while($row = $mysql->fetch_array($result)){
				$this->response($row['response_id']);
			}
		}
	}
	
	/**
	 * Get responses for survey_resident
	 *
	 * @param	string		$survey_resident_id		Survey Resident ID
	 * @return	Resource	$result					MySQL Resource
	 */
	function responses_by_survey_resident($survey_resident_id){
	
		$mysql = new mysql;
	
		$sql = "SELECT *,
		(
			r.response * q.question_weight
		) as weighting
		FROM survey_response r
		INNER JOIN survey_question q ON q.question_id = r.question_id
		WHERE survey_resident_id = '".$survey_resident_id."'";
		$result = $mysql->query($sql, 'Get Answer');
		return $result;
	}
	
	/**
	 * Get total score for specific survey_resident
	 *
	 * @param	string	$survey_resident_id		Survey Resident ID
	 * @return	int								Score
	 */
	function survey_resident_score($survey_resident_id){
	
		$mysql = new mysql;
	
		$sql = "SELECT
		SUM(
			r.response * q.question_weight
		) as weighting
		FROM survey_response r
		INNER JOIN survey_question q ON q.question_id = r.question_id
		WHERE survey_resident_id = '".$survey_resident_id."'";
		$result = $mysql->query($sql, 'Get Answer');
		$num_rows = $mysql->num_rows($result);
		if( $num_rows > 0 ){
			while($row = $mysql->fetch_array($result)){
				return $row['weighting'];
			}
		}else{
			return 0;
		}
		
	}
	
	/**
	 * Get total score for each survey_resident
	 *
	 * @param	string		$survey_id			Survey  ID
	 * @return	Resource	$result				MySQL Resource
	 */
	function each_survey_resident_scores($survey_id){
	
		$mysql = new mysql;
	
		$sql = "SELECT
		sr.resident_num,
		SUM(
			r.response * q.question_weight
		) as weighting
		FROM survey_response r
		INNER JOIN survey_question q ON q.question_id = r.question_id
		INNER JOIN survey_resident sr ON r.survey_resident_id = sr.survey_resident_id
		WHERE sr.survey_id = '".$survey_id."'
		GROUP BY r.survey_resident_id";
		$result = $mysql->query($sql, 'Get Answer');
		return $result;
		
	}
	
	/**
	 * Get total score for survey
	 *
	 * @param	string		$survey_id			Survey  ID
	 * @return	int								Score
	 */
	function perfect_survey_score($survey_id){
		
		$mysql = new mysql;
		
		$sql = "SELECT 
		SUM(
			(
				SELECT max(CAST(answer_value AS unsigned)) max_id
				FROM (
					SELECT * 
					FROM survey_answer
					WHERE answer_discon = 'N'
					ORDER BY answer_value DESC 
				) recent_users 
				WHERE question_id = q.question_id
				GROUP BY question_id
			) 
			* q.question_weight
		) as weighting
		FROM survey_question q
		WHERE q.survey_id = '".$survey_id."'";
		$result = $mysql->query($sql, 'Get Answer');
		$num_rows = $mysql->num_rows($result);
		if( $num_rows > 0 ){
			while($row = $mysql->fetch_array($result)){
				return $row['weighting'];
			}
		}else{
			return 0;
		}
	}
	
	/**
	 * Get Data for property level
	 * 
	 * @param	int			$survey_id			Survey ID
	 * @param	array		$request			$_REQUEST object
	 * @param	boolean		$tenant				Tenant Table (true|false)
	 * @return	Object		$search_builder		search_builder Object
	 */
	function get_rmc_data($survey_id, $request, $tenant=false){
		
		$mysql = new mysql();
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();
		$no_fields = $request['no_fields'];
		$sql_sub_query = '';

		$values_array = $search_builder->request_to_array($request, 'search_filter_');
		
		$input = $request;
		
		if ($no_fields > 0 ){
			foreach($values_array as $key=>$value) {
		
				$search_term = $value;
		
				switch($key){
					case "from":
						$field = 'sr.survey_resident_sent_date';
						$search_builder->add_filter($field, ">=", $data->date_to_ymd( $search_term ), 'AND');
						$sql_sub_query .= "
						AND survey_resident_sent_date >= '" . $data->date_to_ymd( $search_term ) . "'";
						break;
					case "to":
						$field = 'sr.survey_resident_sent_date';
						$search_builder->add_filter($field, "<=", $data->date_to_ymd( $search_term ), 'AND');
						$sql_sub_query .= "
						AND survey_resident_sent_date <= '" . $data->date_to_ymd( $search_term ) . "'";
						break;
					default:
						$field = $key;
						break;
				}
			}
		}

		$search_builder->add_index_column('rmc.rmc_num');
		
		if($tenant == false){
			$search_builder->add_column('lrmc.rmc_ref');
		}
		
		if($request['graph_level'] == 'od'){
			
			$search_builder->add_column('rmc.rmc_op_director_name');
			$sql_level = "AND rm.rmc_op_director_name = rmc.rmc_op_director_name"; 
			
		}elseif($request['graph_level'] == 'rm'){
			
			$search_builder->add_column('rmc.regional_manager');
			$sql_level = "AND rm.regional_manager = rmc.regional_manager";
			
		}elseif($request['graph_level'] == 'pm'){
			
			$search_builder->add_column('rmc.property_manager');
			$sql_level = "AND rm.property_manager = rmc.property_manager";
			
		}elseif($request['graph_level'] == 'tenant'){

			$search_builder->add_column('lres.resident_ref');
			$search_builder->add_column('cr.resident_name');
			$sql_level = "AND res.resident_num = cr.resident_num";
			
		}elseif($request['graph_level'] == 'rmc'){
			$search_builder->add_column('rmc.rmc_name');
			$sql_level = "AND res.rmc_num = rmc.rmc_num";
			
		}

		$sql_query = "
		IFNULL(
			ROUND(
				SUM(r.response * q.question_weight)
				/
				(
					SELECT COUNT(*)
					FROM survey_resident sres
					INNER JOIN cpm_residents res ON res.resident_num = sres.resident_num
					INNER JOIN cpm_rmcs rm ON rm.rmc_num = res.rmc_num
					WHERE survey_id = sr.survey_id
					".$sql_level.$sql_sub_query."
					AND sres.survey_resident_complete_ymdhis <> ''
				)
			, 2),
			'-' 
		)";
		
		$search_builder->add_subquery_column('average_score', $sql_query);

		$sql_query = "
		IFNULL(
			CONCAT(
				ROUND(
					(
						(
							SUM(r.response * q.question_weight)
							/
							(
								SELECT COUNT(*)
								FROM survey_resident sres
								INNER JOIN cpm_residents res ON res.resident_num = sres.resident_num
								INNER JOIN cpm_rmcs rm ON rm.rmc_num = res.rmc_num
								WHERE survey_id = sr.survey_id
								".$sql_level.$sql_sub_query."
								AND sres.survey_resident_complete_ymdhis <> ''
							)
							/
							(
								SELECT 
								SUM(
									(
										SELECT max(CAST(answer_value AS unsigned)) max_id
										FROM (
											SELECT * 
											FROM survey_answer
											WHERE answer_discon = 'N'
											ORDER BY answer_value DESC 
										) recent_users 
										WHERE question_id = qu.question_id
										GROUP BY question_id
									) 
									* qu.question_weight
								) as weighting
								FROM survey_question qu
								WHERE qu.survey_id = sr.survey_id
							)
						)
						* 100
					), 2
				), '%'
			),
			'-'
		)";
		
		$search_builder->add_subquery_column('percentage', $sql_query);
		
		if($tenant == false){
			$sql_query = "
			SELECT COUNT(*)
			FROM survey_resident sres
			INNER JOIN cpm_residents res ON res.resident_num = sres.resident_num
			INNER JOIN cpm_rmcs rm ON rm.rmc_num = res.rmc_num
			WHERE survey_id = sr.survey_id
			".$sql_level.$sql_sub_query."";
			
			$search_builder->add_subquery_column('no_sent', $sql_query);
			
			$sql_query = "
			SELECT COUNT(*)
			FROM survey_resident sres
			INNER JOIN cpm_residents res ON res.resident_num = sres.resident_num
			INNER JOIN cpm_rmcs rm ON rm.rmc_num = res.rmc_num
			WHERE survey_id = sr.survey_id
			".$sql_level.$sql_sub_query."
			AND sres.survey_resident_complete_ymdhis <> ''";
			
			$search_builder->add_subquery_column('no_received', $sql_query);
			
			$sql_query = "
			CONCAT(
				ROUND(
					(
						(
							SELECT COUNT(*)
							FROM survey_resident sres
							INNER JOIN cpm_residents res ON res.resident_num = sres.resident_num
							INNER JOIN cpm_rmcs rm ON rm.rmc_num = res.rmc_num
							WHERE survey_id = sr.survey_id
							".$sql_level.$sql_sub_query."
							AND sres.survey_resident_complete_ymdhis <> ''
						) /
						(
							SELECT COUNT(*)
							FROM survey_resident sres
							INNER JOIN cpm_residents res ON res.resident_num = sres.resident_num
							INNER JOIN cpm_rmcs rm ON rm.rmc_num = res.rmc_num
							WHERE survey_id = sr.survey_id
							".$sql_level.$sql_sub_query."
						)
					) * 100
				, 2)
			, '%')";
			
			$search_builder->add_subquery_column('s_v_r', $sql_query);
		}
		
		$iColumnCount = count($search_builder->columns);
		
		$search_builder->add_filter('sr.survey_id', "=", $survey_id, 'AND');
		
		if($request['graph_level'] == 'rm'){
			$search_builder->add_filter('rmc.rmc_op_director_name', "=", $request['graph_value'], 'AND');
		}
		
		if($request['graph_level'] == 'pm'){
			$search_builder->add_filter('rmc.regional_manager', "=", $request['graph_value'], 'AND');
		}
		
		if($request['graph_level'] == 'rmc'){
			$search_builder->add_filter('rmc.property_manager', "=", $request['graph_value'], 'AND');
		}
		
		if($request['graph_level'] == 'tenant'){
			$search_builder->add_filter('lrmc.rmc_ref', "=", $request['graph_value'], 'AND');
		}
		
		// Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
		
		// Ordering
		$iSortingCols = intval( $request['iSortingCols'] );
		for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
			if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
				$search_builder->add_order($search_builder->columns[intval($request['iSortCol_'.$i])], ($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
		
		// Filtering
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
		
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
		
		$sql = "
		FROM survey_resident sr
		LEFT JOIN survey_response r ON r.survey_resident_id = sr.survey_resident_id
		LEFT JOIN survey_question q ON q.question_id = r.question_id
		INNER JOIN cpm_residents cr ON cr.resident_num = sr.resident_num
		INNER JOIN cpm_rmcs rmc ON rmc.rmc_num = cr.rmc_num
		INNER JOIN cpm_lookup_rmcs lrmc ON lrmc.rmc_lookup = cr.rmc_num
		INNER JOIN cpm_lookup_residents lres ON lres.resident_lookup = cr.resident_num";
		
		$search_builder->add_sql($sql);


		if($request['graph_level'] == 'od'){
			$search_builder->add_group('rmc.rmc_op_director_name');
		}elseif($request['graph_level'] == 'rm'){
			$search_builder->add_group("rmc.regional_manager");
		}elseif($request['graph_level'] == 'pm'){
			$search_builder->add_group("rmc.property_manager");
		}elseif($request['graph_level'] == 'tenant'){
			$search_builder->add_group("cr.resident_num");
		}else{
			$search_builder->add_group("rmc.rmc_num");
		}
		
		return $search_builder;
	}
	
	/**
	 * Get DataTables Results at property level
	 * 
	 * @param	int			$survey_id			Survey ID
	 * @param	array		request				$_REQUEST object
	 * @return	array		$output				Datatables formatted array
	 */
	function get_rmc_results($survey_id, $request){
		
		if($request['graph_level'] == 'tenant'){
			$search_builder = $this->get_rmc_data($survey_id, $request, true);
		}else{
			$search_builder = $this->get_rmc_data($survey_id, $request);
		}
		
		$rmc = new rmc;
		
		$output = $search_builder->output($request);
		
		$average = 0;
		$percentage = 0;
		$sent = 0;
		$received = 0;
		$svr = 0;
		
		$count1 = 0;
		$count2 = 0;
		
		for($i=0;$i<count($output['aaData']);$i++){
			
			$data = $output['aaData'][$i];
			
			if($request['graph_level'] == 'od'){
				$output['previous_value'] = '';
			}elseif($request['graph_level'] == 'rm'){
				$output['previous_value'] = '';
			}elseif($request['graph_level'] == 'pm'){
				$output['previous_value'] = $rmc->get_od_from_pm($data[1]);
			}elseif($request['graph_level'] == 'tenant'){
				$output['previous_value'] =  $rmc->get_pm_from_resident_ref($data[0]);
			}elseif($request['graph_level'] == 'rmc'){
				$output['previous_value'] = $rmc->get_rm_from_rmc_ref($data[0]);
			}
			
			if($data[2] != '-'){
				$average += $data[2];
				$count1 ++;
			}
			if($data[3] != '-'){
				$percentage += $data[3];
				$count2 ++;
			}
			$sent += $data[4];
			$received += $data[5];
			$svr += $data[6];
			
		}
		
		if($request['graph_level'] == 'od'){
			$row = array();
			$row['0'] = '0';
			$row['1'] = 'Total';
			$row['2'] = round($average / $count1, 2);
			$row['3'] = round($percentage / $count2, 2) . "%";
			$row['4'] = $sent;
			$row['5'] = $received;
			$row['6'] = round(($received / $sent) * 100, 2) . "%";
			$output['aaData'][] = $row;
		}
		
		return $output;
	}
	
	/**
	 * Get Graph at property level
	 * 
	 * @param	int			$survey_id			Survey ID
	 * @param	array		request				$_REQUEST object
	 * @return	array		$result_array		Flot formatted array
	 */
	function get_rmc_graph($survey_id, $request){
		
		$mysql = new mysql;
		$rmc = new rmc;
		
		$search_builder = $this->get_rmc_data($survey_id, $request);
		$sql_main = $search_builder->build_sql();
		$result = $mysql->query($sql_main, 'Get Results');
		
		$output = array(
				"label"		=>		'Property Results',
				"data"		=>		array(),
				"color"		=>		'#5BC0DE'
		);
		
		while($aRow = $mysql->fetch_array($result)){
			$row = array();
			
			if($request['graph_level'] == 'od'){
				$output['previous_value'] = '';
				$row[0] = $aRow['rmc_op_director_name'];
			}elseif($request['graph_level'] == 'rm'){
				$output['previous_value'] = '';
				$row[0] = $aRow['regional_manager'];
			}elseif($request['graph_level'] == 'pm'){
				$output['previous_value'] = $rmc->get_od_from_pm($aRow['property_manager']);
				$row[0] = $aRow['property_manager'];
			}elseif($request['graph_level'] == 'tenant'){
				$output['previous_value'] =  $rmc->get_pm_from_resident_ref($aRow['resident_ref']);
				$row[0] = $aRow['resident_name'];
			}elseif($request['graph_level'] == 'rmc'){
				$output['previous_value'] = $rmc->get_rm_from_rmc_ref($aRow['rmc_ref']);
				$row[0] = $aRow['rmc_ref'];
			}
			
			$row[1] = str_replace('%', '', $aRow['percentage']);
			$output['data'][] = $row;
		}
		
		return $output;
	}
	
	/**
	 * Get Question Data for property level
	 * 
	 * @param	int			$survey_id			Survey ID
	 * @param	array		request				$_REQUEST object
	 * @param	boolean		$graph				graph (true|false)
	 * @return	Object		$search_builder		search_builder Object
	 */
	function get_rmc_question_data($survey_id, $request, $graph=false){
		
		$rmc = new rmc;
		
		$mysql = new mysql();
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();
		$no_fields = $request['no_fields'];
		$sql_sub_query = '';

		$values_array = $search_builder->request_to_array($request, 'search_filter_question_');
		
		$input = $request;
		
		if ($no_fields > 0 ){
			foreach($values_array as $key=>$value) {
		
				$search_term = $value;
				$type = '';
		
				switch($key){
					case "rmc":
						$field = "rmc.rmc_num";
						$type = "equals";
						$rmc_num = $security->clean_query( $search_term );
						$sql_sub_query .= "
						AND rm.rmc_num = '" . $security->clean_query( $search_term ) . "'";
						break;
					case "from":
						$field = 'sr.survey_resident_sent_date';
						$search_builder->add_filter($field, ">=", $data->date_to_ymd( $search_term ), 'AND');
						$sql_sub_query .= "
						AND survey_resident_sent_date >= '" . $data->date_to_ymd( $search_term ) . "'";
						break;
					case "to":
						$field = 'sr.survey_resident_sent_date';
						$search_builder->add_filter($field, "<=", $data->date_to_ymd( $search_term ), 'AND');
						$sql_sub_query .= "
						AND survey_resident_sent_date <= '" . $data->date_to_ymd( $search_term ) . "'";
						break;
					case "od":
						$field = $rmc->short_type_to_db('od');
						$type = "equals";
						$sql_sub_query .= "
						AND $field = '" . $security->clean_query( $search_term ) . "'";
						break;
					case "rm":
						$field = $rmc->short_type_to_db('rm');
						$type = "equals";
						$sql_sub_query .= "
						AND $field = '" . $security->clean_query( $search_term ) . "'";
						break;
					case "pm":
						$field = $rmc->short_type_to_db('pm');
						$type = "equals";
						$sql_sub_query .= "
						AND $field = '" . $security->clean_query( $search_term ) . "'";
						break;
					default:
						$field = $key;
						break;
				}
				
				if($type == "equals"){
					$search_builder->add_filter($field, "=", $security->clean_query( $search_term ), 'AND');
				}
			}
		}
		
		$input = $request;

		$search_builder->add_index_column('q.question_id');
		$search_builder->add_column('q.question_text');

		$sql_query = "
		(
			SELECT a.answer_text
			FROM survey_response sres
			INNER JOIN survey_resident sre ON sre.survey_resident_id = sres.survey_resident_id
			INNER JOIN cpm_residents res ON res.resident_num = sre.resident_num
			INNER JOIN cpm_rmcs rm ON rm.rmc_num = res.rmc_num
			INNER JOIN survey_answer a ON a.answer_value = sres.response AND sres.question_id = a.question_id
			WHERE sres.question_id = q.question_id
			". $sql_sub_query ."
			GROUP BY sres.response
			ORDER BY COUNT(sres.response) DESC
			LIMIT 1
		)";
		
		$search_builder->add_subquery_column('mode_value', $sql_query);
		
		$sql_query = "				
		ROUND(
			SUM(r.response)
			/
			COUNT(r.response)
		, 2)";
		
		$search_builder->add_subquery_column('average_score', $sql_query);
		
		if($graph){
			$search_builder->add_column('a.answer_text');
		}

		$sql_query = "
		IFNULL(
			(
				CONCAT(
					ROUND(
						(
							(
								(
									SELECT COUNT(sres.response)
									FROM survey_response sres
									INNER JOIN survey_resident sre ON sre.survey_resident_id = sres.survey_resident_id
									INNER JOIN cpm_residents res ON res.resident_num = sre.resident_num
									INNER JOIN cpm_rmcs rm ON rm.rmc_num = res.rmc_num
									WHERE sres.question_id = q.question_id
									AND sres.response = a.answer_value
									". $sql_sub_query ."
									LIMIT 1
								)
								/
								(
									SELECT COUNT(sres.response)
									FROM survey_response sres
									INNER JOIN survey_resident sre ON sre.survey_resident_id = sres.survey_resident_id
									INNER JOIN cpm_residents res ON res.resident_num = sre.resident_num
									INNER JOIN cpm_rmcs rm ON rm.rmc_num = res.rmc_num
									WHERE sres.question_id = q.question_id
									". $sql_sub_query ."
								)
							)
							* 100
						), 2
					), '%'
				)
			),
			'0%'
		)";
		
		$search_builder->add_subquery_column('percentage', $sql_query);
		
		$iColumnCount = count($search_builder->columns);
		
		$search_builder->add_filter('sr.survey_id', "=", $survey_id, 'AND');
		
		// Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
		
		// Filtering
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
		
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
		
		$sql = "
		FROM survey_response r
		INNER JOIN survey_question q ON q.question_id = r.question_id
		INNER JOIN survey_answer a ON a.question_id = q.question_id
		INNER JOIN survey_resident sr ON r.survey_resident_id = sr.survey_resident_id
		INNER JOIN cpm_residents cr ON cr.resident_num = sr.resident_num
		INNER JOIN cpm_rmcs rmc ON rmc.rmc_num = cr.rmc_num
		INNER JOIN cpm_lookup_rmcs lrmc ON lrmc.rmc_lookup = cr.rmc_num
		INNER JOIN cpm_lookup_residents lres ON lres.resident_lookup = cr.resident_num";
		
		$search_builder->add_sql($sql);
		
		if($graph){
			$search_builder->add_group("a.answer_id");
		}else{
			$search_builder->add_group("q.question_id");
		}
		
		$search_builder->add_order("q.question_order", 'asc');
		
		if($graph){
			$search_builder->add_order("CAST(a.answer_text AS UNSIGNED)", 'asc');
			$search_builder->add_order("a.answer_text", 'asc');
		}
		
		return $search_builder;
	}
	
	/**
	 * Get DataTables Results at property level
	 * 
	 * @param	int			$survey_id			Survey ID
	 * @param	array		request				$_REQUEST object
	 * @return	array		$output				Datatables formatted array
	 */
	function get_rmc_question_results($survey_id, $request){
		
		$search_builder = $this->get_rmc_question_data($survey_id, $request);
		$rmc = new rmc;
		
		$sql_main = $search_builder->build_sql();
		$output = $search_builder->output($request);
		$output['sql'] = $sql_main;
		
		for($i=0;$i<count($output['aaData']);$i++){
			
			$data = $output['aaData'][$i];
			
			if($request['graph_level'] == 'od'){
				$output['previous_value'] = '';
			}elseif($request['graph_level'] == 'rm'){
				$output['previous_value'] = '';
			}elseif($request['graph_level'] == 'pm'){
				$output['previous_value'] = $rmc->get_od_from_pm($data[1]);
			}elseif($request['graph_level'] == 'tenant'){
				$output['previous_value'] =  $rmc->get_pm_from_resident_ref($data[0]);
			}elseif($request['graph_level'] == 'rmc'){
				$output['previous_value'] = $rmc->get_rm_from_rmc_ref($data[0]);
			}
			
		}
		
		return $output;
	}
	
	/**
	 * Get DataTables Results at property level
	 * 
	 * @param	int			$survey_id			Survey ID
	 * @param	array		request				$_REQUEST object
	 * @return	array		$result_array		Flot formatted array
	 */
	function get_rmc_question_graph($survey_id, $request){
		
		$mysql = new mysql;
		
		$search_builder = $this->get_rmc_question_data($survey_id, $request, true);
		$sql_main = $search_builder->build_sql();
		$result = $mysql->query($sql_main, 'Get Results');
		
		$result_array = array();
		$output = array();
		
		$question_text = '';
		
		while($aRow = $mysql->fetch_array($result)){
		
			if($question_text != $aRow['question_text']){
				
				if($question_text != ''){
					$result_array[] = $output;
				}
				
				$output = array(
						"label"		=>		$aRow['question_text'],
						"data"		=>		array(),
						"color"		=>		'#5BC0DE'
				);
			}
			
			$row = array();
			
			$row[0] = $aRow['answer_text'];			
			$row[1] = str_replace('%', '', $aRow['percentage']);
			$output['data'][] = $row;
			
			$question_text = $aRow['question_text'];
		}
		
		$result_array[] = $output;
		
		return $result_array;
	}

	/**
	 * Get Data for sent Vs Rec'd
	 *
	 * @param	array		$request			$_REQUEST object
	 * @return	Object		$search_builder		search_builder Object
	 */
	function get_sent_recd_data($request){
	
		$mysql = new mysql;
	
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();
		$sql_sub_query = '';
	
		$input = $request;
	
		
		if($request['sent_recd_from_input'] != ''){
			$field = 'sr.survey_resident_sent_date';
			$search_builder->add_filter($field, ">=", $data->date_to_ymd( $request['sent_recd_from_input'] ), 'AND');
			$field = 'SUBSTRING(sr.survey_resident_complete_ymdhis, 1, 8)';
			$search_builder->add_filter($field, ">=", $data->date_to_ymd( $request['sent_recd_from_input'] ), 'AND');
		}
		
		if($request['sent_recd_to_input'] != ''){
			$field = 'sr.survey_resident_sent_date';
			$search_builder->add_filter($field, "<=", $data->date_to_ymd( $request['sent_recd_to_input'] ), 'AND');
			$field = 'SUBSTRING(sr.survey_resident_complete_ymdhis, 1, 8)';
			$search_builder->add_filter($field, "<=", $data->date_to_ymd( $request['sent_recd_to_input'] ), 'AND');
		}
		
	
		$search_builder->add_column('sr.survey_resident_sent_date');
		$search_builder->add_column('sr.survey_resident_complete_ymdhis');
		$search_builder->add_column('sr.survey_resident_click_ymdhis');
		
		$sql_query = "
		SELECT COUNT(survey_resident_sent_date)
		FROM survey_resident sres
		WHERE sres.survey_resident_sent_date = sr.survey_resident_sent_date";
		
		$search_builder->add_subquery_column('no_sent', $sql_query);
		
		$sql_query = "
		SELECT COUNT(survey_resident_sent_date)
		FROM survey_resident sres
		WHERE sres.survey_resident_sent_date = sr.survey_resident_sent_date
		AND sres.survey_resident_complete_ymdhis = sr.survey_resident_complete_ymdhis
		AND sres.survey_resident_complete_ymdhis <> ''";
		
		$search_builder->add_subquery_column('no_received', $sql_query);
		
		$sql_query = "
		SELECT COUNT(survey_resident_sent_date)
		FROM survey_resident sres
		WHERE sres.survey_resident_sent_date = sr.survey_resident_sent_date
		AND sres.survey_resident_click_ymdhis = sr.survey_resident_click_ymdhis
		AND sres.survey_resident_click_ymdhis <> ''";
		
		$search_builder->add_subquery_column('no_click', $sql_query);
	
		$iColumnCount = count($search_builder->columns);
	
		// Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
	
		// Ordering
		$iSortingCols = intval( $request['iSortingCols'] );
		for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
			if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
				$search_builder->add_order($search_builder->columns[intval($request['iSortCol_'.$i])], ($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
	
		// Filtering
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
	
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
	
		$sql = "
		FROM survey_resident sr";
	
		$search_builder->add_sql($sql);
	
		$search_builder->add_group("sr.survey_resident_complete_ymdhis");
		$search_builder->add_group("sr.survey_resident_sent_date");
		
		$search_builder->add_order('sr.survey_resident_sent_date','asc');
		$search_builder->add_order('sr.survey_resident_complete_ymdhis','asc');
	
		return $search_builder;
	}
	
	/**
	 * Get Graph Results for sent vs received
	 * 
	 * @param	array		request				$_REQUEST object
	 * @return	array		$result_array		Flot formatted array
	 */
	function get_sent_recd_graph( $request){
		
		$mysql = new mysql;
		$data = new data;
		
		$number_sent = 0;
		$number_recd = 0;
		
		$from_date = '';
		
		$sql_main = "SELECT (
			SELECT COUNT(survey_resident_sent_date)
			FROM survey_resident sres
			WHERE sres.survey_resident_sent_date = sr.survey_resident_sent_date
		) as no_sent, 
		(
			SELECT COUNT(survey_resident_sent_date)
			FROM survey_resident sres
			WHERE sres.survey_resident_sent_date = sr.survey_resident_sent_date
			AND sres.survey_resident_complete_ymdhis = sr.survey_resident_complete_ymdhis
			AND sres.survey_resident_complete_ymdhis <> ''
		) as no_received, 
		(
			SELECT COUNT(survey_resident_sent_date)
			FROM survey_resident sres
			WHERE sres.survey_resident_sent_date = sr.survey_resident_sent_date
			AND sres.survey_resident_click_ymdhis = sr.survey_resident_click_ymdhis
			AND sres.survey_resident_click_ymdhis <> ''
		) as no_click, 
		sr.survey_resident_sent_date, 
		sr.survey_resident_complete_ymdhis, 
		sr.survey_resident_click_ymdhis
		FROM survey_resident sr
		WHERE sr.survey_resident_sent_date < '" . $data->date_to_ymd( $request['sent_recd_from_input'] ) . "' 
		AND SUBSTRING(sr.survey_resident_complete_ymdhis, 1, 8) < '" . $data->date_to_ymd( $request['sent_recd_from_input'] ) . "' 
		GROUP BY sr.survey_resident_complete_ymdhis, sr.survey_resident_sent_date
		ORDER BY sr.survey_resident_sent_date desc, sr.survey_resident_complete_ymdhis desc";
		$result = $mysql->query($sql_main, 'Get Results');
		$num_rows = $mysql->num_rows($result);
		if($num_rows > 0){
			$aRow = $mysql->fetch_array($result);
			$number_sent = $aRow['no_sent'];
			$number_recd = $aRow['no_received'];
			$number_click = $aRow['no_click'];
		}
		
		$search_builder = $this->get_sent_recd_data($request);
		$sql_main = $search_builder->build_sql();
		$result = $mysql->query($sql_main, 'Get Results');
		$num_rows = $mysql->num_rows($result);
		
		$result_array = array();
		$temp_array = array();
		$output = array();
		
		$sent_date = '';
		$recd_date = '';
		$click_date = '';
		$no_sent = 0;
		$no_recd = 0;
		$no_click = 0;
		$no_sent = $number_sent;
		$no_recd = $number_recd;
		$no_click = $number_click;
		$perc = 0;
		
		$output[] = array(
				"label"		=>		'Sent vs Received',
				"data"		=>		array(),
				"color"		=>		'#5BC0DE'
		);
		
		$output[] = array(
				"label"		=>		'Sent vs Visited',
				"data"		=>		array(),
				"color"		=>		'#fbb450'
		);
		
		$diff = $data->find_time($data->date_to_ymd($request['sent_recd_from_input'], 'U'), '%d', $data->date_to_ymd($request['sent_recd_to_input'], 'U'));
		$current_date = $data->date_to_ymd($request['sent_recd_from_input']);
		
		for($j=0;$j<=$diff;$j++){
			$row = array();
			$date = $data->ymd_to_date($current_date);
			$date = strtotime($data->date_to_ymd($date, 'Y-m-d') . " UTC") * 1000;
			$row[0] = $date;
			if($j==0){
				$row[1] = $no_recd;
				$row[2] = $no_sent;
				$row[3] = $no_click;
			}else{
				$row[1] = 0;
				$row[2] = 0;
				$row[3] = 0;
			}
			$temp_array[] = $row;
			
			$current_date = $data->date_plus_days($current_date, 1, 'Ymd', 'Ymd');
		}
		
		if($num_rows > 0){
			while($aRow = $mysql->fetch_array($result)){
				
				if($sent_date == ''){
					$recd_date = $aRow['survey_resident_sent_date'] . '000000';
				}
				
				if($sent_date != $aRow['survey_resident_sent_date']){
					
					$date = $data->ymd_to_date($aRow['survey_resident_sent_date']);
					$date = strtotime($data->date_to_ymd($date, 'Y-m-d') . " UTC") * 1000;
					
					$parent_key = $this->find_key($temp_array, 0, $date);
	
					$no_sent += $aRow['no_sent'];
					
					$temp_array[$parent_key][2] = $no_sent;
					
				}
					
				$sent_date = $aRow['survey_resident_sent_date'];
			}
			
			$current_date = $data->date_to_ymd($request['sent_recd_from_input']);
			
			$last_value = 0;
			
			for($j=0;$j<=$diff;$j++){
				if($temp_array[$j][2] == "0"){
					$temp_array[$j][2] = $last_value;
				}else{
					$last_value = $temp_array[$j][2];
				}
			}
	
			$mysql->data_seek($result);
			
			while($aRow = $mysql->fetch_array($result)){
					
				if($recd_date != $aRow['survey_resident_complete_ymdhis'] && $recd_date != ''){
			
					$date = $data->ymdhis_to_date($recd_date);
					$date = strtotime($data->date_to_ymd($date, 'Y-m-d') . " UTC") * 1000;
			
					$parent_key = $this->find_key($temp_array, 0, $date);
			
					$temp_array[$parent_key][1] = $no_recd;
				}
					
				$no_recd += $aRow['no_received'];
			
				$recd_date = $aRow['survey_resident_complete_ymdhis'];
			}
			
			$date = $data->ymdhis_to_date($recd_date);
			$date = strtotime($data->date_to_ymd($date, 'Y-m-d') . " UTC") * 1000;
			
			$parent_key = $this->find_key($temp_array, 0, $date);
			
			$temp_array[$parent_key][1] = $no_recd;
			
			$current_date = $data->date_to_ymd($request['sent_recd_from_input']);
			
			$last_value = 0;
			
			for($j=0;$j<=$diff;$j++){			
				if($temp_array[$j][1] == "0"){
					$temp_array[$j][1] = $last_value;
				}else{
					$last_value = $temp_array[$j][1];
				}
			}
	
			$mysql->data_seek($result);
			
			while($aRow = $mysql->fetch_array($result)){
					
				if($click_date != $aRow['survey_resident_click_ymdhis'] && $click_date != ''){
			
					$date = $data->ymdhis_to_date($click_date);
					$date = strtotime($data->date_to_ymd($date, 'Y-m-d') . " UTC") * 1000;
			
					$parent_key = $this->find_key($temp_array, 0, $date);
			
					$temp_array[$parent_key][3] = $no_click;
				}
					
				$no_click += $aRow['no_click'];
			
				$click_date = $aRow['survey_resident_click_ymdhis'];
			}
			
			$date = $data->ymdhis_to_date($click_date);
			$date = strtotime($data->date_to_ymd($date, 'Y-m-d') . " UTC") * 1000;
			
			$parent_key = $this->find_key($temp_array, 0, $date);
			
			$temp_array[$parent_key][3] = $no_click;
			
			$current_date = $data->date_to_ymd($request['sent_recd_from_input']);
			
			$last_value = 0;
			
			for($j=0;$j<=$diff;$j++){			
				if($temp_array[$j][3] == "0"){
					$temp_array[$j][3] = $last_value;
				}else{
					$last_value = $temp_array[$j][3];
				}
			}
			
			for($j=0;$j<count($temp_array);$j++){
				$output[0]['data'][$j] = array();
				$output[1]['data'][$j] = array();
				
				$output[0]['data'][$j][0] = $temp_array[$j][0];
				$output[1]['data'][$j][0] = $temp_array[$j][0];
				
				if($temp_array[$j][2] == 0){
					$output[0]['data'][$j][1] = 0;
					$output[1]['data'][$j][1] = 0;
				}else{
					$output[0]['data'][$j][1] = round(((intval($temp_array[$j][1]) / intval($temp_array[$j][2])) * 100), 2);
					
					$output[1]['data'][$j][1] = round(((intval($temp_array[$j][3]) / intval($temp_array[$j][2])) * 100), 2);
				}
			}			
		}
		
		return $output;
	}
	
	/**
	 * Find key within multidimensional array
	 * 
	 * @param	array		$haystack			array
	 * @param	int/string	$key				which key should we be looking in
	 * @param	int/string	$needle				value of key
	 * @return	int/string	$parent_key			key record is in
	 */
	function find_key($haystack, $key, $needle){
		foreach($haystack as $parent_key => $value){
			if ( $value[$key] === $needle )
				return $parent_key;
		}
		return false;
	}

	/**
	 * Get Data for company wide
	 *
	 * @param	array		$request			$_REQUEST object
	 * @return	Object		$search_builder		search_builder Object
	 */
	function get_all_survey_data($request){
	
		$mysql = new mysql();
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();
		$sql_sub_query = '';
	
		$input = $request;
	
		if($request['survey_perc_from_input'] != ''){
			$field = 'SUBSTRING(sr.survey_resident_complete_ymdhis, 1, 8)';
			$search_builder->add_filter($field, ">=", $data->date_to_ymd( $request['survey_perc_from_input'] ), 'AND');
			$sql_sub_query .= "
			AND SUBSTRING(sr.survey_resident_complete_ymdhis, 1, 8) >= '" . $data->date_to_ymd( $request['survey_perc_from_input'] ) . "'";
		}
		
		if($request['survey_perc_to_input'] != ''){
			$field = 'SUBSTRING(sr.survey_resident_complete_ymdhis, 1, 8)';
			$search_builder->add_filter($field, "<=", $data->date_to_ymd( $request['survey_perc_to_input'] ), 'AND');
			$sql_sub_query .= "
			AND SUBSTRING(sr.survey_resident_complete_ymdhis, 1, 8) <= '" . $data->date_to_ymd( $request['survey_perc_to_input'] ) . "'";
		}
	
		$search_builder->add_index_column('sr.survey_id');
		$search_builder->add_column('lres.resident_ref');
		$search_builder->add_column('cr.resident_name');
		$sql_sub_query .= "
		AND res.resident_num = cr.resident_num";
	
		$sql_query = "
		ROUND(
			SUM(r.response * q.question_weight)
			/
			(
				SELECT COUNT(*)
				FROM survey_resident sres
				INNER JOIN cpm_residents res ON res.resident_num = sres.resident_num
				WHERE survey_id = sr.survey_id
				AND sres.survey_resident_complete_ymdhis <> ''
				".$sql_sub_query."
			)
		, 2)";
	
		$search_builder->add_subquery_column('average_score', $sql_query);
	
		$sql_query = "
		CONCAT(
			ROUND(
				(
					(
						SUM(r.response * q.question_weight)
						/
						(
							SELECT COUNT(*)
							FROM survey_resident sres
							INNER JOIN cpm_residents res ON res.resident_num = sres.resident_num
							WHERE survey_id = sr.survey_id
							AND sres.survey_resident_complete_ymdhis <> ''
							".$sql_sub_query."
						)
						/
						(
							SELECT
							SUM(
								(
									SELECT max(CAST(answer_value AS unsigned)) max_id
									FROM (
										SELECT *
										FROM survey_answer
										WHERE answer_discon = 'N'
										ORDER BY answer_value DESC
									) recent_users
									WHERE question_id = qu.question_id
									GROUP BY question_id
								)
								* qu.question_weight
							) as weighting
							FROM survey_question qu
							WHERE qu.survey_id = sr.survey_id
						)
					)
					* 100
				), 2
			), '%'
		)";
	
		$search_builder->add_subquery_column('percentage', $sql_query);
	
		$iColumnCount = count($search_builder->columns);
	
		// Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
	
		// Ordering
		$iSortingCols = intval( $request['iSortingCols'] );
		for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
			if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
				$search_builder->add_order($search_builder->columns[intval($request['iSortCol_'.$i])], ($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
	
		// Filtering
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
	
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
	
		$sql = "
		FROM survey_response r
		INNER JOIN survey_question q ON q.question_id = r.question_id
		INNER JOIN survey_resident sr ON r.survey_resident_id = sr.survey_resident_id
		INNER JOIN cpm_residents cr ON cr.resident_num = sr.resident_num
		INNER JOIN cpm_rmcs rmc ON rmc.rmc_num = cr.rmc_num
		INNER JOIN cpm_lookup_rmcs lrmc ON lrmc.rmc_lookup = cr.rmc_num
		INNER JOIN cpm_lookup_residents lres ON lres.resident_lookup = cr.resident_num";
	
		$search_builder->add_sql($sql);
		
		$search_builder->add_group("cr.resident_num");
	
		return $search_builder;
	}
	

	/**
	 * Get Graph at property level
	 *
	 * @param	array		request				$_REQUEST object
	 * @return	array		$result_array		Flot formatted array
	 */
	function get_all_survey_graph($request){
	
		$mysql = new mysql;
		$rmc = new rmc;
	
		$search_builder = $this->get_all_survey_data($request);
		$sql_main = $search_builder->build_sql();
		$result = $mysql->query($sql_main, 'Get Results');
		
		$temp_array = array();
	
		$output = array(
				"label"		=>		'Quality of Survey Results',
				"data"		=>		array(),
				"color"		=>		'#5BC0DE',
				"sql"		=>		$sql_main
		);
		
		$data = 0;
		$data_0 = 0;
		$data_30 = 0;
		$data_60 = 0;
		
		while($aRow = $mysql->fetch_array($result)){
			$perc = str_replace('%', '', $aRow['percentage']);
			
			if($perc < 30){
				$data_0 ++;
			}elseif($perc > 60){
				$data_60 ++;
			}else{
				$data_30 ++;
			}
			
			$data++;
		}
		
		$row = array();
		$row[0] = "< 30%";
		$row[1] = (($data_0 / $data) * 100);
		$row[2] = '#ee5f5b';
		$output['data'][] = $row;
		
		$row = array();
		$row[0] = "30% - 60%";
		$row[1] = (($data_30 / $data) * 100);
		$row[2] = '#fbb450';
		$output['data'][] = $row;
		
		$row = array();
		$row[0] = "> 60%";
		$row[1] = (($data_60 / $data) * 100);
		$row[2] = '#62c462';
		$output['data'][] = $row;
	
		return $output;
	}
	
	/**
	 * Get Question Data for property level
	 * 
	 * @param	array		request				$_REQUEST object
	 * @return	Object		$search_builder		search_builder Object
	 */
	function get_latest_results_data($request){
		
		$rmc = new rmc;
		
		$mysql = new mysql();
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();
		$sql_sub_query = '';

		$search_builder->add_column('cr.resident_name');
		$search_builder->add_column('s.survey_name');
		
		$sql_query = "
		(
			SELECT SUM(res.response * q.question_weight)	
			FROM survey_response res
			INNER JOIN survey_question q ON q.question_id = res.question_id
			INNER JOIN survey_resident sres ON res.survey_resident_id = sres.survey_resident_id
			INNER JOIN cpm_residents cres ON cres.resident_num = sres.resident_num
			WHERE cres.resident_num = cr.resident_num
		)";
		
		$search_builder->add_subquery_column('score', $sql_query);

		$sql_query = "
		ROUND(
			(
				SELECT SUM(res.response * q.question_weight)	
				FROM survey_response res
				INNER JOIN survey_question q ON q.question_id = res.question_id
				INNER JOIN survey_resident sres ON res.survey_resident_id = sres.survey_resident_id
				INNER JOIN cpm_residents cres ON cres.resident_num = sres.resident_num
				WHERE sres.survey_id = sr.survey_id
			)
			/
			(
				SELECT COUNT(*)
				FROM survey_resident sres
				INNER JOIN cpm_residents res ON res.resident_num = sres.resident_num
				INNER JOIN cpm_rmcs rm ON rm.rmc_num = res.rmc_num
				WHERE survey_id = sr.survey_id
				AND sres.survey_resident_complete_ymdhis <> ''
			)
		, 2)";
		
		$search_builder->add_subquery_column('average', $sql_query);
		
		$search_builder->add_function_column('sr.survey_resident_complete_ymdhis', 'data', 'ymdhis_to_date');
		
		$sql = "
		FROM survey_response r
		INNER JOIN survey_resident sr ON r.survey_resident_id = sr.survey_resident_id
		INNER JOIN cpm_residents cr ON cr.resident_num = sr.resident_num
		INNER JOIN survey s ON s.survey_id = sr.survey_id
		INNER JOIN cpm_lookup_residents lres ON lres.resident_lookup = cr.resident_num";
		
		$search_builder->add_sql($sql);
		
		$search_builder->add_group("r.survey_resident_id");
		$search_builder->add_order("sr.survey_resident_complete_ymdhis", 'DESC');
		$search_builder->paging(0, 10);
		
		return $search_builder;
	}
	
	/**
	 * Get DataTables Results at property level
	 * 
	 * @param	array		request				$_REQUEST object
	 * @return	array		$output				Datatables formatted array
	 */
	function get_latest_results($request){
		
		$search_builder = $this->get_latest_results_data($request);
		$rmc = new rmc;
		
		$sql_main = $search_builder->build_sql();
		$output = $search_builder->output($request);
		
		return $output;
	}
}