<html>
<body>
<font face="verdana" size="2">
<p>Dear <?=$user_firstname;?>,</p>
<br />
<p>A new password has been generated for you on the RMG Surveys website. Your details are as follows:</p>
<table style="font-family:Verdana; font-size:12px;" border="0">
	<tr>
		<td style="font-weight:bold;width:130px;">Username:</td>
		<td><?=$username;?></td>
	</tr>
	<tr>
		<td style="font-weight:bold;">Password:</td>
		<td><?=$password;?></td>
	</tr>
</table>
<p><a href="<?=$UTILS_URL_BASE;?>admin/">Click here</a> to login.<br /></p>
<p></p>
<p>(this email was automatically generated from rmgsurveys.co.uk.)</p>
</font>
</body>
</html>