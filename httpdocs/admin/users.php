<?php
require_once("utils.php");

// Check if logged in
if($_SESSION['admin_user_serial'] == ""){
	header("Location: /admin/login.php");
	exit;
}

require_once($UTILS_SERVER_PATH."library/classes/template/admin_template.class.php");
require_once($UTILS_SERVER_PATH."library/classes/user/user.class.php");
$admin_user = new user($_SESSION['admin_user_serial'], "serial");

if($_REQUEST['a'] == 'users'){

	$mysql = new mysql();
	$i = 0;
	$result_array = array();
	
	$user = new user();
	$result_array = $user->get_list($_REQUEST);
	
	echo json_encode($result_array);
	exit;
}
elseif($_REQUEST['a'] == 'delete_user'){
	
	$result_array = array();
	$result_array['save_result'] = "fail";
	
	$user = new user($_REQUEST['s'], 'id');
	$save_result = $user->delete_user();
	
	if( $save_result === true ){
		$result_array['save_result'] = "success";
	}
	
	echo json_encode($result_array);
	exit;
}
else{
	
	$title = 'Users';
	$icon = 'file-alt';
	$tpl = new admin_template(get_defined_vars());
	$tpl->set( 'user_data', $tpl->set_datatable($UTILS_SERVER_PATH."templates/user_row.tpl") );
	$tpl->set( 'session', $_SESSION );
	echo $tpl->fetch();
}
?>