/*
 * jQuery UI SlideDeck
 */
(function( $, undefined ) {
	
	var emptyFunction = function(){};
	
	$.widget( "ui.slidedeck", {
		options: {
			speed: 500,
            transition: 'swing',
            start: 1,
            activeCorner: true,
            index: true,
            scroll: true,
            keys: true,
            autoPlay: false,
            autoPlayInterval: 5000,
            hideSpines: false,
            cycle: false,
            slideTransition: 'slide',
            current: 1,
	        deck: $(),
	        former: -1,
	        spines: $(),
	        slides: $(),
	        controlTo: 1,
	        session: [],
	        disabledSlides: [],
	        pauseAutoPlay: false,
	        isLoaded: false,
	        browser: {},
	        width: 0,
	        height: 0,
	        spine_inner_width: 0,
	        spine_outer_width: 0,
	        slide_width: 0,
	        sliding_width: 0,
	        spine_half_width: 0,
	        looping: false,
	        prefix: "",
	        before: emptyFunction
		},
		
		classes: {
            slide: 'slide',
            spine: 'spine',
            label: 'label',
            index: 'index',
            active: 'active',
            indicator: 'indicator',
            activeCorner: 'activeCorner',
            disabled: 'disabled',
            vertical: 'slidesVertical',
            previous: 'previous',
            next: 'next'
        },
		
		_create: function() {
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
			options.deck = this.element;
			options.spines = options.deck.children('dt');
			options.slides = options.deck.children('dd');
			
			var UA = navigator.userAgent.toLowerCase();
			options.browser = {
		        chrome: UA.match(/chrome/) ? true : false,
		        firefox: UA.match(/firefox/) ? true : false,
		        firefox2: UA.match(/firefox\/2\./) ? true : false,
		        firefox30: UA.match(/firefox\/3\.0/) ? true : false,
		        msie: UA.match(/msie/) ? true : false,
		        msie6: (UA.match(/msie 6/) && !UA.match(/msie 7|8/)) ? true : false,
		        msie7: UA.match(/msie 7/) ? true : false,
		        msie8: UA.match(/msie 8/) ? true : false,
		        msie9: UA.match(/msie 9/) ? true : false,
		        chromeFrame: (UA.match(/msie/) && UA.match(/chrome/)) ? true : false,
		        opera: UA.match(/opera/) ? true : false,
		        safari: (UA.match(/safari/) && !UA.match(/chrome/)) ? true : false
	        }
			
			for(var b in options.browser){
	            if(options.browser[b] === true){
	                options.browser._this = b;
	            }
	        }
	        if(options.browser.chrome === true && !options.browser.chromeFrame) {
	            options.browser.version = UA.match(/chrome\/([0-9\.]+)/)[1];
	        }
	        if(options.browser.firefox === true) {
	            options.browser.version = UA.match(/firefox\/([0-9\.]+)/)[1];
	        }
	        if(options.browser.msie === true) {
	            options.browser.version = UA.match(/msie ([0-9\.]+)/)[1];
	        }
	        if(options.browser.opera === true) {
	            options.browser.version = UA.match(/version\/([0-9\.]+)/)[1];
	        }
	        if(options.browser.safari === true) {
	            options.browser.version = UA.match(/version\/([0-9\.]+)/)[1];
	        }
			
	        switch(options.browser._this){
	            case "firefox":
	            case "firefox3":
	            	options.prefix = "-moz-";
	            break;
	            
	            case "chrome":
	            case "safari":
	            	options.prefix = "-webkit-";
	            break;
	            
	            case "opera":
	            	options.prefix = "-o-";
	            break;
	        }
	        	        
	        self.initialize();
		},
		
		FixIEAA: function(spine){
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
            if(options.browser.msie && !options.browser.msie9){
                var bgColor = spine.css('background-color');
                var sBgColor = bgColor;
                if(sBgColor == "transparent"){
                    bgColor = "#ffffff";
                } else {
                    if(sBgColor.match('#')){
                        // Hex, convert to RGB
                        if(sBgColor.length < 7){
                            var t = "#" + sBgColor.substr(1,1) + sBgColor.substr(1,1) + sBgColor.substr(2,1) + sBgColor.substr(2,1) + sBgColor.substr(3,1) + sBgColor.substr(3,1);
                            bgColor = t;
                        }
                    }
                }
                bgColor = bgColor.replace("#","");
                var cParts = {
                    r: bgColor.substr(0,2),
                    g: bgColor.substr(2,2),
                    b: bgColor.substr(4,2)
                };
                var bgRGB = "#";
                var hexVal = "01234567890ABCDEF";
                for(var k in cParts){
                    cParts[k] = Math.max(0,(parseInt(cParts[k],16) - 1));
                    cParts[k] = hexVal.charAt((cParts[k] - cParts[k]%16)/16) + hexVal.charAt(cParts[k]%16);
                    
                    bgRGB += cParts[k];
                }
                
                spine.find('.' + self.classes.index).css({
                    'filter': 'progid:DXImageTransform.Microsoft.BasicImage(rotation=1) chroma(color=' + bgRGB + ')',
                    backgroundColor: bgRGB
                });
            }
        },
        
        autoPlay: function(){    
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			        
            setInterval(this.gotoNext,options.autoPlayInterval);
        },
        
        getCurrent: function(){

			var self = this;
			var options = this.options;
			var classes = this.classes;
			
			return options.current;
			     
        },
        
        gotoNext: function(){   
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			        
            if(options.pauseAutoPlay === false){
                if(options.cycle === false && options.current == options.slides.length){
                    options.pauseAutoPlay = true;
                } else {
                    this.next();
                }
            }
        },
        
        /**
         * Modify the positioning and z-indexing for slides upon build
         * 
         * @param string transition The slideTransition being used to determine how to build the slides
         * @param integer i The index of the slide to be modified
         */
        buildSlideTransition: function(transition, i){  
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			        
            var slideCSS = {
                display: 'block'
            };
            slideCSS[options.prefix + 'transform-origin'] = "50% 50%";
            slideCSS[options.prefix + 'transform'] = "";
            
            if(i < options.current) {
                var offset = i * options.spine_outer_width;
                if(options.hideSpines === true){
                    if(i == options.current - 1){
                        offset = 0;
                    } else {
                        offset = 0 - (options.start - i - 1) * self.width();
                    }
                }
            } else {
                var offset = i * options.spine_outer_width + options.slide_width;
                if(options.hideSpines === true){
                    offset = (i + 1 - options.start) * self.width();
                }
            }

            switch(transition){
                case "slide":
                default:
                    slideCSS.left = offset;
                    slideCSS.zIndex = 100000;
                    // Other things to modify the slideCSS specifically for default transitions
                break;
            }
            
            options.slides.eq(i).css(options.prefix + 'transition', "").css(slideCSS);
            
            return offset;
        },
        
        buildDeck: function(){ 
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			        
            if($.inArray(options.deck.css('position'),['position','absolute','fixed'])){
            	options.deck.css('position', 'relative');
            }
            options.deck.css('overflow', 'hidden');
            
            for(var i=0; i<options.slides.length; i++){
                var slide = $(options.slides[i]);
                if(options.spines.length > i){
                    var spine = $(options.spines[i]);
                }
                var sPad = {
                    top: parseInt(slide.css('padding-top'),10),
                    right: parseInt(slide.css('padding-right'),10),
                    bottom: parseInt(slide.css('padding-bottom'),10),
                    left: parseInt(slide.css('padding-left'),10)
                };
                var sBorder = {
                    top: parseInt(slide.css('border-top-width'),10),
                    right: parseInt(slide.css('border-right-width'),10),
                    bottom: parseInt(slide.css('border-bottom-width'),10),
                    left: parseInt(slide.css('border-left-width'),10)
                };
                for(var k in sBorder){
                    sBorder[k] = isNaN(sBorder[k]) ? 0 : sBorder[k];
                }
                
                if(i < options.current) {
                    if(i == options.current - 1){
                        if(options.hideSpines !== true){
                            spine.addClass(classes.active);
                        }
                        slide.addClass(classes.active);
                    }
                }
                
                options.sliding_width = options.slide_width - sPad.left - sPad.right - sBorder.left - sBorder.right;
                
                var slideCSS = {
                    position: 'absolute',
                    zIndex: 100000,
                    height: (options.height - sPad.top - sPad.bottom - sBorder.top - sBorder.bottom) + "px",
                    width: options.sliding_width + "px",
                    margin: 0,
                    paddingLeft: sPad.left + options.spine_outer_width + "px"
                };
                
                var offset = self.buildSlideTransition(options.slideTransition, i);
                
                slide.css(slideCSS).addClass(classes.slide).addClass(classes.slide + "_" + (i + 1));
                
                if (options.hideSpines !== true) {
                    var spinePad = {
                        top: parseInt(spine.css('padding-top'),10),
                        right: parseInt(spine.css('padding-right'),10),
                        bottom: parseInt(spine.css('padding-bottom'),10),
                        left: parseInt(spine.css('padding-left'),10)
                    };
                    for(var k in spinePad) {
                        if(spinePad[k] < 10 && (k == "left" || k == "right")){
                            spinePad[k] = 10;
                        }
                    }
                    var spinePadString = spinePad.top + "px " + spinePad.right + "px " + spinePad.bottom + "px " + spinePad.left + "px";
                    var spineStyles = {
                        position: 'absolute',
                        zIndex: 300000,
                        display: 'block',
                        left: offset,
                        width: (options.height - spinePad.left - spinePad.right) + "px",
                        height: options.spine_inner_width + "px",
                        padding: spinePadString,
                        rotation: '270deg',
                        '-webkit-transform': 'rotate(270deg)',
                        '-webkit-transform-origin': options.spine_half_width + 'px 0px',
                        '-moz-transform': 'rotate(270deg)',
                        '-moz-transform-origin': options.spine_half_width + 'px 0px',
                        '-o-transform': 'rotate(270deg)',
                        '-o-transform-origin': options.spine_half_width + 'px 0px',
                        textAlign: 'right'
                    };
                    
                    if( !options.browser.msie9 ){
                        spineStyles.top = (options.browser.msie) ? 0 : (options.height - options.spine_half_width) + "px";
                        spineStyles.marginLeft = ((options.browser.msie) ? 0 : (0 - options.spine_half_width)) + "px";
	                    spineStyles.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=3)';
                    }

                    spine.css( spineStyles ).addClass(classes.spine).addClass(classes.spine + "_" + (i + 1));
                    
                    if(options.browser.msie9){
                        spine[0].style.msTransform = 'rotate(270deg)';
                        spine[0].style.msTransformOrigin = Math.round(parseInt(options.height, 10) / 2) + 'px ' + Math.round(parseInt(options.height, 10) / 2) + 'px';
                    }
                    
                } else {
                    if(typeof(spine) != "undefined"){
                        spine.hide();
                    }
                }
                if(i == options.slides.length-1){
                    slide.addClass('last');
                    if(options.hideSpines !== true){
                        spine.addClass('last');
                    }
                }
                
                // Add slide active corners
                if(options.activeCorner === true && options.hideSpines === false){
                    var corner = document.createElement('DIV');
                        corner.className = classes.activeCorner + ' ' + (classes.spine + '_' + (i + 1));
                    
                    spine.after(corner);
                    spine.next('.' + classes.activeCorner).css({
                        position: 'absolute',
                        top: '25px',
                        left: offset + options.spine_outer_width + "px",
                        overflow: "hidden",
                        zIndex: "20000"
                    }).hide();
                    if(spine.hasClass(classes.active)){
                        spine.next('.' + classes.activeCorner).show();
                    }
                }
                
                if (options.hideSpines !== true) {
                    // Add spine indexes, will always be numerical if unlicensed
                    var index = document.createElement('DIV');
                        index.className = classes.index;
                        
                    if(options.index !== false){
                        var textNode;
                        if(typeof(options.index) != 'boolean'){
                            textNode = options.index[i%self.options.index.length];
                        } else {
                            textNode = "" + (i + 1);
                        }
                        index.appendChild(document.createTextNode(textNode));
                    }
                            
                    spine.append(index);
                    spine.find('.' + classes.index).css({
                        position: 'absolute',
                        zIndex: 2,
                        display: 'block',
                        width: options.spine_inner_width + "px",
                        height: options.spine_inner_width + "px",
                        textAlign: 'center',
                        bottom: ((options.browser.msie) ? 0 : (0 - options.spine_half_width)) + "px",
                        left: ((options.browser.msie) ? 5 : 20) + "px",
                        rotation: "90deg",
                        '-webkit-transform': 'rotate(90deg)',
                        '-webkit-transform-origin': options.spine_half_width + 'px 0px',
                        '-moz-transform': 'rotate(90deg)',
                        '-moz-transform-origin': options.spine_half_width + 'px 0px',
                        '-o-transform': 'rotate(90deg)',
                        '-o-transform-origin': options.spine_half_width + 'px 0px'
                    });
                    
                    if(options.browser.msie9){
                        spine.find('.' + classes.index)[0].style.msTransform = 'rotate(90deg)';
                    }

                    self.FixIEAA(spine);
                }
            }
            
            if(options.hideSpines !== true){
                // Setup Click Interaction
            	options.spines.bind('click', function(event){
                    event.preventDefault();
                    self.goTo(options.spines.index(this)+1);
                    self._trigger('_spine_click');
                });
              }
            
            // Setup Keyboard Interaction
            if(options.keys !== false){
                $(document).bind('keydown', function(event){
                    if($(event.target).parents().index(options.deck) == -1){
                        if(event.keyCode == 39) {
                        	options.pauseAutoPlay = true;
                            self.next();
                        } else if(event.keyCode == 37) {
                        	options.pauseAutoPlay = true;
                            self.prev();
                        }
                    }
                });
            }
            
            // Setup Mouse Wheel Interaction
            /*if(typeof($.event.special.mousewheel) != "undefined"){
                self.bind("mousewheel", function(event){
                    if(options.scroll !== false){
                        var delta = event.detail ? event.detail : event.wheelDelta;
                        if(options.browser.msie || options.browser.safari || options.browser.chrome){
                            delta = 0 - delta;
                        }

                        var internal = false;
                        if($(event.originalTarget).parents(options.deck).length){
                            if($.inArray(event.originalTarget.nodeName.toLowerCase(),['input','select','option','textarea']) != -1){
                                internal = true;
                            }
                        }

                        if (internal !== true) {
                            if (delta > 0) {
                                switch (options.scroll) {
                                    case "stop":
                                        event.preventDefault();
                                        break;
                                    case true:
                                    default:
                                        if (options.current < options.slides.length || options.cycle == true) {
                                            event.preventDefault();
                                        }
                                    break;
                                }
                                options.pauseAutoPlay = true;
                                self.next();
                            }
                            else {
                                switch (options.scroll) {
                                    case "stop":
                                        event.preventDefault();
                                        break;
                                    case true:
                                    default:
                                        if (options.current != 1 || options.cycle == true) {
                                            event.preventDefault();
                                        }
                                    break;
                                }
                                options.pauseAutoPlay = true;
                                self.prev();
                            }
                        }
                    }    
                });
            }*/
            
            $(options.spines[options.current - 2]).addClass(classes.previous);
            $(options.spines[options.current]).addClass(classes.next);

            if(options.autoPlay === true){
                self.autoPlay();
            }
            
            options.isLoaded = true;
        },   
        
        getValidSlide: function(ind){ 
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			  
            ind = Math.min(options.slides.length,Math.max(1,ind));
            return ind;
        },

       completeCallback: function(params){
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
			var afterFunctions = [];
            if(typeof(self.options.complete) == "function"){
                afterFunctions.push(function(){ self.options.complete(self); });
            }
            switch(typeof(params)){
                case "function":    // Only function passed
                    afterFunctions.push(function(){ params(self); });
                break;
                case "object":        // One of a pair of functions passed
                    afterFunctions.push(function(){ params.complete(self); });
                break;
            }
            
            var callbackFunction = function(){
                self.looping = false;
                
                for(var z=0; z<afterFunctions.length; z++){
                    afterFunctions[z]();
                }
            };
            
            return callbackFunction;
        },        
        
        transitions: {
            /**
             * Classic SlideDeck transition: Slide
             * 
             * This transition will take a stack or line of slides and move them all to the left or
             * the right keeping their order.
             */
            slide: function(ind, params, forward, self){

    			var options = self.options;
    			var classes = self.classes;
    			
                for (var i = 0; i < options.slides.length; i++) {
                    var pos = 0;
                    if(self.options.hideSpines !== true){
                        var spine = $(options.spines[i]);
                    }
                    var slide = $(self.options.slides[i]);
                    if (i < self.options.current) {
                        if (i == (self.options.current - 1)) {
                            slide.addClass(self.classes.active);
                            if(self.options.hideSpines !== true){
                                spine.addClass(self.classes.active);
                                spine.next('.' + self.classes.activeCorner).show();
                            }
                        }
                        pos = i * self.options.spine_outer_width;
                    }
                    else {
                        pos = i * self.options.spine_outer_width + self.options.slide_width;
                    }
                    
                    if(self.options.hideSpines === true){
                        pos = (i - self.options.current + 1) * self.width();
                    }

                    var animOpts = {
                        duration: self.options.speed,
                        easing: self.options.transition
                    };

                    // Detect a function to run after animating
                    if(i == (forward === true && self.options.current - 1) || i == (forward === false && self.options.current)){
                        if(i === 0) {
                            animOpts.complete = self.completeCallback(params);
                        }
                    }

                    slide.stop().animate({
                        left: pos + "px",
                        width: options.sliding_width + "px"
                    }, animOpts);
                    
                    if(self.options.hideSpines !== true){
                        self.FixIEAA(spine);
                        if(spine.css('left') != pos+"px"){
                            spine.stop().animate({
                                left: pos + "px"
                            },{
                                duration: self.options.speed,
                                easing: self.options.transition
                            });

                            spine.next('.' + self.classes.activeCorner).stop().animate({
                                left: pos + self.options.spine_outer_width + "px"
                            },{
                                duration: self.options.speed,
                                easing: self.options.transition
                            });
                        }
                    }
                }
            }
        },
        
       slide: function(ind, params){ 
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
            ind = self.getValidSlide(ind);
            
            if ((ind <= options.controlTo || options.controlProgress !== true) && options.looping === false) {
                // Determine if we are moving forward in the SlideDeck or backward, 
                // this is used to determine when the callback should be run
                var forward = true;
                if(ind < options.current){
                    forward = false;
                }
                
                var classReset = [self.classes.active, self.classes.next, self.classes.previous].join(' ');
                
                options.former = options.current;
                options.current = ind;
                
                // Detect a function to run before animating
                if (typeof(self.options.before) == "function") {
                    self.options.before(self);
                }
                if (typeof(params) != "undefined") {
                    if (typeof(params.before) == "function") {
                        params.before(self);
                    }
                }
                
                options.spines.removeClass(classReset);
                options.slides.removeClass(classReset);
                options.deck.find('.' + self.classes.activeCorner).hide();
                
                options.spines.eq(options.current - 2).addClass(self.classes.previous);
                options.spines.eq(options.current).addClass(self.classes.next);
                
                if(options.current != options.former){
                    var slideTransition = 'slide';
                    if(typeof(self.transitions[self.options.slideTransition]) != 'undefined'){
                        slideTransition = self.options.slideTransition;
                    }
                    
                    self.transitions[slideTransition](ind, params, forward, self);
                }
            }
        },
        
        disable: function(){
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
			for(var i=0; i<options.slides.length; i++){
               
                if(options.spines.length > i){
                    var spine = $(options.spines[i]);
                    spine.unbind('click');
                }
            }
        },

        enable: function(){
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
			options.spines.bind('click', function(event){
                event.preventDefault();
                self.goTo(options.spines.index(this)+1);
            });
        },
        
       setOption: function(opts, val){
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
            var newOpts = opts;
            
            if(typeof(opts) === "string"){
                newOpts = {};
                newOpts[opts] = val;
            }
            
            for(var key in newOpts){
                val = newOpts[key];
                
                switch(key){
                    case "speed":
                    case "start":
                        val = parseFloat(val);
                        if(isNaN(val)){
                            val = self.options[key];
                        }
                    break;                    
                    case "scroll":
                    case "keys":
                    case "activeCorner":
                    case "hideSpines":
                    case "autoPlay":
                    case "cycle":
                        if(typeof(val) !== "boolean"){
                            val = self.options[key];
                        }
                    break;                    
                    case "transition":
                        if(typeof(val) !== "string"){
                            val = self.options[key];
                        }
                    break;
                    case "complete":
                    case "before":
                        if(typeof(val) !== "function"){
                            val = self.options[key];
                        }
                    break;
                    case "index":
                        if(typeof(val) !== "boolean"){
                            if(!$.isArray(val)){
                                val = self.options[key];
                            }
                        }
                    break;                    
                }
                self.options[key] = val;
            }
        },
        
        setupDimensions: function(){
        	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
			if(options.height == 0){
				options.height = options.deck.parent().actual('height');
			}
			if(options.width == 0){
				options.width = options.deck.parent().actual('width');
			}

            options.deck.css('height', options.height + "px");
    
            options.spine_inner_width = 0;
            options.spine_outer_width = 0;
            
            if(self.options.hideSpines !== true && options.spines.length > 0){
            	options.spine_inner_width = $(options.spines[0]).height();
            	options. spine_outer_width = $(options.spines[0]).outerHeight();
            }

            options.slide_width = options.width - options.spine_outer_width*options.spines.length;
            
            if(self.options.hideSpines === true){
            	options.slide_width = options.width;
            }
            
            options.spine_half_width = Math.ceil(options.spine_inner_width/2);
        },
        
        initialize: function(){ 
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
            // Halt all processing for unsupported browsers
            if((options.browser.opera && options.browser.version < "10.5") || options.browser.msie6 || options.browser.firefox2 || options.browser.firefox30){
                if(typeof(console) != "undefined"){
                    if(typeof(console.error) == "function"){
                        console.error("This web browser is not supported by SlideDeck. Please view this page in a modern, CSS3 capable browser or a current version of Inernet Explorer");
                    }
                }
                return false;
            }

            // Override hideSpines option if no spines are present in the DOM
            if(options.spines.length < 1){
                self.options.hideSpines = true;
            }
            // Turn off activeCorner if hideSpines is on
            if(self.options.hideSpines === true){
                self.options.activeCorner = false;
            }
            
            // Force self.options.slideTransition to "slide" as it is the only option for Lite
            self.options.slideTransition = 'slide';
            
            options.current = Math.min(options.slides.length,Math.max(1,self.options.start));
            
            if(options.deck.height() > 0){
                self.setupDimensions();
                self.buildDeck();
            } else {
                var startupTimer;
                startupTimer = setTimeout(function(){
                    self.setupDimensions();
                    if(options.deck.height() > 0){
                        clearInterval(startupTimer);
                        self.setupDimensions();
                        self.buildDeck();
                    }
                }, 20);
            }
        },
        
        loaded: function(func){ 
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
            var thisTimer;
            thisTimer = setInterval(function(){
                if(options.isLoaded === true){
                    clearInterval(thisTimer);
                    func(self);
                }
            }, 20);
        },
        
        next: function(params){ 
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
			var nextSlide = Math.min(options.slides.length,(options.current + 1));
            if(self.options.cycle === true){
                if(options.current + 1 > options.slides.length){
                    nextSlide = 1;
                }
            }
            self.slide(nextSlide,params);
            return self;
        },
        
        prev: function(params){ 
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
            var prevSlide = Math.max(1,(options.current - 1));
            if(self.options.cycle === true){
                if(options.current - 1 < 1){
                    prevSlide = options.slides.length;
                }
            }
            self.slide(prevSlide,params);
            return self;
        },
        
        goTo: function(ind,params){ 
	
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
			if(options.current != ind){
				options.pauseAutoPlay = true;
				self.slide(Math.min(options.slides.length,Math.max(1,ind)),params);
			}
			
            return self;
			
        },
        
        // Fallback for vertical function calls to prevent trigger of JavaScript error when run even with Lite library
        vertical: function(){
            return false;
        }
	});
}( jQuery ) );