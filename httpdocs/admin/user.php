<?php
require_once("utils.php");

// Check if logged in
if($_SESSION['admin_user_serial'] == ""){
	header("Location: /admin/login.php");
	exit;
}

require_once($UTILS_SERVER_PATH."library/classes/user/user.class.php");
require_once($UTILS_SERVER_PATH."library/classes/template/admin_template.class.php");
$user = new user($_REQUEST['user_id']);
$admin_user = new user($_SESSION['admin_user_serial'], "serial");


if($_REQUEST['a'] == "send_user_pass"){
	
	$send_result = $user->send_password();
	if($send_result === true){
		$send_result = "success";
	}
	
	echo json_encode($send_result);
	exit;
}
elseif($_REQUEST['a'] == 'get_user'){
	
	$result_array = array();
	$result_array['id'] = $user->survey_user_id;
	$result_array['user_firstname'] = $user->survey_user_firstname;
	$result_array['user_lastname'] = $user->survey_user_lastname;
	$result_array['user_username'] = $user->survey_user_username;
	$result_array['user_email'] = $user->survey_user_email;
	$result_array['administrate_users'] = $user->survey_user_administrate_users;
	$result_array['create_surveys'] = $user->survey_user_create_surveys;
	$result_array['delete_surveys'] = $user->survey_user_delete_surveys;
	$result_array['send_surveys'] = $user->survey_user_send_surveys;
	
	echo json_encode($result_array);
	exit;
}
elseif($_REQUEST['a'] == 'save_user'){	
	
	$save_result = array();
	$save_result['save_result'] = "success";
	$save_result['id'] = "";
	
	$save_result = $user->check_user($_REQUEST);
	if($save_result['save_result'] == "success"){
		$save_result = $user->save_user($_REQUEST);
		if($save_result['save_result'] != "success"){
			$save_result['save_msg'] = $save_result['save_result'];
		}
	}

	echo json_encode($save_result);
	exit;
}
else{

	$un = ($_REQUEST['user_id'] != "") ? stripslashes($user->survey_user_firstname." ".$user->survey_user_lastname) : "New User";	
	$title = 'User - '.$un;
	$icon = 'file-alt';
	$tpl = new admin_template(get_defined_vars());
	$tpl->set( 'session', $_SESSION );
	echo $tpl->fetch();
}
?>