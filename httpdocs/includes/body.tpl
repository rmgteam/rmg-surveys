<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title><?=$title?></title>
		
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
		
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/reset.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/custom-theme/jquery-ui-1.10.1.custom.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/font-awesome.min.css" />
	    <!--[if IE 7]>
	    <link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/font-awesome-ie7.min.css">
	    <![endif]-->
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.qtip.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/main.css" />
		
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-ui-1.10.1.custom.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/bootstrap-acknowledgeinput.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.questionnaire.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.qtip.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-build_tooltip.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.processing.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.html5placeholder.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/bootstrap-select.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/common.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#<?=$UTILS_PAGE?>_btn').addClass("active");
			});
		</script>
	</head>
	<body>
		<div class="wrapper">
			<div class="navbar navbar-fixed-top navbar-inverse">
				<div class="navbar-inner">
					<div class="container">
						<img class="brand" src="/images/logo-trans.png" width="100" />
						<ul class="nav">
							<li id="index_btn"><a href="/"><i class="icon-home"></i> Home</a></li>
							<li class="divider-vertical"></li>
							<!--<li id="survey_btn" class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-file-alt"></i> 
									Surveys
									<i class="caret"></i>
								</a>
								<ul class="dropdown-menu">
									<li>
										<a href="/survey/abc123/">Customer Service Survey</a>
									</li>
									<li>
										<a href="/test.php">Test Page</a>
									</li>
								</ul>
							</li>-->
						</ul>
					</div>
				</div>
			</div>
			<?=$header;?>
			<?=$content;?>
		</div>
	</body>
</html>