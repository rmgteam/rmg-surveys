<?php
require_once($UTILS_SERVER_PATH."library/classes/data/data.class.php");

class fso {

	public $ws_network;
	public $ws_fs;
	
	function __construct(){

		$this->ws_network = new COM("WScript.Network"); 
		$this->ws_fs = new COM("Scripting.FileSystemObject");
	}
	
	function RemoveNetworkDrive($drive_letter){
		$this->ws_network->RemoveNetworkDrive($drive_letter . ":");
	}
	
	function MapNetworkDrive($drive_letter, $destination){

		$drives = $this->ws_fs->Drives; 
	
		foreach($drives as $drive ){ 
			$drive_obj = $this->ws_fs->GetDrive($drive);
			if(strtoupper($drive_obj->DriveLetter) == strtoupper($drive_letter)){
				$this->ws_network->RemoveNetworkDrive($drive_letter . ":", True, True);
			}
		}
		
		return $this->ws_network->MapNetworkDrive($drive_letter . ":" , $destination , FALSE, 'Administrator', '4Execu3aXuTrAm94');
		
	}
	
	function MoveFile($source_file, $destination){
		if($this->ws_fs->CopyFile($source_file, $destination)){
			$this->ws_fs->DeleteFile($source_file);
		}
	}
	
	function create_folder($folder_name){
		
		$folder_name = $this->format_folder($folder_name);
		
		$folders = explode("\\", $folder_name);
		
		$check_folder = '';
		
		foreach($folders as $folder){
			$check_folder .= $folder . '\\';
			$this->create_folder_levels($check_folder);
		}
	}
	
	function create_folder_levels($folder_name){
		if(!$this->ws_fs->FolderExists($folder_name)){
			$this->ws_fs->CreateFolder($folder_name);
		}
	}
	
	function get_folder($folder_name, $folder_exception){
			
		$html = '';
		
		$data = new data();
		
		$folder_name = $data->charset_conv($this->format_folder($folder_name));
		
		if($folder_name != ''){
			
			$folder_array = array();
			
			if(!is_null($folder_exception)){
				$folder_array = explode(",", strtoupper($folder_exception));
			}
			
			if(substr($folder_name, strlen($folder_name) - 1, 1) == '\\'){
				$folder_name = substr($folder_name, 0, -1);
			}
			
			$fname = explode('\\', $folder_name);
			
			$local_folder = $fname[count($fname)-1];
			$folder = $this->ws_fs->GetFolder($folder_name);
			$folders = $folder->SubFolders();
			$output_folders = array();
			$i = 0;
			foreach($folders as $item){
				if(!in_array(str_replace("/", "", strtoupper($item->Name())), $folder_array)){
					array_push($output_folders, $item->Name());
				}
			}
		}
		return $output_folders;
	}
	
	function get_files($folder_name, $file_exception=''){
		
		$file_array = array();
		
		$data = new data();
		
		$folder_name = $data->charset_conv($this->format_folder($folder_name));
		
		if($folder_name != ''){
			$exception_array = array();
			
			if(!is_null($file_exception)){
				$exception_array = explode(",", strtoupper($file_exception));
			}
				
			if(substr($folder_name, strlen($folder_name) - 1, 1) == '\\'){
				$folder_name = substr($folder_name, 0, -1);
			}
			
			$fname = explode('\\', $folder_name);
			
			$folder = $this->ws_fs->GetFolder($folder_name);
			$files = $folder->Files();
			
			foreach($files as $item){
				if(!in_array(strtoupper($item->Name()), $exception_array)){
					array_push($file_array, $item->Name());
				}
			}
		}
		return $file_array;
	}
	
	function format_folder($orig){
		if($orig != ''){
			if(substr($orig, 0, 1) == '\\'){
				$orig = substr($orig, 1, strlen($orig));
			}
			if(substr($orig, -1) == '/'){
				$orig = substr($orig, 0, strlen($orig)-1);
			}
			if(substr($orig, -1) == '\\'){
				$orig = substr($orig, 0, strlen($orig)-1);
			}
			$orig = str_replace("/", "\\", $orig);
			$orig = str_replace("\\\\", "\\", $orig);
		}	
		
		return $orig;
	}

	function CopyFile($source, $dest){
		return $this->ws_fs->CopyFile($source, $dest);	
	}

	function DeleteFile($source){
		try {
			if($this->FileExists($source)){
				return $this->ws_fs->DeleteFile($source);
			}else{
				return false;	
			}
		}catch(Exception $e){
			$log = new log();
			$log->string_log($source, 'Delete File', $e->getMessage());
		}	
	}

	function FileExists($source){
		return $this->ws_fs->FileExists($source);	
	}

	function FolderExists($source){
		return $this->ws_fs->FolderExists($source);	
	}

	function CreateFolder($source){
		return $this->ws_fs->CreateFolder($source);	
	}

	function DeleteFolder($source){
		try {
			$source = $this->format_folder($source);
			if($this->FolderExists($source)){
				return $this->ws_fs->DeleteFolder($source, true);
			}else{
				return false;	
			}
		}catch(Exception $e){
			$log = new log();
			$log->string_log($source, 'Delete Folder', $e->getMessage());
		}
	}

	function directory_to_array($directory, $recursive) {
		
		$directory = $this->format_folder($directory);
		
		$array_items = array();
		
		if ($handle = opendir($directory)) {
			
			while (false !== ($file = readdir($handle))) {
				
				if ($file != "." && $file != "..") {
					
					if($file == "_notes"){
						continue;
					}
					
					if (is_dir($directory. "\\" . $file)) {
						
						if($recursive) {
							$array_items = array_merge($array_items, $this->directory_to_array($directory. "\\" . $file, $recursive));
						}
						$file = $directory . "\\" . $file;
						$array_items[] = preg_replace("/\/\//si", "/", $file);
					} else {
						$file = $directory . "\\" . $file;
						$array_items[] = preg_replace("/\/\//si", "/", $file);
					}
				}
			}
			closedir($handle);
		}
		return $array_items;
	}

	function CopyFolder($src, $dst, $exclude_dir_array="", $exclude_file_array="") {
		
		try {
			
			$src = $this->format_folder($src);
			$dst = $this->format_folder($dst);
			
			$dir = opendir($src);
			$this->create_folder($dst);
			while(false !== ( $file = readdir($dir)) ) {
				
				if (( $file != '.' ) && ( $file != '..' )) {
					
					if ( is_dir($src . '\\' . $file) ) {
						
						if( is_array($exclude_dir_array) && in_array($file, $exclude_dir_array) ){
							// skip
						}
						else{
							$this->CopyFolder($src.'\\'.$file, $dst.'\\'.$file, $exclude_dir_array);
						}
					}
					else {
						if( is_array($exclude_file_array) && in_array($file, $exclude_file_array) ){
							// skip
						}
						else{
							$this->CopyFile($src.'\\'.$file, $dst.'\\'.$file);
						}
					}
				}
			}
			closedir($dir);
		}catch(Exception $e){
			$log = new log();
			$log->string_log($dst, 'Copy Folder '.$src, $e->getMessage());
		}
	}

	function MoveFolder($source, $destination){
		try {
			$source = $this->format_folder($source);
			$destination = $this->format_folder($destination);
			return $this->ws_fs->MoveFolder($source, $destination);
		}catch(Exception $e){
			$log = new log();
			$log->string_log($destination, 'Move Folder '.$source, $e->getMessage());
		}
	}

	function GetFile($source){
		return $this->ws_fs->GetFile($source);	
	}
	
	function __call($method, $args){
		try{
			if(method_exists($this->ws_fs, $method)){
				array_unshift($args,$this->ws_fs);
            	return call_user_func_array(array($this->ws_fs, $method),$args);
			}else{
				die($method." does not exist");
			}
		}catch(Exception $e){
			$log = new log();
			$log->string_log($args, 'Call '.$method, $e->getMessage());
		}
	}
	
	function execute($cmd){
		$ws_shell = new COM("WScript.Shell");
		$exec = $ws_shell->Exec('cmd /c '.$cmd); 
		
		while ($exec->Status <> 1){
		    sleep(1);
		}
		
		return $exec;
	}
	
	function ReadExecOutput($exec) {
		If (!$exec->StdOut->AtEndOfStream) {
			return $exec->StdOut->ReadAll;
		}
		
		If (!$exec->StdErr->AtEndOfStream) {
			return "error: " . $exec->StdErr->ReadAll;
		}
		
 		return -1;
	}
}
