<?php
require_once($UTILS_SERVER_PATH."library/classes/template/template.class.php");
require_once($UTILS_SERVER_PATH."library/classes/survey/survey.class.php");
require_once($UTILS_SERVER_PATH."library/classes/data/data.class.php");

Class admin_template Extends Template {
	
	function admin_template($array){
		
		$this->parse($array);
		
		$data = new data;
		
		Global $UTILS_WEBROOT;
		Global $UTILS_LOG_PATH;
		Global $UTILS_WEBROOT;
		Global $UTILS_URL_BASE;
		Global $UTILS_SERVER_PATH;

		$page = str_replace('.php','',basename($_SERVER['PHP_SELF']));
		
		$this->Template($UTILS_SERVER_PATH.'admin/includes/body.tpl');
		
		$surveys = new survey;
		$new_surveys = $surveys->num_new_surveys('20130301');
		
		$this->parse(get_defined_vars());
		
		$this->get_file($UTILS_SERVER_PATH.'admin/includes/'.$page.'_header.tpl', 'header');
		$this->get_file($UTILS_SERVER_PATH.'admin/includes/menu.tpl', 'menu');
		
		$this->main_content = $UTILS_SERVER_PATH.'admin/includes/'.$page.'.tpl';
		$this->var_content = 'content';
		
	}
	
}