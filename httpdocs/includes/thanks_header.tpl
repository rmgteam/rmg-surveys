<div class="header_bar">
	<div class="header-unit" style="margin-top:20px;">
		<div class="text">
			<h1>Survey Complete</h1>
			<p>We are devoting this site to improving relations between ourselves and our customer.</p>
		</div>
		<div class="actions">
			<i class="icon-coffee"></i>
		</div>
		<div class="text" style="margin-top:20px;">
			<a id="warning-btn" class="btn btn-success btn-large">
				<i class="icon-eye-open icon-2x pull-left"></i>
				<div class="pull-right" style="margin-left:8px;">
					Thank you for
					<br>
					Completing a Survey
				</div>
			</a>
		</div>
	</div>
</div>