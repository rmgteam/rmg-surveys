
$(document).ready(function(){
	
	$('#user_send_pass_button').click(function(){
		pre_send_pass();
	});
	
	$('#user_save_button').click(function(){
		do_user_save();
	});
	
	$('#user_cancel_button').click(function(){
		location.href = "/admin/users/";
	});
	
	$('#user_cancel_edit_button').click(function(){
		form_read('user');
		$('#user_save_button').hide();
    	$('#user_cancel_button').show();
    	$('#user_cancel_edit_button').hide();
    	$('#user_send_pass_button').show();
    	$('#user_edit_button').show();
	});
	
	var page_crumbs = {
    	0 : 	{ 'url' : "/admin/",		'text'	: "Dashboard" },
    	1 : 	{ 'url'	: "/admin/users/",	'text'	: "Users" },
    	2 : 	{ 'url'	: "/admin/user/",	'text'	: "New User" }
    };
    gen_crumbs(page_crumbs); 
    
    $('#user_save_button').hide();
    $('#user_cancel_edit_button').hide();
    
    $('#user_edit_button').click(function(){
		form_edit('user');
		$('#user_save_button').show();
    	$('#user_cancel_button').hide();
    	$('#user_cancel_edit_button').show();
    	$('#user_edit_button').hide();
    	$('#user_send_pass_button').hide();
	});
	
	if(user_id == ""){
		form_edit('user');
		$('#user_save_button').show();
		$('#user_cancel_button').hide();
		$('#user_cancel_edit_button').show();
		$('#user_edit_button').hide();
		$('#user_send_pass_button').hide();
	}else{
		do_user(user_id);
	}
});

	
// Return a helper with preserved width of cells
var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};


function do_user_save(){

	clear_errors();	

	var processor = $('body').processing('start', 'Saving User');

	$.post($(location).attr('href'), 
	$('#user-form').serialize() + "&a=save_user",
	function(data){

		$('body').processing('end', processor);
	
		if(data['save_result'] == 'success'){
			do_user(data['id']);
		}
		else{
			show_error(data['field_names'], data['save_msg']);
		}
	},
	'json');
}


function do_user(id){
	
	clear_user();
	form_read('user');
	get_user(id);
}


var get_user = function(id, callback){

	if(id != ""){

		var processor = $('body').processing('start', 'Retrieving User Details');
		
		$.post($(location).attr('href'), 
		{'a': 'get_user','user_id' : id},
		function(data){
			if(data['user_id'] != ''){
				
				$('#user_id').val(data['id']); 
				$('#user_firstname_input').val(data['user_firstname']);
				$('#user_lastname_input').val(data['user_lastname']);
				$('#user_username_input').val(data['user_username']);
				$('#user_email_input').val(data['user_email']);
				$('#user_administrate_select').selectpicker('val', data['administrate_users']);
				$('#user_create_survey_select').selectpicker('val', data['create_surveys']);
				$('#user_delete_survey_select').selectpicker('val', data['delete_surveys']);
				$('#user_send_survey_select').selectpicker('val', data['send_surveys']);
				
				$('#user_firstname_span').html(data['user_firstname']);
				$('#user_lastname_span').html(data['user_lastname']);
				$('#user_username_span').html(data['user_username']);
				$('#user_email_span').html(data['user_email']);
				$('#user_administrate_span').html(data['administrate_users']);
				$('#user_create_survey_span').html(data['create_surveys']);
				$('#user_delete_survey_span').html(data['delete_surveys']);
				$('#user_send_survey_span').html(data['send_surveys']);
				
				$('#page_title_container h5').html("User - " + data['user_firstname'] + ' ' + data['user_lastname']);
				
				var page_crumbs = {
			    	0 : 	{ 'url' : "/admin/",		'text'	: "Dashboard" },
			    	1 : 	{ 'url'	: "/admin/users.php",	'text'	: "Users" },
			    	2 : 	{ 'url'	: "/admin/user/" + data['id'] + "/",	'text'	: data['user_firstname'] + ' ' + data['user_lastname'] }
			    };
			    gen_crumbs(page_crumbs); 
								
				if(administrate_users == 'Y'){
					$('#user_edit_button').show();
				}else{
					$('#user_edit_button').hide();
				}
				$('#user_cancel_button').show();
				$('#user_save_button').hide();
				$('#user_cancel_edit_button').hide();
				$('#user_send_pass_button').show();
				clear_errors();	
				
				if($.isFunction(callback) === true){
					callback();
				}
			}

			$('body').processing('end', processor);
		},
		'json');
	}
};


function pre_send_pass(){

	$("#msg_dialog").dialog("option", "title", "Confirm...");
	$('#msg_dialog_message').html("Are you sure you want to issue this user with a new password?");
	$("#msg_dialog").dialog({
		buttons : [
			{
				text: 'Yes, continue',
				click: function (){
					$(this).dialog('close');	
					send_pass();
				}
			},
			{
				text: 'No',
				click: function (){
					$(this).dialog('close');	
				}
			}
		]
	});
	$("#msg_dialog").dialog('open');
}


function send_pass(){

	$.post($(location).attr('href'), 
		{'a': 'send_user_pass','user_id' : $("#user_id").val() },
		function(data){
			if(data == 'success'){
				$("#msg_dialog").dialog("option", "title", "");
				$('#msg_dialog_message').html("The password for this user has been sent.");
				$("#msg_dialog").dialog({
					buttons : [
						{
							text: 'Ok',
							click: function (){
								$(this).dialog('close');	
							}
						}
					]
				});
				$("#msg_dialog").dialog('open');	
			}
			else{
				$("#msg_dialog").dialog("option", "title", "");
				$('#msg_dialog_message').html("Technical fault - the password for this user could not be sent.");
				$("#msg_dialog").dialog({
					buttons : [
						{
							text: 'Ok',
							click: function (){
								$(this).dialog('close');	
							}
						}
					]
				});
				$("#msg_dialog").dialog('open');	
			}
		},
		'json');
}


function clear_user(){
	$('#user_id').val('');
	$('#user_firstname_input').val('');
	$('#user_lastname_input').val('');
	$('#user_username_input').val('');
	$('#user_email_input').val('');
	$('#user_administrate_select').selectpicker('clear');
	$('#user_create_survey_select').selectpicker('clear');
	$('#user_delete_survey_select').selectpicker('clear');
	$('#user_send_survey_select').selectpicker('clear');
	
	$('#user_firstname_span').html('');
	$('#user_lastname_span').html('');
	$('#user_username_span').html('');
	$('#user_email_span').html('');
	$('#user_administrate_span').html('');
	$('#user_create_survey_span').html('');
	$('#user_delete_survey_span').html('');
	$('#user_send_survey_span').html('');
}
