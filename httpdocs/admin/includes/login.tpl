<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
		
		<title>RMG Surveys - Admin | <?=$title?></title>
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/reset.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/custom-theme/jquery-ui-1.10.1.custom.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>admin/css/bootstrap-select.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/font-awesome.min.css" />
	    <!--[if IE 7]>
	    <link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/font-awesome-ie7.min.css">
	    <![endif]-->
	    <!--[if lt IE 9]>
	    <link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/custom-theme/jquery.ui.1.10.1.ie.css"/>
	    <![endif]-->
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/bbGrid.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>admin/css/main.css" />
		
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-ui.triggers.js"></script>
        <script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/underscore.min.js"></script>
        <script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/backbone.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-ui-1.10.1.custom.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/bootstrap-acknowledgeinput.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/bootstrap-select.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/bbGrid.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.html5placeholder.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.actual.min.js"></script>
		<script type="text/javascript">
			var interval;
			$(document).ready(function(){

				$().acknowledgeinput();
				$(':input[placeholder]').placeholder({
					placeholderCSS: { 
					      'color':'#a5a2a5', 
					      'position': 'absolute', 
					      'left':'5px',
					      'top':'-13px',
					      'margin': '0',
					      'text-align': 'left'
					    }
				});
				
				
				$('.button').button();
				$('body').tooltip({
				      selector: "[data-toggle=tooltip]"
			    });
			    
			});
			
			function do_login(){
				$.post($(location).attr('href'), 
				$('#login-form').serialize() + "&a=login",
				function(data){
					if(data['save_result'] == 'success'){
						$(location).attr('href', '/admin/');
					}
					else{		
						show_error(data['save_msg']);
					}
				},
				'json');
			}
			
			function reset_password(){
				
				$('#reset_button').attr('disabled', 'disabled');
			
				$.post($(location).attr('href'), 
				$('#login-form').serialize() + "&a=reset",
				function(data){
					if(data['save_result'] == 'success'){
						$('#success_msg').show();
					}
					else{		
						show_error(data['save_msg']);
						$('#reset_button').removeAttr('disabled');
					}
				},
				'json');
			}
			
			function show_error(msg){
				$('#error_msg').html(msg);
				$('#error_msg').show();
			}
			
		</script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top navbar-inverse">
			<div class="navbar-inner">
				<div class="container">
					<img class="brand" src="/images/logo-trans.png" width="100" />
				</div>
			</div>
		</div>
		<div style="width:100%; margin-top:48px;">&nbsp;</div>
		
		
		<div class="container">
			<div class="row">
				
				<div class="span12">
					
					<div id="error_msg" class="alert alert-error" style="width:350px;display:none;"></div>
					<div id="success_msg" class="alert alert-success" style="width:350px;display:none;">A new password has been emailed to you</div>
					
					<form id="login-form" method="post" class="form-horizontal" style="width:350px;margin:0;">
						<input type="hidden" name="a" id="a" value="login" />
						<fieldset>
							<div class="control-group">
								<label class="control-label">Username</label>
								<div class="controls">
								       <input name="username_input" id="username_input" type="text" required="required" placeholder="Username" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Password</label>
								<div class="controls">
									<input name="password_input" id="password_input" type="password" required="required" placeholder="Password" />
								</div>
							</div>
							<div class="control-group" style="border-bottom:none;">
								<label class="control-label"></label>
								<div class="controls pull-left">
									<button id="login_button" onclick="do_login();return false;" class="btn">Log In</button>
									<button id="reset_button" onclick="reset_password()" class="btn">Reset Password</button>
								</div>
							</div>
				    	</fieldset>
					</form>
					
				</div>
			</div>
		</div>
	</body>
</html>