<?php
require_once($UTILS_SERVER_PATH."library/classes/core/core.class.php");
require_once($UTILS_SERVER_PATH."library/classes/survey/survey.class.php");

class question extends core {
	
	var $question_id;
	var $type_id;
	var $survey_id;
	var $question_text;
	var $question_info;
	var $question_weight;
	var $question_mandatory;
	var $question_mandatory_text;
	var $question_max_selections;
	var $question_order;
	var $question_discon;
	
	function __construct($ref=""){
		$this->question($ref);
	}
	
	/**
	 * Get Question information
	 * 
	 * @param	string	$ref			Question ID
	 */
	function question($ref=""){
		
		if($ref != ''){
				
			$mysql = new mysql();
				
			$sql = "
			SELECT * 
			FROM survey_question 
			WHERE
			question_id = '".$ref."'";
			$result = $mysql->query($sql, 'Get Question');
			$row = $mysql->fetch_array($result);
		}
		
		$this->question_id = $row['question_id'];
		$this->type_id = $row['type_id'];
		$this->survey_id = $row['survey_id'];
		$this->question_text = $row['question_text'];
		$this->question_info = $row['question_info'];
		$this->question_weight = $row['question_weight'];
		$this->question_mandatory = $row['question_mandatory'];
		$this->question_mandatory_text = $row['question_mandatory_text'];
		$this->question_max_selections = $row['question_max_selections'];
		$this->question_order = $row['question_order'];
		$this->question_discon = $row['question_discon'];		
	}

	/**
	 * Get Num of questions
	 * 
	 * @param	id		$question_id	Question ID
	 * @return	int		$row			Number of questions
	 */
	function get_num_questions($question_id){
		$mysql = new mysql;
		
		$sql = "SELECT COUNT(*)
		FROM survey_answer
		WHERE question_id = '".$question_id."'
		AND answer_discon = 'N'";
		$result = $mysql->query($sql, 'Get Num Answers');
		$row = $mysql->fetch_array($result);
		
		return $row[0];
		
	}

	/**
	 * Get questions
	 * 
	 * @param	id			$survey_id		Survey ID
	 * @param	string		$return_type	Format to return results
	 * @return	Resource	$result			MySQL Resource
	 * OR
	 * @return	string		$id_list		Comma delimited list of questions
	 */
	function get_questions($survey_id="", $return_type="result_set"){
	
		$mysql = new mysql();
	
		if($survey_id == ""){
			$survey_id = $this->survey_id;
		}
	
		$sql = "SELECT *,
		(
			SELECT COUNT(*)
			FROM survey_answer
			WHERE question_id = q.question_id
		) as no_answers
		FROM survey_question q
		INNER JOIN survey_type t ON t.type_id = q.type_id
		WHERE
		q.survey_id = '".$survey_id."' AND
		q.question_discon = 'N'
		ORDER BY q.question_order ASC, q.question_id ASC
		";
		$result = $mysql->query($sql, 'Get Questions');
		if( $return_type == "id_list" ){
			$id_list_array = array();
			while( $row = $mysql->fetch_array($result) ){
				$id_list_array[] = $row['question_id'];
			}
			$id_list = implode(",", $id_list_array);
			return $id_list;
		}
		else{
			return $result;
		}
	}

	/**
	 * Get List of questions
	 * 
	 * @param	id		$survey_id		Survey ID
	 * @param	array	$request		$_REQUEST object
	 * @return	array	$output			Datatables formatted array
	 */
	function get_list ($survey_id, $request){
	
		$mysql = new mysql();
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();
	
		$input = $request;
	
		$search_builder->add_index_column('q.question_id');
		$search_builder->add_column('q.question_order');
		$search_builder->add_column('q.question_text');
		if(!isset($request['order'])){
			$search_builder->add_column('t.type_string');
			$search_builder->add_function_column('q.question_id', 'question', 'get_num_questions');
			$search_builder->add_button_column('q.question_id', 'Delete', "pre_delete_question", array(), 'button ui-button-info');
		}
		$iColumnCount = count($search_builder->columns);
	
		$search_builder->add_filter('question_discon', "=", "N", 'AND');
		$search_builder->add_filter('survey_id', "=", $survey_id, 'AND');
	
		//Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
	
		//Ordering
		$iSortingCols = intval( $request['iSortingCols'] );
		for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
			if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
				$search_builder->add_order($search_builder->columns[intval($request['iSortCol_'.$i])], ($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
	
		//Filtering
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
	
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
	
		if(isset($request['letters'])){
			$search_builder->add_filter('cpm_contractors_letter_sent', "=", $request['letters'], 'AND');
		}
	
		$sql = "
		FROM survey_question q
		INNER JOIN survey_type t ON t.type_id = q.type_id";
	
		$search_builder->add_sql($sql);
	
		$output = $search_builder->output($request);
	
		return $output;
	
	}
	
	/**
	 * Get array of question information
	 * 
	 * @param	int		$question_id	Question ID
	 * @return	array	$array			Array of information
	 */
	function get_question_info($question_id = ''){
		if($question_id != ''){
			$i = 0;
			$array = array();
				
			$mysql = new mysql();
				
			$sql = "SELECT q.question_id,
			q.question_text,
			q.question_info,
			q.question_weight,
			q.type_id,
			t.type_string,
			q.question_mandatory,
			q.question_max_selections,
			q.question_mandatory_text,
			IFNULL(
				d.dependant_question_id, '0'
			) as dependant_question_id,
			IFNULL(
				d.dependant_answer_id, '0'
			) as dependant_answer_id,
			a.answer_id,
			a.answer_value,
			a.answer_text,
			a.answer_discon
			FROM survey_question q
			LEFT JOIN survey_answer a ON q.question_id = a.question_id
			LEFT JOIN survey_dependant d ON d.question_id = q.question_id
			LEFT JOIN survey_type t ON t.type_id = q.type_id
			WHERE q.question_id = '".$question_id."'
			AND q.question_discon = 'N'";
			$result = $mysql->query($sql, 'Get Question Infor');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				while($row = $mysql->fetch_array($result)){
					$array['id'] = $row['question_id'];
					$array['question'] = $row['question_text'];
					$array['info'] = $row['question_info'];
					$array['weight'] = $row['question_weight'];
					$array['type'] = $row['type_id'];
					$array['type_string'] = $row['type_string'];
					$array['question_mandatory'] = $row['question_mandatory'];
					if($row['question_mandatory'] == 'Y'){
						$array['mandatory'] = $row['question_mandatory_text'];
					}else{
						$array['mandatory'] = '';
					}
					$array['selections'] = $row['question_max_selections'];
					$array['dependant_q'] = $row['dependant_question_id'];
					$array['dependant_a'] = $row['dependant_answer_id'];
					if($row['answer_discon'] == 'N' || $row['answer_discon'] == NULL){
						$array['answers'][$i]['id'] = $row['answer_id'];
						$array['answers'][$i]['value'] = $row['answer_value'];
						$array['answers'][$i]['text'] = $row['answer_text'];
							
						$i++;
					}
				}
			}
				
			return $array;
		}
	}
	
	/**
	 * Check Data has been entered
	 * 
	 * @param	array	$request		$_REQUEST object
	 * @return	array	$results_array	Array of message, field, boolean if errored
	 */
	function check($request){
	
		$error_fields = array();
		$security = new security();
		$data = new data;
		$field = new field();
		$error_counter = 0;
		
		$is_valid = true;
			
		// check if the question name is filled in, if not show error
			
		$error_message[$error_counter] = "Please enter the question text";
		$error_fields[$error_counter] = "question_name_label";
		
		if ($request["question_name_input"] == ""){
			$is_valid = false;
			$error_has[$error_counter] = "true";
		}else{
			$error_has[$error_counter] = "false";
		}
			
		$error_counter++;
			
		// check if question type is filled in, if not show error
			
		$error_message[$error_counter] = "Please select a question type";
		$error_fields[$error_counter] = "question_type_label";
		
		if ($request["question_type_select"] == ""){
			$is_valid = false;
			$error_has[$error_counter] = "true";
		}else{
			$error_has[$error_counter] = "false";
		}
			
		$error_counter++;
		
		if ($request["question_type_select"] != "2"){
			
			// check if the question name is filled in, if not show error
				
			$error_message[$error_counter] = "Please enter the question weight";
			$error_fields[$error_counter] = "question_weight_label";
			
			if ($request["question_weight_select"] == ""){
				$is_valid = false;
				$error_has[$error_counter] = "true";
			}else{
				$error_has[$error_counter] = "false";
			}
				
			$error_counter++;
		}
			
		// check if question type is filled in, if not show error
			
		$error_message[$error_counter] = "Please select whether the question is mandatory";
		$error_fields[$error_counter] = "question_mandatory_label";
		
		if ($request["question_mandatory_select"] == ""){
			$is_valid = false;
			$error_has[$error_counter] = "true";
		}else{
			$error_has[$error_counter] = "false";
		}
			
		$error_counter++;
	
		if($is_valid == true){
			$results_array["save_result"] = "success";
			$results_array["message"] = implode("|", $error_message);
			$results_array["fields"] = implode("|", $error_fields);
			$results_array["errored"] = implode("|", $error_has);
		}else{
			$results_array["save_result"] = "error";
			$results_array["message"] = implode("|", $error_message);
			$results_array["fields"] = implode("|", $error_fields);
			$results_array["errored"] = implode("|", $error_has);
		}
		
		return $results_array;
	}
	
	/**
	 * Get last order number of questions
	 * 
	 * @param	int		$survey_id		Survey id
	 * @return	int						Order Number
	 */
	function get_next_q_order($survey_id){
		
		$mysql = new mysql();
		
		$sql = "
		SELECT question_order 
		FROM survey_question 
		WHERE 
		survey_id = '".$survey_id."'
		ORDER BY question_order DESC 
		LIMIT 1 
		";
		$result = $mysql->query($sql, "Select Next Question Order");
		$row = $mysql->fetch_array($result);
		if($row[0] == ""){
			return 1;
		}
		else{
			return $row[0] + 1;
		}
	}
	
	/**
	 * Save question
	 * 
	 * @param	array	$request		$_REQUEST object
	 * @return	array	$results_array	Array of whether it completed and ID of record
	 */
	function save($request){
		
		try{
	
			$mysql = new mysql();
			$security = new security();
			$data = new data;
			
			$q_order = $this->get_next_q_order($request['survey_id']);
			
			//if id is filled in then we are editing, if not it is a new record
			if($request['question_id'] == ''){
				
				$serial = $this->gen_unique_serial("survey_question","question_serial");
				
				$sql = "INSERT INTO survey_question SET
				question_serial = '" . $serial . "', 
				survey_id = '" . $request['survey_id'] . "', 
				question_order = '".$q_order."',";
			}else{
				$sql = "UPDATE survey_question SET ";
			}
			
			$max_selections = $security->clean_query($request['question_no-selections_input']);
			if($max_selections == ''){
				$max_selections = 0;
			}
			
			$weight = $security->clean_query($request['question_weight_select']);
			if($weight == ''){
				$weight = 0;
			}
			
			$sql_middle = "
			type_id = '". $security->clean_query($request['question_type_select']) . "',
			question_text = '". $security->clean_query($request['question_name_input']) . "',
			question_info = '". $security->clean_query($request['question_info_input']) . "',
			question_weight = '". $weight . "',
			question_mandatory = '". $security->clean_query($request['question_mandatory_select']) . "',
			question_mandatory_text = '". $security->clean_query($request['question_mandatory_text_input']) . "',
			question_max_selections = '". $max_selections . "'";
			
			$results_array["id"] = $request['question_id'];
			
			$sql .= $sql_middle;
			
			// insert record
			if($request['question_id'] == ''){
				$msg = 'Insert New Question';
			}else{
				$sql .= " 
				WHERE question_id = '".$request['question_id']."'";
				$msg = 'Update Question';
			}
			$result = $mysql->insert($sql, $msg);
			
			//if there was no issues inserting, return the id of the new tracker
			if( $result !== false ){
				
				if($request['question_id'] == ''){
					$results_array["id"] = $result;
				}
				$results_array["save_result"] = "success";
				return $results_array;
			}
			
			$results_array["save_result"] = "Technical fault - this record failed to save.";
			$results_array["sql"] = $sql;
			return $results_array;
			
		}catch(Exception $e){
			$results_array["save_result"] = "Technical fault - this record failed to save.";
			$results_array["sql"] = $e->getMessage();
			return $results_array;
		}
	}
	
	/**
	 * Save question order
	 * 
	 * @param	array	$request		$_REQUEST object
	 * @return	boolean					Successful save
	 */
	function save_question_order($request){
		
		$mysql = new mysql();
		$security = new security();
		
		if( $request['order'] != "" ){
			
			$update_error = "N";
			$sql_msg = 'Update Question Order';
			$q_ids = explode(",", $request['order']);
			for($q=0;$q < count($q_ids);$q++){
				$sql = "
				UPDATE survey_question SET 
					question_order = '".($q+1)."' 
				WHERE question_id = '".$q_ids[$q]."'
				";
				if( $mysql->insert($sql, 'Update question order') === false ){
					$update_error = "Y";
				}
			}
			if( $update_error == "Y" ){
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Delete Question
	 * 
	 * @return	boolean					Has question been deleted
	 */
	function delete(){
	
		$mysql = new mysql();
		$survey = new survey;
		
		$survey_id = $this->survey_id;
	
		$sql = "
		UPDATE survey_question SET 
		question_discon = 'Y' 
		WHERE 
		question_id = '".$this->question_id."'
		";
		$result = $mysql->query($sql, 'Delete question');
		if( $result !== false ){
			
			// Update question order in the database
			$q_list = $this->get_questions($survey_id, "id_list");
			$request_q = array();
			$request_q['order'] = $q_list;
			$this->save_question_order($request_q);
			
			return true;
		}
		return false;
	}
	
}



?>