<?php
require_once($UTILS_SERVER_PATH."library/classes/template/template.class.php");


Class survey_template Extends Template {
	
	function survey_template($array){
		
		$this->parse($array);
		
		Global $UTILS_WEBROOT;
		Global $UTILS_LOG_PATH;
		Global $UTILS_WEBROOT;
		Global $UTILS_URL_BASE;
		Global $UTILS_SERVER_PATH;

		$page = str_replace('.php','',basename($_SERVER['PHP_SELF']));
		
		$this->Template($UTILS_SERVER_PATH.'/includes/body.tpl');
		
		$this->parse(get_defined_vars());
		
		$this->get_file($UTILS_SERVER_PATH.'/includes/'.$page.'_header.tpl', 'header');
		
		$this->main_content = $UTILS_SERVER_PATH.'/includes/'.$page.'.tpl';
		$this->var_content = 'content';
	}
	
}
?>