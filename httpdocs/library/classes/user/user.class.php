<?php
require_once($UTILS_SERVER_PATH."library/classes/core/core.class.php");
require_once($UTILS_SERVER_PATH."library/classes/field/field.class.php");
require_once($UTILS_SERVER_PATH."library/classes/security/security.class.php");
require_once($UTILS_SERVER_PATH."library/classes/mailer/mailer.class.php");


class user extends core {
	
	var $survey_user_id;
	var $survey_user_serial;
	var $survey_user_username;
	var $survey_user_pass;
	var $survey_user_firstname;
	var $survey_user_lastname;
	var $survey_user_email;
	var $survey_user_administrate_users;
	var $survey_user_create_surveys;
	var $survey_user_delete_surveys;
	var $survey_user_send_surveys;
	var $survey_user_created_ymdhi;
	var $survey_user_discon;
	
	var $error_fields;
	
	
	function __construct($ref="", $ref_type="id"){
	
		$this->error_fields = array();
		$this->error_fields['field_names'] = array();
		$this->error_fields['save_msg'] = "";
	
		if( $ref != "" ){
			$this->user($ref, $ref_type);
		}
	}
	

	function user($ref='', $ref_type="id"){
			
		$mysql = new mysql();
		
		if( $ref_type == "serial" ){
			$ref_clause = " survey_user_serial = '".$ref."' ";
		}
		else{
			$ref_clause = " survey_user_id = '".$ref."'";
		}
		
		$sql = "
		SELECT *
		FROM survey_users
		WHERE 
		".$ref_clause;
		$result = $mysql->query($sql, 'Get User');
		$num_rows = $mysql->num_rows($result);
		if( $num_rows > 0 ){
			
			$row = $mysql->fetch_array($result);
			$this->survey_user_id = $row['survey_user_id'];
			$this->survey_user_serial = $row['survey_user_serial'];
			$this->survey_user_username = $row['survey_user_username'];
			$this->survey_user_pass = $row['survey_user_pass'];
			$this->survey_user_firstname = $row['survey_user_firstname'];
			$this->survey_user_lastname = $row['survey_user_lastname'];
			$this->survey_user_email = $row['survey_user_email'];
			$this->survey_user_administrate_users = $row['survey_user_administrate_users'];
			$this->survey_user_create_surveys = $row['survey_user_create_surveys'];
			$this->survey_user_delete_surveys = $row['survey_user_delete_surveys'];
			$this->survey_user_send_surveys = $row['survey_user_send_surveys'];
			$this->survey_user_created_ymdhi = $row['survey_user_created_ymdhi'];
			$this->survey_user_discon = $row['survey_user_discon'];
		}
	}
	
	function get_user_by_username($username){
		
		$mysql = new mysql();
		$security = new security();
		
		$sql = "
		SELECT *
		FROM survey_users
		WHERE
		survey_user_username = '".$security->clean_query($username)."'";
		$result = $mysql->query($sql, 'Get User');
		$num_rows = $mysql->num_rows($result);
		if( $num_rows > 0 ){
			$row = $mysql->fetch_array($result);
			$this->user($row['survey_user_id']);
			return true;
		}
		
		return false;
	}
	
	
	function delete_user(){
		
		$mysql = new mysql();
		
		$sql = "
		UPDATE survey_users SET 
		survey_user_discon = 'Y' 
		WHERE 
		survey_user_id = ".$this->survey_user_id." 
		";
		$result = $mysql->query($sql, 'Delete user');
		if( $result !== false ){
			return true;
		}
		return false;
	}


	function check_user($request){
	
		$error_fields = array();
		$field = new field;
	
		$this->init_check_errors("Please complete all the required fields highlighted in red.");
		if($request['user_firstname_input'] == ""){$this->push_check_errors("user_firstname_label");}
		if($request['user_lastname_input'] == ""){$this->push_check_errors("user_lastname_label");}
		if($request['user_email_input'] == ""){$this->push_check_errors("user_email_label");}
		if(!isset($request['details'])){
			if($request['user_username_input'] == ""){$this->push_check_errors("user_username_label");}
			if($request['user_administrate_select'] == ""){$this->push_check_errors("user_administrate_label");}
			if($request['user_create_survey_select'] == ""){$this->push_check_errors("user_create_survey_label");}
			if($request['user_delete_survey_select'] == ""){$this->push_check_errors("user_delete_survey_label");}
			if($request['user_send_survey_select'] == ""){$this->push_check_errors("user_send_survey_label");}
		}else{
			if($request['user_new_password_input'] == ""){$this->push_check_errors("user_new_password_label");}
			if($request['user_repeat_password_input'] == ""){$this->push_check_errors("user_repeat_password_label");}
			if($request['user_repeat_password_input'] != $request['user_new_password_input']){$this->push_check_errors("user_repeat_password_label");}
		}
		
		if( $this->error_fields['save_result'] == "fail" ){return $this->error_fields;}
		
		if(!isset($request['details'])){
			$this->init_check_errors("The username must be at least 8 characters long.");
			if(strlen($request['user_username_input']) < 8){$this->push_check_errors("user_username_label");}
			if( $this->error_fields['save_result'] == "fail" ){return $this->error_fields;}
		}
		
		$this->init_check_errors("Please enter a valid email address.");
		if($field->is_valid_email($request['user_email_input']) !== true){$this->push_check_errors("user_email_label");}
		if( $this->error_fields['save_result'] == "fail" ){return $this->error_fields;}
	
		$error_fields['save_result'] = "success";
		return $error_fields;
	}
	
	
	function save_user($request){
		
		try{
	
			$mysql = new mysql();
			
			$security = new security();
			$data = new data;
			
			//if id is filled in then we are editing, if not it is a new record
			if($request['user_id'] == ''){
				$sql = "INSERT INTO survey_users SET
				survey_user_created_ymdhi = '" . date("YmdHi") . "',  
				survey_user_serial = '" . $security->gen_serial(12) . "', 
				";
			}else{
				$sql = "UPDATE survey_users SET";
			}
			
			$sql_middle = "
			survey_user_firstname = '". $security->clean_query($request['user_firstname_input']) . "',
			survey_user_lastname = '". $security->clean_query($request['user_lastname_input']) . "',
			survey_user_email = '". $security->clean_query($request['user_email_input']) . "'";
			
			if(isset($request['details'])){
				$sql_middle .= ",
				survey_user_pass = '".md5($security->clean_query($request['user_new_password_input']))."'";
			}else{
				$sql_middle .= ",
				survey_user_username = '". $security->clean_query($request['user_username_input']) . "',
				survey_user_administrate_users = '". $security->clean_query($request['user_administrate_select']) . "',
				survey_user_create_surveys = '". $security->clean_query($request['user_create_survey_select']) . "',
				survey_user_delete_surveys = '". $security->clean_query($request['user_delete_survey_select']) . "',
				survey_user_send_surveys = '". $security->clean_query($request['user_send_survey_select']) . "'";
			}
			
			$has_error=false;
			
			$last_id = "";
			
			if($request['user_id'] == ''){
				// insert record
				$sql .= $sql_middle;
				$msg = 'Insert New User';
			}else{
				// insert record
				$sql .= $sql_middle.'
				WHERE survey_user_id = "'.$request['user_id'].'"';
				$msg = 'Update User';
				$last_id = $request['user_id'];
			}
			//print $sql;exit;
			$has_error = $mysql->insert($sql, $msg);
			
			//if there was no issues inserting, return the id of the new tracker
			if (!is_bool($has_error) && $request['user_id'] == ''){
				$last_id = $has_error;
			}
			
			if (!is_bool($has_error)){
				$results_array["save_result"] = "success";
				$results_array["id"] = $last_id;
			}else{
				$results_array["save_result"] = "This user could not be saved.";
			}
				
			return $results_array;	
			
		}catch(Exception $e){
			$results_array["save_result"] = "Technical fault - this user could not be saved.";
			$results_array["sql"] = $e->getMessage();
			return $results_array;
		}
	}
	
	
	function init_check_errors($msg){
	
		$this->error_fields['field_names'] = array();
		$this->error_fields['save_msg'] = $msg;
	}
	function push_check_errors($field_label){
	
		array_push($this->error_fields['field_names'], $field_label);
		$this->error_fields['save_result'] = "fail";
	}
	
	function get_list ($request){
	
		$mysql = new mysql();
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();
	
		$input = $request;
		
		$search_builder->add_index_column('survey_user_id');
		$search_builder->add_column('CONCAT(survey_user_firstname, " ", survey_user_lastname)');
		$search_builder->add_column('survey_user_email');
		$search_builder->add_button_column('survey_user_id', 'Delete', "pre_delete_user", array(), 'button ui-button-info');
		$iColumnCount = count($search_builder->columns);
	
		$search_builder->add_filter('survey_user_discon', "=", 'N', 'AND');
	
		//Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
	
		//Ordering
		$iSortingCols = intval( $request['iSortingCols'] );
		for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
			if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
				$search_builder->add_order($search_builder->columns[intval($request['iSortCol_'.$i])], ($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
	
		//Filtering
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
	
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
	
		$sql = "
		FROM survey_users";
	
		$search_builder->add_sql($sql);
	
		$output = $search_builder->output($request);
	
		return $output;
	
	}
	
	function get_users(){
		
		$mysql = new mysql();
	
		$sql = "
		SELECT * 
		FROM survey_users 		
		WHERE 
		survey_user_discon = 'N' 
		ORDER BY survey_user_firstname ASC, survey_user_lastname ASC 
		";
		$result = $mysql->query($sql, 'Get users');
		return $result;
	}
	
	
	function get_fullname(){
		return stripslashes($this->survey_user_firstname." ".$this->survey_user_lastname);
	}
	
	
	function gen_user_password(){
		
		$security = new security;
		$pass = $security->gen_serial(8);
		$mysql = new mysql();
		
		$sql = "
		UPDATE survey_users SET
			survey_user_pass = '".md5($pass)."' 
		WHERE survey_user_id = ".$this->survey_user_id." 
		";
		$result = $mysql->query($sql, 'Gen USer Password');
		
		return $pass;
	}
	
	
	function send_password(){
		
		global $UTILS_SUPPORT_EMAIL;
		global $UTILS_URL_BASE;
		global $UTILS_SERVER_PATH;
		
		$template = new template;
		
		$user_firstname = ucfirst($this->survey_user_firstname);
		$username = $this->survey_user_username;
		$password = $this->gen_user_password();
		
		$text = $template->fetch($UTILS_SERVER_PATH."templates/new_pass.php", get_defined_vars());
		
		$mailer = new mailer;
		$mailer->set_mail_from($UTILS_SUPPORT_EMAIL);
		$mailer->set_mail_to($this->survey_user_email);
		$mailer->set_mail_subject("Login for RMG Surveys...");
		$mailer->set_mail_type("RMG Surveys New Password");
		$mailer->set_mail_html($text);
		$mailer->send();
		
		return true;
	}
	
	
	function do_login($request){
		
		$mysql = new mysql();	
		$security = new security();
		$login_result = "Technical fault - there was a problem trying to process your login.";
		
		// Check fields
		$login_result = "Complete both login fields.";
		if($request['username_input'] == ""){return $login_result;}
		if($request['password_input'] == ""){return $login_result;}
		
		// Check credentials
		$sql = "
		SELECT survey_user_serial,
		survey_user_administrate_users,
		survey_user_create_surveys,
		survey_user_delete_surveys,
		survey_user_send_surveys
		FROM survey_users 
		WHERE 
		survey_user_username IS NOT NULL AND 
		survey_user_username <> '' AND 	
		survey_user_pass IS NOT NULL AND 
		survey_user_pass <> '' AND 
		survey_user_username = '".$security->clean_query($request['username_input'])."' AND 
		survey_user_pass = '".md5($security->clean_query($request['password_input']))."' 
		";
		$result = $mysql->query($sql, 'User Login');
		$num = $mysql->num_rows($result);
		if($num > 0 && $result !== false){	
			
			$login_result = "success";
			$row = $mysql->fetch_array($result);
			$_SESSION['admin_user_serial'] = $row['survey_user_serial'];
			$_SESSION['administrate_users'] = $row['survey_user_administrate_users'];
			$_SESSION['create_surveys'] = $row['survey_user_create_surveys'];
			$_SESSION['delete_surveys'] = $row['survey_user_delete_surveys'];
			$_SESSION['send_surveys'] = $row['survey_user_send_surveys'];
		}
		else{
			
			$login_result = "Your details could not be found.";
		}
		
		return $login_result;
	}
	
	
	function can_access_surveys(){
		
		if( $this->survey_user_create_surveys == "Y" || $this->survey_user_delete_surveys == "Y" || $this->survey_user_send_surveys == "Y" ){
			return true;
		}
		return false;
	}
	
}

?>