<script type="text/javascript">	
	var survey_user_id = '<?=$admin_user->survey_user_id;?>';
</script>

<script type="text/javascript" src="<?=$UTILS_WEBROOT?>admin/includes/my_details.js"></script>

<div id="user-div">
	<div class="clearfix button_group">
		
		<a id="user_cancel_button" class="button ui-button-info pull-right">Cancel</a>
		<a id="user_cancel_edit_button" class="button ui-button-info pull-right">Cancel</a>
		<a id="user_edit_button" class="button ui-button-info pull-right">Edit</a>
		<a id="user_save_button" class="button ui-button-info pull-right">Save</a>
		
	</div>
	<div id="bbGrid-container"></div>
</div>


<div id="user-info" class="form-container">
			
	<form id="user-form" method="post" class="form-horizontal">
	
		<fieldset>
		
			<div class="control-group">
				<label class="control-label" id="user_firstname_label">First Name</label>
				<div class="controls">
				
					<div id="user_firstname_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="Your first name"><i class="icon-question-sign"></i></div>
			        	<input class="span4" name="user_firstname_input" id="user_firstname_input" type="text" required="required" placeholder="" data-type="text" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_firstname_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" id="user_lastname_label">Last Name</label>
				<div class="controls">
				
					<div id="user_lastname_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="Your last name"><i class="icon-question-sign"></i></div>
			        	<input class="span4" name="user_lastname_input" id="user_lastname_input" type="text" required="required" placeholder="" data-type="text" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_lastname_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" id="user_email_label">Email</label>
				<div class="controls">
				
					<div id="user_email_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="Your email address"><i class="icon-question-sign"></i></div>
			        	<input class="span4" name="user_email_input" id="user_email_input" type="text" required="required" placeholder="" data-type="email" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_email_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" id="user_current_password_label">Current Password</label>
				<div class="controls">
				
					<div id="user_current_password_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="Your current password"><i class="icon-question-sign"></i></div>
			        	<input class="span4" name="user_current_password_input" id="user_current_password_input" type="password" required="required" placeholder="" data-type="password" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_current_password_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" id="user_new_password_label">New Password</label>
				<div class="controls">
				
					<div id="user_new_password_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="Your new password"><i class="icon-question-sign"></i></div>
			        	<input class="span4" name="user_new_password_input" id="user_new_password_input" type="password" required="required" placeholder="" data-type="password" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_new_password_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" id="user_repeat_password_label">Repeat New Password</label>
				<div class="controls">
				
					<div id="user_repeat_password_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="Repeat your new password"><i class="icon-question-sign"></i></div>
			        	<input class="span4" name="user_repeat_password_input" id="user_repeat_password_input" type="password" required="required" placeholder="" data-type="password" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_repeat_password_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
		</fieldset>
		
		<input type="hidden" id="user_id" name="user_id" value="" />
		
	</form>

</div>