<?php
require_once($UTILS_SERVER_PATH."library/classes/data/data.class.php");
require_once($UTILS_SERVER_PATH."library/classes/security/security.class.php");

class rmc {

	var $rmc_num;
	var $rmc_name;
	var $rmc_ref;
	var $brand_name;
	var $property_manager_name;
	var $regional_manager_name;
	var $operational_director_name;
	var $rmc_county;
	var $rmc_postcode;
	var $rmc_status;
	var $rmc_is_active;
	var $owner;
	
	/**
	 * Get RMC Information
	 * 
	 * @param	int			$rmc_num				RMC Num
	 */
	function rmc($rmc_num = 0){
		
		$mysql = new mysql;
		
		$sql = "SELECT * 
		FROM cpm_rmcs r
		INNER JOIN cpm_lookup_rmcs lr ON lr.rmc_lookup = r.rmc_num
		INNER JOIN cpm_subsidiary s ON s.subsidiary_id = r.subsidiary_id
		WHERE r.rmc_num = '".$rmc_num."'";	
		$result = $mysql->query($sql, 'get rmc');
		$this->row = $mysql->fetch_array($result); // Old way of populating object (leave in for legacy code)

		$row = $this->row;
		
		$this->rmc_num = $row['rmc_num'];
		$this->rmc_name = $row['rmc_name'];
		$this->rmc_ref = $row['rmc_ref'];
		$this->property_manager_name = $row['property_manager'];
		$this->regional_manager_name = $row['regional_manager'];
		$this->operational_director_name = $row['rmc_op_director_name'];
		$this->rmc_county = $row['rmc_county'];
		$this->rmc_postcode = $row['rmc_postcode'];
		$this->owner = $row['rmc_freeholder_name'];
		$this->rmc_status = $row['rmc_status'];
		$this->brand_name = $row['subsidiary_name'];
	}
	
	/**
	 * Ref to RMC Num
	 * 
	 * @param	int			$rmc_ref				RMC Ref
	 * @param	int			$subsidiary_id			Subsidiary ID
	 * @return	int			$rmc_num				RMC Num
	 */
	function ref_to_num($rmc_ref, $subsidiary_id){
		
		$mysql = new mysql;
		
		if(strpos($subsidiary_id, '-') > -1){
			$subsidiary_id = str_replace('-', '', $subsidiary_id);
			$equals = '<>';
		}else{
			$equals = '=';	
		}
		
		$sql = "
		SELECT * 
		FROM cpm_rmcs r, cpm_lookup_rmcs lr 
		WHERE 
		lr.rmc_lookup=r.rmc_num AND 
		r.subsidiary_id ".$equals." ".$subsidiary_id." AND 
		lr.rmc_ref = '".$rmc_ref."' 
		";
		$result = $mysql->query($sql, 'ref to num');
		$this->row = $mysql->fetch_array($result);
		
		$mysql->data_seek($result,0);
		$row = $mysql->fetch_array($result);
		
		return $row['rmc_num'];
	}
	
	/**
	 * Subsidiary ID to Name 
	 * 
	 * @param	int			$subsidiary_id			Subsidiary ID
	 * @return	String		$subsidiary_name		Subsidiary Name
	 */
	function get_database($subsidiary_id){
		
		$mysql = new mysql;

		$sql = "SELECT * 
		FROM cpm_subsidiary 
		WHERE 
		subsidiary_id = '$subsidiary_id'";
		
		$result = $mysql->query($sql, 'get db');
		if($result !== false){
			$row = $mysql->fetch_array($result);
			return $row['subsidiary_name'];
		}
	}
	
	/**
	 * Get OD from PM Name 
	 * 
	 * @param	String		$pm_name				PM Name
	 * @return	String		$rmc_op_director_name	OD Name
	 */
	function get_od_from_pm($pm_name){
		
		$mysql = new mysql;

		$sql = "SELECT rmc_op_director_name 
		FROM cpm_rmcs 
		WHERE property_manager = '$pm_name'";
		
		$result = $mysql->query($sql, 'get od');
		if($result !== false){
			$row = $mysql->fetch_array($result);
			return $row['rmc_op_director_name'];
		}
	}
	
	/**
	 * Get RM from RMC Ref
	 * 
	 * @param	String		$rmc_ref				RMC Ref
	 * @return	String		$regional_manager		RM Name
	 */
	function get_rm_from_rmc_ref($rmc_ref){
		
		$mysql = new mysql;

		$sql = "SELECT regional_manager 
		FROM cpm_rmcs r
		INNER JOIN cpm_lookup_rmcs lr ON lr.rmc_lookup = r.rmc_num
		WHERE lr.rmc_ref = '$rmc_ref'";
		
		$result = $mysql->query($sql, 'get rm');
		if($result !== false){
			$row = $mysql->fetch_array($result);
			return $row['regional_manager'];
		}
	}
	
	/**
	 * Get PM from Resident Ref
	 * 
	 * @param	String		$resident_ref			Resident Ref
	 * @return	String		$property_manager		PM Name
	 */
	function get_pm_from_resident_ref($resident_ref){
		
		$mysql = new mysql;

		$sql = "SELECT property_manager 
		FROM cpm_rmcs  r
		INNER JOIN cpm_residents res ON res.rmc_num = r.rmc_num
		INNER JOIN cpm_lookup_residents lres ON lres.resident_lookup = res.resident_num
		WHERE lres.resident_ref = '$resident_ref'";
		
		$result = $mysql->query($sql, 'get pm');
		if($result !== false){
			$row = $mysql->fetch_array($result);
			return $row['property_manager'];
		}
	}
	
	/**
	 * Get Number of Users under
	 * 
	 * @param	String		$from_type				From Type
	 * @param	String		$ref					From Reference
	 * @param	String		$to_type				To Type
	 * @return	int			$num					Num of Users 
	 */
	function get_num_users($from_type, $ref, $to_type){
		
		$mysql = new mysql;
		
		$from_type = $this->short_type_to_db($from_type);
		$to_type = $this->short_type_to_db($to_type);

		$sql = "SELECT $to_type
		FROM cpm_rmcs
		WHERE $from_type = '".$ref."'
		AND $to_type <> ''
		GROUP BY $to_type";
		
		$result = $mysql->query($sql, 'get num');
		$num = $mysql->num_rows($result);
		return $num;
	}
	
	/**
	 * Convert short type into field name
	 * 
	 * @param	String		$type					From Type
	 * @return	String		$field					Field Name
	 */
	function short_type_to_db($type){
			
		$field = '';

		switch($type){
			case "od":
				$field = 'rmc_op_director_name';
				break;
			case "rm":
				$field = 'regional_manager';
				break;
			case "pm":
				$field = 'property_manager';
				break;
			case "rmc":
				$field = 'rmc_num';
				break;
		}
		
		return $field;
	
	}
}
?>