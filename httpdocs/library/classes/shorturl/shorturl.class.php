<?php
require_once($UTILS_SERVER_PATH."library/classes/mysql/mysql.class.php");

class shorturl {


	public function mt_rand_str ($l, $c = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890') {
		for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
		return $s;
	}

	/**
	 * Returns string identified by $hash.
	 * @param string $hash
	 * @return string|null
	 */
	public function getStringByHash($hash) {
		
		$mysql = new mysql();
		$sql = "SELECT hash, url FROM survey_short_url where hash='".$hash."'";
		$result = $mysql->query($sql, 'Check if hash exists');
		$num_rows = $mysql->num_rows($result);
		if($num_rows > 0){
			while($row = $mysql->fetch_array($result)){
				return $row['url'];
			}
		}
		
		return null;
	}

	/**
	 * Returns hash identified by $string.
	 * @param string $string
	 * @return string|null
	 */
	public function getHashByString($string) {

		$mysql = new mysql();
		$sql = "SELECT hash, url FROM survey_short_url where url='".$string."'";
		$result = $mysql->query($sql, 'Check if url exists');
		$num_rows = $mysql->num_rows($result);
		if($num_rows > 0){
			while($row = $mysql->fetch_array($result)){
				return $row['hash'];
			}
		}

		return null;
	}
	
	public function hash_exist($hash){
		
		$mysql = new mysql();
		$sql = "SELECT hash FROM survey_short_url where hash='".$hash."'";
		$result = $mysql->query($sql, 'Check if hash exists');
		$num_rows = $mysql->num_rows($result);
		if($num_rows > 0){
			return true;
		}
		
		return false;
	}

	/**
	 * Adds hash identified by $string if it does not already exist.
	 * @param string $string
	 * @return string
	 */
	public function addHashByString($string) {
		
		$mysql = new mysql;
		$hash = $this->getHashByString($string);
		
		if ($hash !== null) { //hash already exists
			return $hash;
		}

		$hash = $this->mt_rand_str(6);
				
		$sql = "insert into survey_short_url set hash = '".$hash."', url = '".$string."'";
		$lastInsertId = $mysql->insert($sql, 'Insert ');	

		$this->addHashByString($string);
		
		return $hash;
		
	}
}