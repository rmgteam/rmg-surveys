<?php
require_once("utils.php");
require_once($UTILS_SERVER_PATH."library/classes/survey/survey.class.php");
require_once($UTILS_SERVER_PATH."library/classes/survey/survey_resident.class.php");
require_once($UTILS_SERVER_PATH."library/classes/tenant/tenant.class.php");
require_once($UTILS_SERVER_PATH."library/classes/user/user.class.php");
require_once($UTILS_SERVER_PATH."library/classes/template/survey_template.class.php");

if($_REQUEST['a'] == 'q'){
	$i = 0;
	
	$mysql = new mysql();
	$array = array();
	
	$survey = new survey();
	$survey->serial_to_survey($_REQUEST["survey_id"]);
	
	if(strlen($_REQUEST["serial_ref"]) == 8){
	
		$survey_resident = new survey_resident();
		
		$survey_resident->serial_to_survey_resident($_REQUEST["serial_ref"], $survey->survey_id);
		$survey_resident->click_through();
		$can_send = $survey_resident->answered();
		
	}else{
		$admin_user = new user($_REQUEST["serial_ref"], "serial");
		
		if(is_numeric($admin_user->survey_user_id)){
			$can_send = true;
		}else{
			$can_send = false;
		}
	}
	
	if($can_send){
	
		$survey_question = new question();
		
		$result = $survey_question->get_questions($survey->survey_id);
		$num_rows = $mysql->num_rows($result);
		if($num_rows > 0){
			while($row = $mysql->fetch_array($result)){
				$array[$i] = $survey_question->get_question_info($row['question_id']);
				$i++;
			}
		}
	}else{
		$array["save_result"] = "error";
	}
	
 	echo json_encode($array);
 	exit;
}
elseif($_REQUEST['a'] == 'save'){
	
	$survey = new survey_resident();
	$array = $survey->save($_REQUEST);
	
	echo json_encode($array);
 	exit;
}
elseif($_REQUEST['a'] == 'check'){
	
	$tenant = new tenant();
	$tenant->ref_to_tenant($_REQUEST['tenant_ref_input']);
	
	$survey = new survey();
	$survey->serial_to_survey($_REQUEST["survey_id"]);
	
	$survey_resident = new survey_resident();
	
	$correct_details = $survey_resident->check_details($survey->survey_id, $_REQUEST['serial_ref'], $tenant->tenant_num);
	
	if($correct_details){
		$done = $survey_resident->survey_to_do($survey->survey_id, $_REQUEST['serial_ref'], $tenant->tenant_num);
		
		$array = array();
		if($tenant->rmc_name != '' && $done == false){
			// Add frequency
			$array['result'] = "success";
			$array['reason'] = "";
		}elseif($done == true){
			$array['result'] = "fail";
			$array['reason'] = "done";
		}
		
		$array['man_co'] = $tenant->rmc_name." (".$tenant->rmc_ref.")";
	}else{
		$array['result'] = "fail";
		$array['reason'] = "ref";
	}
	
	echo json_encode($array);
 	exit;
}
else{

	$page = str_replace('.php','',basename($_SERVER['PHP_SELF']));
	
	// Populate survey object 
	$survey = new survey();
	$survey->serial_to_survey($_REQUEST["survey_id"]);
	
	$title = 'RMG Surveys - ' . $survey->survey_name;
	$survey_name = $survey->survey_name;
	$survey_desc = $survey->survey_desc;


	$icon = 'home';
	$tpl = new survey_template(get_defined_vars());
	echo $tpl->fetch();
}
?>