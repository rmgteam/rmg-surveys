(function( $, undefined ) {
	
	$.widget( "ui.processing", {
		options: {
			element: {},
			processing_dialog: {},
			no_processing: 0
		},
	
		_create: function() {
			
			var self = this;
			var options = this.options;
	
			options.element = this.element;
			self.build();
		},
			
		build: function() {
			
			var self = this;
			var options = this.options;
			
			options.processing_dialog = $( document.createElement('div') )
				.attr({
					'id' :		'processing-dialog', 
					'title' :	"Loading..."
				})
				.addClass('dialog_1');			
			
			$('body').append(options.processing_dialog);
	
			$('#processing-dialog').dialog({
				modal: true,
				autoOpen: false,
				resizable: false,
				width: '600px',
			   	closeOnEscape: false,
			   	open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
			   	close: function(event, ui) { $(".ui-dialog-titlebar-close").show();}
			});

			self._trigger('build');
		},
		
		start: function(text, start){
			var self = this;
			var options = this.options;
			
			if(typeof text === 'undefined'){
				text = '';
			}
			
			if(typeof start === 'undefined'){
				start = 100;
			}
			
			options.no_processing ++;
			no_processing = options.no_processing;
			
			var processing_dialog_inner = $( document.createElement('div') )
				.attr({
					'id' :		'processing-progress_' + options.no_processing
				})
				.addClass('progress progress-striped active');
	
			var processing_bar = $( document.createElement('div') )
				.addClass('bar')
				.css({
					'width' : start + '%'
				})
				.html(text)
				.appendTo(processing_dialog_inner);
			
			options.processing_dialog.append(processing_dialog_inner);
			
			$('#processing-dialog').dialog('open');

			self._trigger('start');
			
			return no_processing;

		},
		
		update: function(i, amount){
			var self = this;
			var options = this.options;

			$('#processing-progress_' + i).find('div').css('width', amount + '%');

			self._trigger('update');
		},
		
		end: function(i){
			var self = this;
			var options = this.options;
			
			this.update(i, 100);

			$('#processing-progress_' + i).remove();
			
			var still_processing = false;
			
			for(var j=0;j<=options.no_processing;j++){
				if($('#processing-progress_' + j).length > 0){
					still_processing = true;
				}
			}
			
			if(still_processing == false){
				$('#processing-dialog').dialog('close');
			}

			self._trigger('end');
		},
       
		destroy: function() {
			var options = this.options;
			
			$('#processing-dialog').remove();

			this._trigger('destroy');

			return $.Widget.prototype.destroy.call( this );
		},
		
		wait: function(millis){
			var date = new Date();
			var curDate = null;
			do { curDate = new Date(); }
			while(curDate-date < millis);
			return true;
		}
       
	});
}( jQuery ) );