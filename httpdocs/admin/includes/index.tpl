<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.rmc_selector.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.staff_selector.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.search_filter.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.slidedeck.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.radio.js"></script>

<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/flot/jquery.flot.categories.min.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/flot/jquery.flot.time.min.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/flot/jquery.flot.selection.min.js"></script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/flot/jquery.flot.pie.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/selectors.css" />
<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.search_filter.css" />
<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.radio.css" />
<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/slidedeck.skin.css" />

<script type="text/javascript" src="<?=$UTILS_WEBROOT?>admin/includes/index.js"></script>

<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/flot.css" />

<div id="sent_vs_recd">
</div>
<div id="survey_perc">
</div>
<!--
<div id="results_div" class="flot_box" style="width: 100%; float: none;">
	<div class="record_heading">
		<div class="full">Latest Surveys</div>
	</div>
	<div class="form-container">
		<div class="small_table">
			<table id="results_table" class="display" width="100%">
				<?=$latest_results_data;?>
			</table>
		</div>
	</div>
</div>
-->
<div id="sent_recd_dialog" title="Settings" style="display:none;">
	<form id="form_sent_recd" name="form_sent_recd" class="form-horizontal">
		<fieldset>
			<div class="control-group">
				<label class="control-label" id="survey_name_label">From Date</label>
				<div class="controls">
					<div class="input-prepend input-append" data-role="acknowledge-input">
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter a from date."><i class="icon-question-sign"></i></div>
			        	<input type="text" class="date" id="sent_recd_from_input" name="sent_recd_from_input" value="<?=$days30;?>" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" id="survey_name_label">To Date</label>
				<div class="controls">
					<div class="input-prepend input-append" data-role="acknowledge-input">
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter a to date."><i class="icon-question-sign"></i></div>
			        	<input type="text" class="date" id="sent_recd_to_input" name="sent_recd_to_input" value="<?=$today;?>" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>

<div id="survey_perc_dialog" title="Settings" style="display:none;">
	<form id="form_survey_perc" name="form_survey_perc" class="form-horizontal">
		<fieldset>
			<div class="control-group">
				<label class="control-label" id="survey_name_label">From Date</label>
				<div class="controls">
					<div class="input-prepend input-append" data-role="acknowledge-input">
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter a from date."><i class="icon-question-sign"></i></div>
			        	<input type="text" class="date" id="survey_perc_from_input" name="survey_perc_from_input" value="<?=$days30;?>" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" id="survey_name_label">To Date</label>
				<div class="controls">
					<div class="input-prepend input-append" data-role="acknowledge-input">
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter a to date."><i class="icon-question-sign"></i></div>
			        	<input type="text" class="date" id="survey_perc_to_input" name="survey_perc_to_input" value="<?=$today;?>" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>