<?php
require_once($UTILS_SERVER_PATH."library/classes/template/template.class.php");


Class login_template Extends Template {
	
	function login_template($array){
		
		$this->parse($array);
		
		Global $UTILS_WEBROOT;
		Global $UTILS_LOG_PATH;
		Global $UTILS_WEBROOT;
		Global $UTILS_URL_BASE;
		Global $UTILS_SERVER_PATH;

		$page = str_replace('.php','',basename($_SERVER['PHP_SELF']));
		
		$this->Template($UTILS_SERVER_PATH.'/admin/includes/login.tpl');

		$this->parse(get_defined_vars());
		
		$this->main_content = $UTILS_SERVER_PATH.'/admin/includes/'.$page.'.tpl';
		$this->var_content = 'content';
	}
	
}