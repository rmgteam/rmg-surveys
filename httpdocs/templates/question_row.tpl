<table id="question_row" class="template_include">
	<thead>
		<tr>
			<th width="7%">No.</th>
			<th width="55%">Question</th>
			<th width="9%">Type</th>
			<th width="15%">No. Answers</th>
			<th width="14%">Delete</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataTables_empty">Loading</td>
			<td class="dataTables_empty">Data</td>
			<td class="dataTables_empty">From</td>
			<td class="dataTables_empty">Server</td>
			<td class="dataTables_empty">&nbsp;</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th><input type="text" value="Search No" class="search_init" style="display:none;" /></th>
			<th><input type="text" value="Search Question" class="search_init" /></th>
			<th><input type="text" value="Search Type" class="search_init" /></th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</tfoot>
</table>