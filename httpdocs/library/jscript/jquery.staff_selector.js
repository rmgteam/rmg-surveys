(function( $, undefined ) {

	var emptyFunction = function(){};

	$.widget( "ui.staff_selector", {
		options: {
			objID: '',
			buttonText: '(Please Select)',
			disabledNonText: '(None Selected)',
			link: '',
			link_attach: false,
			multi_select: false,
			type: 'od',
			parent_id: '',
			is_disabled: false,
			beforeEnd: emptyFunction,
			show_ref: true,
			myApp: {},
			css_width: '',
			page: '/templates/staff_selector.php',
			template: '/templates/staff_selector_row.tpl'
		},
	
		_create: function() {
			
			var self = this;
			var options = this.options;
			var classes = this.classes;
			
			$.get(options.template, function (data) {
				var template_html = data;
				self.build(template_html);
				self._trigger('_create');
			});
		},
		
		build: function(template_html) {
			
			var self = this;
			var options = this.options;
			var classes = this.classes;
	
			options.objID = this.element.attr("id");
			
			var staff_search_dialog = $( document.createElement('div') )
				.attr({
					'id' :		options.objID + '_search_dialog', 
					'title' :	"Find A Member of Staff"
				})
				.addClass('dialog_1');
	
			var search_dialog_inner = $( document.createElement('div') )
				.css({
					'margin':	'0px',
					'overflow': 'hidden'
				});
	
			var form = $( document.createElement('form') )
				.attr({
					'id' :		options.objID + '_search_form', 
					'name' :	options.objID + '_search_form'
				});
	
			var slidedeck_frame = $( document.createElement('div') )
				.attr({
					'id' :		options.objID + '_slidedeck_frame'
				})
				.addClass('skin-slidedeck');
			
			var dl = $( document.createElement('dl') )
				.attr({
					'id' :		options.objID + '_slide'
				})
				.addClass('slidedeck');			
	
			var dt = $( document.createElement('dt') )
				.html('Search');
			
			dl.append(dt);
	
			var dd = $( document.createElement('dd') );

			var list = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_list'
				});
	
			var heading = $( document.createElement('div') )
				.addClass('record_heading');
	
			var heading_text = $( document.createElement('div') )
				.addClass('left')
				.html('Search Facility');
	
			heading.append(heading_text);
			list.append(heading);
	
			var filter = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_search_filter'
				})
				.appendTo(list);

			dd.append(list);
			dl.append(dd);
	
			var dt = $( document.createElement('dt') )
				.html('Results');
			
			dl.append(dt);
	
			var dd = $( document.createElement('dd') );
	
			var waiting = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_waiting'
				})
				.addClass('selector_waiting')
				.html('<p style="text-align:center;font-size:16px;">Retrieving data...</p><p style="text-align:center; margin-top:15px;"><img src="/images/ajax-loader-2.gif" /></p>')
				.appendTo(dd);
	
			var list = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_results_list'
				});
	
			var table_div = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_results_table_div'
				});
				
			var table = $( document.createElement('table') )
				.attr({
					'id':			options.objID  + '_results_table',
					'cellpadding':	'0',
					'cellspacing':	'0',
					'border':	'0',
					'width':	'100%'
				})
				.addClass('display')
				.html(template_html)
				.appendTo(table_div);
	
			list.append(table_div);
			
			dd.append(list);
			dl.append(dd);

			var dt = $( document.createElement('dt') )
				.html('staff');
			
			dl.append(dt);
	
			var dd = $( document.createElement('dd') );

			var waiting = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_details_waiting'
				})
				.addClass('selector_waiting')
				.html('<p style="text-align:center;font-size:16px;">Retrieving data...</p><p style="text-align:center; margin-top:15px;"><img src="/images/ajax-loader-2.gif" /></p>')
				.appendTo(dd);

			var row = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_details_no_result'
				})
				.html('Please select a staff member')
				.appendTo(dd);

			var list = $( document.createElement('div') )
				.attr({
					'id':		options.objID + '_details_list'
				});

			var row = $( document.createElement('div') )
				.addClass('search_filter_row');

			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>Name</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_name'
				})
				.appendTo(row);

			list.append(row);

			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>No. of Regional Managers</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_rms'
				})
				.appendTo(row);
			
			list.append(row);

			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>No. of Property Managers</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_pms'
				})
				.appendTo(row);
				
			list.append(row);

			var row = $( document.createElement('div') )
				.addClass('search_filter_row');
	
			var col = $( document.createElement('div') )
				.addClass('search_filter_label')
				.html('<strong>No. of Properties</strong>')
				.appendTo(row);
			
			var col = $( document.createElement('div') )
				.addClass('search_filter_fields')
				.attr({
					'id':		options.objID + '_details_rmcs'
				})
				.appendTo(row);
				
			list.append(row);

			dd.append(list);
			dl.append(dd);
			
			slidedeck_frame.append(dl)
			form.append(slidedeck_frame);
	
			search_dialog_inner.append(form)
			
			var save_button = $( document.createElement('a') )
				.html('Save')
				.attr({
					'id':		options.objID + '_multi_save'
				})
				.addClass('jButton')
				.css({
					'float':'right',
					'position':'relative',
					'margin': '0 10px 0 0'
				})
				.bind("click", {'self' : self}, function(e){

					var self = e.data['self'];
					var options = self.options;
					var staff_refs = '';
					
					$('#' + options.objID + '_results_table tbody div.tickbox').each(function (i) {
						if($(this).find("input[type='checkbox']").is(':checked')){
							staff_refs += $(this).closest('tr').attr('id') + ','; 
						}
					});
					
					self.set(staff_refs.slice(0,-1));
					
				});
	
			search_dialog_inner.append(save_button)
			
			staff_search_dialog.append(search_dialog_inner);
			
			$('body').append(staff_search_dialog);
	
			$('#' + options.objID + '_search_dialog').dialog({
				modal: true, 
				width: 900, 
				height:570,
				autoOpen: false,
				open: function(event, ui) { 
					$(".ui-dialog-titlebar-close").show();
				},
				close: function(event, ui) {
					$('#' + options.objID + '_slide').slidedeck('goTo','1');
				}
			});
			
			$('#' + options.objID + '_slide').slidedeck();
			$('#' + options.objID + '_slide').bind('slidedeck_spine_click', {'self':self}, function(e){
				var self = e.data['self'];
				var options = self.options;
				
				if($(this).slidedeck('getCurrent') == '3'){
					$('#' + options.objID + '_details_no_result').show();
					$('#' + options.objID + '_details_list').hide();
				}
			});
			
			if(options.link_attach === true){
				
				$("#" + options.objID).attr("href","Javascript:;");
				options.link = options.objID;
			}
			else{
		
				options.link = options.objID + '_a';
	
				var selector_block = $( document.createElement('div') )
					.attr('id', options.objID + '_block')
					.addClass('selector_block');
	
				var selector_buttons = $( document.createElement('div') )
					.attr('id', options.objID + '_buttons')
					.addClass('selector_buttons');
	
				var search_button = $( document.createElement('a') )
					.attr({
						'id':	 	options.objID + '_a',
						'title':	"Search"
					})
					.addClass('ui-button-info')
					.appendTo(selector_buttons);
				
				var clear_button = $( document.createElement('a') )
					.attr({
						'id':	 	options.objID + '_clear',
						'title':	"Clear"
					})
					.addClass('ui-button-info')
					.appendTo(selector_buttons);
				
				var info_button = $( document.createElement('a') )
					.attr({
						'id':	 	options.objID + '_info',
						'title':	"View"
					})
					.addClass('ui-button-info')
					.appendTo(selector_buttons);
	
				selector_block.append(selector_buttons);
	
				var chosen_item = $( document.createElement('div') )
					.attr('id', options.objID + '_select')
					.addClass('ui-blue-text')
					.html(options.buttonText)
					.appendTo(selector_block);
	
				var hidden_value = $( document.createElement('input') )
					.attr({
						'id':	 	options.objID + '_input',
						'name':		options.objID + '_input',
						'type':		'hidden'
					})
					.appendTo(selector_block);
	
				$("#" + options.objID).append(selector_block);
				
				// append the loading message
				var loading = $( document.createElement('div') )
					.attr('id', options.objID + '_loading')
					.addClass('loading_msg_1');
		
				$("#" + options.objID).append(loading);
			
			}
			
			if(options.multi_select == false){
				$('#' + options.objID + '_multi_save').hide();
			}

			$('#' + options.objID + '_search_filter').search_filter({
				'values':[
					{
						'id': 		options.objID + '_staff_name_input',
						'type': 	'input',
						'value': 	'staff_name',
						'html':		'Name'
					}
				],
				'override': true,
				'page': options.page,
				'type_name': options.objID + '_type'
			});
			
			//Bind click event to the icon
			$('#'+options.objID + '_a')
				.button({
					text: false,
					icons: {
			            primary: "ui-icon-search"
			        }
				})
				.bind('click', {'self' : self}, function(e){
					var self = e.data['self'];
					self.open();
				})
				.append('<i class="icon-search"></i>')
				.find('span').each(function(){
					$(this).remove();
				});
			
			$('#'+options.objID + '_clear')
				.button( {
					text: false,
					icons: {
						primary: "ui-icon-closethick"
					}
				})
				.click(function() {
					self.clear();
				})
				.removeClass('ui-corner-all')
				.append('<i class="icon-remove"></i>')
				.find('span').each(function(){
					$(this).remove();
				});
			
			$('#'+options.objID + '_info')
				.button( {
					text: false,
					icons: {
						primary: "ui-icon-info2"
					}
				})
				.click(function() {
					self.view($("#" + options.objID + "_input").val());
				})
				.removeClass('ui-corner-all')
				.addClass('ui-corner-right')
				.append('<i class="icon-info-sign"></i>')
				.find('span').each(function(){
					$(this).remove();
				});

			options.css_width = $("#" + options.objID).css('width').replace('px', '');

			var ss = document.styleSheets;

		    for (var i=0; i<ss.length; i++) {
		        var rules = ss[i].cssRules || ss[i].rules;

		        for (var j=0; j<rules.length; j++) {
			        var selector_text = rules[j].selectorText;
		            if (selector_text === ".search_filter_fields div:first-child" || selector_text === ".search_filter_fields DIV:first-child") {
		            	options.css_width = rules[j].style['width'];
		            }
		        }
		    }
			
			$('.jButton:not(.ui-button)').button();
			this.set_bind();
			this.clear();

			this._trigger('_create');
		},

		set_bind: function(){
			var self = this;
			var options = this.options;

			var parent = $("#" + options.objID).parent();
			var has_dn = parent.css('display');

			var i = 0;

			while(has_dn != 'none' && i < 5){
				parent = parent.parent();
				has_dn = parent.css('display');
				i++;
			}

			parent
				.data('overrideShow', "true")
				.bind('beforeShow', {'options' : options}, function(e){
					var options = e.data['options'];
					
					if($("#" + options.objID + "_input").val() != ''){
						$("#" + options.objID + "_select").css({
							"width": '0px'
						});
					}
				})
				.bind('afterShow', {'self' : self}, function(e){
					var self = e.data['self'];
					var options = self.options;
	
					if($("#" + options.objID + "_input").val() != ''){
						self.recalculate();
					}
				});

			this._trigger('set_bind');
		},

		open: function() {
			var self = this;
			var options = this.options;
			
			$('#' + options.objID + '_search_dialog').dialog('open');

			if(options.is_disabled == false){
				if(options.multi_select == false){
					$('#' + options.objID + '_multi_save').hide();
				}else{
					$('#' + options.objID + '_multi_save').show();
				}
			}else{
				$('#' + options.objID + '_multi_save').hide();
			}

			$('#' + options.objID + '_results_list').hide();
			$('#' + options.objID + '_waiting').hide();

			$('#' + options.objID + '_details_list').hide();
			$('#' + options.objID + '_details_waiting').hide();
			$('#' + options.objID + '_details_no_result').hide();

			$('#' + options.objID + '_search_filter').search_filter('setOption', 'callback', function(){$('#' + options.objID).staff_selector('search');});

			if(options.type != 'all'){
				$('#' + options.objID + '_search_filter').search_filter('setOption', 'search_empty',true);
			}else{
				$('#' + options.objID + '_search_filter').search_filter('setOption', 'search_empty',false);
			}
			
			$('#' + options.objID + '_search_filter').search_filter('set_focus', 1, 'staff_name');
			
			self.reset_pos();
			
			setTimeout(function(){
				$('#' + options.objID + '_search_filter').search_filter('show_field', 1);
			}, 500);

			this._trigger('open');
		},

		reset_pos: function(){
			var self = this;
			var options = this.options;


			if(options.is_disabled == true){
				$('#' + options.objID + '_search_filter').hide();
			}else{
				$('#' + options.objID + '_search_filter').show();
			}

			this._trigger('reset_pos');
		},
		
		recalculate: function(vari) {
			
			var options = this.options;

			var pwidth = $("#" + options.objID).actual('width');
			var tag_name = $("#" + options.objID).get(0).tagName;
			var css_width = options.css_width;
			
			if((tag_name == 'SPAN' || tag_name == 'DIV') && (css_width == '100%' || css_width == 'auto')){
				pwidth = $("#" + options.objID).parent().actual('width');
				$("#" + options.objID).addClass('selector_surround');
			}
			
			var bwidth = $("#" + options.objID + "_buttons").outerWidth(true);
			
			var span_width_2 = pwidth - (bwidth + 1);

			var t = $( document.createElement('span') )
				.attr('id', options.objID + '_text_test')
				.addClass('ui-blue-text')
				.css({
					'display': 'inline-block'
				})
				.html($("#" + options.objID + "_select").html());
			
			$('body').prepend(t);
			
			var span_width = $('#' + options.objID + '_text_test').actual( 'outerWidth', { includeMargin : true });
			
			$('#' + options.objID + '_text_test').remove();
			
			if(span_width > span_width_2){
				span_width = span_width_2;
			}
			
			if(vari == true){
				span_width = 'auto';
			}else{
				span_width += 'px';
			}
			
			$("#" + options.objID + "_select").css({
				"width": span_width
			});

			var pheight =  $("#" + options.objID + "_select").actual('outerHeight');

			if($("#" + options.objID).parent().actual('outerHeight') < pheight){
				$("#" + options.objID).parent().css("height", pheight + "px");
			}
		},

		setOption: function(key, value){

			$.Widget.prototype._setOption.apply( this, arguments );
		},
	
		/* Enable the rmc selector. */
		enable: function() {
			var self = this;
			var options = this.options;
			if(options.is_disabled == true){
				options.is_disabled = false;
				
				if( $('#' + options.objID + '_input').val() != "" ){
					$('#' + options.objID + '_clear').show();
				}
				
				if(options.multi_select == false){
					$('#' + options.objID + '_multi_save').hide();
				}else{
					$('#' + options.objID + '_multi_save').show();
				}

				if($("#" + options.objID + '_select').html() == options.disabledNonText){
					$("#" + options.objID + '_select').html(options.buttonText);
					self.recalculate();
				}
				
				$('#' + options.link).show();

				$("#" + options.objID + '_info').removeClass('ui-corner-left');
				$("#" + options.objID + '_info').removeClass('ui-corner-all');
				$("#" + options.objID + '_info').addClass('ui-corner-right');
				$("#" + options.objID + '_slide').slidedeck("enable");
			}
			
			this._trigger('enable');
		},
	
		/* Enable the rmc selector. */
		disable: function() {
			var self = this;
			var options = this.options;
			if(options.is_disabled == false){
				options.is_disabled = true;
				$('#' + options.objID + '_clear').hide();

				$('#' + options.objID + '_multi_save').hide();

				if($("#" + options.objID + '_select').html() == options.buttonText){
					$("#" + options.objID + '_select').html(options.disabledNonText);
					self.recalculate();
				}
				
				$('#' + options.link).hide();
				$("#" + options.objID + '_info').removeClass('ui-corner-left');
				$("#" + options.objID + '_info').addClass('ui-corner-all');
				$("#" + options.objID + '_slide').slidedeck("disable");
			}
			
			this._trigger('disable');
		},
		
		/* Erase the input field and hide the staff selector. */
		clear: function() {
		
			var options = this.options;
			
			$("#" + options.objID + "_input").val('');
	  	 	$("#" + options.objID + "_input_1").val('');
	 	  	$("#" + options.objID + "_select").html(options.buttonText);
			$('#' + options.objID + '_clear').hide();
			$("#" + options.objID + '_info').hide();

			$("#" + options.link).removeClass('ui-corner-left');
			$("#" + options.link).addClass('ui-corner-all');

			$('#' + options.objID + '_results_table tbody div.tickbox').each(function (i) {
				$(this).find("input[type='checkbox']").removeAttr('checked');
			});

			this.recalculate(true);
			this._trigger('clear');
		},

		search: function() {
			var self = this;
			var options = this.options;
			
            if(options.multi_select == false || (options.multi_select == true && options.is_disabled == true)){
				var show_multi = false;
				var name_width = '545';
			}else{
				var show_multi = true;
				var name_width = '500';
			}

			var col_defs = [
				{
					"bVisible":		show_multi,
					"bSortable":	false,
					"sWidth": 		'25' 
				},
				{
					"bVisible":		true,
					"bSortable":	true,
					"sWidth": 		name_width
				},
				{
					"bVisible":		true,
					"bSortable":	false,
					"sWidth": 		'65' 
				}		
			];
			
			$('#' + options.objID + '_slide').slidedeck('goTo', '2');
			
			var sort_array = new Array();

			sort_array = [[ 1 , 'asc' ], [ 2 , 'asc' ]];
			
			options.myApp.table = $('#' + options.objID + '_results_table').dataTable({
				"bJQueryUI": true,
				"bProcessing": false,
				"bDestroy": true,
				"bAutoWidth": false,
				"sScrollY": "330px",
				"sScrollX" : "670",
				"sScrollXInner" : "670",
				"bLengthChange": false,
				"bScrollCollapse": true,
				"bDeferRender": true,
				"bPaginate": false,
			    "bFilter": false,
			    "oLanguage": {
					"sInfoFiltered": ""
				},
				"aoColumns": col_defs,
				"bServerSide": true,
				"sAjaxSource": options.page,
				"sServerMethod": "POST",
				"fnServerData": function ( sSource, aoData, fnCallback ) {
				
					$('#' + options.objID + '_waiting').show();
					$('#' + options.objID + '_results_list').hide();

					var field_values = '';
					var field_type = $("#" + options.objID + '_input').get(0).tagName;
		
					if (field_type == "INPUT"){
						field_values += $("#" + options.objID + '_input').val() + "|";
					}

					var staff_refs = '';
					
					$('#' + options.objID + '_results_table tbody div.tickbox').each(function (i) {
						if($(this).find("input[type='checkbox']").is(':checked')){
							staff_refs += $(this).closest('tr').attr('id') + ','; 
						}
					});
		        	
		        	pushFormData('#' + options.objID + '_search_form', aoData);
		        	aoData.push({name : 'no_fields', value : $('#' + options.objID + '_search_filter').search_filter('get_no_fields')});
		        	aoData.push({name : 'staff_search_php', value : 'get_staff'});
		        	aoData.push({name : 'field_values', value : field_values});
		        	aoData.push({name : 'select', value : options.objID});
		        	aoData.push({name : 'multi', value : options.multi_select});
		        	aoData.push({name : 'type', value : options.type});
		        	aoData.push({name : 'parent_id', value : options.parent_id});
		        	aoData.push({name : 'staff', value : staff_refs});
		        	
		        	$.post(sSource, 
		        	aoData, 
					function(json){
		        		 fnCallback(json);
					}, 
					"json");
		            
		        },
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					var no_tds = 0;

					var id = $(nRow).attr('id');

					if(options.multi_select == false || (options.multi_select == true && options.is_disabled == true)){
						$(nRow).find('td').each(function(){
							if(no_tds != 2){
								$(this).bind('click', {'self':self}, function(e){
									var self = e.data['self'];
									
									self.rowClick(id);
								});
							}
	
							no_tds++;
						});
					}
		        },
		        "fnDrawCallback": function( oSettings ) {
		        	$('#' + options.objID + '_waiting').hide();
					$('#' + options.objID + '_results_list').show();
	        		$('.jButton:not(.ui-button)').button();
	        		
	        		var css_var = {
		        		'overflow':'hidden'
		        	};

	        		var rows = $('#' + options.objID + '_results_table tbody tr').length;
	        		if(rows < 12){
		        		css_var['height'] = 'auto';
	        		}else{
	        			css_var['overflow-y'] = 'scroll';
	        		}

	        		$('#' + options.objID + '_results_table').parent().css(css_var);
        		

					$('#' + options.objID + '_results_table tbody div.tickbox').each(function(){
						var id = $(this).closest('tr').attr('id');
						var checked = false;
						
						if($(this).hasClass('ticked_1')){
							checked = true;
						}

						$(this).removeClass('unticked_1 ticked_1');
						$(this).attr('id', options.objID + '_radio_' + id);

						if($('#' + options.objID + '_radio_' + id  + "_answer").length == 0){
							$(this).radio({'checked':checked});
						}
					});
					
		        },
		        "aaSorting": sort_array
			});

			$('#' + options.objID + '_results_table_wrapper div.fg-toolbar:first').hide();

			this._trigger('search');
		},
	
		/* Set the date(s) directly. */
		set: function(staff_ref) {

			var self = this;
			var options = this.options;
			var field = options.hidden_db_fields;
	
			if(staff_ref != "" && staff_ref != "null" && staff_ref != null){
	
				$("#" + options.objID + "_block").hide();
				$("#" + options.objID + "_loading").show();
				
				$("#" + options.objID + "_input").val(staff_ref);
			   	$("#" + options.objID + "_input_1").val(staff_ref);
			   	$("#" + options.objID + "_select").html(options.buttonText);
	
				if(options.multi_select == false){
					
					$.post(options.page, 
					"staff_search_php=get_details&staff_ref=" + staff_ref + "&select=" + options.objID, 
					function(data){
						var staff_name = data['name'];
			
						if (staff_name == ''){
							staff_name = options.buttonText;
						}
						
						$("#" + data['select'] + "_select").html(staff_name);
						
						if(options.is_disabled == false){
							$('#' + options.objID + '_clear').show();
						}
						
						options.beforeEnd();
						
						$("#" + options.objID + '_info').show();

						$("#" + options.objID + "_loading").hide();
						$("#" + options.objID + "_block").show();
						
						self.recalculate();
						if($('#' + options.objID + '_search_dialog').dialog('isOpen')){
							$('#' + options.objID + '_search_dialog').dialog('close');
						}
						
						if(options.rmc_id != "" && $('#' + options.rmc_id + '_input').val() == ''){
							$('#' + options.rmc_id).rmc_selector('set', data['rmc_num']);
						}

						self._trigger('set', null, data);
					}, 
					"json");
				}
				else{
					
					$.post(options.page, 
					"staff_search_php=set_multi_staff&select=" + options.objID + "&staff=" + staff_ref + "&button=" + options.buttonText, 
					function(data){
						
						$("#" + options.objID + "_input").val(data['staff']);
						
						if (data['num_rows'] > 1){
							$("#" + data['select'] + "_select").html('Multiple Staff Members');
						}else{
							$("#" + data['select'] + "_select").html(data['results']);
						}
						
						if(options.is_disabled == false){
							$('#' + options.objID + '_clear').show();
						}

						options.beforeEnd();
						
						$("#" + options.objID + '_info').show();

						$("#" + options.objID + "_loading").hide();
						$("#" + options.objID + "_block").show();
						
						self.recalculate();
						if($('#' + options.objID + '_search_dialog').dialog('isOpen')){
							$('#' + options.objID + '_search_dialog').dialog('close');
						}
						
						self._trigger('set', null, data);
					}, 
					"json");
				}
	
				$("#" + options.link).removeClass('ui-corner-all');
				$("#" + options.link).addClass('ui-corner-left');
			}
			else{	
				this.clear();
			}
		},
		
		rowClick: function(staff_ref) {
			var self = this;
			var options = this.options;
			this.set(staff_ref);
			$('#' + options.objID + '_search_dialog').dialog('close');
			this._trigger('rowclick');
		},

		view: function(staff_ref) {
			var self = this;
			var options = this.options;

			if(options.is_disabled == false){
				if(options.multi_select == false){
					$('#' + options.objID + '_multi_save').hide();
				}else{
					$('#' + options.objID + '_multi_save').show();
				}
			}else{
				$('#' + options.objID + '_multi_save').hide();
			}
			
			if(!$('#' + options.objID + '_search_dialog').dialog('isOpen')){
				self.open();
				if(options.multi_select == false){
					this.do_view(staff_ref);
				}else{
					$('#' + options.objID + '_search_filter').search_filter('set_field', 1, 'staff_name', '*');
					$('#' + options.objID + '_search_filter').bind('search_filter_submit', function(){
						$('#' + options.objID + '_search_filter').search_filter('set_field', 1, 'staff_name', '');
					});
					$('#' + options.objID + '_search_filter').search_filter('submit');
					$('#' + options.objID + '_search_filter').unbind('search_filter_submit');
					$('#' + options.objID + '_slide').slidedeck('goTo','2');
				}
			}else{
				this.do_view(staff_ref);
			}

			this._trigger('view');
		},

		do_view: function(staff_ref) {
			var self = this;
			var options = this.options;

			if(options.is_disabled == true && options.multi_select == false){
				$('#' + options.objID + '_slide').slidedeck('disable');
			}else{
				$('#' + options.objID + '_slide').slidedeck('enable');
			}

			$('#' + options.objID + '_slide').slidedeck('goTo', 3);
			
			$('#' + options.objID + '_details_start').hide();
			$('#' + options.objID + '_details_list').hide();
			$('#' + options.objID + '_details_waiting').show();
			$('#' + options.objID + '_details_no_result').hide();
			
			$.post(options.page, 
			"staff_search_php=get_details&staff_ref=" + staff_ref + "&select=" + options.objID, 
			function(data){

				$('#' + options.objID + '_details_name').html(data['name']);
				$('#' + options.objID + '_details_rmcs').html(data['rmcs']);
				$('#' + options.objID + '_details_pms').html(data['pms']);
				$('#' + options.objID + '_details_rms').html(data['rms']);
				
				$('#' + options.objID + '_details_list').show();
				$('#' + options.objID + '_details_waiting').hide();
				$('#' + options.objID + '_details_no_result').hide();
			}, 
			"json");

			this._trigger('do_view');
		}
	});
}( jQuery ) );