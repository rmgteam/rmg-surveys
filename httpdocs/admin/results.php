<?php
ini_set("max_execution_time", "1440000");
require_once("utils.php");

// Check if logged in
if($_SESSION['admin_user_serial'] == ""){
	header("Location: /admin/login.php");
	exit;
}

require_once($UTILS_SERVER_PATH."library/classes/survey/survey.class.php");
require_once($UTILS_SERVER_PATH."library/classes/question/question.class.php");
require_once($UTILS_SERVER_PATH."library/classes/answer/answer.class.php");
require_once($UTILS_SERVER_PATH."library/classes/response/response.class.php");
require_once($UTILS_SERVER_PATH."library/classes/template/admin_template.class.php");
require_once($UTILS_SERVER_PATH."library/classes/user/user.class.php");
require_once($UTILS_SERVER_PATH."library/classes/reports/excel.class.php");

$admin_user = new user($_SESSION['admin_user_serial'], "serial");
$survey = new survey($_REQUEST['survey_id'], "id");
$response = new response();

	
if($_REQUEST['a'] == 'rmcs'){

	$result_array = array();
	
	$result_array = $response->get_rmc_results($_REQUEST['survey_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;

}elseif($_REQUEST['a'] == 'question'){

	$result_array = array();
	
	$result_array = $response->get_rmc_question_results($_REQUEST['survey_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;

}elseif($_REQUEST['a'] == 'rmcs_graph'){

	$result_array = array();
	
	$result_array = $response->get_rmc_graph($_REQUEST['survey_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;

}elseif($_REQUEST['a'] == 'question_graph'){

	$result_array = array();

	$result_array = $response->get_rmc_question_graph($_REQUEST['survey_id'], $_REQUEST);

	echo json_encode($result_array);
	exit;
	
}elseif($_REQUEST['a'] == "dump"){
	
	$result_array = dump($_REQUEST);
	
	echo json_encode($result_array);
	exit;
}elseif ($_REQUEST['a'] == "excel"){
	header("Content-type: application/vnd.ms-excel");
	header("Content-disposition: attachment; filename=data_dump-".date("d-F-Y", time()).".xls");
	header("Cache-Control: maxage=1");
	header("Pragma: public");
	
	$report_name = '';
	$info_name = '';
	
	if($_REQUEST['search_select'] == 'dump'){
		$data_array = dump($_REQUEST);
		$report_name = "Data Dump";
	}
	
	$report = new excel($report_name);
	$report->headers($data_array['header']);
	$report->data($data_array['data']);
	$report->save();
	exit;
	
}else{
	
	$title = 'Results - ' . $survey->survey_name;
	$perfect_score = $response->perfect_survey_score($survey->survey_id);
	$icon = 'file-alt';
	$tpl = new admin_template(get_defined_vars());
	$tpl->set( 'results_data', $tpl->set_datatable($UTILS_SERVER_PATH."templates/results_row.tpl") );
	$tpl->set( 'question_data', $tpl->set_datatable($UTILS_SERVER_PATH."templates/question_results_row.tpl") );
	$tpl->set( 'tenants_data', $tpl->set_datatable($UTILS_SERVER_PATH."templates/tenant_results_row.tpl") );
	echo $tpl->fetch();
	
}

function dump($request){


	$mysql = new mysql();
	$data = new data();
	$answer = new answer;
	$result_array = array();
	$header_array = array();
	
	array_push($header_array, 'Property Ref.');
	array_push($header_array, 'Property Name');
	array_push($header_array, 'Operational Director');
	array_push($header_array, 'Regional Manager');
	array_push($header_array, 'Property Manager');
	array_push($header_array, 'Survey Name');
	array_push($header_array, 'Date Survey Sent');
	array_push($header_array, "Date Survey Rec'd");
	array_push($header_array, 'Resident Ref.');
	array_push($header_array, 'Resident Name');
	array_push($header_array, 'Question');
	array_push($header_array, 'Response');
	array_push($header_array, 'Response Value');
	
	$from_date = $data->date_to_ymd($request['dump_from_input']) . '000000';
	$to_date = $data->date_to_ymd($request['dump_to_input']) . '235959';
	
	$sql = "SELECT lrmc.rmc_ref,
	rmc.rmc_name,
	rmc.rmc_op_director_name,
	rmc.regional_manager,
	rmc.property_manager,
	s.survey_name,
	sr.survey_resident_sent_date,
	sr.survey_resident_complete_ymdhis,
	lres.resident_ref,
	cr.resident_name,
	q.question_id,
	q.question_text,
	r.response
	FROM survey_response r
	INNER JOIN survey_resident sr ON r.survey_resident_id = sr.survey_resident_id
	INNER JOIN survey_question q ON r.question_id = q.question_id
	INNER JOIN survey s ON s.survey_id = sr.survey_id
	INNER JOIN cpm_residents cr ON cr.resident_num = sr.resident_num
	INNER JOIN cpm_lookup_residents lres ON lres.resident_lookup = cr.resident_num
	INNER JOIN cpm_rmcs rmc ON rmc.rmc_num = cr.rmc_num
	INNER JOIN cpm_lookup_rmcs lrmc ON lrmc.rmc_lookup = rmc.rmc_num
	WHERE s.survey_id = '".$request['survey_id']."'
	AND survey_resident_complete_ymdhis >= '".$from_date."'
	AND survey_resident_complete_ymdhis <= '".$to_date."'
	ORDER BY r.response_id ASC, q.question_order ASC";
	
	$result_search = $mysql->query($sql, 'Get Dump');
	
	$num_search = $mysql->num_rows($result_search);
	
	$i = 0;
	if($num_search > 0){
	
		while($row_search = $mysql->fetch_array($result_search)){
	
			$result_array[$i][] = $row_search['rmc_ref'];
			$result_array[$i][] = $row_search['rmc_name'];
			$result_array[$i][] = $row_search['rmc_op_director_name'];
			$result_array[$i][] = $row_search['regional_manager'];
			$result_array[$i][] = $row_search['property_manager'];
			$result_array[$i][] = $row_search['survey_name'];
			$result_array[$i][] = $data->ymd_to_date($row_search['survey_resident_sent_date']);
			$result_array[$i][] = $data->ymdhis_to_date($row_search['survey_resident_complete_ymdhis']);
			$result_array[$i][] = $row_search['resident_ref'];
			$result_array[$i][] = $row_search['resident_name'];
			$result_array[$i][] = $row_search['question_text'];
			$result_array[$i][] = $answer->get_answer_text($row_search['response'], $row_search['question_id']);
			$result_array[$i][] = $row_search['response'];
				
			$i++;
				
		}
	}
	
	$output = array();
	$output['header'] = $header_array;
	$output['data'] = $result_array;
	
	return $output;
}
?>