<?php
require_once("utils.php");
require_once($UTILS_SERVER_PATH."library/classes/letter.class.php");
require_once($UTILS_SERVER_PATH."library/classes/tenant/tenant.class.php");
require_once($UTILS_SERVER_PATH."library/classes/survey/survey.class.php");
require_once($UTILS_SERVER_PATH."library/classes/question/question.class.php");
require_once($UTILS_SERVER_PATH."library/classes/data/data.class.php");
require_once($UTILS_SERVER_PATH."library/classes/security/security.class.php");
require_once($UTILS_SERVER_PATH."library/classes/template/template.class.php");
require_once($UTILS_SERVER_PATH."library/classes/pdf/form_to_pdf.class.php");
require_once($UTILS_SERVER_PATH."library/classes/shorturl/shorturl.class.php");

$mysql = new mysql;
$security = new security;
$template = new Template;
$data = new data;
$pdf = new form_to_pdf();

Global $UTILS_SERVER_PATH, $UTILS_CCC_PHONE;
		
$date_today = $data->ymd_to_date($data->today_to_ymd());

$sql = "SELECT r.* 
FROM cpm_residents r
INNER JOIN cpm_residents_extra e ON e.resident_num = r.resident_num
WHERE r.resident_is_active = '1'
AND r.resident_email = ''
AND e.survey_optout = 'N'";

$tenants = $_REQUEST['residents'];

$tenant_array = explode(",", $tenants);

if(count($tenant_array) > 0){
	
	$sql_clause = '';

	foreach($tenant_array as $resident){
		if($resident != ''){
			$sql_clause .= "
			r.resident_num = '".$resident."' OR";
		}
	}

	$sql_clause = substr($sql_clause, 0, -3);

	if($sql_clause != ''){
		$sql .= "
		AND (" . $sql_clause . "
		)";
	}
}

$survey = new survey($_REQUEST['survey_id'], "id");
if($survey->survey_director == 'Y'){
	$sql .= "
		AND is_resident_director = 'Y'";
}
if($sql_clause == ''){
	$sql .= "
		AND rmc_num = '".$_REQUEST['rmc_num']."'";

	$sql_clause = substr($sql_clause, 0, -3);

	if($sql_clause != ''){
		$sql .= "
		AND (" . $sql_clause . "
		)";
	}
}
	
$survey = new survey($_REQUEST['survey_id'], "id");
if($survey->survey_director == 'Y'){
	$sql .= "
	AND is_resident_director = 'Y'";
}
if($sql_clause == ''){
	$sql .= "
	AND rmc_num = '".$_REQUEST['rmc_num']."'";
}

$ai = 0;

$result = $mysql->query($sql, 'Get Post Contractors');
$num_rows = $mysql->num_rows($result);
if($num_rows > 0){

	$letter = new letter('P', 'mm', 'A4');
	$letter->SetDefaultFont('Arial', '', 10);
	$letter->setStyle("b","Arial","B");
	$letter->setStyle("i","Arial","I");
	$letter->setStyle("h","Arial","B", 18);
	$letter->bMargin = 35;
	$letter->header_template = $UTILS_SERVER_PATH . "templates/RMG_Letter_Headed_paper.pdf";
	$letter->line_height = 4.5;
		
	$letter->include_header = true;
	$letter->SetMargins(30, 35, 30);

	while($row = $mysql->fetch_array($result)){
		
		$ai ++;
		
		$tenant = new tenant($row['resident_num']);
		$survey_resident = new survey_resident();		
		$survey = new survey($_REQUEST['survey_id'], 'id');
		
		$letter_type = 1;
		
		$request = array();
		$request['survey_id'] = $_REQUEST['survey_id'];
		$request['tenant_num'] = $tenant->tenant_num;
		
		$c_send = $survey_resident->can_send($survey->survey_id, $tenant->tenant_num);
		
		if($c_send){
			$results_array = $survey_resident->create($request);
			
			$survey_resident = new survey_resident($results_array["id"]);
			
			$survey_resident_serial = $survey_resident->survey_resident_serial;
			$survey_ref = $survey->survey_serial;

			$letter->add_page();

			$letter->parse_html("<p>".$tenant->tenant_name."</p>");

			$address = $tenant->get_address('array');
			$address_lines = count($address);
			$letter->add_letter_address($address);
			for($i=$address_lines;$i<=9;$i++){
				$letter->Ln();
			}
			$letter->parse_html('<p><br />'.$date_today."<br /><br /></p>");
			
			$letter->add_salutation($tenant->tenant_name);
			
			$survey_url = "http://www.rmgsurveys.co.uk/survey/".$survey_ref."/".$survey_resident_serial;
			
			$short_url = new shorturl();
			
			$short_url_string = "http://www.rmgsurveys.co.uk/".$short_url->addHashByString($survey_url);
			
			$text = $template->fetch($UTILS_SERVER_PATH."templates/letter_text.php", get_defined_vars());
				
			$text = $pdf->convert_html($text);
			
			$letter->parse_html($text);
			
			$letter->add_para();
			$letter->add_closing("", "Yours sincerely,", "");
			$letter->parse_html("<p><br />Customer Services<br />Residential Management Group Ltd<br /></p>");
				
			$letter->add_page();
			$letter->parse_html("<p><h>".$survey->survey_name."</h></p>");
			$letter->parse_html("<p><b>Unique Ref: ".$survey_resident_serial."</b></p>");
			$letter->parse_html("<p><b>Tenant Ref: ".$tenant->tenant_ref."</b></p>");
			$letter->add_para();
			$letter->parse_html("<p>For each question please circle your chosen answer. If you need more room to write a comment, please use the reverse of this page.</p>");

			$survey_question = new question();

			$result2 = $survey_question->get_questions($survey->survey_id);
			$num_rows2 = $mysql->num_rows($result2);
			if($num_rows2 > 0){
				while($row2 = $mysql->fetch_array($result2)){

					$the_question = $survey_question->get_question_info($row2['question_id']);

					$text = $template->fetch($UTILS_SERVER_PATH."templates/letter_question.php", get_defined_vars());

					$text = $pdf->convert_html($text);

					$letter->parse_html($text);
				}
			}
		}
	
	}
	$letter->Output();
}else{
	print("<p>There are no letters to print.</p>");	
}
?>