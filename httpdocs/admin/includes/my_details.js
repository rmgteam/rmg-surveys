
$(document).ready(function(){
	
	$('#user_save_button').click(function(){
		do_user_save();
	});
	
	$('#user_cancel_button').click(function(){
		location.href = "/admin/my_details/";
	});
	
	$('#user_cancel_edit_button').click(function(){
		form_read('user');
		$('#user_save_button').hide();
    	$('#user_cancel_button').show();
    	$('#user_cancel_edit_button').hide();
    	$('#user_send_pass_button').show();
    	$('#user_edit_button').show();
	});
	
	var page_crumbs = {
    	0 : 	{ 'url' : "/admin/",		'text'	: "Dashboard" },
    	1 : 	{ 'url'	: "/admin/my_details/",	'text'	: "My Details" }
    };
    gen_crumbs(page_crumbs); 
    
    $('#user_save_button').hide();
    $('#user_cancel_edit_button').hide();
    
    $('#user_edit_button').click(function(){
		form_edit('user');
		$('#user_save_button').show();
    	$('#user_cancel_button').hide();
    	$('#user_cancel_edit_button').show();
    	$('#user_edit_button').hide();
    	$('#user_send_pass_button').hide();
    	
    	
	});
	
	do_user(survey_user_id);
});

	
// Return a helper with preserved width of cells
var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};


function do_user_save(){

	clear_errors();	
	
	var processor = $('body').processing('start', 'Saving Details');
	
	$.post($(location).attr('href'), 
	$('#user-form').serialize() + "&a=save_user",
	function(data){
	
		if(data['save_result'] == 'success'){
			do_user(data['id']);
		}
		else{
			show_error(data['field_names'], data['save_msg']);
		}
		
		$('body').processing('end', processor);
	},
	'json');
}


function do_user(id){
	
	clear_user();
	form_read('user');
	get_user(id);
}


var get_user = function(id, callback){

	if(id != ""){

		var processor = $('body').processing('start', 'Retrieving Details');
		
		$.post($(location).attr('href'), 
		{'a': 'get_user','user_id' : id},
		function(data){
			if(data['user_id'] != ''){
				
				$('#user_id').val(data['id']); 
				
				$('#user_firstname_input').val(data['user_firstname']);
				$('#user_lastname_input').val(data['user_lastname']);
				$('#user_email_input').val(data['user_email']);
				
				$('#user_firstname_span').html(data['user_firstname']);
				$('#user_lastname_span').html(data['user_lastname']);
				$('#user_email_span').html(data['user_email']);
				
				
		    	$('input').each(function(){
		    		if($(this).data('type') == 'password'){
		    			$(this).val('');
		    		}
		    	});
				
				$('#page_title_container h5').html("User - " + data['user_firstname'] + ' ' + data['user_lastname']);
				
				var page_crumbs = {
			    	0 : 	{ 'url' : "/admin/",		'text'	: "Dashboard" },
			    	1 : 	{ 'url'	: "/admin/users.php",	'text'	: "Users" },
			    	2 : 	{ 'url'	: "/admin/user/" + data['id'] + "/",	'text'	: data['user_firstname'] + ' ' + data['user_lastname'] }
			    };
			    gen_crumbs(page_crumbs); 
				
				$('#user_edit_button').show();
				$('#user_cancel_button').show();
				$('#user_save_button').hide();
				$('#user_cancel_edit_button').hide();
				$('#user_send_pass_button').show();
				clear_errors();	
				
				if($.isFunction(callback) === true){
					callback();
				}
			}

			$('body').processing('end', processor);
		},
		'json');
	}
};

function clear_user(){
	$('#user_id').val('');
	$('#user_firstname_input').val('');
	$('#user_lastname_input').val('');
	$('#user_username_input').val('');
	$('#user_email_input').val('');
	$('#user_administrate_select').selectpicker('clear');
	$('#user_create_survey_select').selectpicker('clear');
	$('#user_delete_survey_select').selectpicker('clear');
	$('#user_send_survey_select').selectpicker('clear');
	
	$('#user_firstname_span').html('');
	$('#user_lastname_span').html('');
	$('#user_username_span').html('');
	$('#user_email_span').html('');
	$('#user_administrate_span').html('');
	$('#user_create_survey_span').html('');
	$('#user_delete_survey_span').html('');
	$('#user_send_survey_span').html('');
}
