<?php
require_once("utils.php");

// Check if logged in
if($_SESSION['admin_user_serial'] == ""){
	header("Location: /admin/login.php");
	exit;
}

require_once($UTILS_SERVER_PATH."library/classes/survey/survey.class.php");
require_once($UTILS_SERVER_PATH."library/classes/template/admin_template.class.php");
require_once($UTILS_SERVER_PATH."library/classes/user/user.class.php");
$admin_user = new user($_SESSION['admin_user_serial'], "serial");

if($_REQUEST['a'] == 'surveys'){

	$mysql = new mysql();
	$i = 0;
	$result_array = array();
	
	$survey = new survey();
	$result_array = $survey->get_list($_REQUEST);
	
	echo json_encode($result_array);
	exit;
}
elseif($_REQUEST['a'] == 'delete_survey'){
	
	$survey = new survey($_REQUEST['s'], 'id');
	$result_array = array();
	$result_array['save_result'] = "fail";
	
	$save_result = $survey->delete();
	if( $save_result === true ){
		$result_array['save_result'] = "success";
	}
	
	echo json_encode($result_array);
	exit;
	
}
elseif($_REQUEST['a'] == 'save'){	
	
	$survey = new survey;
	$save_result = array();
	$save_result['save_result'] = "success";
	$save_result['id'] = "";
	
	$save_result = $survey->check($_REQUEST);
	if($save_result['save_result'] == "success"){
		$save_result = $survey->save($_REQUEST);
	}

	echo json_encode($save_result);
	exit;
}else{

	$title = 'Surveys';
	$icon = 'file-alt';
	$tpl = new admin_template(get_defined_vars());
	$tpl->set( 'survey_data', $tpl->set_datatable($UTILS_SERVER_PATH."templates/survey_row.tpl") );
	$tpl->set( 'session', $_SESSION );
	echo $tpl->fetch();
}
?>