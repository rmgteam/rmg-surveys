<?
//==================
// Mail script
//==================
ini_set('display_errors', '1');
ini_set("max_execution_time", "1440000");

require_once("utils.php");

Global $UTILS_SERVER_PATH;
Global $UTILS_EMAIL;

require_once($UTILS_SERVER_PATH."library/classes/survey/survey.class.php");

$a 					= $argv;

if(strpos($a[1], 'id=') !== false){
	$id 			= str_replace("id=", "", $a[1]);
}else{
	$id				= $_REQUEST['survey_id'];
}

$thistime = time();
$mysql = new mysql();		

$survey = new survey($id, 'id');

$table_name = "survey_temp_" . $survey->survey_serial; 

$sql = "CREATE TABLE " . $table_name . "(
`survey_temp_id`  bigint(50) NOT NULL AUTO_INCREMENT ,
`resident_num`  varchar(50) NOT NULL DEFAULT '' ,
`mailer_id`  bigint(50) NOT NULL ,
PRIMARY KEY (`survey_temp_id`)
)";

mysql_query($sql);

$template = new Template();
$iCount = 1000;

$sql = "SELECT *
FROM cpm_residents
WHERE resident_is_active = '1'
AND resident_email <> ''";

$result = $mysql->query($sql, 'Get Post Contractors');
$num_rows = $mysql->num_rows($result);
if($num_rows > 0){	
	$i = 0;
	while($row = $mysql->fetch_array($result)){
		
		if($i == $iCount){
			sleep(30);
			$i = 0;
		}

		$tenant = new tenant($row['resident_num']);
		$survey_resident = new survey_resident();		
		$survey = new survey($id, 'id');
		
		$request = array();
		$request['survey_id'] = $id;
		$request['tenant_num'] = $tenant->tenant_num;
		
		if($survey_resident->can_send($survey->survey_id, $tenant->tenant_num)){
			$results_array = $survey_resident->create($request);
			
			$survey_resident = new survey_resident($results_array["id"]);
			
			$survey_resident_serial = $survey_resident->survey_resident_serial;
			$survey_ref = $survey->survey_serial;
			$letter_type = '1';
		
			$survey_url = "http://www.rmgsurveys.co.uk/survey/".$survey_ref."/".$survey_resident_serial;
			
			$short_url = new shorturl();
			
			$short_url_string = "http://www.rmgsurveys.co.uk/".$short_url->addHashByString($survey_url);
			
			$text = $template->fetch($UTILS_SERVER_PATH."templates/email_text.php", get_defined_vars());
	
			$headers = 'From: ' .$UTILS_EMAIL . "\r\n";
			$headers .= 'Reply-To: ' . $UTILS_EMAIL;
			
			$email = new mailer();
			$email->set_mail_type("Welcome Email");
			$email->set_mail_to($tenant->tenant_email);
			//$email->set_mail_to("marc.evans@rmgltd.co.uk");
			//$email->set_mail_to("clive.gannon@rmgltd.co.uk");
			$email->set_mail_from($UTILS_EMAIL);
			$email->set_mail_subject("Welcome to RMG Surveys");
			$email->set_mail_html($text);
			$email->set_mail_headers($headers);
			$id = $email->send();
			
			if(is_numeric($id)){
				$sql = "INSERT INTO " . $table_name . " SET
				resident_num = '".$survey_resident->resident_num."',
				mailer_id = '".$id."'";
				$mysql->insert($sql);
			}
		}
		
		$i ++;
	}	
}
?>