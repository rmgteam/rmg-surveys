<?
// Modal dialog box used to search for Manager/Directors
require_once("utils.php");
require_once($UTILS_SERVER_PATH."library/classes/rmc.class.php");
require_once($UTILS_SERVER_PATH."library/classes/search_builder/search_builder.class.php");

if ($_REQUEST['rmc_search_php'] == "set_multi_staff"){

	$staff_field = $_REQUEST['staff'];	
	$staff_array = explode(',', $staff_field);
	
	$result_array['results'] = $staff_field;
	$result_array['select'] = $_REQUEST['select'];
	$result_array['staff'] = $_REQUEST['staff'];
	$result_array['num_rows'] = count($staff_array);
	
	echo json_encode($result_array);	
	exit;
}

if ($_REQUEST['staff_search_php'] == "get_details"){
	
	$html_string = '';
	$rmc = new rmc();
	
	$result_array['name'] = $_REQUEST['staff_ref'];
	$result_array['rms'] = $rmc->get_num_users('od', $_REQUEST['staff_ref'], 'rm');
	$result_array['pms'] = $rmc->get_num_users('od', $_REQUEST['staff_ref'], 'pm');
	$result_array['rmcs'] = $rmc->get_num_users('od', $_REQUEST['staff_ref'], 'rmc');
	
	$result_array['select'] = $_REQUEST['select'];
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['staff_search_php'] == "get_staff"){

	$rmc = new rmc();
	$search_builder = new search_builder;
	$mysql = new mysql;
	$security = new security;

	$no_fields = $_REQUEST['no_fields'];
	$field_values = explode("|", $_REQUEST['field_values']);
	$select = $_REQUEST['select'] . '_';
	$select_type = $_REQUEST['select'] . '_type';
	$type = $_REQUEST['type'];
	
	if(strpos($_REQUEST['rmc'], ',') !== false){
		$selected_array = explode(",", $_REQUEST['staff']);
	}else{
		$selected_array = array();
	}
	
	if(strpos($field_values[0], ',') !== false){
		$rmc_array = explode(',', $field_values[0]);
	}else{
		$rmc_array = array();
		if($field_values[0] != ''){
			array_push($rmc_array, $field_values[0]);
		}
	}
	
	$check_array = array_merge($selected_array, $rmc_array);
	
	$field = $rmc->short_type_to_db($type);
	
	/**
	 * Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	*/
	$search_builder->add_index_column('rm.' . $field);
	$search_builder->add_tick_column('rm.' . $field, $check_array);
	$search_builder->add_column('rm.' . $field);
	$search_builder->add_button_column('rm.' . $field, 'View', "$('#" . $_REQUEST['select'] . "').staff_selector", array('view'));
	
	$iColumnCount = count($search_builder->columns);
	
	$values_array = $search_builder->request_to_array($_REQUEST, $select);

	$input =& $_REQUEST;

	$no_fields = $_REQUEST['no_fields'];

	$search_builder->paging(0,100);
	
	// Individual column filtering
	for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
		if ( isset($request['bSearchable_'.$i]) && $_REQUEST['bSearchable_'.$i] == 'true' && $_REQUEST['sSearch_'.$i] != '' ) {
			$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $_REQUEST['sSearch_'.$i] ).'%', 'AND');
		}
	}

	// Input method (use $_GET, $_POST or $_REQUEST)
	$input =& $_REQUEST;
	
	$search_for_ref = false;

	if ($no_fields > 0 ){
		foreach($values_array as $key=>$value) {
			
			$search_term = $value;
			
			switch($key){

				case "staff_name":
					
					break;
				default:
					break;
			}
			
			$search_builder->add_filter('rm.' . $field, 'LIKE', '%'.$search_term.'%', 'AND');
				
		}
	}
	
	if($_REQUEST['multi'] == "true"){
		$search_builder->add_filter('', "(", "", 'AND');
		if(count($rmc_array) > 0){
	
			$sql_query = '';
			foreach($rmc_array as $pa){
	
				$can_continue = false;
	
				if(count($selected_array) > 0){
					for($sa=0;$sa<count($selected_array);$sa++){
						if($selected_array[$sa] == $pa){
							$can_continue = true;
						}
	
						$continue_error .= $selected_array[$sa] . ' ';
					}
				}else{
					$can_continue = true;
				}
				
				if($pa != ''){
					if($can_continue == true){
						$search_builder->add_filter('rm.' . $field, "=", $pa, 'OR');
					}
				}
			}
		}
		if(count($selected_array) > 0){
			foreach($selected_array as $sa){
				if($sa != ''){
					$search_builder->add_filter('rm.' . $field, "=", $sa, 'OR');
				}
			}
		}
		$search_builder->add_filter('', ")", "", 'AND');
	}

	/**
	 * Paging
	 */
	$search_builder->paging(0,100);
	
	$search_builder->add_group('rm.' . $field);

	/**
	 * Ordering
	 */
	$aOrderingRules = array();
	if ( isset( $input['iSortCol_0'] ) ) {
		$iSortingCols = intval( $input['iSortingCols'] );

		if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == 'true' ) {
			$search_builder->add_order($search_builder->columns[intval($_REQUEST['iSortCol_'.$i])], ($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
		}
	}

	$sql = "
	FROM cpm_rmcs rm
	INNER JOIN cpm_lookup_rmcs lr ON (rm.rmc_num = lr.rmc_lookup)
	INNER JOIN cpm_subsidiary s ON (s.subsidiary_id = rm.subsidiary_id)";

	$search_builder->add_sql($sql);
	$output = $search_builder->output($_REQUEST);
	echo json_encode($output);

	exit;
}

?>

<?require_once($UTILS_SERVER_PATH."templates/staff_selector_row.tpl"); ?>