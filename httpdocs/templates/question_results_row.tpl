<table id="survey_row" class="template_include">
	<thead>
		<tr>
			<th width="60%">Name</th>
			<th width="25%">Mode Answer</th>
			<th width="15%">Average</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataTables_empty">Loading Data</td>
			<td class="dataTables_empty">From</td>
			<td class="dataTables_empty">Server</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th><input type="text" value="Search Name" class="search_init" /></th>
			<th><input type="text" value="Search Answer" class="search_init" /></th>
			<th><input type="text" value="Search Average" class="search_init" /></th>
		</tr>
	</tfoot>
</table>