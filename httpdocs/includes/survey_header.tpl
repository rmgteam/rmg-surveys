<div class="header_bar">
	<div class="header">
		<div class="exit">
			<a id="exit_button" class="ui-button-primary ui-btn-small" onclick="$(location).attr('href', '/');"><i class="icon-signout"></i> Exit Survey</a>
		</div>
	</div>
	<div class="header-unit">
		<div class="text">
			<h1><?=$survey_name?></h1>
			<p><?=$survey_desc?></p>
		</div>
		<div class="actions">
			<i class="icon-coffee"></i>
		</div>
	</div>
</div>