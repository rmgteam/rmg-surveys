<?php
require_once("utils.php");	echo 'test';
require_once($UTILS_SERVER_PATH."library/classes/shorturl/shorturl.class.php");


try {
	

	$hash = isset($_GET['hash']) ? $_GET['hash'] : null;
 
	if (!$hash) {
      	header( 'Location: http://www.rmgsurveys.co.uk' ) ;
            	
        exit;
    }
 
    $shorturl = new shorturl();
    $string = $shorturl->getStringByHash($hash);

    header( 'Location: '.$string ) ;
            
     exit;
     
} catch (Exception $e) {

	header( 'Location: http://www.rmgsurveys.co.uk' ) ;
            	
    exit;
}