<?

class field {

	/**
	 * Test if email is valid
	 *
	 * @param	string	$email			Email Address
	 * @return	Boolean					Boolean for success
	 */
	function is_valid_email($email){
	
		if( (preg_match('/(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/', $email)) || (preg_match('/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/',$email)) ) { 
			return true;
		}
		return false;
	}

	/**
	 * Test if date is valid
	 *
	 * @param	string	$date			Date
	 * @param	string	$format			Date format (default 'dd/mm/yyyy')
	 * @return	Boolean					Boolean for success
	 */
	function is_valid_date($date, $format="dd/mm/yyyy"){
		
		if($format == "dd/mm/yyyy"){
			if(preg_match("/([0-9]{2})/([0-9]{2})/([0-9]{4})/", $date) === 1){return true;}
		}
		
		return false;
	}

	/**
	 * Test if date is future date
	 *
	 * @param	string	$date			Date
	 * @param	string	$format			Date format (default 'dd/mm/yyyy')
	 * @return	Boolean					Boolean for success
	 */
	function is_date_in_future($date, $format="dd/mm/yyyy"){
		
		if($format == "dd/mm/yyyy"){
			
			$today_ymd = date("Ymd");
			$date_parts = explode("/", $date);
			$date_ymd = $date_parts[2].$date_parts[1].$date_parts[0];
			
			if($date_ymd > $today_ymd){
				return true;
			}
		}
		
		return false;
	}
	
	
	function is_number($string){
	
		if(preg_match("/^-{0,1}\d+$/", $string) == 1){
			return true;
		}
		return false;
	}
	
}


?>