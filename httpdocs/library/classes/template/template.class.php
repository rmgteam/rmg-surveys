<?php

class Template {
    var $vars = array(); /// Holds all the template variables
    var $file;
	var $main_content = '';
	var $var_content = '';

    /**
     * Constructor
     *
     * @param $file string the file name you want to load
     */
    function Template($file = null) {
        $this->file = $file;
    }

    /**
     * Set a template variable.
     */
    function set($name, $value) {
	   $this->vars[$name] = $value; 
    }

    /**
     * Parse a variable array
	 * Keeps adding to array with every call
     */
    function parse($value) {
		if(is_array($value)){
			$this->vars = array_merge($this->vars, $value);	
		}
    }

    /**
     * Open, parse, and return the template file.
     *
     * @param $file string the template file name
     */
    function fetch($file = null, $vars="") {
		
        if(!$file){
			$file = $this->file;
			if($this->main_content != '' && $this->var_content != ''){
				$this->get_file($this->main_content, $this->var_content);
			}
		}
		if (file_exists($file)) {

			extract($this->vars);
			if($vars == ''){
				extract($this->vars);		// Extract the vars to local namespace
			}else{
				extract($vars);				// Extract the vars to local namespace
			}
			ob_start();						// Start output buffering
			include($file);					// Include the file
			$contents = ob_get_contents();	// Get the contents of the buffer
			ob_end_clean();					// End buffering and discard
			return $contents;				// Return the contents
		}
		return "not found";
    }

    /**
     * Open, parse, and set variable with the file content.
     *
     * @param $filename string the template file name
	 * @param $variable string the string to be set with the file contents
     */
	function get_file($filename, $variable) {
	     $this->set($variable, $this->fetch($filename));
	}
	
	/**
	 * Load html for datatable
	 * 
	 * @param	string		$file			the template file name
	 * @param	array		$class_array
	 * @param	array		$replace_array
	 * @return	string		html for datatable
	 */
	function set_datatable($file, $class_array = array(), $replace_array = array()){
		if (file_exists($file)) {
			$tpl = file_get_contents($file);
			if(strpos($tpl, '<?') > -1){
				$tpl = $this->get_content($file);
			}
			$tpl = preg_replace("/<table[^>]+\>/i", "", $tpl);
			$tpl = str_replace("</table>", "", $tpl);
				
			if(!empty($class_array) && !empty($replace_array)){
				for( $i = 0; $i <= count($class_array); $i++){
					$tpl = str_replace($class_array[$i], $replace_array[$i], $tpl);
				}
			}
				
			return (preg_replace('/\n\r|\r\n|\t/', " ", $tpl));
		}
	}
}

?>