<?
class data {
	
	function date_to_ts($input_date, $input_format="YmdHi"){
		if($input_date != ''){
			$date = $this->str_to_date($input_date, $input_format);
			if(gettype($date) == 'string'){
				return $date;
			}else{
				return $date->format("U");
			}
			
		}else{
			return '';
		}
	}
	
	function today_to_ymd($output_format="Ymd"){
		$dateTime = new DateTime("now");
		return $dateTime->format($output_format);			
	}
	
	function ymd_to_date($input_date="", $output_format="d/m/Y"){
		if($input_date != ''){
			$date = $this->str_to_date($input_date, 'Ymd');
			if(gettype($date) == 'string'){
				return $date;
			}else{
				return $date->format($output_format);
			}
		}else{
			return '';
		}
	}
	
	function date_to_ymd($input_date="", $output_format="Ymd"){
		
		if($input_date != ''){
			$input_format = '';
			
			if( $input_date != "" && preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $input_date) ){
				$input_format = 'd/m/Y';		
			}elseif( $input_date != "" && preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/", $input_date) ){
				$input_format = 'd/m/Y H:i:s';
			}
			
			if($input_format != ''){
				$date = $this->str_to_date($input_date, $input_format);
				if(gettype($date) == 'string'){
					return $date;
				}else{
					return $date->format($output_format);
				}
			}
		}
		
		return "";
	}
	
	function ymdhis_to_date($input_date, $output_format="d/m/Y"){
		if($input_date != ''){
			$date = $this->str_to_date($input_date, 'YmdHis');
			if(gettype($date) == 'string'){
				return $date;
			}else{
				return $date->format($output_format);
			}
		}else{
			return '';	
		}
	}
	
	// return the number of months between two dates (inclusive of the months in the dates provided).
	function get_month_diff($from, $to, $input_format="Ymd"){
		
		if( $from != "" && $to != "" ){
			
			if( $input_format == "Ymd" || $input_format == "Ym" ){
				
				$from_y = substr($from, 0,4);
				$from_m = substr($from, 4,2);
				$from_m_singular = intval(ltrim($from_m, "0"));
				$to_y = substr($to, 0,4);
				$to_m = substr($to, 4,2);
				
				$from_ym = intval($from_y.$from_m);
				$to_ym = intval($to_y.$to_m);
				
				$month_counter = 0;
				while( $from_ym <= $to_ym ){
					
					$month_counter++;
					$from_m_singular++;
					
					if( $from_m_singular > 12 ){
						
						$from_m_singular = 1;
						$from_y++;	
					}
					
					$from_m = str_pad($from_m_singular, 2, "0", STR_PAD_LEFT);
					$from_ym = intval($from_y.$from_m);
				}
				
				return $month_counter;
			}
			
		}
	}
	
	// return an array of month names between two dates (inclusive of the months in the dates provided).
	function get_month_diff_names($from, $to, $input_format="Ymd", $output_format="M"){
		
		if( $from != "" && $to != "" ){
			
			if( $input_format == "Ymd" ){
				
				$from_y = substr($from, 0,4);
				$from_m = substr($from, 4,2);
				$from_m_singular = ltrim($from_m, "0");
				$to_y = substr($to, 0,4);
				$to_m = substr($to, 4,2);
				
				$month_name_array = array();
				while( $from_y.$from_m <= $to_y.$to_m ){
					
					$month_name_array[] = date($output_format, mktime(1,1,1,$from_m_singular,1,$from_y)); 

					$from_m_singular++;
					if( $from_m_singular > 12 ){
						
						$from_m_singular = 1;
						$from_y++;	
					}
					$from_m = str_pad($from_m_singular, 2, "0", STR_PAD_LEFT);
				}
				return $month_name_array;
			}
		}
	}
	
	
	// return an array of month dates between two dates (inclusive of the months in the dates provided).
	function get_month_diff_dates($from, $to, $input_format="Ymd", $output_format="Ym"){
		
		if( $from != "" && $to != "" ){
			
			if( $input_format == "Ymd" ){
				
				$from_y = substr($from, 0,4);
				$from_m = substr($from, 4,2);
				$from_m_singular = ltrim($from_m, "0");
				$to_y = substr($to, 0,4);
				$to_m = substr($to, 4,2);
				
				$month_date_array = array();
				while( $from_y.$from_m <= $to_y.$to_m ){
					
					$month_date_array[] = date($output_format, mktime(1,1,1,$from_m_singular,1,$from_y)); 

					$from_m_singular++;
					if( $from_m_singular > 12 ){
						
						$from_m_singular = 1;
						$from_y++;	
					}
					$from_m = str_pad($from_m_singular, 2, "0", STR_PAD_LEFT);
				}
				return $month_date_array;
			}
		}
	}
	
	function date_minus_days($input_date="", $days, $input_format="d/m/Y", $output_format="Ymd"){
		return $this->date_adjustment($input_date, $days, 'days', '-', $input_format, $output_format);
	}
	
	function date_plus_days($input_date="", $days, $input_format="d/m/Y", $output_format="Ymd"){
		return $this->date_adjustment($input_date, $days, 'days', '+', $input_format, $output_format);
	}
	
	function date_plus_months($input_date="", $months, $input_format="d/m/Y", $output_format="Ymd"){
		return $this->date_adjustment($input_date, $months, 'months', '+', $input_format, $output_format);
	}
	
	/**
	* function date_adjustment () 
	*
	* @param $input_date [str]
	* @param $amount [int]
	* @param $type  = year,month,day,hour,minutes,second [str]
	* @param $dir = + or - [str]
	* @param $input_format [str]
	* @param $output_format [str]
	* @return date in $output_format
	*/
	function date_adjustment($input_date="", $amount, $type, $dir, $input_format="d/m/Y", $output_format="Ymd"){
		
		if($input_date != ""){
			$date = $this->str_to_date($input_date, $input_format);
		} 
		
		if($type != "" && $dir != "" && $amount != ""){
			
			if($amount > 0){
				$date->modify($dir.$amount.' '.$type);
			}
		}

		return $date->format($output_format);
	}
	
	function get_earliest_date($date_array, $return_type="date", $input_date_format = "Ymd", $output_date_format = "d/m/Y"){

		$earliest_date = "";
		$index = "";
		for($d=0;$d<count($date_array);$d++){
			
			if(($date_array[$d] != "" && $date_array[$d] < $earliest_date) || $earliest_date == ""){
				$earliest_date = $date_array[$d];
				$index = $d;
			}
		}
		
		if($return_type == "index"){
			return $index;
		}
		else{
			if($earliest_date != ""){
				if($input_date_format == "Ymd" && $output_date_format == "d/m/Y"){
					return substr($earliest_date,6,2)."/".substr($earliest_date,4,2)."/".substr($earliest_date,0,4);
				}
				elseif($input_date_format == "Ymd" && $output_date_format == "Ymd"){
					return $earliest_date;
				}
			}
		}	
		
		return "";
	}
	
	
	function get_greatest_risk($risk_array, $return_type="risk"){

		$risk = "";
		$index = "";
		for($d=0;$d<count($risk_array);$d++){
			
			if(($risk_array[$d] != "" && $risk_array[$d] > $risk) || $risk == ""){
				$risk = $risk_array[$d];
				$index = $d;
			}
		}
		
		if($return_type == "index"){
			return $index;
		}
		else{
			return $risk;
		}
	}
	
	
	// Moves blank records to end of array (used in ordering search results)...
	function splice_blank_records($input_array, $excl_char=""){
	
		$copy_array = array_values($input_array);
		for($e=0;$e<count($copy_array);$e++){
			if($copy_array[$e] != $excl_char){break;}
		}
		if($e > 0){
			$splice_array = array_splice($input_array, 0, $e);
			$output_array = array_merge($input_array, $splice_array);
		}
		else{
			$output_array = $input_array;
		}
		
		return $output_array;
	}
	
	
	function get_only_one_value($input_array){
		
		$is_first_change = "Y";
		$prev_value = "";
		for($e=0;$e<count($input_array);$e++){
			
			if($input_array[$e] != $prev_value){
				if($is_first_change == "Y"){
					$prev_value = $input_array[$e];
					$is_first_change = "N";
				}
				else{
					return "(Various)";	
				}
			}
		}
		
		return $prev_value;
	}	
	
	/*
	 * Function to get difference two dates
	 * $timestamp - date (unix ts)
	 * $format :
	 * 			1) to return a number, use acombination of the following:
	 * 			%d - days
	 * 			%h - hours
	 * 			%m - minutes
	 * 			%s - seconds
	 * 			%ho - difference returned in hours, e.g. 2 days will be 24 hours
	 * 			%mo - difference returned in minutes, e.g. 2 days will be 7200 minutes
	 * 
	 * 			%d Days - x Days
	 * 			%h Hours - X Hours, etc.
	 * 			
	 * $end - (unix ts. If not present it will use time() )
	 */
	function find_time($timestamp, $format, $end = false) {

		if($end == false){
    		$end = time();
    	}
		
        $difference = $timestamp - $end;

        if($difference < 0){
            $difference = $end - $timestamp;
		}
		
		$min_only = intval(floor($difference / 60));
		$hour_only = intval(floor($difference / 3600));
           
		$days = intval(floor($difference / 86400));
		$difference = $difference % 86400;
		$hours = intval(floor($difference / 3600));
		$difference = $difference % 3600;
		$minutes = intval(floor($difference / 60));
		if($minutes == 60){
			$hours = $hours+1;
 			$minutes = 0;
		}
           
 		if($days == 0){
			$format = str_replace('Days', '?', $format);
			$format = str_replace('Ds', '?', $format);
			$format = str_replace('%d', '', $format);
		}
		if($hours == 0){
			$format = str_replace('Hours', '?', $format);
			$format = str_replace('Hs', '?', $format);
			$format = str_replace('%h', '', $format);
		}
		if($minutes == 0){
 			$format = str_replace('Minutes', '?', $format);
			$format = str_replace('Mins', '?', $format);
			$format = str_replace('Ms', '?', $format);       
			$format = str_replace('%m', '', $format);
		}
           
		$format = str_replace('?,', '', $format);
		$format = str_replace('?:', '', $format);
		$format = str_replace('?', '', $format);
           
		$timeLeft = str_replace('%d', number_format($days), $format);       
		$timeLeft = str_replace('%ho', number_format($hour_only), $timeLeft);
		$timeLeft = str_replace('%mo', number_format($min_only), $timeLeft);
		$timeLeft = str_replace('%h', number_format($hours), $timeLeft);
		$timeLeft = str_replace('%m', number_format($minutes), $timeLeft);
               
		if($days == 1){
			$timeLeft = str_replace('Days', 'Day', $timeLeft);
			$timeLeft = str_replace('Ds', 'D', $timeLeft);
		}
		if($hours == 1 || $hour_only == 1){
			$timeLeft = str_replace('Hours', 'Hour', $timeLeft);
			$timeLeft = str_replace('Hs', 'H', $timeLeft);
		}
		if($minutes == 1 || $min_only == 1){
			$timeLeft = str_replace('Minutes', 'Minute', $timeLeft);
			$timeLeft = str_replace('Mins', 'Min', $timeLeft);
			$timeLeft = str_replace('Ms', 'M', $timeLeft);           
		}
		
		if(trim($timeLeft) == "" && $hours == 1){
			$timeLeft = "< 1 Hour";	
		}
		
		if(trim($timeLeft) == "" && $days < 1){
			$timeLeft = "0";	
		}
		return $timeLeft;
    }
    
	/* Time format is UNIX timestamp or PHP strtotime compatible strings
  	 * $return_interval - 'year','month','day','hour','minute','second'
  	 */
	function dateDiff($time1, $time2, $precision = 6, $return_interval="") {
		// If not numeric then convert texts to unix timestamps
	    if (!is_int($time1)) {
			$time1 = strtotime($time1);
	    }
	    
	    if (!is_int($time2)) {
			$time2 = strtotime($time2);
	    }
 
		// If time1 is bigger than time2
		//Then swap time1 and time2
		if ($time1 > $time2) {
      		$ttime = $time1;
      		$time1 = $time2;
      		$time2 = $ttime;
    	}
 
    	if($return_interval == ""){
    	
	    	// Set up intervals and diffs arrays
	    	$intervals = array('year','month','day','hour','minute','second');
	    	$diffs = array();
	 
	    	
		    // Loop thru all intervals
	    	foreach ($intervals as $interval) {
	    		
				// Set default diff to 0
		      	$diffs[$interval] = 0;
		      	// Create temp time from time1 and interval
		      	$ttime = strtotime("+1 " . $interval, $time1);
		      	// Loop until temp time is smaller than time2
		      	while ($time2 >= $ttime) {
					$time1 = $ttime;
					$diffs[$interval]++;
					// Create new temp time from time1 and interval
					$ttime = strtotime("+1 " . $interval, $time1);
		      	}
		    }
 
			// Loop thru all diffs
			$count = 0;
			$times = array();
			foreach ($diffs as $interval => $value) {
				
				// Break if we have needed precission
	      		if ($count >= $precision) {
					break;
	      		}
				// Add value and interval 
				// if value is bigger than 0
	      		if ($value > 0) {
					// Add s if value is not 1
					if ($value != 1) {
						$interval .= "s";
					}
					// Add value and interval to times array
					$times[] = $value . " " . $interval;
					$count++;
		      	}
			}
		 
		    // Return string with times
			return implode(", ", $times);
    	}
    	else{
    		
    		$diffs = 0;
    		$ttime = strtotime("+1 " . $return_interval, $time1);
    		
	      	// Loop until temp time is smaller than time2
	      	while ($time2 >= $ttime) {
				$time1 = $ttime;
				$diffs++;
				// Create new temp time from time1 and interval
				$ttime = strtotime("+1 " . $return_interval, $time1);
	      	}
	      	
	      	return $diffs;
    	}
	}
	
	
	function clean_filename($filename="", $rep_space="_"){
		
		$bad = array("<", ">", ":", '"', "/", "\\", "|", "?", "*", "[", "]", "+", "=", ";", ",", ".");
		$filename = str_replace($bad, "", $filename);
		
		if($rep_space != ""){
			$filename = str_replace(" ", $rep_space, $filename);
		}
		
		return $filename;
	}
	
	
	// Get difference between two microtimes - used for debugging slow scripts
	function diff_microtime($mt_old,$mt_new){
		
		list($old_usec, $old_sec) = explode(' ',$mt_old);
		list($new_usec, $new_sec) = explode(' ',$mt_new);
		$old_mt = ((float)$old_usec + (float)$old_sec);
		$new_mt = ((float)$new_usec + (float)$new_sec);
		return $new_mt - $old_mt;
	}
	
	function find_working_day($input_date, $input_format, $dir){
		if($dir == '+'){
			return $this->find_next_working_day($input_date, $input_format);
		}elseif($dir == '-'){
			return $this->find_previous_working_day($input_date, $input_format);
		}
	}
	
	function find_previous_working_day($input_date, $input_format){
		
		$date = $this->str_to_date($input_date, $input_format);
		
		if($this->is_working_day($input_date, $input_format) === false){
			$output = $this->date_adjustment($date->format('Ymd'), 1, 'day', '-', 'Ymd', 'Ymd');
			return $this->find_previous_working_day($output, 'Ymd');
		}else{
			return $input_date;
		}
	}
	
	function find_next_working_day($input_date, $input_format){
		
		$date = $this->str_to_date($input_date, $input_format);
		
		if($this->is_working_day($input_date, $input_format) === false){
			$output = $this->date_adjustment($date->format('Ymd'), 1, 'day', '+', 'Ymd', 'Ymd');
			return $this->find_next_working_day($output, 'Ymd');
		}else{
			return $input_date;
		}
	}
	
	function is_working_day($input_date, $input_format){
		$date = $this->str_to_date($input_date, $input_format);
		
		if($this->is_bank_holiday($input_date, $input_format) !== false || $this->ymd_to_date($date->format('Ymd'), 'w') == '6' || $this->ymd_to_date($date->format('Ymd'), 'w') == '0'){
			return false;
		}
		
		return true;
	}
	
	function working_day_adjustment($input_date="", $amount, $dir, $input_format="d/m/Y", $output_format="Ymd"){
		
		$date = $this->str_to_date($input_date, $input_format);
		
		for($d=0; $d < $amount; $d++){
			$date = $this->str_to_date($this->date_adjustment($date->format($output_format), 1, 'day', $dir, $output_format, $output_format), $output_format);
			
			if($this->is_working_day($date->format($output_format), $output_format) !== true){
				$d--;	
			}
		}
		
		return $date->format($output_format);
	}
	
	
	/*
	 * Function to get next day of type, i.e. get the next monday. Includes the date originally provided.
	 * $input_date	- date to start from (inclusive)
	 * $day_type	- 1 (for Monday) through 7 (for Sunday) 
	 * $dir			- direction to look in; backwards(-) or forwards(+) or both(blank) 
	 */
	function get_next_day_of_type($input_date, $day_type, $dir, $input_format="d/m/Y", $output_format="Ymd"){
		
		$date = $this->str_to_date($input_date, $input_format);
		if($dir != ""){
			for($d=0; $d < 7; $d++){
				
				if($date->format("N") == $day_type){
					return $date->format($output_format);	
				}
				$date = $this->str_to_date($this->date_adjustment($date->format($output_format), 1, 'day', $dir, $output_format, $output_format), $output_format);		
			}
		}
		else{
			$f_date = $date;
			$b_date = $date;
			for($f=0; $f < 7; $f++){
				
				if($f_date->format("N") == $day_type){
					$forward_date = $f_date->format($output_format);
					break;	
				}
				$f_date = $this->str_to_date($this->date_adjustment($f_date->format($output_format), 1, 'day', "+", $output_format, $output_format), $output_format);		
			}
			for($b=0; $b < 7; $b++){
				
				if($b_date->format("N") == $day_type){
					$back_date = $b_date->format($output_format);
					break;	
				}
				$b_date = $this->str_to_date($this->date_adjustment($b_date->format($output_format), 1, 'day', "-", $output_format, $output_format), $output_format);		
			}
			if($b < $f){
				return $b_date->format($output_format);
			}
			else{
				return $f_date->format($output_format);
			}
		}
	}
	
	
	function is_bank_holiday($date_str, $input_format){
		
		$date = $this->str_to_date($date_str, $input_format);
		
		$result_array = $this->bank_holidays($date->format('Y'));
		
		foreach($result_array as $key=>$value){
			if($value == $date->format('Ymd')){
				return true;	
			}
		}
		
		return false;
	}
	
	function bank_holidays($year){
		
		if($year != ''){
		
			$newyears = $year . "0101";
			$newyears = $this->date_adjustment($newyears, 1, 'day', '-', 'Ymd', 'Ymd');	
			
			for($d=0; $d<=31; $d++){
				$newyears = $this->date_adjustment($newyears, 1, 'day', '+', 'Ymd', 'Ymd');
				
				if($this->ymd_to_date($newyears, 'w') != 0 && $this->ymd_to_date($newyears, 'w') != 6){
					break;
				}
			}
			
			$result_array['New Years Day'] = $newyears;
			
			$easter_sunday = $this->get_easter_days($year);
			$result_array['Good Friday'] = $this->date_adjustment($easter_sunday, 2, 'day', '-', 'Ymd', 'Ymd');
			$result_array['Easter Monday'] = $this->date_adjustment($easter_sunday, 1, 'day', '+', 'Ymd', 'Ymd');	
			
			$mayday = $year . "0501";
			$mayday = $this->date_adjustment($mayday, 1, 'day', '-', 'Ymd', 'Ymd');	
			
			for($d=0; $d<=31; $d++){
				$mayday = $this->date_adjustment($mayday, 1, 'day', '+', 'Ymd', 'Ymd');
				
				if($this->ymd_to_date($mayday, 'w') == 1){
					break;
				}
			}
			
			$result_array['May Day'] = $mayday;
			
			$spring_holiday = $year . "0531";
			$spring_holiday = $this->date_adjustment($spring_holiday, 1, 'day', '+', 'Ymd', 'Ymd');
			
			for($d=31; $d>0; $d--){
				$spring_holiday = $this->date_adjustment($spring_holiday, 1, 'day', '-', 'Ymd', 'Ymd');
				
				if($this->ymd_to_date($spring_holiday, 'w') == 1){
					break;
				}
			}
			
			$result_array['Spring Bank Holiday'] = $spring_holiday;
			
			$august_holiday = $year . "0831";
			$august_holiday = $this->date_adjustment($august_holiday, 1, 'day', '+', 'Ymd', 'Ymd');
			
			for($d=31; $d>0; $d--){
				$august_holiday = $this->date_adjustment($august_holiday, 1, 'day', '-', 'Ymd', 'Ymd');
				
				if($this->ymd_to_date($august_holiday, 'w') == 1){
					break;
				}
			}
	
			$result_array['Summer Bank Holiday'] = $august_holiday;
			
			$xmas_day = $year . "1225";
			$xmas_day = $this->date_adjustment($xmas_day, 1, 'day', '-', 'Ymd', 'Ymd');
			
			for($d=0; $d<=31; $d++){
				$xmas_day = $this->date_adjustment($xmas_day, 1, 'day', '+', 'Ymd', 'Ymd');
				
				if($this->ymd_to_date($xmas_day, 'w') != 0 && $this->ymd_to_date($xmas_day, 'w') != 6){
					break;
				}
			}
			
			$result_array['Christmas Day'] = $xmas_day;
			
			$boxing_day = $year . "1226";
			$boxing_day = $this->date_adjustment($boxing_day, 1, 'day', '-', 'Ymd', 'Ymd');
			
			for($d=0; $d<=31; $d++){
				$boxing_day = $this->date_adjustment($boxing_day, 1, 'day', '+', 'Ymd', 'Ymd');
				
				if($this->ymd_to_date($boxing_day, 'w') != 0 && $this->ymd_to_date($boxing_day, 'w') != 6){
					break;
				}
			}
			
			if($xmas_day == $boxing_day){
				$boxing_day = $this->date_adjustment($boxing_day, 1, 'day', '+', 'Ymd', 'Ymd');
			}
			
			$result_array['Boxing Day'] = $boxing_day;
			
			foreach($result_array as $key=>$value){
				$sql = "SELECT *
				FROM bank_holiday
				WHERE bank_hol_replace_ymd = '".$value."'";
				//print_r($sql);
				$result = mysql_query($sql);
				$num = mysql_num_rows($result);
				if($num > 0){
					while( $row = mysql_fetch_array($result) ){
						unset($result_array[$key]); 
						$result_array[$row['bank_hol_name']] = $row['bank_hol_ymd'];
					}
				}	
			}
			
			$i = 0;
			
			$sql = "SELECT *
			FROM bank_holiday
			WHERE bank_hol_replace_ymd = ''
			AND bank_hol_ymd LIKE '".$year."%'";
			$result = mysql_query($sql);
			$num = mysql_num_rows($result);
			if($num > 0){
				while( $row = mysql_fetch_array($result) ){
					$i++;
					$result_array[$row['bank_hol_name']] = $row['bank_hol_ymd'];
				}
			}	
			
			return $result_array;
		}
	}
	
	function get_easter_days($year) {
        #First calculate the date of easter using Delambre's algorithm.
        $a = $year % 19;
        $b = floor($year / 100);
        $c = $year % 100;
        $d = floor($b / 4);
        $e = $b % 4;
        $f = floor(($b + 8) / 25);
        $g = floor(($b - $f + 1) / 3);
        $h = (19 * $a + $b - $d - $g + 15) % 30;
        $i = floor($c / 4);
        $k = $c % 4;
        $l = (32 + 2 * $e + 2 * $i - $h - $k) % 7;
        $m = floor(($a + 11 * $h + 22 * $l) / 451);
        $n = ($h + $l - 7 * $m + 114);
        $month = floor($n / 31);
        $day = $n % 31 + 1;
        
        $date = new DateTime();
        $date->setDate($year, $month, $day);
        
        return $date->format("Ymd");
    } 
    
    function str_to_date($date_str, $input_format){
    	try {
	   		if($input_format == "YmdHi"){
	
				$y = substr($date_str,0,4);
				$m = substr($date_str,4,2);
				$d = substr($date_str,6,2);
				$hour = substr($date_str,8,2);
				$min = substr($date_str,10,2);
				$sec = 0;
	
				$date = new DateTime($y."-".$m."-".$d." ".$hour.":".$min);
	   		}elseif($input_format == 'YmdHis'){
	   			$s = substr($date_str, 12, 2);
				$i = substr($date_str, 10, 2);
				$h = substr($date_str, 8, 2);
				$d = substr($date_str, 6, 2);
				$m = substr($date_str, 4, 2);
				$y = substr($date_str, 0, 4);		
				
				$date = new DateTime($y."-".$m."-".$d." ".$h.":".$i.":".$s);
			}elseif($input_format == "Ymd"){
	
				$y = substr($date_str,0,4);
				$m = substr($date_str,4,2);
				$d = substr($date_str,6,2);
	
				$date = new DateTime($y."-".$m."-".$d);
			}elseif( $input_format == 'd/m/Y' || preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $date_str) ){
				
				$date = new DateTime();
				
				$date_parts = explode("/", $date_str);
				$y = $date_parts[2];
				$m = $date_parts[1];
				$d = $date_parts[0];
				
				$date->setDate($y, $m, $d);
				
			}elseif( $input_format == 'd/m/Y H:i' || preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/", $date_str) ){
				
				$date = new DateTime();
				
				$date_parts = explode("/", $date_str);
				$y = $date_parts[2];
				$m = $date_parts[1];
				
				$o_parts = explode(" ", $date_parts[0]);
				$d = $o_parts[0];
				
				$t_parts = explode(" ", $o_parts[1]);
				
				$s = $t_parts[2];
				$i = $t_parts[1];
				$h = $t_parts[0];
				
				$date->setDate($y, $m, $d);
				$date->setTime($h, $i, $s);
			}
    	
			return $date;
    	}catch(Exception $e){
			return $date_str;
		}
    }
    
	function numberToRoman($num){
		// Make sure that we only use the integer portion of the value
		$n = intval($num);
		$result = '';
	 
		// Declare a lookup array that we will use to traverse the number:
		$lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
		'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
		'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
	 
		foreach ($lookup as $roman => $value){
			// Determine the number of matches
			$matches = intval($n / $value);
			
			// Store that many characters
			$result .= str_repeat($roman, $matches);
			
			// Substract that from the number
			$n = $n % $value;
		}
		
		// The Roman numeral should be built, return it
		return $result;
	 }
	 
	function convert_number_to_words($number) {
	   
	    $hyphen      = '-';
	    $conjunction = ' and ';
	    $separator   = ', ';
	    $negative    = 'negative ';
	    $decimal     = ' point ';
	    $dictionary  = array(
	        0                   => 'zero',
	        1                   => 'one',
	        2                   => 'two',
	        3                   => 'three',
	        4                   => 'four',
	        5                   => 'five',
	        6                   => 'six',
	        7                   => 'seven',
	        8                   => 'eight',
	        9                   => 'nine',
	        10                  => 'ten',
	        11                  => 'eleven',
	        12                  => 'twelve',
	        13                  => 'thirteen',
	        14                  => 'fourteen',
	        15                  => 'fifteen',
	        16                  => 'sixteen',
	        17                  => 'seventeen',
	        18                  => 'eighteen',
	        19                  => 'nineteen',
	        20                  => 'twenty',
	        30                  => 'thirty',
	        40                  => 'fourty',
	        50                  => 'fifty',
	        60                  => 'sixty',
	        70                  => 'seventy',
	        80                  => 'eighty',
	        90                  => 'ninety',
	        100                 => 'hundred',
	        1000                => 'thousand',
	        1000000             => 'million',
	        1000000000          => 'billion',
	        1000000000000       => 'trillion',
	        1000000000000000    => 'quadrillion',
	        1000000000000000000 => 'quintillion'
	    );
	   
	    if (!is_numeric($number)) {
	        return false;
	    }
	   
	    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
	        // overflow
	        trigger_error(
	            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
	            E_USER_WARNING
	        );
	        return false;
	    }
	
	    if ($number < 0) {
	        return $negative . $this->convert_number_to_words(abs($number));
	    }
	   
	    $string = $fraction = null;
	   
	    if (strpos($number, '.') !== false) {
	        list($number, $fraction) = explode('.', $number);
	    }
	   
	    switch (true) {
	        case $number < 21:
	            $string = $dictionary[$number];
	            break;
	        case $number < 100:
	            $tens   = ((int) ($number / 10)) * 10;
	            $units  = $number % 10;
	            $string = $dictionary[$tens];
	            if ($units) {
	                $string .= $hyphen . $dictionary[$units];
	            }
	            break;
	        case $number < 1000:
	            $hundreds  = $number / 100;
	            $remainder = $number % 100;
	            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
	            if ($remainder) {
	                $string .= $conjunction . $this->convert_number_to_words($remainder);
	            }
	            break;
	        default:
	            $baseUnit = pow(1000, floor(log($number, 1000)));
	            $numBaseUnits = (int) ($number / $baseUnit);
	            $remainder = $number % $baseUnit;
	            $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
	            if ($remainder) {
	                $string .= $remainder < 100 ? $conjunction : $separator;
	                $string .= $this->convert_number_to_words($remainder);
	            }
	            break;
	    }
	   
	    if (null !== $fraction && is_numeric($fraction)) {
	        $string .= $decimal;
	        $words = array();
	        foreach (str_split((string) $fraction) as $number) {
	            $words[] = $dictionary[$number];
	        }
	        $string .= implode(' ', $words);
	    }
	   
	    return $string;
	}
	
	
	function clean_xml_data($str){
	
		$str = str_replace("&", "&amp;", $str);
		$str = str_replace(">", "&gt;", $str);
		$str = str_replace("<", "&lt;", $str);
		$str = str_replace("\"", "&quot;", $str);
		$str = str_replace("'", "&#39;", $str);
		$str = str_replace(chr(150), "&#150;", $str);
		$str = str_replace(chr(151), "&#151;", $str);
		return $str;
	}
	
	function charset_conv($str, $charset_out="UTF-8"){
		// UTF-8 character fix
		$codepage = 'Windows-' . trim( strstr( setlocale( LC_CTYPE, 0 ), '.' ), '.' );
		$str = @mb_convert_encoding($str, $codepage, $charset_out );
		
		return $str;
	}
	
	
	function time_24h_to_12h($h, $i){
		
		return date("g:ia", strtotime($h.":".$i));
	}	
	
	
	// expects num to start from zero, i.e. 0 == A, 1 == B, etc.
	function excel_index_to_col($num,$start=65,$end=90, $zero_indexed=true){
		
		if($zero_indexed === true){
			$num++;
		}
		
		$sig = ($num < 0);
		$num = abs($num);
		$str = "";
		$cache = ($end-$start);
		while($num != 0){
			$str = chr(($num%$cache)+$start-1).$str;
			$num = ($num-($num%$cache))/$cache;
		}
		if($sig){
			$str = "-".$str;
		}
		return $str;
	}
	
	function multi_array_sort($a,$subkey) {
		foreach($a as $k=>$v) {
			$b[$k] = strtolower($v[$subkey]);
		}
		asort($b);
		foreach($b as $key=>$val) {
			$c[] = $a[$key];
		}
		return $c;
	}
}

?>