<?php
require_once($UTILS_SERVER_PATH."library/classes/data/data.class.php");
require_once($UTILS_SERVER_PATH."library/classes/file/fso.class.php");
require_once($UTILS_SERVER_PATH."library/classes/security/security.class.php");
require_once($UTILS_SERVER_PATH."library/classes/dom/dom_element_extend.class.php");

class log {
	
	var $ws_fs;			//Variable to hold FSO object
	var $file_name;		//Variable to hold filename
	var $guid;			//Variable to hold GUID
	
	function log(){
		$security = new security();
		$this->guid = $security->gen_serial(16);
	}

	function string_log($input, $type_of="Generic String", $output=""){
		
		Global $UTILS_SERVER_PATH;
		Global $UTILS_LOG_PATH;
		
		$data = new data();
		$security = new security();
		
		$this->file_name = $data->today_to_ymd('Ymd').".xml";
		
		$this->initiate_folder();
		
		$dom = new DOMDocument();
		$dom->registerNodeClass('DOMElement', 'extraElement');
		$dom->load($UTILS_LOG_PATH.$this->file_name);
		
		$nodes = $dom->getElementsByTagName("*");
		$log = $nodes->item(0);
		$type = $log->tagName;

		if($type == 'log'){
			$node = $dom->createElement("item");
			$item = $log->appendChild($node);
			
			$node = $dom->createElement("guid");
			$node->innerHTML = $this->guid;
			$item->appendChild($node);
			
			$node = $dom->createElement("time");
			$node->innerHTML = $data->today_to_ymd('H:i:s');
			$item->appendChild($node);
			
			$node = $dom->createElement("script");
			$node->innerHTML = $_SERVER['PHP_SELF'];
			$item->appendChild($node);
			
			$node = $dom->createElement("user");
			$node->innerHTML = $_SESSION['user_id'];
			$item->appendChild($node);
			
			$node = $dom->createElement("function");
			$node->innerHTML = $this->GetCallingMethodName();
			$item->appendChild($node);
			
			$node = $dom->createElement("type");
			$node->innerHTML = $type_of;
			$item->appendChild($node);
			
			$node = $dom->createElement("input");
			$node->innerHTML = $security->remove_chars($input);
			$item->appendChild($node);
			
			$node = $dom->createElement("output");
			$node->innerHTML = $security->remove_chars($output);
			$item->appendChild($node);
			
			$node = $dom->createElement("request");
			$node->innerHTML = file_get_contents("php://input");
			$item->appendChild($node);
		}
		
		$xml = $dom->saveXML();
		
		$fp = fopen($UTILS_LOG_PATH.$this->file_name, "w");
		fwrite($fp, $xml);
		fclose($fp);
		
	}
	
	function initiate_folder(){
		
		Global $UTILS_SERVER_PATH;
		Global $UTILS_LOG_PATH;
		
		$data = new data();
		
		$this->ws_fs = new fso();
		
		$this->ws_fs->create_folder($UTILS_LOG_PATH);
		
		$this->initiate_file();
	}
	
	function initiate_file(){
		
		Global $UTILS_LOG_PATH;
		
		$this->ws_fs = new fso();
		
		if(!$this->ws_fs->FileExists($UTILS_LOG_PATH.$this->file_name)){
			$fh = fopen($UTILS_LOG_PATH.$this->file_name, 'w') or die("Can't open file");
			fwrite($fh, "<log></log>");
			fclose($fh);
		}
	}
	
	function get_logs($from='', $to=''){
		
		Global $UTILS_SERVER_PATH;
		Global $UTILS_LOG_PATH;
		
		$data = new data();
		
		$this->ws_fs = new fso();
		$files = $this->ws_fs->get_files($UTILS_LOG_PATH);
		
		$return_array = array();
		
		if($from == '' && $to==''){
			$return_array = $files;
		}else{
			for($i=0;$i<count($files);$i++){
				
				$add_file = true;
				
				$filedate = str_replace(".xml", "", $files[$i]);
				
				if($filedate < $data->date_to_ymd($from) && $from != ''){
					$add_file = false;
				}
				
				if($filedate > $data->date_to_ymd($to) && $to != ''){
					$add_file = false;
				}
				
				if($add_file == true){
					array_push($return_array, $files[$i]);
				}
			}
		}
		
		return $return_array;
		
	}
	
	function read_log($filename){
		
		Global $UTILS_SERVER_PATH;
		Global $UTILS_LOG_PATH;
		
		$result_array = array();
		$temp_array = array();
		
		$counter = 0;
		
		$data = new data();
		
		$dom = new DOMDocument();
		$dom->registerNodeClass('DOMElement', 'extraElement');
		$dom->load($UTILS_LOG_PATH.$filename);
		
		$nodes = $dom->getElementsByTagName("*");
		$log = $nodes->item(0);
		$type = $log->tagName;

		if($type == 'log'){
			$items = $log->childNodes;
			for($j=0;$j<$items->length;$j++) {
				$item = $items->item($j);
				$type_item = $item->tagName;
				if($type_item == 'item'){
					$values = $item->childNodes;
					
					for($k=0;$k<$values->length;$k++) {
						$value = $values->item($k);
						$type_value = $value->tagName;
						$node_value = $value->innerHTML;
						
						$temp_array['LOG_ID'][$counter] = $j;
						
						if($type_value == 'user'){
							$tenant = new tenant($node_value);
							$temp_array['LOG_' . strtoupper($type_value)][$counter] = $tenant->get_fullname();
						}else{
							$temp_array['LOG_' . strtoupper($type_value)][$counter] = $node_value;
						}
						
						if($type_value == 'guid'){
							$temp_array['LOG_ONCLICK'][$counter] = "highlightRow('".$node_value."', 'guid');";
						}
					}
					$counter++;
				}
			}
		}
		
		$dom->saveXML();
		
		$guid = '';
		$j = 0;
		
		for($i=0;$i<count($temp_array['LOG_GUID']);$i++){
			
			if($temp_array['LOG_GUID'][$i] != $guid){
				$result_array['LOG_GUID'][$j] = $temp_array['LOG_GUID'][$i];
				$result_array['LOG_ONCLICK'][$j] = $temp_array['LOG_ONCLICK'][$i];
				$result_array['LOG_TIME'][$j] = $temp_array['LOG_TIME'][$i];
				$result_array['LOG_SCRIPT'][$j] = $temp_array['LOG_SCRIPT'][$i];
				$result_array['LOG_FUNCTION'][$j] = $temp_array['LOG_FUNCTION'][$i];
				$result_array['LOG_USER'][$j] = $temp_array['LOG_USER'][$i];
				$result_array['LOG_TYPE'][$j] = $temp_array['LOG_TYPE'][$i];
				$result_array['LOG_INPUT'][$j] = $temp_array['LOG_INPUT'][$i];
				$result_array['LOG_OUTPUT'][$j] = $temp_array['LOG_OUTPUT'][$i];
				
				$j++;
			}
			
			$guid = $temp_array['LOG_GUID'][$i];
		}
	
		$result_array['NUM_RESULTS'] = $j;
		
		return $result_array;
	}
	
	function guid_log($filename, $guid){
		
		Global $UTILS_SERVER_PATH;
		Global $UTILS_LOG_PATH;
		
		$temp_array = array();
		$result_array = array();
		
		$counter = 0;
		
		$data = new data();
		
		$dom = new DOMDocument();
		$dom->registerNodeClass('DOMElement', 'extraElement');
		$dom->load($UTILS_LOG_PATH.$filename);
		
		$nodes = $dom->getElementsByTagName("*");
		$log = $nodes->item(0);
		$type = $log->tagName;

		if($type == 'log'){
			$items = $log->childNodes;
			for($j=0;$j<$items->length;$j++) {
				$item = $items->item($j);
				$type_item = $item->tagName;
				if($type_item == 'item'){
					$values = $item->childNodes;
					for($k=0;$k<$values->length;$k++) {
						$value = $values->item($k);
						$type_value = $value->tagName;
						$node_value = $value->innerHTML;
						if($type_value == 'user'){
							$tenant = new tenant($node_value);
							$temp_array['LOG_' . strtoupper($type_value)][$counter] = $tenant->get_fullname();
						}else{
							$temp_array['LOG_' . strtoupper($type_value)][$counter] = $node_value;
						}
						$temp_array['LOG_ID'][$counter] = $j;
						$temp_array['LOG_ONCLICK'][$counter] = "highlightRow('".$j."', 'log');";
					}
					$counter++;
				}
			}
		}
		
		$dom->saveXML();
		$j = 0;
		
		for($i=0;$i<count($temp_array['LOG_GUID']);$i++){
			
			if($temp_array['LOG_GUID'][$i] == $guid){
				$result_array['LOG_ID'][$j] = $j;
				$result_array['LOG_ONCLICK'][$j] = $temp_array['LOG_ONCLICK'][$i];
				$result_array['LOG_GUID'][$j] = $temp_array['LOG_GUID'][$i];
				$result_array['LOG_TIME'][$j] = $temp_array['LOG_TIME'][$i];
				$result_array['LOG_SCRIPT'][$j] = $temp_array['LOG_SCRIPT'][$i];
				$result_array['LOG_FUNCTION'][$j] = $temp_array['LOG_FUNCTION'][$i];
				$result_array['LOG_USER'][$j] = $temp_array['LOG_USER'][$i];
				$result_array['LOG_TYPE'][$j] = $temp_array['LOG_TYPE'][$i];
				$result_array['LOG_INPUT'][$j] = $temp_array['LOG_INPUT'][$i];
				$result_array['LOG_OUTPUT'][$j] = $temp_array['LOG_OUTPUT'][$i];
				
				$j++;
			}
		}
	
		$result_array['NUM_RESULTS'] = $j;
		
		return $result_array;
	}
	
	function GetCallingMethodName(){
		
		$function_name = '';
		
	    $array = debug_backtrace();
	    
	    for($i=0;$i<count($array);$i++){
	    	if(!is_null($array[$i]['function']) && $array[$i]['function'] != 'require_once'){
	    		$function_name = $array[$i]['function'];
	    	}
	    }
	    
	    return $function_name;
	}
	
}