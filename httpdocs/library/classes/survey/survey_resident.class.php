<?php
require_once($UTILS_SERVER_PATH."library/classes/core/core.class.php");
require_once($UTILS_SERVER_PATH."library/classes/tenant/tenant.class.php");
require_once($UTILS_SERVER_PATH."library/classes/question/question.class.php");
require_once($UTILS_SERVER_PATH."library/classes/answer/answer.class.php");
require_once($UTILS_SERVER_PATH."library/classes/security/security.class.php");
require_once($UTILS_SERVER_PATH."library/classes/search_builder/search_builder.class.php");

class survey_resident extends core {
	
	var $survey_resident_id;
	var $resident_num;
	var $survey_id;
	var $survey_resident_serial;
	var $survey_resident_complete_ymdhis;
	var $survey_resident_sent_date;	
	
	function __construct($ref=""){	
		$this->survey_resident($ref);
	}
	

	function survey_resident($num=''){
		
		if($num != ''){
			
			$mysql = new mysql();
			
			$sql = "
			SELECT *
			FROM survey_resident
			WHERE 
			survey_resident_id = '".$num."'";
			$result = $mysql->query($sql, 'Get Survey Resident');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				
				$row = $mysql->fetch_array($result);
				$this->survey_resident_id = $row['survey_resident_id'];
				$this->resident_num = $row['resident_num'];
				$this->survey_id = $row['survey_resident_id'];
				$this->survey_resident_serial = $row['survey_resident_serial'];
				$this->survey_resident_complete_ymdhis = $row['survey_resident_complete_ymdhis'];
				$this->survey_resident_sent_date = $row['survey_resident_sent_date'];
			}
		}
	}
	
	function serial_to_survey_resident($ref='', $survey_id=''){
		if($ref != ''){
			$mysql = new mysql();
			$sql = "SELECT survey_resident_id
			FROM survey_resident
			WHERE survey_resident_serial = '".$ref."'
			AND survey_id = '".$survey_id."'";
			$result = $mysql->query($sql, 'Get ID');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				$row = $mysql->fetch_array($result);
				$this->survey_resident($row['survey_resident_id']);
			}
		}
	}

	function get_list ($request, $survey_id){
	
		$mysql = new mysql();
		$data = new data();
		$search_builder = new search_builder;
		$security = new security();
	
		$input = $request;
		
		$search_builder->add_index_column('survey_resident_id');
		$search_builder->add_column('survey_resident_serial');
		$search_builder->add_function_column('resident_num', 'tenant', 'tenant');
		$search_builder->add_button_column('survey_resident_id', 'Delete', "view_results", array(), 'button ui-button-info');
		$iColumnCount = count($search_builder->columns);
	
		$search_builder->add_filter('survey_id', "=", $survey_id, 'AND');
	
		//Paging
		if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' ) {
			$search_builder->paging($request['iDisplayStart'], $request['iDisplayLength']);
		}
	
		//Ordering
		$iSortingCols = intval( $request['iSortingCols'] );
		for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
			if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
				$search_builder->add_order($search_builder->columns[intval($request['iSortCol_'.$i])], ($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
	
		//Filtering
		if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
			$search_builder->add_filter('', "(", "", 'AND');
			$search_builder->all_column_filter( $security->clean_query( $request['sSearch'] ));
			$search_builder->add_filter('', ")", "", 'OR');
		}
	
		// Individual column filtering
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == 'true' && $request['sSearch_'.$i] != '' ) {
				$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $request['sSearch_'.$i] ).'%', 'AND');
			}
		}
	
		$sql = "
		FROM survey_resident";
	
		$search_builder->add_sql($sql);
	
		$output = $search_builder->output($request);
	
		return $output;
	
	}
	
	function check_details($survey_id='', $serial_ref= '', $resident_num=''){
		
		if($resident_num != '' && $survey_id != '' && $serial_ref != ''){
			
			$mysql = new mysql();
			$sql = "SELECT *
			FROM survey_resident
			WHERE survey_id = '".$survey_id."'
			AND resident_num = '".$resident_num."'
			AND survey_resident_serial = '".$serial_ref."'";
			
			$result = $mysql->query($sql, 'Get resident-survey');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				return true;
			}
		}

		return false;
	}
	
	function click_through(){
			
		$data = new data;
		$mysql = new mysql();
		
		$sql = "UPDATE survey_resident SET
		survey_resident_click_ymdhis = '". $data->today_to_ymd('Ymdhis') . "'
		WHERE survey_resident_id = '". $this->survey_resident_id . "'";
		$mysql->insert($sql, 'Set click through');
		
	}
	
	function load_last_sent($survey_id='', $resident_num=''){
		
		if($resident_num != '' && $survey_id != ''){
			
			$mysql = new mysql();
			$sql = "SELECT *
			FROM survey_resident
			WHERE survey_id = '".$survey_id."'
			AND resident_num = '".$resident_num."'
			ORDER BY survey_resident_id DESC
			LIMIT 1";
			
			$result = $mysql->query($sql, 'Get resident-survey');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				$row = $mysql->fetch_array($result);
				$this->survey_resident($row['survey_resident_id']);
			}
		}
	}
	
	function survey_to_do($survey_id='', $serial_ref= '', $resident_num=''){
		
		if($resident_num != '' && $survey_id != '' && $serial_ref != ''){
			
			$mysql = new mysql();
			$sql = "SELECT *
			FROM survey_resident
			WHERE survey_id = '".$this->survey_id."'
			AND resident_num = '".$tenant->tenant_num."'
			AND survey_resident_serial = '".$serial_ref."'
			AND survey_resident_complete_ymdhis = ''";
			
			$result = $mysql->query($sql, 'Get resident-survey');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
				return true;
			}
		}

		return false;
	}
	
	function can_send($survey_id='', $resident_num=''){
		
		$data = new data;
		
		if(($resident_num != '' && $survey_id != '') || is_numeric($this->survey_resident_id)){
			
			$survey = new survey($survey_id, 'id');
			$last_sent = '';
			$completed = '';
			
			$mysql = new mysql();
			$sql = "SELECT *
			FROM survey_resident";
			
			if($resident_num != '' && $survey_id != ''){
				$sql .= "
				WHERE survey_id = '".$survey_id."'
				AND resident_num = '".$resident_num."'";
			}elseif(is_numeric($this->survey_resident_id)){
				$sql .= "
				WHERE survey_resident_id = '".$this->survey_resident_id."'";
			}
			
			$sql .= "
			ORDER BY survey_resident_id DESC
			LIMIT 1";
			
			//print($sql);
			
			$tenant = new tenant($resident_num);
			if($tenant->survey_optout == 'Y'){
				return false;
			}else{
			
				$result = $mysql->query($sql, 'Get resident-survey');
				$num_rows = $mysql->num_rows($result);
				if( $num_rows > 0 ){
					
					$row = $mysql->fetch_array($result);
					
					$last_sent = $row['survey_resident_sent_date'];
					$completed = $row['survey_resident_complete_ymdhis'];
					
					if($survey->survey_once == 'N'){
						if($survey->survey_frequency != 0){
							$calc_send = $data->date_adjustment($data->today_to_ymd(), $survey->survey_amount, $survey->survey_frequency_string, '-', 'Ymd', 'Ymd');
						}else{
							$calc_send = $data->today_to_ymd();
						}
						
						if($last_sent < $calc_send){
							return true;
						}else{
							return false;
						}
					}else{
						return false;
					}
				}
			}
			return true;
		}
		
		return false;
	}
	
	function answered($survey_id='', $resident_num=''){
	
		$data = new data;
	
		if(($resident_num != '' && $survey_id != '') || is_numeric($this->survey_resident_id)){
				
			$survey = new survey($survey_id, 'id');
			$last_sent = '';
			$completed = '';
				
			$mysql = new mysql();
			$sql = "SELECT *
			FROM survey_resident";
				
			if($resident_num != '' && $survey_id != ''){
				$sql .= "
				WHERE survey_id = '".$survey_id."'
				AND resident_num = '".$resident_num."'";
			}elseif(is_numeric($this->survey_resident_id)){
				$sql .= "
				WHERE survey_resident_id = '".$this->survey_resident_id."'";
			}
				
			$sql .= "
			ORDER BY survey_resident_id DESC
			LIMIT 1";
				
			//print($sql);
				
			$result = $mysql->query($sql, 'Get resident-survey');
			$num_rows = $mysql->num_rows($result);
			if( $num_rows > 0 ){
	
				$row = $mysql->fetch_array($result);
	
				$last_sent = $row['survey_resident_sent_date'];
				$completed = $row['survey_resident_complete_ymdhis'];
	
				if($survey->survey_once == 'N'){
	
					$calc_send = $data->date_adjustment($data->today_to_ymd(), $survey->survey_amount, $survey->survey_frequency_string, '-', 'Ymd', 'Ymd');
						
					if($last_sent < $calc_send){
						return true;
					}else{
						return false;
					}
				}else{
					if($completed == ''){
						return true;
					}else{
						return false;
					}
				}
			}
	
			return true;
		}
	
		return false;
	}
	
	function can_send_info($resident_num='', $survey_id=''){
		
		$data = new data;
		
		if($resident_num != '' && $survey_id != ''){
			
			$survey = new survey($survey_id, 'id');
			$tenant = new tenant($resident_num);
			
			$type = "letter";
			
			if($tenant->tenant_email != '' && $tenant->tenant_email != NULL){
				$type = "email";
			}
			
			$this->load_last_sent($survey_id, $resident_num);
			
			$can_send = $this->can_send($survey_id, $resident_num);
			if($can_send){
				if($type == "email"){
					return "Send ".$type.".";
				}else{
					return "Send ".$type.'.';
				}
			}else{
				if($survey->survey_once == 'N'){
					return "Sent - " . $data->ymd_to_date($this->survey_resident_sent_date) . " - every " . $survey->survey_amount . " " . $survey->survey_frequency_string . "(s).";
				}else{
					return "Sent - " . $data->ymd_to_date($this->survey_resident_sent_date) . " -  Once.";
				}
			}
		}
	}
	
	function create($request){
		try{
		
			$mysql = new mysql();
				
			$security = new security();
			$data = new data;
				
			//if id is filled in then we are editing, if not it is a new record
				
			$sql = "INSERT INTO survey_resident SET
			resident_num = '". $request["tenant_num"] . "',
			survey_id = '". $request["survey_id"] . "',
			survey_resident_serial = '". $security->gen_unique_serial('survey_resident', 'survey_resident_serial') . "',
			survey_resident_sent_date = '". $data->today_to_ymd() . "'";
				
			$has_error=false;
				
			$last_id = "";
		
			// insert record
			$has_error = $mysql->insert($sql, 'Insert New Survey');
			$last_id = $has_error;
				
			if (!is_bool($has_error)){
				$results_array["result"] = "success";
				$results_array["id"] = $last_id;
			}else{
				$results_array["result"] = "error";
			}
		
			return $results_array;
				
		}catch(Exception $e){
			$results_array["result"] = "error";
			$results_array["sql"] = $e->getMessage();
			return $results_array;
		}
	}
	
	function save($request){
		
		try{
	
			$mysql = new mysql();
			
			$security = new security();
			$data = new data;
			$tenant = new tenant();
			$survey = new survey();
			$question = new question();
			
			$survey->serial_to_survey($request["survey_id"]);
			$tenant->ref_to_tenant($request["tenant_ref"]);
			$this->serial_to_survey_resident($request["serial_ref"], $survey->survey_id);
			
			//if id is filled in then we are editing, if not it is a new record
			
			$sql = "UPDATE survey_resident SET
			survey_resident_complete_ymdhis = '". $data->today_to_ymd('Ymdhis') . "'
			WHERE survey_resident_id = '". $this->survey_resident_id . "'";
			
			$has_error=false;
			
			$last_id = "";
				
			// insert record
			$has_error = $mysql->insert($sql, 'Insert New Survey');
			
			//if there was no issues inserting, return the id of the new tracker
			if (!is_bool($has_error)){
				
				$last_id = $has_error;
				
				$result = $question->get_questions($survey->survey_id);
				$num_rows = $mysql->num_rows($result);
				if($num_rows > 0){
					while($row = $mysql->fetch_array($result)){
						$sql = "INSERT INTO survey_response SET
						survey_resident_id = '".$this->survey_resident_id."',
						question_id = '".$row['question_id']."',
						response = '".$security->clean_query($request[$request['prefix'].'_answer_'.$row['question_id']])."'";
						$has_error = $mysql->insert($sql, 'Insert Answer');
					}
				}
			}
			
			if (!is_bool($has_error)){
				$results_array["result"] = "success";
				$results_array["id"] = $last_id;
			}else{
				$results_array["result"] = "error";
			}
				
			return $results_array;	
			
		}catch(Exception $e){
			$results_array["result"] = "error";
			$results_array["sql"] = $e->getMessage();
			return $results_array;
		}
	}
}

?>