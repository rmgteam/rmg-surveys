(function( $, undefined ) {
		
	var emptyFunction = function(){};

	$.widget( "ui.display_toggle", {
	
		options: {
			objID: '',
			value_delimiter: ',',
			elements: {},
			operators: {
			    "==": function(a,b){return a==b;},
			    "=": function(a,b){return a==b;},
			    "<=": function(a,b){return a<=b;},
			    ">=": function(a,b){return a>=b;},
			    "<": function(a,b){return a<b;},
			    ">": function(a,b){return a>b;},
			    "!=": function(a,b){return a!=b;},
			    "::": function(a,b){
			    	a = String(a);
			    	b = String(b);
			    	if(a.indexOf(b) != -1){
			    		return true;
			    	}else{
			    		return false;
			    	}
			    },
			    "!:": function(a,b){
			    	a = String(a);
			    	b = String(b);
			    	if(a.indexOf(b) == -1){
			    		return true;
			    	}else{
			    		return false;
			    	}
			    }
			}
		},
		
		_create: function(){
			var self = this;
			var options = this.options;
			
			options.objID = this.element;
			
			self._initSource(function(){
				self.initialise();
			});
		},
		
		_initSource: function(callback) {
			var self = this,
				array,
				url;
			
			if ( typeof self.options.elements === "string" ) {
				url = this.options.elements;
				
				$.post(url, 
				{},
				function(data){
					var arr = $.makeArray(data);
					self.options.elements = arr;
					
					if($.isFunction(callback) === true){
						callback();
					}
				}, 
				"json");
			} else {
				this.elements = this.options.elements;
				
				if($.isFunction(callback) === true){
					callback();
				}
			}
		},
		
		disable: function(){
			var self = this;
			var options = this.options;
			var linkId = options.objID.attr('id');
			
			$('#' + linkId).attr('disabled', 'disabled');
			
			options.disabled = true;
			
			this._trigger('disable');
			
		},
		
		enable: function(){
			var self = this;
			var options = this.options;
			var linkId = options.objID.attr('id');
				
			$('#' + linkId).removeAttr('disabled');
			
			options.disabled = false;
			
			this._trigger('enable');
			
		},
		
		execute: function(){
			var self = this;
			var options = this.options;
			//try{
			
				var condition_array = new Array();
				var l=0;
				
				for(l=0;l<options.elements.length;l++){
					if(options.elements[l] !== undefined){
						var this_element = options.elements[l];
							
						var tab = this_element['tab'];
						
						var correct_tab = false;
						
						if(typeof tab === 'undefined'){
							correct_tab = true;
						}else{
							if($(tab).is(':visible')){
								correct_tab = true;
							}
						}
						
						if(correct_tab == true && this_element['conditions'] !== undefined){
							var j = 0; 
							for(j=0;j<this_element['conditions'].length;j++){
								var this_condition = this_element['conditions'][j];
							
								var parent = this_condition['field'];
								var value = this_condition['value'];
								var operator = this_condition['operator'];
								var visible_override = this_condition['visible_override'];
	
								var value_array = [];
	
								if(typeof value === 'string'){
									if(value.indexOf(options.value_delimiter) !== false){
										value_array = value.split(options.value_delimiter);
									}else{
										value_array.push(value);
									}
								}else if(typeof value !== 'undefined'){
									value_array.push(value);
								}
								
								if(typeof operator === 'undefined'){
									operator = "==";
								}
								
								if(typeof visible_override === 'undefined'){
									visible_override = false;
								}
								
								if(typeof parent !== 'undefined'){
	
									var type_of_field = $(parent).get(0).tagName;
	
									var temp_value = '';
									
									if(type_of_field == "INPUT" || type_of_field == "TEXTAREA" || type_of_field == "SELECT"){
										if($(parent).attr('type') == 'radio' || $(parent).attr('type') == 'checkbox'){
											temp_value = $(parent + ':checked').val();
										}else{
											temp_value = $(parent).val();
										}
									}else{
										temp_value = $(parent).html();
									}
	
									var found = false;
									var if_what = true;
	
									if($(parent).is(':visible') || visible_override == true){
										
										if(operator == '!='){
											found = true;
											if_what = false;
										}
										
										var b = 0;
										
										for(b=0;b<value_array.length;b++){
											var the_value = value_array[b];
											
											if (!isNaN(the_value)){
												the_value = parseInt(the_value);
											}
											
											if (!isNaN(temp_value)){
												temp_value = parseInt(temp_value);
											}
											
											if(operator in options.operators && options.operators[operator](temp_value, the_value) == if_what) {
												//console.log(parent + ': ' + temp_value + ' ' + operator + ' ' + the_value);
												if(operator == '!='){
													found = false;
												}else{
													found = true;
												}
											}
										}
									}
									condition_array[j] = found;
								}
							}
							
							if(this_element['logic'] !== undefined){
								var logic = this_element['logic'];
							}else{
								var logic = '0';
							}
							
							var j = 0;
							
							for(j=0;j<this_element['conditions'].length;j++){
								logic = logic.replace(j.toString(), condition_array[j].toString() + ' == true');
							}
							
							var logic_out = eval(logic);
							var the_elements = this_element['id'].split(',');
							var length = the_elements.length;
								
							if(logic_out == true){
								var k = 0;
								for (var k = 0; k < length; k++) {
									$(the_elements[k].replace(/\s+/g, '')).show();
								}
							}else{
								var k = 0;
								for (var k = 0; k < length; k++) {
									$(the_elements[k].replace(/\s+/g, '')).hide();
									$(the_elements[k].replace(/\s+/g, '')).find('input,select,textarea').each(function(){
										var type_of_field = $(this).get(0).tagName;
										
										test_element = $(this);
										if(test_element.hasClass('read-only')){
										}else{
											if(test_element.attr('type')){
												if(test_element.attr('type') == 'hidden'){
													var type = '';
													
													var default_val = '';
													
													if(test_element.data('default') !== undefined){
														default_val = test_element.data('default');
													}
			
													var parent_element = test_element.parent().parent();
													
													if($.hasData(parent_element) == false){
														parent_element.children().each(function(){
															var m = 0;
															$.each($(this).data(), function(key, e) {
																m++;
																return false;
															});
														
															if(m > 0){
																parent_element = $(this);
															}													
														});
													}
													
													if(parent_element.attr('id') === undefined){
														parent_element = test_element.parent();
													}
													
													$.each(parent_element.data(), function(key, e) {
														type = key;	
														return false;
													});
													
													if(type != ''){
														if(default_val == ''){
															parent_element[type]('clear');
														}else{
															try{
																parent_element[type]('set', default_val);
															}catch(e){
																
															}
														}
													}
												}else{
													if(type_of_field == "INPUT" || type_of_field == "TEXTAREA" || type_of_field == "SELECT"){
														if(test_element.attr('type') != 'radio' && test_element.attr('type') != 'checkbox'){
															if(type_of_field == "SELECT" && $(parent + " option[value='']").length > 0){
																test_element.val($(parent + " option:first").val());																
															}else{
																var default_val = '';
																
																if(test_element.data('default') !== undefined){
																	default_val = test_element.data('default');
																}
																
																test_element.val(default_val);
															}
														}
													}
												}
											}else{
												if(type_of_field == "INPUT" || type_of_field == "TEXTAREA" || type_of_field == "SELECT"){
													if(test_element.attr('type') != 'radio' && test_element.attr('type') != 'checkbox'){
														if(type_of_field == "SELECT" && $(parent + " option[value='']").length > 0){
															test_element.val($(parent + " option:first").val());																
														}else{
															var default_val = '';
															
															if(test_element.data('default') !== undefined){
																default_val = test_element.data('default');
															}
															
															test_element.val(default_val);
														}
													}
												}
											}
										}
									});
								}
							}
						}
					}
				}
			/*}catch(err){
				alert('Display_Toggle() - You have defined an element that does not exist on the page');
				//alert(err);
			}*/
		},

		initialise: function(){
			var self = this;
			var options = this.options;
			
			var count = 0;
			var l = 0;
			for(l=0;l<options.elements.length;l++){
				if(options.elements[l] !== undefined){
					var this_element = options.elements[l];
					if(this_element['conditions'] !== undefined){						
						for(j=0;j<this_element['conditions'].length;j++){
							var this_condition = this_element['conditions'][j];
							
							if(this_condition['field'] !== undefined){
								var parents = this_condition['field'].split(',');
								
								var length = parents.length;
								
								for (var k = 0; k < length; k++) {
									var parent = parents[k];
									if($(parent).length > 0){
										
										$(parent).bind('change', {'self':self}, function(event){
											var self = event.data['self'];
											self.execute();
										});
									}
								}
							}
						}
					}
				}
			}
		}
	});
}( jQuery ) );