<?php

class doc_type_icon {

	function doc_type_icon(){
		
	}
	
	
	function get_name($file){
		$names = array(
			"323"     => 'File',
			"acx"     => 'File',
			"ai"      => 'File',
			"aif"     => 'Audio',
			"aifc"    => 'Audio',
			"aiff"    => 'Audio',
			"asf"     => 'Video',
			"asr"     => 'Video',
			"asx"     => 'Video',
			"au"      => 'Audio',
			"avi"     => 'Video',
			"axs"     => 'File',
			"bas"     => 'File',
			"bcpio"   => 'File',
			"bin"     => 'File',
			"bmp"     => 'Image',
			"c"       => 'File',
			"cat"     => 'File',
			"cdf"     => 'File',
			"cer"     => 'File',
			"class"   => 'File',
			"clp"     => 'File',
			"cmx"     => 'Image',
			"cod"     => 'Image',
			"cpio"    => 'File',
			"crd"     => 'File',
			"crl"     => 'File',
			"crt"     => 'File',
			"csh"     => 'File',
			"css"     => 'File',
			"dcr"     => 'File',
			"der"     => 'File',
			"dir"     => 'File',
			"dll"     => 'File',
			"dms"     => 'File',
			"doc"     => 'Word',
			"docx"    => 'Word',
			"dot"     => 'Word',
			"dotx"    => 'Word',
			"dvi"     => 'File',
			"dxr"     => 'File',
			"eps"     => 'File',
			"etx"     => 'File',
			"evy"     => 'File',
			"exe"     => 'File',
			"fif"     => 'File',
			"flr"     => 'File',
			"gif"     => 'Image',
			"gtar"    => 'Compressed',
			"gz"      => 'Compressed',
			"h"       => 'File',
			"hdf"     => 'File',
			"hlp"     => 'File',
			"hqx"     => 'File',
			"hta"     => 'File',
			"htc"     => 'File',
			"htm"     => 'Web Page',
			"html"    => 'Web Page',
			"htt"     => 'Web Page',
			"ico"     => 'Image',
			"ief"     => 'Image',
			"iii"     => 'File',
			"ins"     => 'File',
			"isp"     => 'File',
			"jfif"    => 'Image',
			"jpe"     => 'Image',
			"jpeg"    => 'Image',
			"jpg"     => 'Image',
			"js"      => 'File',
			"latex"   => 'File',
			"lha"     => 'Compressed',
			"lsf"     => 'Video',
			"lsx"     => 'Video',
			"lzh"     => 'Video',
			"m13"     => 'Video',
			"m14"     => 'Video',
			"m3u"     => 'Audio',
			"man"     => 'File',
			"mdb"     => 'File',
			"me"      => 'File',
			"mht"     => 'Mail',
			"mhtml"   => 'Mail',
			"mid"     => 'Audio',
			"mny"     => 'File',
			"mov"     => 'Video',
			"movie"   => 'Video',
			"mp2"     => 'Video',
			"mp3"     => 'Audio',
			"mpa"     => 'Video',
			"mpe"     => 'Video',
			"mpeg"    => 'Video',
			"mpg"     => 'Video',
			"mpp"     => 'File',
			"mpv2"    => 'Video',
			"ms"      => 'File',
			"msg"     => 'Mail',
			"mvb"     => 'File',
			"nws"     => 'Mail',
			"oda"     => 'File',
			"p10"     => 'File',
			"p12"     => 'File',
			"p7b"     => 'File',
			"p7c"     => 'File',
			"p7m"     => 'File',
			"p7r"     => 'File',
			"p7s"     => 'File',
			"pbm"     => 'Image',
			"pdf"     => 'PDF',
			"pfx"     => 'File',
			"pgm"     => 'Image',
			"pko"     => 'File',
			"pma"     => 'File',
			"pmc"     => 'File',
			"pml"     => 'File',
			"pmr"     => 'File',
			"pmw"     => 'File',
			"png"     => 'Image',
			"pnm"     => 'Image',
			"pot"     => 'File',
			"potx"    => 'File',
			"ppm"     => 'File',
			"pps"     => 'File',
			"ppsx"    => 'File',
			"ppt"     => 'File',
			"pptx"    => 'File',
			"prf"     => 'File',
			"ps"      => 'File',
			"pub"     => 'File',
			"qt"      => 'Video',
			"ra"      => 'Audio',
			"ram"     => 'Audio',
			"ras"     => 'Image',
			"rgb"     => 'Image',
			"rmi"     => 'Audio',
			"roff"    => 'File',
			"rtf"     => 'File',
			"rtx"     => 'File',
			"scd"     => 'File',
			"sct"     => 'File',
			"setpay"  => 'File',
			"setreg"  => 'File',
			"sh"      => 'File',
			"shar"    => 'File',
			"sit"     => 'File',
			"snd"     => 'Audio',
			"spc"     => 'File',
			"spl"     => 'File',
			"src"     => 'File',
			"sst"     => 'File',
			"stl"     => 'File',
			"stm"     => 'Web Page',
			"svg"     => 'Image',
			"sv4cpio" => 'File',
			"sv4crc"  => 'File',
			"t"       => 'File',
			"tar"     => 'Compressed',
			"tcl"     => 'File',
			"tex"     => 'File',
			"texi"    => 'File',
			"texinfo" => 'File',
			"tgz"     => 'Compressed',
			"tif"     => 'Image',
			"tiff"    => 'Image',
			"tr"      => 'File',
			"trm"     => 'File',
			"tsv"     => 'File',
			"txt"     => 'File',
			"uls"     => 'File',
			"ustar"   => 'File',
			"vcf"     => 'File',
			"vrml"    => 'File',
			"wav"     => 'Audio',
			"wcm"     => 'File',
			"wdb"     => 'File',
			"wks"     => 'File',
			"wmf"     => 'File',
			"wps"     => 'File',
			"wri"     => 'File',
			"wrl"     => 'File',
			"wrz"     => 'File',
			"xaf"     => 'File',
			"xbm"     => 'Image',
			"xla"     => 'Excel',
			"xlc"     => 'Excel',
			"xlm"     => 'Excel',
			"xls"     => 'Excel',
			"xlsx"    => 'Excel',
			"xlt"     => 'Excel',
			"xlw"     => 'Excel',
			"xof"     => 'File',
			"xpm"     => 'Image',
			"xwd"     => 'Image',
			"z"       => 'Compressed',
			"zip"     => 'Compressed'
		);
	
		$p = explode('.', strtolower($file));
		$pc = count($p);
		
		// send type name
		if($pc > 1 AND isset($names[$p[$pc - 1]])){
			return $names[$p[$pc - 1]]; 
		}else{
		    return 'File';
		} 
		
	}
	
	
	function get_icon($file){
		$icons = array(
			"323"     => '<i class="icon-icomoon-file doc_type"></i>',
			"acx"     => '<i class="icon-icomoon-file doc_type"></i>',
			"ai"      => '<i class="icon-icomoon-file doc_type"></i>',
			"aif"     => '<i class="icon-icomoon-headphones doc_type"></i>',
			"aifc"    => '<i class="icon-icomoon-headphones doc_type"></i>',
			"aiff"    => '<i class="icon-icomoon-headphones doc_type"></i>',
			"asf"     => '<i class="icon-icomoon-film doc_type"></i>',
			"asr"     => '<i class="icon-icomoon-film doc_type"></i>',
			"asx"     => '<i class="icon-icomoon-film doc_type"></i>',
			"au"      => '<i class="icon-icomoon-headphones doc_type"></i>',
			"avi"     => '<i class="icon-icomoon-film doc_type"></i>',
			"axs"     => '<i class="icon-icomoon-file doc_type"></i>',
			"bas"     => '<i class="icon-icomoon-file doc_type"></i>',
			"bcpio"   => '<i class="icon-icomoon-file doc_type"></i>',
			"bin"     => '<i class="icon-icomoon-file doc_type"></i>',
			"bmp"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"c"       => '<i class="icon-icomoon-file doc_type"></i>',
			"cat"     => '<i class="icon-icomoon-file doc_type"></i>',
			"cdf"     => '<i class="icon-icomoon-file doc_type"></i>',
			"cer"     => '<i class="icon-icomoon-file doc_type"></i>',
			"class"   => '<i class="icon-icomoon-file doc_type"></i>',
			"clp"     => '<i class="icon-icomoon-file doc_type"></i>',
			"cmx"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"cod"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"cpio"    => '<i class="icon-icomoon-file doc_type"></i>',
			"crd"     => '<i class="icon-icomoon-file doc_type"></i>',
			"crl"     => '<i class="icon-icomoon-file doc_type"></i>',
			"crt"     => '<i class="icon-icomoon-file doc_type"></i>',
			"csh"     => '<i class="icon-icomoon-file doc_type"></i>',
			"css"     => '<i class="icon-icomoon-file doc_type"></i>',
			"dcr"     => '<i class="icon-icomoon-file doc_type"></i>',
			"der"     => '<i class="icon-icomoon-file doc_type"></i>',
			"dir"     => '<i class="icon-icomoon-file doc_type"></i>',
			"dll"     => '<i class="icon-icomoon-file doc_type"></i>',
			"dms"     => '<i class="icon-icomoon-file doc_type"></i>',
			"doc"     => '<i class="icon-icomoon-file-word word_doc_type"></i>',
			"docx"    => '<i class="icon-icomoon-file-word word_doc_type"></i>',
			"dot"     => '<i class="icon-icomoon-file-word word_doc_type"></i>',
			"dotx"    => '<i class="icon-icomoon-file-word word_doc_type"></i>',
			"dvi"     => '<i class="icon-icomoon-file doc_type"></i>',
			"dxr"     => '<i class="icon-icomoon-file doc_type"></i>',
			"eps"     => '<i class="icon-icomoon-file doc_type"></i>',
			"etx"     => '<i class="icon-icomoon-file doc_type"></i>',
			"evy"     => '<i class="icon-icomoon-file doc_type"></i>',
			"exe"     => '<i class="icon-icomoon-file doc_type"></i>',
			"fif"     => '<i class="icon-icomoon-file doc_type"></i>',
			"flr"     => '<i class="icon-icomoon-file doc_type"></i>',
			"gif"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"gtar"    => '<i class="icon-icomoon-file-zip zip_doc_type"></i>',
			"gz"      => '<i class="icon-icomoon-file-zip zip_doc_type"></i>',
			"h"       => '<i class="icon-icomoon-file doc_type"></i>',
			"hdf"     => '<i class="icon-icomoon-file doc_type"></i>',
			"hlp"     => '<i class="icon-icomoon-file doc_type"></i>',
			"hqx"     => '<i class="icon-icomoon-file doc_type"></i>',
			"hta"     => '<i class="icon-icomoon-file doc_type"></i>',
			"htc"     => '<i class="icon-icomoon-file doc_type"></i>',
			"htm"     => '<i class="icon-icomoon-globe doc_type"></i>',
			"html"    => '<i class="icon-icomoon-globe doc_type"></i>',
			"htt"     => '<i class="icon-icomoon-globe doc_type"></i>',
			"ico"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"ief"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"iii"     => '<i class="icon-icomoon-file doc_type"></i>',
			"ins"     => '<i class="icon-icomoon-file doc_type"></i>',
			"isp"     => '<i class="icon-icomoon-file doc_type"></i>',
			"jfif"    => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"jpe"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"jpeg"    => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"jpg"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"js"      => '<i class="icon-icomoon-file doc_type"></i>',
			"latex"   => '<i class="icon-icomoon-file doc_type"></i>',
			"lha"     => '<i class="icon-icomoon-file-zip zip_doc_type"></i>',
			"lsf"     => '<i class="icon-icomoon-film doc_type"></i>',
			"lsx"     => '<i class="icon-icomoon-film doc_type"></i>',
			"lzh"     => '<i class="icon-icomoon-film doc_type"></i>',
			"m13"     => '<i class="icon-icomoon-film doc_type"></i>',
			"m14"     => '<i class="icon-icomoon-film doc_type"></i>',
			"m3u"     => '<i class="icon-icomoon-headphones doc_type"></i>',
			"man"     => '<i class="icon-icomoon-file doc_type"></i>',
			"mdb"     => '<i class="icon-icomoon-file doc_type"></i>',
			"me"      => '<i class="icon-icomoon-file doc_type"></i>',
			"mht"     => '<i class="icon-icomoon-envelop doc_type"></i>',
			"mhtml"   => '<i class="icon-icomoon-envelop doc_type"></i>',
			"mid"     => '<i class="icon-icomoon-headphones doc_type"></i>',
			"mny"     => '<i class="icon-icomoon-file doc_type"></i>',
			"mov"     => '<i class="icon-icomoon-film doc_type"></i>',
			"movie"   => '<i class="icon-icomoon-film doc_type"></i>',
			"mp2"     => '<i class="icon-icomoon-film doc_type"></i>',
			"mp3"     => '<i class="icon-icomoon-headphones doc_type"></i>',
			"mpa"     => '<i class="icon-icomoon-film doc_type"></i>',
			"mpe"     => '<i class="icon-icomoon-film doc_type"></i>',
			"mpeg"    => '<i class="icon-icomoon-film doc_type"></i>',
			"mpg"     => '<i class="icon-icomoon-film doc_type"></i>',
			"mpp"     => '<i class="icon-icomoon-file doc_type"></i>',
			"mpv2"    => '<i class="icon-icomoon-film doc_type"></i>',
			"ms"      => '<i class="icon-icomoon-file doc_type"></i>',
			"msg"     => '<i class="icon-icomoon-envelop doc_type"></i>',
			"mvb"     => '<i class="icon-icomoon-file doc_type"></i>',
			"nws"     => '<i class="icon-icomoon-envelop doc_type"></i>',
			"oda"     => '<i class="icon-icomoon-file doc_type"></i>',
			"p10"     => '<i class="icon-icomoon-file doc_type"></i>',
			"p12"     => '<i class="icon-icomoon-file doc_type"></i>',
			"p7b"     => '<i class="icon-icomoon-file doc_type"></i>',
			"p7c"     => '<i class="icon-icomoon-file doc_type"></i>',
			"p7m"     => '<i class="icon-icomoon-file doc_type"></i>',
			"p7r"     => '<i class="icon-icomoon-file doc_type"></i>',
			"p7s"     => '<i class="icon-icomoon-file doc_type"></i>',
			"pbm"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"pdf"     => '<i class="icon-icomoon-file-pdf pdf_doc_type"></i>',
			"pfx"     => '<i class="icon-icomoon-file doc_type"></i>',
			"pgm"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"pko"     => '<i class="icon-icomoon-file doc_type"></i>',
			"pma"     => '<i class="icon-icomoon-file doc_type"></i>',
			"pmc"     => '<i class="icon-icomoon-file doc_type"></i>',
			"pml"     => '<i class="icon-icomoon-file doc_type"></i>',
			"pmr"     => '<i class="icon-icomoon-file doc_type"></i>',
			"pmw"     => '<i class="icon-icomoon-file doc_type"></i>',
			"png"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"pnm"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"pot"     => '<i class="icon-icomoon-file doc_type"></i>',
			"potx"    => '<i class="icon-icomoon-file doc_type"></i>',
			"ppm"     => '<i class="icon-icomoon-file doc_type"></i>',
			"pps"     => '<i class="icon-icomoon-file doc_type"></i>',
			"ppsx"    => '<i class="icon-icomoon-file doc_type"></i>',
			"ppt"     => '<i class="icon-icomoon-file doc_type"></i>',
			"pptx"    => '<i class="icon-icomoon-file doc_type"></i>',
			"prf"     => '<i class="icon-icomoon-file doc_type"></i>',
			"ps"      => '<i class="icon-icomoon-file doc_type"></i>',
			"pub"     => '<i class="icon-icomoon-file doc_type"></i>',
			"qt"      => '<i class="icon-icomoon-film doc_type"></i>',
			"ra"      => '<i class="icon-icomoon-headphones doc_type"></i>',
			"ram"     => '<i class="icon-icomoon-headphones doc_type"></i>',
			"ras"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"rgb"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"rmi"     => '<i class="icon-icomoon-headphones doc_type"></i>',
			"roff"    => '<i class="icon-icomoon-file doc_type"></i>',
			"rtf"     => '<i class="icon-icomoon-file doc_type"></i>',
			"rtx"     => '<i class="icon-icomoon-file doc_type"></i>',
			"scd"     => '<i class="icon-icomoon-file doc_type"></i>',
			"sct"     => '<i class="icon-icomoon-file doc_type"></i>',
			"setpay"  => '<i class="icon-icomoon-file doc_type"></i>',
			"setreg"  => '<i class="icon-icomoon-file doc_type"></i>',
			"sh"      => '<i class="icon-icomoon-file doc_type"></i>',
			"shar"    => '<i class="icon-icomoon-file doc_type"></i>',
			"sit"     => '<i class="icon-icomoon-file doc_type"></i>',
			"snd"     => '<i class="icon-icomoon-headphones doc_type"></i>',
			"spc"     => '<i class="icon-icomoon-file doc_type"></i>',
			"spl"     => '<i class="icon-icomoon-file doc_type"></i>',
			"src"     => '<i class="icon-icomoon-file doc_type"></i>',
			"sst"     => '<i class="icon-icomoon-file doc_type"></i>',
			"stl"     => '<i class="icon-icomoon-file doc_type"></i>',
			"stm"     => '<i class="icon-icomoon-globe doc_type"></i>',
			"svg"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"sv4cpio" => '<i class="icon-icomoon-file doc_type"></i>',
			"sv4crc"  => '<i class="icon-icomoon-file doc_type"></i>',
			"t"       => '<i class="icon-icomoon-file doc_type"></i>',
			"tar"     => '<i class="icon-icomoon-file-zip zip_doc_type"></i>',
			"tcl"     => '<i class="icon-icomoon-file doc_type"></i>',
			"tex"     => '<i class="icon-icomoon-file doc_type"></i>',
			"texi"    => '<i class="icon-icomoon-file doc_type"></i>',
			"texinfo" => '<i class="icon-icomoon-file doc_type"></i>',
			"tgz"     => '<i class="icon-icomoon-file-zip zip_doc_type"></i>',
			"tif"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"tiff"    => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"tr"      => '<i class="icon-icomoon-file doc_type"></i>',
			"trm"     => '<i class="icon-icomoon-file doc_type"></i>',
			"tsv"     => '<i class="icon-icomoon-file doc_type"></i>',
			"txt"     => '<i class="icon-icomoon-file doc_type"></i>',
			"uls"     => '<i class="icon-icomoon-file doc_type"></i>',
			"ustar"   => '<i class="icon-icomoon-file doc_type"></i>',
			"vcf"     => '<i class="icon-icomoon-file doc_type"></i>',
			"vrml"    => '<i class="icon-icomoon-file doc_type"></i>',
			"wav"     => '<i class="icon-icomoon-headphones doc_type"></i>',
			"wcm"     => '<i class="icon-icomoon-file doc_type"></i>',
			"wdb"     => '<i class="icon-icomoon-file doc_type"></i>',
			"wks"     => '<i class="icon-icomoon-file doc_type"></i>',
			"wmf"     => '<i class="icon-icomoon-file doc_type"></i>',
			"wps"     => '<i class="icon-icomoon-file doc_type"></i>',
			"wri"     => '<i class="icon-icomoon-file doc_type"></i>',
			"wrl"     => '<i class="icon-icomoon-file doc_type"></i>',
			"wrz"     => '<i class="icon-icomoon-file doc_type"></i>',
			"xaf"     => '<i class="icon-icomoon-file doc_type"></i>',
			"xbm"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"xla"     => '<i class="icon-icomoon-file-excel xcell_doc_type"></i>',
			"xlc"     => '<i class="icon-icomoon-file-excel xcell_doc_type"></i>',
			"xlm"     => '<i class="icon-icomoon-file-excel xcell_doc_type"></i>',
			"xls"     => '<i class="icon-icomoon-file-excel xcell_doc_type"></i>',
			"xlsx"    => '<i class="icon-icomoon-file-excel xcell_doc_type"></i>',
			"xlt"     => '<i class="icon-icomoon-file-excel xcell_doc_type"></i>',
			"xlw"     => '<i class="icon-icomoon-file-excel xcell_doc_type"></i>',
			"xof"     => '<i class="icon-icomoon-file doc_type"></i>',
			"xpm"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"xwd"     => '<i class="icon-icomoon-image-2 picture_doc_type"></i>',
			"z"       => '<i class="icon-icomoon-file-zip zip_doc_type"></i>',
			"zip"     => '<i class="icon-icomoon-file-zip zip_doc_type"></i>'
		);
	
		$p = explode('.', strtolower($file));
		$pc = count($p);
		
		// send glyph 
		if($pc > 1 AND isset($icons[$p[$pc - 1]])){
			return $icons[$p[$pc - 1]]; 
		}else{
		    return '<i class="icon-icomoon-file doc_type"></i>';
		} 
		
	}
}