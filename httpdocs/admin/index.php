<?php
require_once("utils.php");

// Check if logged in
if($_SESSION['admin_user_serial'] == ""){
	
	header("Location: /admin/login.php");
	exit;
}

require_once($UTILS_SERVER_PATH."library/classes/template/admin_template.class.php");
require_once($UTILS_SERVER_PATH."library/classes/user/user.class.php");
require_once($UTILS_SERVER_PATH."library/classes/response/response.class.php");

$admin_user = new user($_SESSION['admin_user_serial'], "serial");

if($_REQUEST['a'] == 'sent_recd'){

	$response = new response;

	$result_array = $response->get_sent_recd_graph($_REQUEST);

	echo json_encode($result_array);
	exit;

}

if($_REQUEST['a'] == 'survey_perc'){

	$response = new response;
	$result_array = array();

	$result_array = $response->get_all_survey_graph($_REQUEST);

	echo json_encode($result_array);
	exit;

}

if($_REQUEST['a'] == 'latest'){

	$response = new response;
	$result_array = array();

	$result_array = $response->get_latest_results($_REQUEST);

	echo json_encode($result_array);
	exit;

}

$title = 'Home';
$icon = 'home';
$data = new data;
$today = $data->today_to_ymd('d/m/Y');
//$today = '10/01/2014';
$days30 = $data->date_minus_days($today, 30, 'd/m/Y', 'd/m/Y');
$days14 = $data->date_minus_days($today, 14, 'd/m/Y', 'd/m/Y');
$tpl = new admin_template(get_defined_vars());
$tpl->set( 'latest_results_data', $tpl->set_datatable($UTILS_SERVER_PATH."templates/latest_results_row.tpl") );
echo $tpl->fetch();
?>