<div id="survey-dialog" title="Edit Survey" class="dialog_1">

	<form id="survey-form" method="post" class="form-horizontal" style="width:470px;margin:0 0 0 100px;">
		<fieldset>
			<div class="control-group">
				<label class="control-label" id="survey_name_label">Name</label>
				<div class="controls">
					<div id="survey_name_edit" class="input-prepend input-append" data-role="acknowledge-input">
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter the formal title of the survey."><i class="icon-question-sign"></i></div>
			        	<input name="survey_name_input" id="survey_name_input" type="text" required="required" placeholder="Name" data-type="text" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="survey_name_span" class="uneditable-input">&nbsp;</span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" id="survey_director_label">Director Only</label>
				<div class="controls">
					<div id="survey_director_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
						<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Select whether or not this survey is to be only answered by Directors"><i class="icon-question-sign"></i></div>
						<select name="survey_director_select" id="survey_director_select" required="required">
							<option value=""> - Please Select - </option>
							<option value="Y">Yes</option>
							<option value="N">No</option>
						</select>
						<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="survey_director_span" class="uneditable-input">&nbsp;</span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" id="survey_once_label">Only Answer Once</label>
				<div class="controls">
					<div id="survey_once_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
						<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Select whether this survey can be answered once or multiple times"><i class="icon-question-sign"></i></div>
						<select name="survey_once_select" id="survey_once_select" required="required">
							<option value=""> - Please Select - </option>
							<option value="Y">Yes</option>
							<option value="N">No</option>
						</select>
						<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="survey_once_span" class="uneditable-input">&nbsp;</span>
				</div>
			</div>
			<div class="control-group" id="survey_frequency_row">
				<label class="control-label" id="survey_frequency_label">Answer Frequency Type</label>
				<div class="controls">
					<div id="survey_frequency_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
						<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Select the type of frequency"><i class="icon-question-sign"></i></div>
						<select name="survey_frequency_select" id="survey_frequency_select" required="required">
							<option value=""> - Please Select - </option>
							<option value="1">Years</option>
							<option value="2">Months</option>
							<option value="3">Weeks</option>
							<option value="0">Indefinitely</option>
						</select>
						<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="survey_frequency_span" class="uneditable-input">&nbsp;</span>
				</div>
			</div>
			<div class="control-group" id="survey_amount_row">
				<label class="control-label" id="survey_amount_label">Answer Frequency</label>
				<div class="controls">
					<div id="survey_amount_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
						<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter the number that corresponds to the frequency type"><i class="icon-question-sign"></i></div>
						<input type="text" data-type="number" name="survey_amount_input" id="survey_amount_input" required="required" />
						<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="survey_amount_span" class="uneditable-input">&nbsp;</span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" id="survey_info_label">Summary</label>
				<div class="controls">
					<div id="survey_info_edit" class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
						<div class="add-on" data-toggle="tooltip" data-placement="left" data-title="Help" data-tooltip="Enter a summary of the survey. This will be shown to the users"><i class="icon-question-sign"></i></div>
						<textarea name="survey_info_input" id="survey_info_input" required="required"></textarea>
						<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="survey_info_span" class="span3 uneditable-textarea">&nbsp;</span>
				</div>
			</div>
    	</fieldset>
	</form>
</div>
<script>
var survey_callback = function(){};

$(document).ready(function(){
	
	$("#survey-dialog").dialog({  
		modal: true,
		autoOpen: false,
		resizable: false,
		width: '800px',
	   	closeOnEscape: false,
	   	open: function(event, ui) {
	   		var dialog = $(event.target).parents(".ui-dialog.ui-widget");
	   		dialog.focus();
	   		dialog.find(".ui-dialog-buttonpane").find('button').removeClass('ui-state-focus ui-state-hover ui-state-active').addClass('ui-button-info').addClass('pull-right');
	   		/*
	   		$('.add-on').each(function(index) { 
			    if($(this).attr('data-toggle') == 'tooltip'){
			        $(this).tooltip();        
			    }
			});
			*/
	   	},
	   	buttons: [
	   		{
	   			text: 'Save',
	   			id: 'survey_button_save',
	   			click: function() {
		   			do_survey_save();
				}
			},
			{
				text: 'Cancel',
	   			id: 'survey_button_cancel',
	   			click: function() {
		   			$(this).dialog('close');
				}
			}
			,
			{
				text: 'Edit',
	   			id: 'survey_button_edit',
	   			click: function() {
		   			form_edit('survey');
		   			$('#survey-form').display_toggle('execute');
					$('#survey_button_save').show();
					$('#survey_button_cancel').show();
					$('#survey_button_edit').hide();
					$('#survey_button_cancel').unbind('click');
					$('#survey_button_cancel').bind('click', function(){
						reset_survey();
					});
				}
			}
		]
	});
		
	$('#survey-form').display_toggle({
		'elements':[
			{
				'id': '#survey_frequency_row',
				'conditions': [
					{
						'field': '#survey_once_select',
						'operator': '=',
						'value' : 'N',
						'visible_override' : true
					}
				]
			},
			{
				'id': '#survey_amount_row',
				'conditions': [
					{
						'field': '#survey_frequency_select',
						'operator': '==',
						'value' : '1,2,3',
						'visible_override' : true
					}
				]
			}
		]
	});
});

function reset_survey(){
	$('#survey_button_save').hide();
	$('#survey_button_cancel').show();
	$('#survey_button_edit').show();
	form_read('survey');
	$('#survey_button_cancel').unbind('click');
	$('#survey_button_cancel').bind('click', function(){
		$('#survey-dialog').dialog('close');
	});
	$('#survey-form').display_toggle('execute');
}


function do_survey_save(){

	clear_errors();	

	var processor = $('body').processing('start', 'Saving Survey');
	$.post($(location).attr('href'), 
	$('#survey-form').serialize() + "&a=save&survey_id=<?=$_REQUEST["survey_id"]?>",
	function(data){

		$('body').processing('end', processor);
		
		if(data['save_result'] == 'success'){
			
			if($.isFunction(survey_callback) === true){
				survey_callback();
			}
			$("#survey-dialog").dialog('close');
		}
		else{		
			show_error(data['fields'], data['message'], data['errored']);
		}
	},
	'json');
}
</script>