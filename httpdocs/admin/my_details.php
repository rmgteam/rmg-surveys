<?php
require_once("utils.php");

// Check if logged in
if($_SESSION['admin_user_serial'] == ""){
	header("Location: /admin/login.php");
	exit;
}

require_once($UTILS_SERVER_PATH."library/classes/user/user.class.php");
require_once($UTILS_SERVER_PATH."library/classes/template/admin_template.class.php");
$admin_user = new user($_SESSION['admin_user_serial'], "serial");

if($_REQUEST['a'] == 'get_user'){
	
	$result_array = array();
	$result_array['id'] = $admin_user->survey_user_id;
	$result_array['user_firstname'] = $admin_user->survey_user_firstname;
	$result_array['user_lastname'] = $admin_user->survey_user_lastname;
	$result_array['user_username'] = $admin_user->survey_user_username;
	$result_array['user_email'] = $admin_user->survey_user_email;
	$result_array['administrate_users'] = $admin_user->survey_user_administrate_users;
	$result_array['create_surveys'] = $admin_user->survey_user_create_surveys;
	$result_array['delete_surveys'] = $admin_user->survey_user_delete_surveys;
	$result_array['send_surveys'] = $admin_user->survey_user_send_surveys;
	
	echo json_encode($result_array);
	exit;
}
elseif($_REQUEST['a'] == 'save_user'){	
	
	$save_result = array();
	$save_result['save_result'] = "success";
	$save_result['id'] = "";
	
	$request['username_input'] = $admin_user->survey_user_username;
	$request['password_input'] = $_REQUEST['user_current_password_input'];
	
	$login_result = $admin_user->do_login($request);
	
	$request = $_REQUEST;
	$request['details'] = true;
	
	if($login_result == 'success'){
		$save_result = $admin_user->check_user($request);
		if($save_result['save_result'] == "success"){
			$save_result = $admin_user->save_user($request);
			if($save_result['save_result'] != "success"){
				$save_result['save_msg'] = $save_result['save_result'];
			}
		}
	}else{
		$save_result['save_result'] = 'fail';
		$save_result['save_msg'] = 'Current Password is Incorrect';
	}

	echo json_encode($save_result);
	exit;
}
else{

	$un = stripslashes($admin_user->survey_user_firstname." ".$admin_user->survey_user_lastname);	
	$title = 'User - '.$un;
	$icon = 'file-alt';
	$tpl = new admin_template(get_defined_vars());
	echo $tpl->fetch();
}
?>