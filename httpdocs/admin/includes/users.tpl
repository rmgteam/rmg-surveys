<script>
	var administrate_users = '<?=$session["administrate_users"];?>';
</script>
<script type="text/javascript" src="<?=$UTILS_WEBROOT?>admin/includes/users.js"></script>

<div class="clearfix button_group">
	<p style="width:600px;float:left;">Below is a list of users that have been created. To view details of a user, click the appropriate row in the table below.</p>
	<button id="new_user_button" class="ui-button-small ui-button-info pull-right">Create a new User</button>
</div>


<table class="display" id="results_table" border="0" cellspacing="0" cellpadding="0" width="100%">
	<?=$user_data ?>
</table>