<table id="answer_row" class="template_include">
	<thead>
		<tr>
			<th width="12%">Property Ref.</th>
			<th width="28%">Property Name</th>
			<th width="18%">Avg. Score</th>
			<th width="10%">As %</th>
			<th width="8%">Sent</th>
			<th width="12%">Returned</th>
			<th width="12%">S vs R</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataTables_empty">Loading</td>
			<td class="dataTables_empty">Data </td>
			<td class="dataTables_empty">From</td>
			<td class="dataTables_empty">Server</td>
			<td class="dataTables_empty">&nbsp;</td>
			<td class="dataTables_empty">&nbsp;</td>
			<td class="dataTables_empty">&nbsp;</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th><input type="text" value="Search Ref." class="search_init" /></th>
			<th><input type="text" value="Search Name" class="search_init" /></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</tfoot>
</table>