var asInitVals = new Array();
var myApp = myApp || {}; 

$(document).ready(function(){
	
	build_user_list();
	
	$('#new_user_button')
		.button()
		.click(
			function(){
				do_new_user();
			});
		
    var page_crumbs = {
    	0 : 	{ 'url' 	: "/admin/",		'text'	: "Dashboard" },
    	1 : 	{ 'url'	: "/admin/users.php",	'text'	: "Users" }
    };
    gen_crumbs(page_crumbs); 

});

function build_user_list(){

	var processor = $('body').processing('start', 'Building User List', 33);
	
	var sort_array = new Array();
	
	if(administrate_users == 'Y'){
		$('#new_user_button').show();
		var aoColumns = [
  			{ bVisible: true },
  			{ bVisible: true },
  			{ bVisible: true }
  		];
	}else{
		$('#new_user_button').hide();
		var aoColumns = [
 			{ bVisible: true },
 			{ bVisible: true },
 			{ bVisible: false }
 		];
	}

	sort_array = [[ 0 , 'asc' ]];

	$('#results_table').show();
	
	myApp.table = $('#results_table')
		.dataTable({
			"bJQueryUI": true,
			"bProcessing": true,
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bDestroy": true,
			"bDeferRender": true,
	       	"oLanguage": {
		       	"sInfoFiltered": "",
		       	"sSearch":"Search Columns Below: ",
	            "sLengthMenu": "Show _MENU_"
	        },
	        "aoColumns": aoColumns,
			"sPaginationType": "full_numbers",
	        "bServerSide": true,
	        "sAjaxSource": $(location).attr('href'),
	        "sServerMethod": "POST",
	        "fnServerData": function ( sSource, aoData, fnCallback ) {
	        	$('#a').val('search')
	        	
	        	$('body').processing('update', processor, 66);
	        	
	        	aoData.push({name : 'a', value : 'users'});

	        	$.post(sSource, 
	        	aoData, 
				function(json){
	        		 fnCallback(json);
				}, 
				"json");
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				var id = $(nRow).attr('id');
				var no_tds = 0;
				
				$(nRow).find('td').each(function(){
					if(no_tds != 2){
						$(this).bind('click', {'id':id}, function(e){
							var id = e.data['id'];
							
							$(location).attr('href', '/admin/user/' + id + '/');
						});
					}

					no_tds++;
				});
	        },
	        "fnDrawCallback": function( oSettings ) {

				$('body').processing('end', processor);
	        	$('.button').not('.ui-button').button();
	        },
	        "aaSorting": sort_array
		});


	if(myApp.table === undefined){

		$("#results_table tfoot input").each( function (i) {
	        asInitVals[i] = this.value;
	    } );
	     
	    $("#results_table tfoot input").bind('focus', function () {
	        if ( this.className == "search_init" )
	        {
	            this.className = "";
	            this.value = "";
	        }
	    } );
	     
	    $("#results_table tfoot input").bind('blur', function (i) {
	        if ( this.value == "" ){
	            this.className = "search_init";
	            this.value = asInitVals[$("#results_table tfoot input").index(this)];
	        }
	    });

	    $("#results_table tfoot input").bind('keypress', function(e){
	    	var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13){ 
        		myApp.table.fnFilter( this.value, $("#results_table tfoot input").index(this) );
			}
	    });

	    $('#results_table_wrapper .dataTables_filter input')
		    .unbind('keypress keyup')
		    .bind('keypress', function(e){
		    	var code = (e.keyCode ? e.keyCode : e.which);
			    if (code == 13) {
			    	myApp.table.fnFilter($(this).val());
			    }
		    });
		    
	}
}


function pre_delete_user(s){
	
	$("#msg_dialog").dialog("option", "title", "Confirm...");
	$('#msg_dialog_message').html("Are you sure you want to delete this user?");
	$("#msg_dialog").dialog({
		buttons : [
			{
				text: 'Yes, continue',
				click: function (){
					$(this).dialog('close');	
					delete_user(s);
				}
			},
			{
				text: 'No',
				click: function (){
					$(this).dialog('close');	
				}
			}
		]
	});
	$("#msg_dialog").dialog('open');
}

function delete_user(s){

	var processor = $('body').processing('start', 'Deleting User');

	$.post($(location).attr('href'), 
	$('#question-form').serialize() + "&a=delete_user&s=" + s,
	function(data){
	
		if(data['save_result'] == 'success'){
			build_user_list();
		}

		$('body').processing('end', processor);
		
	},
	'json');
}


function do_new_user(){
	location.href = "/admin/user/";
}