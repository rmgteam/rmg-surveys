(function( $, undefined ) {

	var emptyFunction = function(){};

	$.widget( "ui.radio", {

		options: {
			objID: '',
			link_object: '',
			classes: 'ui-radio',
			callback: emptyFunction,
			value: '',
			multi: false,
			checked: false
		},

		_create: function() {

			var self = this;
			var options = this.options;

			options.objID = this.element;
			options.objID.data('radio', "true");
			
			var object_id = 'radio';
			
			if(options.objID.attr('id') !== undefined){
				object_id = options.objID.attr('id');
			}
			
			if ( typeof self.options.value === "string" ) {
				var tick_item = [{
					'answers' : [
						{
							'name'	:	options.value,
							'value'	:	"1",
							'default': options.checked
						}
					 ],
					 'id': '1'
				}];
			}else{
				var tick_item = options.value;
			}
			
			tick_item = tick_item[0]
			
			if(options.link_object == ''){
				options.link_object = options.objID
			}
			
			if(tick_item['answers'] !== undefined){
				if(tick_item['answers'].length > 0){
					
					var radio_type = 'radio';
					var type = false;

					if(tick_item['multi'] === undefined){
						type = false;
					}else{
						type = tick_item['multi'];
					}

					if(type === true || tick_item['answers'].length == 1){
						radio_type = 'checkbox';
					}

					var ti = $( document.createElement('input') )
						.attr({
							'id' : options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer",
							'name' : options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer",
							'type': 'hidden'
						});
					
					options.link_object.append(ti);

					for(j=0;j<tick_item['answers'].length;j++){
						var extra = tick_item['answers'][j];

						if(extra['default'] === undefined){
							default_val = false;
						}else{
							default_val = extra['default'];
						}

						var g = $( document.createElement('input') )
							.attr({
								'type' : radio_type,
								'id' : options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer_" + j,
								'name' : options.objID.attr('id') + '_node_radios-' + tick_item['id']  + '_input',
								'value' : String(extra['value'])
							})
							.removeAttr('checked');

						var t = $( document.createElement('span') )
							.attr('id', options.objID.attr('id') + '_'  + tick_item['id'] + "_test_" + j)
							.css('font-size', '11px')
							.html(extra['name']);
						$('body').append(t);

						var text_width = $("#" + options.objID.attr('id') + '_'  + tick_item['id'] + "_test_" + j).width();
						$("#" + options.objID.attr('id') + '_'  + tick_item['id'] + "_test_" + j).remove();

						var h = $( document.createElement('label') )
							.attr({
								'for':options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer_" + j,
								'id': options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_text_" + j
							})
							.html(extra['name'])
							.css({
								'width': (text_width + 45) + "px",
								'font-size': '11px'
							});

						if(options.classes != ''){
							h.addClass(options.classes);
						}

						if(default_val === true){
							h.addClass('active checked');
							g.attr('checked', 'checked');

							ti.val(String(extra['value']));
							
							ti.data('default', String(extra['value']));
						}

						options.link_object.append(g);
						options.link_object.append(h);
					}
				}
			}

			options.link_object.find("input[type!='hidden']").each(function(){

				var icon = 'ui-icon-radio-on';

				if($(this).attr('checked')){
					icon = "ui-icon-check";
				}

				$(this)
					.button({
						icons: {
							primary: icon
						}
					})
					.next('label')
					.removeClass('ui-corner-left')
					.addClass('ui-corner-all')
					.css({
						"margin": "0 5px 0 0",
						"height": "20px"
					})
					.bind('click', {'self' : self, 'tick_item' : tick_item}, function (e) {

						var self = e.data['self'];
						var options = self.options;
						var tick_item = e.data['tick_item'];
						var multi = options.multi;
						var the_input_id = options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer";
						var the_id = $(this).attr('id');
						
						if(multi === undefined){
							item_type = false;
						}else{
							item_type = multi;
						}
						
						var can_continue = true;
						
						if(item_type === false){
							
							var the_parent_level = $(this).parent().parent();
							
							if($(this).parent().parent().get(0).tagName == 'LI'){
								the_parent_level = $(this).parent().parent().parent();
							}
							
							the_parent_level.find("input[type='hidden']").each(function(){
								if($(this).attr('id') != the_input_id && $(this).val() != ''){
									can_continue = false;
								}
							});
						}

						if(can_continue === true){
							if(tick_item['multi'] === undefined){
								type = false;
							}else{
								type = tick_item['multi'];
							}

							$(this).parent().find("input[type='radio']").each(function () {
								if($('#' + $(this).attr('id').replace('_answer_', '_text_')).attr('id') != the_id){
									$('#' + $(this).attr('id').replace('_answer_', '_text_')).removeClass('active checked');
									
									$('#' + $(this).attr('id')).removeAttr('checked');

									$('#' + $(this).attr('id').replace('_answer_', '_text_')).find(".ui-icon")
										.removeClass("ui-icon-check")
										.addClass("ui-icon-radio-on");
								}
								$('#' + options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer")
									.val('')
									.trigger('change');
							});

							$(this).parent().find("input[type='checkbox']").each(function () {
								$('#' + options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer")
									.val('')
									.trigger('change');
							});	

							if ($(this).hasClass('checked')) {
								$(this).removeClass('active checked');
								$('#' + $(this).attr('id').replace('_text_', '_answer_')).removeAttr('checked');
							}else{
								$(this).addClass('active checked');
								$('#' + $(this).attr('id').replace('_text_', '_answer_')).attr('checked', 'checked');
							}

							$(this).parent().find("input[type!='hidden']").each(function () {
								var label = $("label[for='"+$(this).attr('id')+"']");
								
								if ($(this).attr('checked') == 'checked') {
									
									$('#' + options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer")
										.val($('#' + options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer").val() + $(this).val() + ',')
										.trigger('change');
									
									label.find(".ui-icon").removeClass("ui-icon-radio-on").addClass("ui-icon-check");
									
								} else {
									
									label.find(".ui-icon").removeClass("ui-icon-check").addClass("ui-icon-radio-on");
									
									if(type === true){
										var find = $(this).val() + ",";
										var re = new RegExp(find, 'g');
										
										$('#' + options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer")
											.val($('#' + options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer").val().replace(re, ''))
											.trigger('change');
									}
								}
							});
							
							var new_value = $('#' + options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer").val();
							
							if(new_value != ''){
								new_value = new_value.slice(0,-1);
							} 
								
							$('#' + options.objID.attr('id') + '_node_radios-' + tick_item['id']  + "_answer")
								.val(new_value)
								.trigger('change');
							
							self._trigger('_click', null, {'id': $('#' + the_input_id).val()});

							return false;
						}else{
							$('#' + $(this).attr('id').replace('_answer_', '_text_')).removeClass('active checked');
							//$("#msg_dialog").dialog("option", "title", "Error!");
							alert("You may only select one item at this tier!");
							//$("#msg_dialog").dialog('open');
						}
					})
					.find('span').each(function(){
						if($(this).hasClass('ui-button-text')){
							if($(this).html() == ''){
								$(this).parent().css({
									'width': "auto"
								});
								$(this).remove();
								
							}
						}
					});
			});

			if(tick_item['answers'] !== undefined){
				if(tick_item['answers'].length > 0){
					options.link_object.buttonset();
				}
			}
		},
		
		set: function(value){
			var self = this;
			var options = this.options;
			
			if($.isArray(options.value)){
				var id = options.value[0]['id'];
			}else{
				var id = '1';
			}
			
			var part_1 = options.objID.attr('id') + "_node_radios-" + id;
			
			$('#' + options.objID.attr('id') + " input[type='hidden']").val('');
			
			$('#' + options.objID.attr('id') + " input[type!='hidden']").each(function(){
				
				$(this)
					.removeAttr('checked')
					.next("label")
					.removeClass('active checked')
					.find(".ui-icon")
					.addClass("ui-icon-radio-on")
					.removeClass("ui-icon-check");
			});

					
			if(value != ''){
				$('#' + options.objID.attr('id') + " input[type='hidden']").val(value);
				
				$('#' + options.objID.attr('id') + " input[type!='hidden']").each(function(){
					
					if($(this).val() == value){
						$(this)
							.attr('checked', 'checked')
							.next("label")
							.addClass('active checked')
							.find(".ui-icon")
							.removeClass("ui-icon-radio-on")
							.addClass("ui-icon-check");
					}
				});
			}

			this._trigger('load');
		},
		
		clear: function(){
			var self = this;
			var options = this.options;
			
			self.set('');
			
			this._trigger('clear');
		}
	});
	
}( jQuery ) );