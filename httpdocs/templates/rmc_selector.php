<?
// Modal dialog box used to search for Management Companies
require_once("utils.php");
require_once($UTILS_SERVER_PATH."library/classes/rmc.class.php");
require_once($UTILS_SERVER_PATH."library/classes/search_builder/search_builder.class.php");

if ($_REQUEST['rmc_search_php'] == "set_multi_rmc"){

	$mysql = new mysql;

	$form_fields = explode(",", $_REQUEST['form_fields']);
	$db_fields = explode(",", $_REQUEST['db_fields']);
	$type_fields = explode(",", $_REQUEST['type_fields']);
	$rmc_fields = explode(",", $_REQUEST['rmcs']);
	$rmc_field = $_REQUEST['rmcs'];
	$select = $_REQUEST['select'];

	$sql = "SELECT *
	FROM cpm_rmcs rm
	INNER JOIN cpm_lookup_rmcs lr ON (rm.rmc_num = lr.rmc_lookup)
	INNER JOIN cpm_ubsidiary s ON (s.subsidiary_id = rm.subsidiary_id)
	WHERE (";

	foreach($rmc_fields as $sf){
		$sql .= "
		rmc_num = '" . str_replace('_portfolio_', '', str_replace($select, '', $sf)) . "' OR";
	}

	$sql = substr($sql, 0, -3) . ')';

	$html_string = '';
	$selected_fields = 0;

	$result = $mysql->query($sql, 'Set Multi Resident');
	$num_rows = $mysql->num_rows($result);
	if($num_rows > 0){
		while($row = $mysql->fetch_array($result)){
			$selected_fields++;
			$html_string .= $row['rmc_name'] . ' (' . $row['rmc_ref'] . "), ";
		}
	}

	if ($html_string != ''){
		$html_string = substr($html_string,0,-2);
	}

	$result_array['results'] = $html_string;
	$result_array['select'] = $_REQUEST['select'];
	$result_array['rmcs'] = str_replace('_portfolio_', '', str_replace($select, '', $_REQUEST['rmcs']));
	$result_array['num_rows'] = $selected_fields;

	echo json_encode($result_array);
	exit;
}

// Do search
if($_REQUEST['rmc_search_php'] == "rmc_search"){

	$search_builder = new search_builder;
	$mysql = new mysql;
	$security = new security;

	$db_fields = explode(",", $_REQUEST['db_fields']);
	$no_fields = $_REQUEST['no_fields'];
	$field_values = explode("|", $_REQUEST['field_values']);
	$select = $_REQUEST['select'] . '_';
	$select_type = $_REQUEST['select'] . '_type';
	
	if(strpos($_REQUEST['rmc'], ',') !== false){
		$selected_array = explode(",", $_REQUEST['rmc']);
	}else{
		$selected_array = array();
	}
	
	if(strpos($field_values[0], ',') !== false){
		$rmc_array = explode(',', $field_values[0]);
	}else{
		$rmc_array = array();
		if($field_values[0] != ''){
			array_push($rmc_array, $field_values[0]);
		}
	}
	
	$check_array = array_merge($selected_array, $rmc_array);
	
	/**
	 * Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	*/
	$search_builder->add_index_column('rm.rmc_num');
	$search_builder->add_tick_column('rm.rmc_num', $check_array);
	$search_builder->add_function_column('subsidiary_code', '', 'strtoupper');
	$search_builder->add_column('rmc_ref');
	$search_builder->add_column('rmc_name');
	$search_builder->add_button_column('rm.rmc_num', 'View', "$('#" . $_REQUEST['select'] . "').rmc_selector", array('view'));
	
	$iColumnCount = count($search_builder->columns);
	
	$values_array = $search_builder->request_to_array($_REQUEST, $select);

	$input =& $_REQUEST;

	$no_fields = $_REQUEST['no_fields'];

	$search_builder->paging(0,100);
	
	// Individual column filtering
	for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
		if ( isset($request['bSearchable_'.$i]) && $_REQUEST['bSearchable_'.$i] == 'true' && $_REQUEST['sSearch_'.$i] != '' ) {
			$search_builder->add_filter($search_builder->columns[$i], "LIKE", '%'.$security->clean_query( $_REQUEST['sSearch_'.$i] ).'%', 'AND');
		}
	}

	// Input method (use $_GET, $_POST or $_REQUEST)
	$input =& $_REQUEST;

	$no_fields = $_REQUEST['no_fields'];

	$nla_found = false;
	
	if ($no_fields > 0 ){
		for ($i = 1; $i <= $no_fields; $i++) {
			if($_REQUEST[$select_type.$i] == 'nla'){
				$nla_found = $_REQUEST[$select . $_REQUEST[$select_type.$i] . '_input' . $i];
			}
		}
	}
	
	$search_for_ref = false;

	if ($no_fields > 0 ){
		foreach($values_array as $key=>$value) {
			
			$search_term = $value;
				
			$type = "match";
			$field = $key;
			
			switch($key){

				case "rmc_ref":
					$field = "lr.rmc_ref";
					
					if(is_numeric($search_term)){
						$search_for_ref = true;
					}
					
					break;
				case "nla":
					$nla_found = $value;
					$type = "";
					break;
				default:
					break;
			}
			
			if($type == "match"){
				$search_builder->add_filter('', "(", "", 'AND');
				$search_builder->add_filter('rm.rmc_name', 'MATCH AGAINST', $search_term, 'OR');
				$search_builder->add_filter($field, '=', $search_term, 'OR');
				$search_builder->add_filter('', ")", "", 'AND');
				
			}
		}
	}
	
	if($nla_found == false || $nla_found == 'N'){
		$search_builder->add_filter('rmc_is_active', "=", '1', 'AND');
	}
	
	if($_REQUEST['multi'] == "true"){
		$search_builder->add_filter('', "(", "", 'AND');
		if(count($rmc_array) > 0){
	
			$sql_query = '';
			foreach($rmc_array as $pa){
	
				$can_continue = false;
	
				if(count($selected_array) > 0){
					for($sa=0;$sa<count($selected_array);$sa++){
						if($selected_array[$sa] == $pa){
							$can_continue = true;
						}
	
						$continue_error .= $selected_array[$sa] . ' ';
					}
				}else{
					$can_continue = true;
				}
				
				if($pa != ''){
					if($can_continue == true){
						$search_builder->add_filter('rm.rmc_num', "=", $pa, 'OR');
					}
				}
			}
		}
		if(count($selected_array) > 0){
			foreach($selected_array as $sa){
				if($sa != ''){
					$search_builder->add_filter('rm.rmc_num', "=", $sa, 'OR');
				}
			}
		}
		$search_builder->add_filter('', ")", "", 'AND');
	}

	/**
	 * Paging
	 */
	$search_builder->paging(0,100);

	/**
	 * Ordering
	 */
	$aOrderingRules = array();
	if ( isset( $input['iSortCol_0'] ) ) {
		$iSortingCols = intval( $input['iSortingCols'] );

		if($search_for_ref == false){
			if ($no_fields > 0 ){
				foreach($values_array as $key=>$value) {
						
					$search_term = $value;
			
					$type = "match";
					$field = $key;
						
					switch($key){
						case "rmc_ref":
							$search_builder->add_relevance_order('rmc_name', $search_term, 'DESC');
							break;
					}
				}
			}
		}else{
			if ( $_REQUEST[ 'bSortable_'.intval($_REQUEST['iSortCol_'.$i]) ] == 'true' ) {
				$search_builder->add_order($search_builder->columns[intval($_REQUEST['iSortCol_'.$i])], ($_REQUEST['sSortDir_'.$i]==='asc' ? 'asc' : 'desc'));
			}
		}
	}

	$sql = "
	FROM cpm_rmcs rm
	INNER JOIN cpm_lookup_rmcs lr ON (rm.rmc_num = lr.rmc_lookup)
	INNER JOIN cpm_subsidiary s ON (s.subsidiary_id = rm.subsidiary_id)";

	$search_builder->add_sql($sql);
	$output = $search_builder->output($_REQUEST);
	echo json_encode($output);

	exit;
}

if($_REQUEST['rmc_search_php'] == "rmc_get"){
	if ($_REQUEST['rmc_ref'] != ''){
		$this_rmc = new rmc($_REQUEST['rmc_ref']);
		
		$result_array['rmc_num'] = $this_rmc->rmc_num;
		$result_array['rmc_name'] = $this_rmc->rmc_name;
		$result_array['rmc_county'] = $this_rmc->rmc_county;
		$result_array['rmc_postcode'] = $this_rmc->rmc_postcode;
		$result_array['rmc_ref'] = $this_rmc->rmc_ref;
		$result_array['brand_name'] = $this_rmc->brand_name;
		$result_array['property_manager_name'] = $this_rmc->property_manager_name;
		$result_array['regional_manager_name'] = $this_rmc->regional_manager_name;
		$result_array['operational_director_name'] = $this_rmc->operational_director_name;
		$result_array['rmc_status'] = $this_rmc->rmc_status;
	}else{
		$result_array['rmc_num'] = '';
		$result_array['rmc_name'] = '';
		$result_array['rmc_county'] = '';
		$result_array['rmc_postcode'] = '';
		$result_array['rmc_ref'] = '';
		$result_array['brand_name'] = '';
		$result_array['property_manager_name'] = '';
		$result_array['regional_manager_name'] = '';
		$result_array['operational_director_name'] = '';
		$result_array['rmc_status'] = '';
	}
	
	$result_array['select'] = $_REQUEST['select'];
	
	echo json_encode($result_array);
	exit;
}

?>

<?require_once($UTILS_SERVER_PATH."templates/rmc_selector_row.tpl"); ?>