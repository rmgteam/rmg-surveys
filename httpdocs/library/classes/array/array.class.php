<?php
Class arrays{
	
	function remove_element_with_value($array, $key, $value){
		foreach($array as $subKey => $subArray){
			if($subArray[$key] == $value){
				array_splice($array, $subKey, 1);
			}
		}
		return $array;
	}
	
	function make_comparer() {
		// Normalize criteria up front so that the comparer finds everything tidy
		$criteria = func_get_args();
		foreach ($criteria as $index => $criterion) {
			$criteria[$index] = is_array($criterion)
			? array_pad($criterion, 3, null)
			: array($criterion, SORT_ASC, null);
		}
	
		return function($first, $second) use ($criteria) {
			foreach ($criteria as $criterion) {
				// How will we compare this round?
				list($column, $sortOrder, $projection) = $criterion;
				$sortOrder = $sortOrder === SORT_DESC ? -1 : 1;
	
				// If a projection was defined project the values now
				if ($projection) {
					$lhs = call_user_func($projection, $first[$column]);
					$rhs = call_user_func($projection, $second[$column]);
				}
				else {
					$lhs = $first[$column];
					$rhs = $second[$column];
				}
	
				// Do the actual comparison; do not return if equal
				if ($lhs < $rhs) {
					return -1 * $sortOrder;
				}
				else if ($lhs > $rhs) {
					return 1 * $sortOrder;
				}
			}
	
			return 0; // tiebreakers exhausted, so $first == $second
		};
	}
	
	function multi_sort($array, $key, $sub='data'){
		# get a list of sort columns and their data to pass to array_multisort
		$params = array();		
		$dir = array();
		$sort = array();
		
		if(count($key[0]) > 2){
			$key = array_chunk($key[0], 2);
		}
		
		$arr = $array[0];
		
		if(is_array($array[0][$sub])){
			$arr = $array[0][$sub];
		}
		
		for($i=0;$i<count($arr);$i++){
			$sort[$i] = array();
			$dir[$i] = '';
		}
		
		for($r=0;$r<count($array);$r++){
			
			$v = $array[$r];
			
			for($i=0;$i<count($key);$i++){
				if(is_array($key[$i])){
					if(is_array($v[$sub])){
						$sort[$key[$i][0]][] = $v[$sub][$key[$i][0]];
					}else{
						$sort[$key[$i][0]][] = $v[$key[$i][0]];
					}
					if($dir[$key[$i][0]] == ''){
						if($key[$i][1] == 0){
							$dir[$key[$i][0]] = 'ASC';
						}else{
							$dir[$key[$i][0]] = 'DESC';	
						}
					}
				}
			}
		}
		
		for($i=0;$i<count($arr);$i++){
			if(count($sort[$i]) > 0){
				$params[] = &$sort[$i];
				if($dir[$i] != ''){
					if($dir[$i] == 'ASC'){
						$params[] = SORT_ASC;
					}else{
						$params[] = SORT_DESC;	
					}
				}
			}
		}
		
		$params[] = &$array;
		
		call_user_func_array("array_multisort", $params);
		
		return $array;
	}
	
	function output_array($array){
		
		$info = '[';
		
		if(is_array($array)){
			foreach($array as $key=>$value){
				if(is_array($value)){
				// the value of the current array is also a array, so call this function again to process that array
					$info .= '[' . $this->output_array($value) . ']';
				}else{
				// This part of the array is just a key/value pair
					$info.= "$key: $value<br>";
				}
			}
		}
		
		$info .= ']';
		
		return $info;
	}
	
	function strposa($haystack, $needle, $offset=0) {
		if(!is_array($needle)){
			$needle = array($needle);
		}
		foreach($needle as $query) {
			$pos = strpos($haystack, $query, $offset);
			if($pos !== false) return ord($query); // stop on first true result
		}
		return false;
	}
}