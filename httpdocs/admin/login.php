<?php
require_once("utils.php");
require_once($UTILS_SERVER_PATH."library/classes/template/login_template.class.php");
require_once($UTILS_SERVER_PATH."library/classes/user/user.class.php");
$user = new user;

if($_REQUEST['a'] == "login"){
	
	$result_array = array();
	
	$login_result = $user->do_login($_REQUEST);
	
	if($login_result == 'success'){
		$result_array['save_result'] = 'success';
	}else{
		$result_array['save_result'] = 'fail';
		$result_array['save_msg'] = $login_result;
	}
	
	echo json_encode($result_array);
	exit;
}
else if($_REQUEST['a'] == "logout"){
	
	$_SESSION['admin_user_serial'] = "";
}else if($_REQUEST['a'] == "reset"){
	
	$result_array = array();
	
	$outcome = $user->get_user_by_username($_REQUEST['username_input']);
	if($outcome == true){
		$user->send_password();
		$result_array['save_result'] = 'success';
	}else{
		$result_array['save_result'] = 'fail';
		$result_array['save_msg'] = 'This username cannot be found';
	}
	
	echo json_encode($result_array);
	exit;
}



$title = 'Login';
$tpl = new login_template(get_defined_vars());
echo $tpl->fetch();

?>
