<?php $user = new user($_REQUEST['user_id']); ?>

<script type="text/javascript">
	var user_id = '<?=$_REQUEST['user_id'];?>';
	
	var administrate_users = '<?=$session["administrate_users"];?>';
</script>



<script type="text/javascript" src="<?=$UTILS_WEBROOT?>admin/includes/user.js"></script>

<div id="user-div">
	<div class="clearfix button_group">
		
		<a id="user_cancel_button" class="button ui-button-info pull-right">Cancel</a>
		<a id="user_cancel_edit_button" class="button ui-button-info pull-right">Cancel</a>
		<a id="user_edit_button" class="button ui-button-info pull-right">Edit</a>
		<a id="user_save_button" class="button ui-button-info pull-right">Save</a>
		<a id="user_send_pass_button" class="button ui-button-info pull-right">Send Password...</a>
		
	</div>
	<div id="bbGrid-container"></div>
</div>


<div id="user-info" class="form-container">
			
	<form id="user-form" method="post" class="form-horizontal">
	
		<fieldset>
		
			<div class="control-group">
				<label class="control-label" id="user_firstname_label">First Name</label>
				<div class="controls">
				
					<div id="user_firstname_edit" class="input-prepend input-append" data-role="acknowledge-input">
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="The users first name"><i class="icon-question-sign"></i></div>
			        	<input class="span4" name="user_firstname_input" id="user_firstname_input" type="text" required="required" placeholder="" data-type="text" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_firstname_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" id="user_lastname_label">Last Name</label>
				<div class="controls">
				
					<div id="user_lastname_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="The users last name"><i class="icon-question-sign"></i></div>
			        	<input class="span4" name="user_lastname_input" id="user_lastname_input" type="text" required="required" placeholder="" data-type="text" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_lastname_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			
			<div class="control-group">
				<label class="control-label" id="user_username_label">Username<br><span style="font-weight:normal;">(at least 8 characters long)</span></label>
				<div class="controls">
				
					<div id="user_username_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="The credential used to log in with"><i class="icon-question-sign"></i></div>
			        	<input class="span4" name="user_username_input" id="user_username_input" type="text" required="required" placeholder="" data-type="text" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_username_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" id="user_email_label">Email</label>
				<div class="controls">
				
					<div id="user_email_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="The users email address"><i class="icon-question-sign"></i></div>
			        	<input class="span4" name="user_email_input" id="user_email_input" type="text" required="required" placeholder="" data-type="email" />
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_email_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" id="user_administrate_label">Allow to adminstrate Users?</label>
				<div class="controls">
				
					<div id="user_administrate_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="This will allow this user to add/edit user accounts to this system."><i class="icon-question-sign"></i></div>
			        		
			        		<select class="span4" name="user_administrate_select" id="user_administrate_select" required="required">
			        			<option value=""> - Please Select - </option>
			        			<option value="Y">Yes</option>
			        			<option value="N">No</option>
			        		</select>
			        		
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_administrate_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" id="user_create_survey_label">Allow to create/edit Surveys?</label>
				<div class="controls">
				
					<div id="user_create_survey_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="This will allow this user to create/edit surveys."><i class="icon-question-sign"></i></div>
			        		
			        		<select class="span4" name="user_create_survey_select" id="user_create_survey_select" required="required">
			        			<option value=""> - Please Select - </option>
			        			<option value="Y">Yes</option>
			        			<option value="N">No</option>
			        		</select>
			        		
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_create_survey_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" id="user_delete_survey_label">Allow to delete Surveys?</label>
				<div class="controls">
				
					<div id="user_delete_survey_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="This will allow this user to delete surveys."><i class="icon-question-sign"></i></div>
			        		
			        		<select class="span4" name="user_delete_survey_select" id="user_delete_survey_select" required="required">
			        			<option value=""> - Please Select - </option>
			        			<option value="Y">Yes</option>
			        			<option value="N">No</option>
			        		</select>
			        		
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_delete_survey_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" id="user_send_survey_label">Allow to send Surveys?</label>
				<div class="controls">
				
					<div id="user_send_survey_edit" class="input-prepend input-append" data-role="acknowledge-input">
					
			    		<div class="add-on" data-toggle="tooltip" data-placement="left" title="This will allow this user to send surveys."><i class="icon-question-sign"></i></div>
			        		
			        		<select class="span4" name="user_send_survey_select" id="user_send_survey_select" required="required">
			        			<option value=""> - Please Select - </option>
			        			<option value="Y">Yes</option>
			        			<option value="N">No</option>
			        		</select>
			        		
			        	<div class="add-on" data-role="acknowledgement"><i></i></div>
					</div>
					<span id="user_send_survey_span" class="span5 uneditable-input">&nbsp;</span>
					
				</div>
			</div>
			
		</fieldset>
		
		<input type="hidden" id="user_id" name="user_id" value="" />
		
	</form>

</div>