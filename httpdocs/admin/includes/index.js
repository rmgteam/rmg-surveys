var asInitVals = new Array();
var myApp = myApp || {};

// Bar chart options
var options = {
	lines: {
		show: true,
		align: "center"
	},
	points: {
		show: true
	},
	grid: {
		hoverable: true,
		clickable: true,
		color: '#2F96B4'
	},
	xaxis: {
		mode: 'time',
		timeformat: "%d.%m.%Y",
		tickSize: [7, "day"]
		
	},
	yaxis: {
		min: 0,
		max: 100,
		tickDecimals: 0,
		tickSize: 10,
		zoomRange: [0,0],
		panRange: [0,100]
	},
	legend: {
		 position: "se"
	}
};
    
// Half width Pie
var pie_options = {
	series: {
		pie: {
            radius: 80,
			innerRadius: 0.35,
			show: true,
			offset: {
				top: 2,
				left: -50
			}
		}
	},
	legend: {
		 position: "se"
	},
	grid: {
		hoverable: true,
		clickable: true
	}
};

$(document).ready(function(){
	
	// Breadcrumbs
	var page_crumbs = {
    	0 : 	{ 'url' : "/admin/",		'text'	: "Dashboard" }
    };
    
    gen_crumbs(page_crumbs);
	
	$('#sent_recd_dialog').dialog({
		modal: true,
		autoOpen: false,
		resizable: false,
		width: '800px',
	   	closeOnEscape: false,
	   	open: function(event, ui) {
	   		var dialog = $(event.target).parents(".ui-dialog.ui-widget");

	   		var i = 0;
	   		dialog.find(".ui-dialog-buttonpane").find("button").each(function(){
	   			i++;
	   			if(i==1){
		   			$(this).addClass("ui-button-primary");
	   			}
	   		});
	   		dialog.find(".ui-dialog-buttonpane").find('button').removeClass('ui-state-focus ui-state-hover ui-state-active');
		},
	   	buttons: {
	   		Done: function() {
	   			sent_vs_recd();
				$(this).dialog('close');
			},
			Cancel: function() {
				$(this).dialog('close');
			}
		}
	});
	
	$('#survey_perc_dialog').dialog({
		modal: true,
		autoOpen: false,
		resizable: false,
		width: '800px',
	   	closeOnEscape: false,
	   	open: function(event, ui) {
	   		var dialog = $(event.target).parents(".ui-dialog.ui-widget");

	   		var i = 0;
	   		dialog.find(".ui-dialog-buttonpane").find("button").each(function(){
	   			i++;
	   			if(i==1){
		   			$(this).addClass("ui-button-primary");
	   			}
	   		});
	   		dialog.find(".ui-dialog-buttonpane").find('button').removeClass('ui-state-focus ui-state-hover ui-state-active');
		},
	   	buttons: {
	   		Done: function() {
	   			survey_perc();
				$(this).dialog('close');
			},
			Cancel: function() {
				$(this).dialog('close');
			}
		}
	});
	
	$('#results_div').hide();
	$('#sent_vs_recd')
		.html("Click Here To See Sent Vs Rec'd")
		.on('click', function(){sent_vs_recd()})
		.css({
			'float' : 'left',
			'width' : '58%'
		})
		.addClass('record_heading');

	$('#survey_perc')
		.html("Click Here To See the Survey Percentage")
		.on('click', function(){survey_perc()})
		.css({
			'float' : 'right',
			'width' : '38%'
		})
		.addClass('record_heading');
	//build_latest_surveys_list();
});

function sent_vs_recd_settings(){
	$('#sent_recd_dialog').dialog('open');	
}

function survey_perc_settings(){
	$('#survey_perc_dialog').dialog('open');
}

function sent_vs_recd(){
	
	var processor = $('body').processing('start', 'Building Sent Vs. Received', 33);
	
	$('#sent_vs_recd')
		.html('')
		.removeClass('record_heading')
		.off('click');
		
	var aoData = [];

	pushFormData('#form_sent_recd', aoData);
	aoData.push({name : 'a', value : 'sent_recd'});
	
	var dataurl = "/admin/index.php";
	
	$.post(dataurl, 
	aoData,
	function(series){
		
		var initial_perc = 50;
	
		$('body').processing('update', processor, initial_perc);

		var height = '300px';
		var width = '100%';
		
		var i = 0;
		
		chart_options = options;
		var data = [];
		
		var increment =  (100 - initial_perc) / series.length;
		
		for(var j=0; j<series.length;j++){
			
			initial_perc += increment;
			$('body').processing('update', processor, initial_perc);
			
			data.push(series[j]);
			
		}
		
		var graph = create_graph('bar', series, i, width, height, 'none', 'sent_recd-', 'time');
		
		var container = graph.find('.form-container');
		
		graph.appendTo($('#sent_vs_recd'));

		$.plot("#sent_recd-graph-" + i , data, chart_options);
		$(window).on('resize', {'i':i, 'data':data, 'chart_options':chart_options}, function(e) {
			var i = e.data['i'];
			var data = e.data['data'];
			var chart_options = e.data['chart_options'];
			
			$.plot("#sent_recd-graph-" + i , data, chart_options);
		});
		
		var settings_button = $( document.createElement('a') )
			.attr({
				'id':	 	'sent_vs_recd_a',
				'title':	"Settings"
			})
			.addClass('ui-button-info')
			.appendTo($('#' + container.attr('id')));
			
		$('#sent_vs_recd_a')
			.button({
				text: false,
				icons: {
		            primary: "ui-icon-search"
		        }
			})
			.bind('click', function(e){
				sent_vs_recd_settings();
			})
			.append('<i class="icon-cog"></i>')
			.find('span').each(function(){
				$(this).remove();
			});
		
		$('body').processing('end', processor);
		
	},
	'json');
}

function survey_perc(callback){
	
	var processor = $('body').processing('start', 'Building Quality of Surveys', 33);

	$('#survey_perc')
		.html('')
		.removeClass('record_heading')
		.off('click');
	
	var aoData = [];
	
	pushFormData('#form_survey_perc', aoData);
	aoData.push({name : 'a', value : 'survey_perc'});
	
	var dataurl = "/admin/index.php";
	
	$.post(dataurl, 
	aoData,
	function(series){
		
		var initial_perc = 50;
		
		$('body').processing('update', processor, initial_perc);
		
		var i = 0;

		var height = '200px';
		var width = '100%';
		chart_options = pie_options;
		
		var data = [];
			
		var no_answers = series['data'].length;
		
		var increment =  (100 - initial_perc) / no_answers;

		// Loop through each answer for this question, adjusting the data for pie format
		for(j=0;j<no_answers;j++){
			
			initial_perc += increment;
			$('body').processing('update', processor, initial_perc);
			
			d_array = {
				'label': series['data'][j][0],
				'data': series['data'][j][1],
				'color': series['data'][j][2]
			}
			
			data.push(d_array);
		}
		
		var graph = create_graph('pie', series, i, width, height, 'none', 'survey_perc-');
		
		var container = graph.find('.form-container');
		
		graph.appendTo($('#survey_perc'));

		$.plot("#survey_perc-graph-" + i , data, chart_options);
		$(window).on('resize', {'i':i, 'data':data, 'chart_options':chart_options}, function(e) {
			var i = e.data['i'];
			var data = e.data['data'];
			var chart_options = e.data['chart_options'];
			
			$.plot("#survey_perc-graph-" + i , data, chart_options);
		});
		
		var settings_button = $( document.createElement('a') )
			.attr({
				'id':	 	'survey_perc_a',
				'title':	"Settings"
			})
			.addClass('ui-button-info')
			.appendTo($('#' + container.attr('id')));
			
		$('#survey_perc_a')
			.button({
				text: false,
				icons: {
		            primary: "ui-icon-search"
		        }
			})
			.bind('click', function(e){
				survey_perc_settings();
			})
			.append('<i class="icon-cog"></i>')
			.find('span').each(function(){
				$(this).remove();
			});

		$('body').processing('end', processor);

		if($.isFunction(callback)){
			callback();
		}
		
	},
	'json');
}

function build_latest_surveys_list() {
	
	var processor = $('body').processing('start', 'Building Latest Results', 0);
	
	var sort_array = new Array();

	sort_array = [[ 0 , 'asc' ]];

	$('#results_table').show();
	
	myApp.table = $('#results_table')
		.dataTable({
			"bJQueryUI": true,
			"bDestroy": true,
	        "bAutoWidth": true,
			"bDeferRender": true,
	        "bFilter": false,
	        "oLanguage": {
		       	"sInfoFiltered": "",
		       	"sSearch":"Search Columns Below: ",
	            "sLengthMenu": "Show _MENU_"
	        },
	        "bServerSide": true,
	        "sAjaxSource": $(location).attr('href'),
	        "sServerMethod": "POST",
	        "fnServerData": function ( sSource, aoData, fnCallback ) {
	        	$('#a').val('search')

        		$('body').processing('update', processor, 33);
	        	
	        	aoData.push({name : 'a', value : 'latest'});

	        	$('#results_table_wrapper').find('.fg-toolbar').each(function(){
	        		$(this).hide();
	        	});

	        	$.post(sSource, 
	        	aoData, 
				function(json){
					
	        		$('body').processing('update', processor, 50);
	        		
					$('#graph_previous_value').val(json['previous_value']);
					
	        		fnCallback(json);
				}, 
				"json");
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					
				$(nRow).on('click', function(e){
					var id = $(this).attr('id');
					
					
				});
	        },
	        "fnDrawCallback": function( oSettings ) {
        		$('body').processing('end', processor);
        		
	        	$('#results_div').show();
	        },
	        "aaSorting": sort_array
		});
}