<?php 
require_once($UTILS_SERVER_PATH."library/classes/data/data.class.php");
require_once($UTILS_SERVER_PATH."library/classes/security/security.class.php");
require_once($UTILS_SERVER_PATH."library/classes/mysql/mysql.class.php");


class core {
	
	/**
	 * Generate Unique Serial
	 * 
	 * @param	string	$table			Table name
	 * @param 	string	$table_field	Field name
	 * @return 	string	$new_serial		Serial
	 */
	function gen_unique_serial($table, $table_field){
	
		$mysql = new mysql();
		$security = new security;
	
		$serial_unique = "N";
		while($serial_unique == "N"){
			
			$new_serial = $security->gen_serial(32);
				
			$result = $mysql->query("SELECT * FROM ".$table." WHERE ".$table_field." = '".$new_serial."'");
			$num_serials = $mysql->num_rows($result);
				
			if($num_serials == 0){
				$serial_unique = "Y";
			}
		}
	
		return $new_serial;
	}
	
}


?>