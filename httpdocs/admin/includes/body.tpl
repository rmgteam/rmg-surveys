<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
	
		<title>RMG Surveys - Admin | <?=$title?></title>
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/reset.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/custom-theme/jquery-ui-1.10.1.custom.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>admin/css/bootstrap-select.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/font-awesome.min.css" />
	    <!--[if IE 7]>
	    <link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/font-awesome-ie7.min.css">
	    <![endif]-->
	    <!--[if lt IE 9]>
	    <link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/custom-theme/jquery.ui.1.10.1.ie.css"/>
	    <![endif]-->
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.qtip.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jquery.datatable.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>css/jqueryui-editable.css" />
		<link rel="stylesheet" type="text/css" href="<?=$UTILS_WEBROOT?>admin/css/main.css" />
		
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-ui-1.10.1.custom.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/bootstrap-acknowledgeinput.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/bootstrap-select.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.processing.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.Datatables.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.qtip.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery-build_tooltip.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.html5placeholder.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.actual.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jquery.display_toggle.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/jqueryui-editable.min.js"></script>
		<script type="text/javascript" src="<?=$UTILS_WEBROOT?>library/jscript/common.js"></script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top navbar-inverse">
			<div class="navbar-inner">
				<div class="container">
					<img class="brand" src="/images/logo-trans.png" width="100" />
					<ul class="nav pull-right">
						<li>
							<a style="cursor:pointer;" id="header-logout-button"><i class="icon-signout"></i> Log Out</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div style="width:100%; margin-top:48px;">&nbsp;</div>
		
		<?=$header;?>
		
		<div id="msg_dialog" title="Error" class="dialog_1">
			<p id="msg_dialog_message"></p>
		</div>
		
		<div id="login-dialog" title="Login">
			<form id="login-form" method="post" class="form-horizontal" style="width:470px;margin:0 0 0 100px;">
				<fieldset>
					<div class="control-group">
						<label class="control-label">Username</label>
						<div class="controls">
							<div class="input-prepend input-append" data-role="acknowledge-input">
					    		<div class="add-on"><i class="icon-envelope"></i></div>
						        <input name="user_input" id="user_input" type="text" required="required" placeholder="Email Address Required" data-type="email" />
						        <div class="add-on" data-role="acknowledgement"><i></i></div>
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Password</label>
						<div class="controls">
							<div class="input-prepend input-append" data-role="acknowledge-input" style="position:relative; margin:5px auto;">
								<div class="add-on"><i class="icon-key"></i></div>
								<input name="pass_input" id="pass_input" type="password" required="required" placeholder="Password" />
								<div class="add-on" data-role="acknowledgement"><i></i></div>
							</div>
						</div>
					</div>
		    	</fieldset>
			</form>
		</div>
		
		<div class="container">
			<div class="row">
				<?=$menu;?>
				<div class="span10">
					<section id="main_section">
					
						<div id="page_title_container" class="navbar">
							<div class="navbar-inner">
								<h5 class="pull-left"><?=$title;?></h5>
							</div>
						</div>
					
						<div id="breadcrumbs_container" class="clearfix"></div>
						
						<?=$content;?>
					</section>
				</div>
			</div>
		</div>
	</body>
</html>